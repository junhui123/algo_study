package DP;


/**
 * 집합에서 부분집합을 선택해 원하는 합을 만드는 방법
 * {6,9,13,14,20,21,22,30,49,55} 
 * 110 => (6,14,20,21,49}, {6,49,55}
 * 
 * 1. 모든 부분 집합을 구해서 합을 확인하는 방법
 * 2. 부분집합의 합이 마지막 원소를 포함하는 집합과 포함하지 않는 부분집합으로 나누어 생각
 *  	  n=0, m=0 possible(arr, n, m)=1
 *  	  n=0, m!=0 possible(arr, n, m)=0
 *		  n>0, possible(s,n,m)=max(possible(s,n-1,m-s[n-1]), possible(s,n-1,m)) 
 */
public class SubSetSUM {
	public static void main(String[] args) {
//		int m=110, n=10;
//		int[] s = {6,9,13,14,20,21,22,30,49,55};
		int m=32, n=7;
		int[] s = {2,4,6,9,12,14,15};
		System.out.println(subset_Sum(s, n, m));
		System.out.println(caclculate_Subset_Sum(s, n, m));
	}
	
	private static int subset_Sum(int[] s, int n, int m) {
		if(n==0) {
			if(m==0) {
				return 1;
			} else {
				return 0;
			}
		}
		
		int n1 = n-1;
		int sn1= s[n-1];
		int m1 = m-sn1;
		
		return Math.max(subset_Sum(s, n1, m1), subset_Sum(s, n1, m));
	}
	
	private static int caclculate_Subset_Sum(int[] s, int n, int m) {
		int[][] c = new int[n][m];
		
		for(int i=0; i<n; i++) 
			c[i][0] = 1;
		
		for(int i=1; i<m; i++) 
			c[0][i] = 0;
		
		for(int i=1; i<n; i++) {
			for(int j=1; j<m; j++) {
				c[i][j] = 0; //현재 나누어 떨어지지 않는 것
				
				if(j>=s[i-1]) { //현재  검사 값이 조사 값보다 크거나 같다면
					if(c[i-1][j-s[i-1]] == 1) 
						c[i][j] = 1;
				}
				
				if(c[i-1][j] == 1) 
					c[i][j] =1;
			}
		}
		
		return c[n-1][m-1];
	}
}


