package DP;

//출근길 알고리즘
public class wayToWork {
	private static int n = 5, m = 5;
	public static void main(String[] args) {
		int[][] arr = { 
						{ 1, 1, 1, 1, 1 },
						{ 1, 1, 0, 0, 1 },
						{ 1, 1, 1, 1, 1 },
						{ 1, 1, 1, 0, 1 },
						{ 0, 0, 1, 1, 1 }
					  };
		System.out.println(CountWayToWork_Iter(n, m, arr));
		System.out.println("");
		
		int[][] arr2 = { 
				{ 1, 1, 1, 1, 1 },
				{ 1, 1, 0, 0, 1 },
				{ 1, 1, 1, 1, 1 },
				{ 1, 1, 1, 0, 1 },
				{ 0, 0, 1, 1, 1 }
			  };
		System.out.println(CountWayToWork_Recursion(1, 1, arr2));
		System.out.println();
		
		//책 방법
		int[][] arr3 = { 
				{ 1, 1, 1, 1, 1 },
				{ 1, 1, 0, 0, 1 },
				{ 1, 1, 1, 1, 1 },
				{ 1, 1, 1, 0, 1 },
				{ 0, 0, 1, 1, 1 }
			  };
		
		copy(arr3, map);
		System.out.println(Book_CountWayToWork_Recursion(m-1, n-1));
		System.out.println();

		System.out.println(Book_CountWayToWork_Other(m, n, arr3));
	}
	
	public static void copy(int[][] arr, int[][] tmp) {
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<arr[0].length; j++) {
				tmp[i][j] = arr[i][j];
			}
		}
	}
	
	public static void print(int n, int m, int[][] tmp) {
		for(int i=0; i<n; i++) {
			for(int j=0; j<m; j++) {
				System.out.print(tmp[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	///////////////////////////////////////////////////////////////////////////////
	
	//O(n^2)
	public static int CountWayToWork_Iter(int n, int m, int[][] arr) {
		for(int i=1; i<=n-1; i++) {
			for(int j=1; j<=m-1; j++) {
				if(arr[i][j] != 0) {
					arr[i][j] = arr[i-1][j] + arr[i][j-1];
				}
			}
		}
		return arr[n-1][m-1];
	}
	
	public static int CountWayToWork_Recursion(int n, int m, int[][] arr) {
		if(n >= arr.length || m>=arr[0].length) {
			return 0;
		}
		
		if(arr[n][m] != 0)
			arr[n][m] = arr[n-1][m] + arr[n][m-1];
		
		if(m==arr[0].length-1) {
			n = n+1;
			m = 1;
		} else {
			m = m+1;
		}
		
		CountWayToWork_Recursion(n, m, arr);
		
		return arr[arr.length-1][arr[0].length-1];
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////
	
	private static int[][] map = new int[100][100];
	public static long Book_CountWayToWork_Recursion(int n, int m) {
		if(map[n][m] == 0) 
			return 0;
		if(n==0 && m==0)
			return 1;
		
		return ( (n>0) ? Book_CountWayToWork_Recursion(n-1, m) : 0 )
			 + ( (m>0) ? Book_CountWayToWork_Recursion(n, m-1) : 0 );
	}
	
	public static int Book_CountWayToWork_Other(int n, int m, int[][] arr) {
		int[][] path = new int[n][m];
		copy(arr, path);
		
		path[0][0] = map[0][0];
		for(int i=1; i<n; i++) {
			if(map[i][0] == 0 )
				path[i][0] = 0;
			else
				path[i][0] = path[i-1][0];
		}
		
		for(int j=1; j<m; j++) {
			if(map[0][j] == 0) 
				path[0][j] = 0;
			else
				path[0][j] = path[0][j-1];
		}
		
		for(int i=1; i<n; i++) {
			for(int j=1; j<n; j++) {
				if(map[i][j] == 0 )
					path[i][j] = 0;
				else
					path[i][j] = path[i-1][j]+path[i][j-1];
			}
		}
		return path[n-1][m-1];
	}
}
