	package DP;

//최단거리이면서 누적경로값이 최대인 경로
public class wayToWork2 {
	private static int m=5, n=5;
	private static int[][] map = {
									{1,2,2,1,5},	
									{1,4,4,3,1},	
									{2,1,1,1,2},	
									{1,3,5,1,1},	
									{1,5,1,2,2}
								 };
	public static void main(String[] args) {
//		System.out.println(max_joy(m-1, n-1));
		System.out.println(max_joy_tmp(m-1, n-1));
		
		System.out.println(caclulate_joy(m, n));
		
		System.out.println(caclulate_joy_trace(m, n));
		
		print_path(m-1, n-1);
	}
	
	public static void print(int n, int m, int[][] tmp) {
		for(int i=0; i<n; i++) {
			for(int j=0; j<m; j++) {
				System.out.print(tmp[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	public static int max_joy(int m, int n) {
		if (m == 0 && n == 0) {
			return map[0][0];
		}

		if (m == 0) {
			return max_joy(m, n - 1) + map[m][n];
		}

		if (n == 0) {
			return max_joy(m - 1, n) + map[m][n];
		}

		return Math.max(max_joy(m - 1, n), max_joy(m, n - 1)) + map[m][n];
	}
	
	private static int[][] tmp = new int[m][n];
	public static int max_joy_tmp(int m, int n) {
		if (m == 0 && n == 0) {
			tmp[0][0] = map[0][0];
			return tmp[0][0];
		}

		if (m == 0) {
			tmp[m][n-1] = max_joy_tmp(m, n - 1) + map[m][n];
			return tmp[m][n-1];
		}

		if (n == 0) {
			tmp[m-1][n] = max_joy_tmp(m-1, n) + map[m][n];
			return tmp[m-1][n];
		}

		tmp[m][n]=Math.max(max_joy_tmp(m - 1, n), max_joy_tmp(m, n - 1)) + map[m][n];
		return tmp[m][n];
	}
	
	private static int[][] joy = new int[m][n];
	public static int caclulate_joy(int m, int n) {
		joy[0][0] = map[0][0];
		for(int i=1; i<m; i++) {
			joy[i][0] = joy[i-1][0]+map[i][0];
		}
		for(int j=1; j<n; j++) {
			joy[0][j] = joy[0][j-1]+map[0][j];
		}
		
		for(int i=1; i<m; i++) {
			for(int j=1; j<n; j++) {
				joy[i][j] = Math.max(joy[i-1][j], joy[i][j-1]) + map[i][j];
			}
		}
		return joy[m-1][n-1];
	}
	
	private enum DIRECTION {
		LEFT("LEFT"), UP("UP");
		
		private final String value;
		private DIRECTION(String value) {
			this.value=value;
		}
		public String getValue() {
			return value;
		}
	}
	private static String[][] from = new String[m][n];
	public static int caclulate_joy_trace(int m, int n) {
		joy[0][0] = map[0][0];
		for(int i=1; i<m; i++) {
			joy[i][0] = joy[i-1][0]+map[i][0];
			from[i][0] = DIRECTION.LEFT.getValue();
		}
		for(int j=1; j<n; j++) {
			joy[0][j] = joy[0][j-1]+map[0][j];
			from[0][j] = DIRECTION.UP.getValue();
		}
		
		for(int i=1; i<m; i++) {
			for(int j=1; j<n; j++) {
				
				if(joy[i-1][j] > joy[i][j-1]) {
					from[i][j] = DIRECTION.LEFT.getValue();
				} else {
					from[i][j] = DIRECTION.UP.getValue();
				}
				joy[i][j] = Math.max(joy[i-1][j], joy[i][j-1]) + map[i][j];
			}
		}
		return joy[m-1][n-1];
	}
	
	public static void print_path(int m, int n) {
		if (m == 0 && n == 0) {
			return;
		}
		
		System.out.print("("+m+","+n+") ");
		if(from[m][n] == DIRECTION.LEFT.getValue()) {
			print_path(m-1, n);	
		} else {
			print_path(m, n-1);	
		}
	}
}
