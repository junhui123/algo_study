package Graph;

import java.util.LinkedList;
import java.util.Queue;

public class BFS {

	public void adjacentArray(int[][] adjMatrix, int v) {
		boolean[] visited = new boolean[adjMatrix.length];
		visited[v] = true;

		Queue<Integer> q = new LinkedList<>();
		q.add(v);

		while (!q.isEmpty()) {
			int w = q.poll();
			System.out.print(w+" ");
			for (int i = 0; i < adjMatrix.length; i++) {
				if (adjMatrix[w][i] == 1 && visited[i] == false) {
					q.offer(i);
					visited[i] = true;
				}
			}
		}
	}

	public void adjacentList(LinkedList[] adjList, int v) {
		boolean[] visited = new boolean[adjList.length];
		Queue<Integer> q = new LinkedList<>();
		q.add(v);
		visited[v] = true;

		while (!q.isEmpty()) {
			int w = q.poll();
			System.out.print(w+" ");
			LinkedList<Integer> list = adjList[w];
			for (int i = 0; i < list.size(); i++) {
				int z = list.get(i);
				if (visited[z] == false) {
					q.offer(z);
					visited[z] = true;
				}
			}
		}
	}

	public static void main(String[] args) {
		BFS bfs = new BFS();
		UndirectedGraph.AdjacentArray graphAdjArray = undirected_Matrix3();
		bfs.adjacentArray(graphAdjArray.getAdjMatrix(), 0);
		
		System.out.println();
		System.out.println();
		
		UndirectedGraph.AdjacentList graphAdjList = undirected_List3();
		bfs.adjacentList(graphAdjList.getAdjList(), 0);
	}
	
	private static UndirectedGraph.AdjacentList undirected_List1() {
		UndirectedGraph.AdjacentList graphAdjList = new UndirectedGraph.AdjacentList();
		graphAdjList.init(10);
		graphAdjList.AddEdge(0, 1);
		graphAdjList.AddEdge(1, 2);
		graphAdjList.AddEdge(1, 3);
		graphAdjList.AddEdge(2, 4);
		graphAdjList.AddEdge(3, 5);
		graphAdjList.AddEdge(3, 6);
		graphAdjList.AddEdge(3, 8);
		graphAdjList.AddEdge(4, 7);
		graphAdjList.AddEdge(4, 9);
		return graphAdjList;
	}
	
	private static UndirectedGraph.AdjacentList undirected_List2() {
		UndirectedGraph.AdjacentList graphAdjList = new UndirectedGraph.AdjacentList();
		graphAdjList.init(10);
		graphAdjList.AddEdge(0, 1);
		graphAdjList.AddEdge(0, 2);
		graphAdjList.AddEdge(1, 3);
		graphAdjList.AddEdge(2, 4);
		graphAdjList.AddEdge(2, 5);
		graphAdjList.AddEdge(4, 6);
		return graphAdjList;
	}
	
	private static UndirectedGraph.AdjacentList undirected_List3() {
		UndirectedGraph.AdjacentList graphAdjList = new UndirectedGraph.AdjacentList();
		graphAdjList.init(10);
		graphAdjList.AddEdge(0, 3);
		graphAdjList.AddEdge(0, 4);
		graphAdjList.AddEdge(3, 7);
		graphAdjList.AddEdge(3, 8);
		graphAdjList.AddEdge(4, 6);
		graphAdjList.AddEdge(4, 9);
		return graphAdjList;
	}	
	
	private static UndirectedGraph.AdjacentArray undirected_Matrix1() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(10, 10);
		graphAjArray.addEdge(0, 1);
		graphAjArray.addEdge(1, 2);
		graphAjArray.addEdge(1, 3);
		graphAjArray.addEdge(2, 4);
		graphAjArray.addEdge(3, 5);
		graphAjArray.addEdge(3, 6);
		graphAjArray.addEdge(3, 8);
		graphAjArray.addEdge(4, 7);
		graphAjArray.addEdge(4, 9);
		return graphAjArray;
	}
	
	private static UndirectedGraph.AdjacentArray undirected_Matrix2() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(10, 10);
		graphAjArray.addEdge(0, 1);
		graphAjArray.addEdge(0, 2);
		graphAjArray.addEdge(1, 3);
		graphAjArray.addEdge(2, 4);
		graphAjArray.addEdge(2, 5);
		graphAjArray.addEdge(4, 6);
		return graphAjArray;
	}	
	
	private static UndirectedGraph.AdjacentArray undirected_Matrix3() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(10, 10);
		graphAjArray.addEdge(0, 3);
		graphAjArray.addEdge(0, 4);
		graphAjArray.addEdge(3, 7);
		graphAjArray.addEdge(3, 8);
		graphAjArray.addEdge(4, 6);
		graphAjArray.addEdge(4, 9);
		return graphAjArray;
	}	
}
