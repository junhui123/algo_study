package Graph;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Stack;

public class DFS {

	private boolean[] visited;

	public DFS(int visitedSize) {
		this.visited = new boolean[visitedSize];
	}
	
	public DFS() {
		this.visited = new boolean[100];
	}
	
	public void adjacentArray(int[][] adjMatrix, int n, int v) {
		visited[v] = true;
		System.out.print(v+" ");
		for (int w = 0; w < n; w++) {
			if (adjMatrix[v][w] == 1 && visited[w] == false) {
				adjacentArray(adjMatrix, n, w);
			}
		}
	}
	
	public void adjacentArray_Stack(int[][] adjMatrix, int v) {
		boolean[] visited = new boolean[adjMatrix.length];
		boolean flag = false;
		Stack<Integer> stack = new Stack<>();
		stack.push(v);
		visited[v] = true;
		System.out.print(v+" ");
		while (!stack.isEmpty()) {
			int u = stack.peek();
			flag = false;
			for(int i=0; i<adjMatrix.length; i++) {
				if(adjMatrix[u][i]==1 && visited[i] == false) {
					stack.push(i);
					visited[i] = true;
					flag = true;
					System.out.print(i+" ");
					break;
				}
			}
			if(!flag) {
				stack.pop();
			}
		}
		System.out.println();
	}
	
	public void adjacentList(LinkedList[] adjList, int v) {
		visited[v] = true;
		System.out.print(v+" ");
		LinkedList<Integer> lList = adjList[v];
		for (int j = 0; j <lList.size(); j++) {
			int w = lList.get(j);
			if (visited[w] == false) {
				adjacentList(adjList, w);
			}
		}
	}
	
	public void adjacentList_Stack(LinkedList[] adjList, int v) {
		boolean[] visited = new boolean[adjList.length];
		boolean flag = false;
		Stack<Integer> stack = new Stack<>();
		stack.push(v);
		visited[v] = true;
		System.out.print(v+" ");
		while (!stack.isEmpty()) {
			int u = stack.peek();
			flag = false;
			LinkedList<Integer> lList = adjList[u];
			for(int i: lList) {
				if(visited[i] == false) {
					stack.push(i);
					visited[i] = true;
					flag = true;
					System.out.print(i+" ");
					break;
				}
			}
			if(!flag) {
				stack.pop();
			}
		}
		System.out.println();
	}

	
	public boolean CycleCheck(int[][] adjMatrix, int n, int v) {
		boolean[] visited = new boolean[n];
		Stack<Integer> cycleCheck = new Stack<>();
		Stack<Integer> stack = new Stack<>();
		stack.push(v);
		while (!stack.isEmpty()) {
			int u = stack.pop();
			cycleCheck.push(u);
			if (visited[u] == false) {
				visited[u] = true;
				for (int i = 0; i < n; i++) {
					if (adjMatrix[u][i] == 1 && visited[i] == false) {
						stack.push(i);
					}
				}
			}
		}

		HashSet<Integer> set = new HashSet<>();
		set.addAll(cycleCheck);
		if (set.size() == cycleCheck.size()) {
			return false;
		} else {
			return true;
		}
	}
	
	public boolean CycleCheck(LinkedList[] adjList, int v) {
		boolean[] visited = new boolean[adjList.length];
		return CycleCheck(adjList, v, v, visited); 
	}
	
	public boolean CycleCheck(LinkedList[] adjList, int v, int w, boolean[] visited) {
		visited[v] = true;
		LinkedList<Integer> lList = adjList[v];
		for (int j = 0; j <lList.size(); j++) {
			int z = lList.get(j);
			if (visited[z] == false) {
				if(CycleCheck(adjList, z, v, visited)) {
					return true;
				}
			} 
			else if(z != w) {
				return true;
			}
		}
		return false;
	}
	
	public static void main(String[] args) {
		UndirectedGraph.AdjacentArray graphAdjArray = undirectedCycle2();
		DFS dfsArray = new DFS();
		System.out.println("adjArray Recursive");
		dfsArray.adjacentArray(graphAdjArray.getAdjMatrix(), graphAdjArray.getnV(), 0);
		System.out.println();
		
		System.out.println("adjArray Stack");
		dfsArray.adjacentArray_Stack(graphAdjArray.getAdjMatrix(), 0);
		System.out.println(dfsArray.CycleCheck(graphAdjArray.getAdjMatrix(), graphAdjArray.getnV(), 0));
		System.out.println();
		
		UndirectedGraph.AdjacentList graphAdjList = undirectedCycle_list2();
		DFS dfsList = new DFS();
		System.out.println("adjList Recursive");
		dfsList.adjacentList(graphAdjList.getAdjList(), 0);
		System.out.println();
		
		System.out.println("adjList Stack");
		dfsList.adjacentList_Stack(graphAdjList.getAdjList(), 0);
		System.out.println(dfsList.CycleCheck(graphAdjList.getAdjList(), 0));
	}

	private static UndirectedGraph.AdjacentList undirectedCycle_list1() {
		UndirectedGraph.AdjacentList graphAdjList = new UndirectedGraph.AdjacentList();
		graphAdjList.init(8);
		graphAdjList.AddEdge(0, 1);
		graphAdjList.AddEdge(0, 2);
		graphAdjList.AddEdge(1, 3);
		graphAdjList.AddEdge(1, 2);
		graphAdjList.AddEdge(3, 2);
		graphAdjList.AddEdge(3, 4);
		return graphAdjList;
	}
	
	private static UndirectedGraph.AdjacentList undirectedCycle_list2() {
		UndirectedGraph.AdjacentList graphAdjList = new UndirectedGraph.AdjacentList();
		graphAdjList.init(8);
		graphAdjList.AddEdge(0, 1);
		graphAdjList.AddEdge(0, 2);
		graphAdjList.AddEdge(1, 3);
		graphAdjList.AddEdge(2, 4);
		graphAdjList.AddEdge(2, 5);
		graphAdjList.AddEdge(4, 5);
		graphAdjList.AddEdge(4, 6);
		return graphAdjList;
	}
	
	private static UndirectedGraph.AdjacentList undirectedCycle_list3() {
		UndirectedGraph.AdjacentList graphAdjList = new UndirectedGraph.AdjacentList();
		graphAdjList.init(8);
		graphAdjList.AddEdge(0, 1);
		graphAdjList.AddEdge(0, 2);
		graphAdjList.AddEdge(1, 3);
		graphAdjList.AddEdge(2, 4);
		graphAdjList.AddEdge(3, 6);
		graphAdjList.AddEdge(4, 6);
		graphAdjList.AddEdge(6, 5);
		return graphAdjList;
	}
	
	private static UndirectedGraph.AdjacentList undirected_list1() {
		UndirectedGraph.AdjacentList graphAdjList = new UndirectedGraph.AdjacentList();
		graphAdjList.init(8);
		graphAdjList.AddEdge(0, 1);
		graphAdjList.AddEdge(0, 2);
		graphAdjList.AddEdge(0, 3);
		graphAdjList.AddEdge(3, 4);
		return graphAdjList;
	}
	
	private static UndirectedGraph.AdjacentList undirected_list2() {
		UndirectedGraph.AdjacentList graphAdjList = new UndirectedGraph.AdjacentList();
		graphAdjList.init(8);
		graphAdjList.AddEdge(0, 1);
		graphAdjList.AddEdge(1, 4);
		graphAdjList.AddEdge(4, 2);
		graphAdjList.AddEdge(4, 3);
		graphAdjList.AddEdge(4, 5);
		graphAdjList.AddEdge(5, 6);
		return graphAdjList;
	}
	
	private static UndirectedGraph.AdjacentArray undirectedCycle1() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(8, 8);
		graphAjArray.addEdge(0, 1);
		graphAjArray.addEdge(0, 2);
		graphAjArray.addEdge(1, 2);
		return graphAjArray;
	}
	
	private static UndirectedGraph.AdjacentArray undirectedCycle2() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(8, 8);
		graphAjArray.addEdge(0, 1);
		graphAjArray.addEdge(0, 2);
		graphAjArray.addEdge(1, 3);
		graphAjArray.addEdge(2, 4);
		graphAjArray.addEdge(2, 5);
		graphAjArray.addEdge(4, 5);
		graphAjArray.addEdge(4, 6);
		return graphAjArray;
	}
	
	private static UndirectedGraph.AdjacentArray undirectedCycle3() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(8, 8);
		graphAjArray.addEdge(1, 0);
		graphAjArray.addEdge(1, 2);
		graphAjArray.addEdge(2, 0);
		graphAjArray.addEdge(0, 3);
		graphAjArray.addEdge(3, 4);
		return graphAjArray;
	}
	
	private static UndirectedGraph.AdjacentArray undirectedCycle4() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(8, 8);
		graphAjArray.addEdge(0, 1);
		graphAjArray.addEdge(1, 2);
		graphAjArray.addEdge(1, 5);
		graphAjArray.addEdge(1, 3);
		graphAjArray.addEdge(5, 4);
		graphAjArray.addEdge(3, 4);
		return graphAjArray;
	}
	
	private static UndirectedGraph.AdjacentArray undirectedCycle5() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(8, 8);
		graphAjArray.addEdge(0, 1);
		graphAjArray.addEdge(1, 2);
		graphAjArray.addEdge(2, 3);
		graphAjArray.addEdge(2, 5);
		graphAjArray.addEdge(3, 4);
		graphAjArray.addEdge(4, 5);
		graphAjArray.addEdge(4, 6);
		return graphAjArray;
	}
	
	private static UndirectedGraph.AdjacentArray undirectedCycle6() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(8, 8);
		graphAjArray.addEdge(0, 1);
		graphAjArray.addEdge(0, 2);
		graphAjArray.addEdge(1, 2);
		graphAjArray.addEdge(1, 3);
		graphAjArray.addEdge(3, 4);
		graphAjArray.addEdge(2, 4);
		graphAjArray.addEdge(4, 5);
		graphAjArray.addEdge(5, 6);
		return graphAjArray;
	}
	
	private static UndirectedGraph.AdjacentArray undirected1() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(8, 8);
		graphAjArray.addEdge(0, 1);
		graphAjArray.addEdge(1, 4);
		graphAjArray.addEdge(4, 2);
		graphAjArray.addEdge(4, 3);
		graphAjArray.addEdge(4, 5);
		graphAjArray.addEdge(5, 6);
		return graphAjArray;
	}
	
	private static UndirectedGraph.AdjacentArray undirected2() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(10, 10);
		graphAjArray.addEdge(0, 1);
		graphAjArray.addEdge(0, 2);
		graphAjArray.addEdge(1, 3);
		graphAjArray.addEdge(1, 4);
		graphAjArray.addEdge(3, 6);
		graphAjArray.addEdge(3, 7);
		graphAjArray.addEdge(4, 8);
		graphAjArray.addEdge(4, 9);
		graphAjArray.addEdge(2, 5);
		return graphAjArray;
	}
	
	private static UndirectedGraph.AdjacentArray undirected3() {
		UndirectedGraph.AdjacentArray graphAjArray = new UndirectedGraph.AdjacentArray();
		graphAjArray.init(10, 10);
		graphAjArray.addEdge(2, 3);
		graphAjArray.addEdge(2, 4);
		return graphAjArray;
	}
	
}
