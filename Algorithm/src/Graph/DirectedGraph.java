package Graph;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicInteger;

import Graph.UndirectedGraph.Edge;
import Graph.UndirectedGraph.WeightedAdjacencyList;

public class DirectedGraph {

	static class AdjacentArray {
		private int[][] adjMatrix;
		private int nV = 0;
		private int nE = 0;

		public void init(int nV, int nE) {
			this.nV = nV;
			this.nE = nE;
			adjMatrix = new int[nV][nE];
			for (int i = 0; i < adjMatrix.length; i++)
				Arrays.fill(adjMatrix[i], 0);
		}
		
		public void addEdge(int i, int j) {
			if (i >= nV || j >= nE) {
				System.out.println("Add Edge Fail");
				return;
			}
			adjMatrix[i][j]=1;
		}
		
		public void printAdjcent(int v) {
			for (int i = 0; i < nE; i++) {
				if (adjMatrix[v][i] == 1)
					System.out.println(v + " Adjcent " + i);
			}
		}

		public void print() {
			for (int i = 0; i < adjMatrix.length; i++) {
				for (int j = 0; j < adjMatrix[0].length; j++) {
					System.out.print(String.format("%5d", adjMatrix[i][j]));
				}
				System.out.println();
			}
		}
	}
	
	public static class WeightedAdjacentArray {
		private int[][] adjMatrix;
		private int nV = 0;
		private int nE = 0;

		public void init(int nV, int nE) {
			this.nV = nV;
			this.nE = nE;
			adjMatrix = new int[nV][nE];
			for (int i = 0; i < adjMatrix.length; i++)
				Arrays.fill(adjMatrix[i], 0);
		}
		
		public void init(int[][] adjMatrix) {
			this.adjMatrix = adjMatrix;
			this.nV = adjMatrix.length;
			this.nE = adjMatrix[0].length;
		}

		public void addEdge(int i, int j, int w) {
			if (i >= nV || j >= nE) {
				System.out.println("Add Edge Fail");
				return;
			}
			adjMatrix[i][j] = w;
		}

		// 가중치가 있으므로 검사 기준 또는 배열 형태를 Edge[]로 변경 필요.
		// public void printAdjcent(int v) {
		// for (int i = 0; i < nE; i++) {
		// if (adjMatrix[v][i] == 1)
		// System.out.println(v + " Adjcent " + i);
		// }
		// }

		public void print() {
			for (int i = 0; i < adjMatrix.length; i++) {
				for (int j = 0; j < adjMatrix[0].length; j++) {
					System.out.print(String.format("%5d", adjMatrix[i][j]));
				}
				System.out.println();
			}
		}

		public int[][] getAdjMatrix() {
			return adjMatrix;
		}

		public int getnV() {
			return nV;
		}		
	}
	
	
	static class AdjacentList {
		private LinkedList[] adjList;
		private int nV = 0;

		public void init(int nV) {
			this.nV = nV;
			adjList = new LinkedList[nV];
			for(int i=0; i<nV; i++) {
				adjList[i] = new LinkedList<Integer>(); 
			}
		}
		
		public void AddEdge(int i, int j) {
			if (i >= nV) {
				System.out.println("Add Edge Fail.("+i+","+j+")");
				return;
			}
			adjList[i].add(j);
		}
		
		public void print() {
			AtomicInteger integer = new AtomicInteger(0);
			Arrays.stream(adjList).forEach((list)->{
				System.out.println(integer.getAndIncrement()+"->"+list.toString());
			});
		}
		
		public void printAdjacent(int v) {
			if(v>=nV) {
				System.out.println("Adjacent Does't Exsist");
				return;
			}
			LinkedList<Integer> list = adjList[v];
			System.out.println(v+"->"+list.toString());
		}
	}
	
	static class WeightedAdjacencyList {
		private LinkedList[] adjList;
		private int nV = 0;

		public void init(int nV) {
			this.nV = nV;
			adjList = new LinkedList[nV];
			for(int i=0; i<nV; i++) {
				adjList[i] = new LinkedList<Edge>(); 
			}
		}
		
		public void init(LinkedList[] adjList) {
			this.adjList = adjList;
			this.nV = adjList.length;
		}
		public void AddEdge(int i, int j, int w) {
			if (i >= nV || j>=nV) {
				System.out.println("Add Edge Fail.("+i+","+j+")");
				return;
			}
			adjList[i].add(new Edge(j, w));
		}
		
		public void print() {
			AtomicInteger integer = new AtomicInteger(0);
			Arrays.stream(adjList).forEach((list)->{
				System.out.println(integer.getAndIncrement()+"->"+list.toString());
			});
		}
		
		public void printAdjacent(int v) {
			if(v>=nV) {
				System.out.println("Adjacent Does't Exsist");
				return;
			}
			LinkedList<Integer> list = adjList[v];
			System.out.println(v+"->"+list.toString());
		}
	}
	
	
	static class Edge {
		int v, w;
		public Edge(int v, int w) {
			this.v = v; this.w=w;
		}
		@Override
		public String toString() {
			return "Edge [v=" + v + ", w=" + w + "]";
		}
	}
	
	public static void main(String[] args) {
		adjArray();
//		System.out.println();
//		adjList();
//		System.out.println();
//		weightedAdjList();
		
	}
	
	public static void adjArray() {
		AdjacentArray graph = new AdjacentArray();
		graph.init(8, 8);
		
		graph.addEdge(0, 1);
		graph.addEdge(0, 2);
		graph.addEdge(1, 3);
		graph.addEdge(1, 4);
		graph.addEdge(4, 5);
		graph.addEdge(4, 6);
		graph.addEdge(6, 3);
		graph.addEdge(6, 7);
		graph.print();
		graph.printAdjcent(0);
		graph.printAdjcent(2);		
	}
	
	public static void adjList() {
		AdjacentList graph = new AdjacentList();
		graph.init(8);
		
		graph.AddEdge(1, 2);
		graph.AddEdge(1, 3);
		graph.AddEdge(2, 4);
		graph.AddEdge(2, 5);
		graph.AddEdge(3, 6);
		graph.AddEdge(5, 6);
		graph.AddEdge(7, 4);
		graph.AddEdge(7, 8);
		graph.print();
		System.out.println();
		
		graph.printAdjacent(0);
		graph.printAdjacent(1);
		graph.printAdjacent(3);
	}
	
	
	public static void weightedAdjList() {
		WeightedAdjacencyList graph = new WeightedAdjacencyList();
		graph.init(4);
		
		graph.AddEdge(5, 5, 1);
		graph.AddEdge(2, 0, 2);
		graph.AddEdge(3, 2, 3);
		graph.AddEdge(4, 1, 3);
		graph.AddEdge(1, 0, 4);
		graph.print();
		System.out.println();
		
		graph.printAdjacent(0);
		graph.printAdjacent(3);	
	}
}
