package Graph.MST;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.function.Supplier;

import Graph.UndirectedGraph;
import Graph.UndirectedGraph.Edge;
import Graph.MST.disjoint_set.DisJoint_Set;

public class Kruskal {

	public LinkedList<EdgeUVW> kruskal(LinkedList[] adjList) {
		DisJoint_Set disjoint_set = new DisJoint_Set(adjList.length);
		LinkedList<EdgeUVW> MST = new LinkedList<>();
		for (int i = 0; i < adjList.length; i++) { //각 노드를 가지는 유일한 집합 생성
			disjoint_set.make_set(i);
		}
		
		LinkedList<EdgeUVW> sortedWeightList = SortEdge_by_Weight(adjList);
		for(EdgeUVW e : sortedWeightList) {
			if(disjoint_set.find_set(e.u) != disjoint_set.find_set(e.v)) {
				MST.add(e);
				disjoint_set.union(e.u, e.v);
			}
		}
		return MST;
	}
	
	public LinkedList<EdgeUVW> SortEdge_by_Weight(LinkedList[] adjList) {
		LinkedList<EdgeUVW> sortedList = new LinkedList<>();
		for(int u=0; u<adjList.length; u++) {
			LinkedList<Edge> list = adjList[u];
			for(int i=0; i<list.size(); i++) {
				Edge e = list.get(i);
				sortedList.add(new EdgeUVW(u, e.v, e.w));
			}
		}
		
		sortedList.sort(new Comparator<EdgeUVW>() {
			@Override
			public int compare(EdgeUVW o1, EdgeUVW o2) {
				return o1.w - o2.w; //내림차순 정렬
			}
		});
		
		return sortedList;
	}

	public LinkedList<EdgeUVW> kruskal(int[][] adjMatrix) {
		DisJoint_Set disjoint_set = new DisJoint_Set(adjMatrix.length);
		LinkedList<EdgeUVW> MST = new LinkedList<>();
		for (int i = 0; i < adjMatrix.length; i++) { //각 노드를 가지는 유일한 집합 생성
			disjoint_set.make_set(i);
		}
		
		LinkedList<EdgeUVW> sortedWeightList = SortEdge_by_Weight(adjMatrix);
		for(EdgeUVW e : sortedWeightList) {
			if(disjoint_set.find_set(e.u) != disjoint_set.find_set(e.v)) {
				disjoint_set.union(e.u, e.v);
				MST.add(e);
			}
		}
		return MST;
	}
	
	public LinkedList<EdgeUVW> SortEdge_by_Weight(int[][] adjMatrix) {
		LinkedList<EdgeUVW> sortedList = new LinkedList<>();
		for(int u=0; u<adjMatrix.length; u++) {
			for(int v=0; v<adjMatrix[u].length; v++) {
				if(adjMatrix[u][v] > 0 ) {
					sortedList.add(new EdgeUVW(u, v, adjMatrix[u][v]));
				} 
			}
		}
		
		sortedList.sort(new Comparator<EdgeUVW>() {
			@Override
			public int compare(EdgeUVW o1, EdgeUVW o2) {
				return o1.w - o2.w; //내림차순 정렬
			}
		});
		
		return sortedList;
	}
	
	
	static class EdgeUVW {
		int u;
		int v;
		int w;
		public EdgeUVW(int u, int v, int w) {
			this.u = u;
			this.v = v;
			this.w = w;
		}
	}
	
	public static void main(String[] args) {
		Test_AdjacentList(Kruskal::weightedAdjList3);
		System.out.println();
		System.out.println();
		Test_AdjacentArray(Kruskal::weightedAdjArray3);
	}
	
	static void Test_AdjacentList(Supplier<UndirectedGraph.WeightedAdjacencyList> s) {
		UndirectedGraph.WeightedAdjacencyList graph = s.get();

		Kruskal mstKruskal = new Kruskal();
		LinkedList<EdgeUVW> edge = mstKruskal.kruskal(graph.getAdjList());
		int total = 0;
		for (EdgeUVW edgeUVW : edge) {
			System.out.println(edgeUVW.u+", "+edgeUVW.v+", "+edgeUVW.w);
			total += edgeUVW.w;
		}
		System.out.println("total = "+ total);
	}
	
	static void Test_AdjacentArray(Supplier<UndirectedGraph.WeightedAdjacentArray> s) {
		UndirectedGraph.WeightedAdjacentArray graph = s.get();
		
		Kruskal mstKruskal = new Kruskal();
		LinkedList<EdgeUVW> edge = mstKruskal.kruskal(graph.getAdjMatrix());
		int total = 0;
		for (EdgeUVW edgeUVW : edge) {
			System.out.println(edgeUVW.u+", "+edgeUVW.v+", "+edgeUVW.w);
			total += edgeUVW.w;
		}
		System.out.println("total = "+ total);
	}
	
	public static UndirectedGraph.WeightedAdjacencyList weightedAdjList1() {
		UndirectedGraph.WeightedAdjacencyList graph = new UndirectedGraph.WeightedAdjacencyList();
		graph.init(9);
		graph.AddEdge(0, 1, 4);
		graph.AddEdge(0, 6, 8);
		graph.AddEdge(1, 6, 11);
		graph.AddEdge(1, 2, 8);
		graph.AddEdge(2, 4, 2);
		graph.AddEdge(4, 6, 2);
		graph.AddEdge(6, 7, 1);
		graph.AddEdge(7, 8, 2);
		graph.AddEdge(4, 7, 6);
		graph.AddEdge(2, 8, 4);
		graph.AddEdge(2, 3, 7);
		graph.AddEdge(3, 8, 14);
		graph.AddEdge(3, 5, 9);
		graph.AddEdge(5, 8, 10);
		return graph;
	}

	//total : 37 경로는 2가지 가능. (1-2, 0-6의 가중치가 8로 중복)
	//0-1, 0-6, 6-7, 7-8, 8-2, 2-4, 2-3, 3-5 / 0-1, 0-2, 2-4, 2-8, 8-7, 8-6, 2-3, 3-5
	public static UndirectedGraph.WeightedAdjacencyList weightedAdjList2() {
		UndirectedGraph.WeightedAdjacencyList graph = new UndirectedGraph.WeightedAdjacencyList();
		graph.init(9);
		graph.AddEdge(0, 1, 4);
		graph.AddEdge(0, 6, 8);
		graph.AddEdge(1, 6, 11);
		graph.AddEdge(1, 2, 8);
		graph.AddEdge(2, 4, 2);
		graph.AddEdge(4, 6, 7);
		graph.AddEdge(6, 7, 1);
		graph.AddEdge(7, 8, 2);
		graph.AddEdge(4, 7, 6);
		graph.AddEdge(2, 8, 4);
		graph.AddEdge(2, 3, 7);
		graph.AddEdge(3, 8, 14);
		graph.AddEdge(3, 5, 9);
		graph.AddEdge(5, 8, 10);
		return graph;
	}

	public static UndirectedGraph.WeightedAdjacencyList weightedAdjList3() {
		UndirectedGraph.WeightedAdjacencyList graph = new UndirectedGraph.WeightedAdjacencyList();
		graph.init(6);
		graph.AddEdge(0, 1, 6);
		graph.AddEdge(0, 2, 3);
		graph.AddEdge(1, 2, 2);
		graph.AddEdge(1, 3, 5);
		graph.AddEdge(2, 3, 3);
		graph.AddEdge(2, 4, 4);
		graph.AddEdge(3, 4, 2);
		graph.AddEdge(3, 5, 3);
		graph.AddEdge(4, 5, 5);
		return graph;
	}
	
	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray1() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(9, 9);
		graph.addEdge(0, 1, 4);
		graph.addEdge(0, 6, 8);
		graph.addEdge(1, 6, 11);
		graph.addEdge(1, 2, 8);
		graph.addEdge(2, 4, 2);
		graph.addEdge(4, 6, 2);
		graph.addEdge(6, 7, 1);
		graph.addEdge(7, 8, 2);
		graph.addEdge(4, 7, 6);
		graph.addEdge(2, 8, 4);
		graph.addEdge(2, 3, 7);
		graph.addEdge(3, 8, 14);
		graph.addEdge(3, 5, 9);
		graph.addEdge(5, 8, 10);
		return graph;
	}

	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray2() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(9, 9);
		graph.addEdge(0, 1, 4);
		graph.addEdge(0, 6, 8);
		graph.addEdge(1, 6, 11);
		graph.addEdge(1, 2, 8);
		graph.addEdge(2, 4, 2);
		graph.addEdge(4, 6, 7);
		graph.addEdge(6, 7, 1);
		graph.addEdge(7, 8, 2);
		graph.addEdge(4, 7, 6);
		graph.addEdge(2, 8, 4);
		graph.addEdge(2, 3, 7);
		graph.addEdge(3, 8, 14);
		graph.addEdge(3, 5, 9);
		graph.addEdge(5, 8, 10);
		return graph;
	}

	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray3() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(6, 6);
		graph.addEdge(0, 1, 6);
		graph.addEdge(0, 2, 3);
		graph.addEdge(1, 2, 2);
		graph.addEdge(1, 3, 5);
		graph.addEdge(2, 3, 3);
		graph.addEdge(2, 4, 4);
		graph.addEdge(3, 4, 2);
		graph.addEdge(3, 5, 3);
		graph.addEdge(4, 5, 5);
		return graph;
	}
}
