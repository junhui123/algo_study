package Graph.MST;

import java.util.Arrays;

import Graph.UndirectedGraph;

public class Prim_array {
	public static int get_min_vertex(int[] dist, boolean[] visited) {
		int x = Integer.MAX_VALUE;
		int y = -1;
		for (int i=0; i<dist.length; i++) {
			if(visited[i]==false && dist[i] < x) {
				y = i;
				x = dist[i];
			}
		}
		return y;
	}
	
	public static void prim_array(int[][] matrix, int s, int n) {
		int[] dist = new int[n];
		int[] parent = new int[n];
		boolean[] visited = new boolean[n];

		int inf = Integer.MAX_VALUE;
		for (int u = 0; u < n; u++) {
			dist[u] = inf;
		}
		dist[0] = 0;
		
		
		for (int i = 0; i < n; i++) {
			int u = get_min_vertex(dist, visited);
			visited[u] = true;
			System.out.print(u + " ");
			for (int v = 0; v < n; v++) {
				if (matrix[u][v] > 0) {
					if (!visited[v] && matrix[u][v] < dist[v]) {
						dist[v] = matrix[u][v];
						parent[v] = u;
					}
				}
			}
		}
		System.out.println();
		System.out.println("total = " + Arrays.stream(dist).sum());
	}

	public static void main(String[] args) {
		UndirectedGraph.WeightedAdjacentArray graph = weightedAdjArray3();
		graph.print();

		prim_array(graph.getAdjMatrix(), 0, graph.getnV());
	}

	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray1() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(9, 9);
		graph.addEdge(0, 1, 4);
		graph.addEdge(0, 6, 8);
		graph.addEdge(1, 6, 11);
		graph.addEdge(1, 2, 8);
		graph.addEdge(2, 4, 2);
		graph.addEdge(4, 6, 2);
		graph.addEdge(6, 7, 1);
		graph.addEdge(7, 8, 2);
		graph.addEdge(4, 7, 6);
		graph.addEdge(2, 8, 4);
		graph.addEdge(2, 3, 7);
		graph.addEdge(3, 8, 14);
		graph.addEdge(3, 5, 9);
		graph.addEdge(5, 8, 10);
		return graph;
	}

	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray2() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(9, 9);
		graph.addEdge(0, 1, 4);
		graph.addEdge(0, 6, 8);
		graph.addEdge(1, 6, 11);
		graph.addEdge(1, 2, 8);
		graph.addEdge(2, 4, 2);
		graph.addEdge(4, 6, 7);
		graph.addEdge(6, 7, 1);
		graph.addEdge(7, 8, 2);
		graph.addEdge(4, 7, 6);
		graph.addEdge(2, 8, 4);
		graph.addEdge(2, 3, 7);
		graph.addEdge(3, 8, 14);
		graph.addEdge(3, 5, 9);
		graph.addEdge(5, 8, 10);
		return graph;
	}

	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray3() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(6, 6);
		graph.addEdge(0, 1, 6);
		graph.addEdge(0, 2, 3);
		graph.addEdge(1, 2, 2);
		graph.addEdge(1, 3, 5);
		graph.addEdge(2, 3, 3);
		graph.addEdge(2, 4, 4);
		graph.addEdge(3, 4, 2);
		graph.addEdge(3, 5, 3);
		graph.addEdge(4, 5, 5);
		return graph;
	}
}
