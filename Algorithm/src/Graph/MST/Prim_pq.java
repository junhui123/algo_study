package Graph.MST;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.PriorityQueue;

import Graph.UndirectedGraph;
import Graph.UndirectedGraph.Edge;

public class Prim_pq {

	static class Triple implements Comparable<Triple> {
		int weight; int v; int u;

		public Triple(int weight, int v, int u) {
			this.weight = weight; this.v = v; this.u = u;
		}

		public int getWeight() {
			return weight;
		}

		public int getV() {
			return v;
		}

		public int getU() {
			return u;
		}

		@Override
		public String toString() {
			return "Triple [weight=" + weight + ", v=" + v + ", u=" + u + "]";
		}

		@Override
		public int compareTo(Triple o) {
			return this.weight-o.weight;
		}
	}
	
	static void prim_pq(LinkedList[] adjList, int s) {
		boolean[] visited = new boolean[adjList.length];
		PriorityQueue<Triple> pq = new PriorityQueue<>();
//		PriorityQueue<Triple> pq = new PriorityQueue<>(new Comparator<Triple>() {
//			@Override
//			public int compare(Triple o1, Triple o2) {
//				return o1.weight - o2.weight;
//			}
//		});
		
		queueNeighbors(adjList, visited, s, pq);
		
		int mstCost = 0;
		while(!pq.isEmpty()) {
			Triple edge = pq.peek();
			int u = edge.getU();
			pq.poll();
			if(!visited[u]) {
				mstCost+=edge.getWeight();
				System.out.println("Added : "+edge);
				queueNeighbors(adjList, visited, u, pq);
			}
		}
		System.out.println("Total="+mstCost);
	}

	private static void queueNeighbors(LinkedList[] adjList, boolean[] visited, int v, PriorityQueue<Triple> pq) {
		visited[v] = true;
		LinkedList<Edge> neighbors = adjList[v]; // list of (v,w) pairs
		for (Edge p : neighbors) {
			if (!visited[p.v])
				pq.offer(new Triple(p.w, v, p.v)); // (weight, v, u) triple
		}
	} 
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	@Deprecated
	static void prim_pq2(LinkedList[] adjList, int n) {
		boolean[] visited = new boolean[adjList.length];
		PriorityQueue<Integer> pq = new PriorityQueue<>();		
		int[] D = new int[adjList.length];
		int[] P = new int[adjList.length];
		
		for(int i=0; i<adjList.length; i++) {
			D[i] = Integer.MAX_VALUE;
			P[i] = -1;
		}
		pq.offer(n);
		D[n] = 0;

		while(!pq.isEmpty()) {
			int u = pq.peek();
			LinkedList<Edge> edges = adjList[u];
			pq.poll();
			for(Edge e : edges) {
				if(e.w < D[e.v]) {
					D[e.v] = e.w;
					P[e.v] = u;
					pq.offer(e.v);
				}
			}
		}
		
		System.out.println(Arrays.toString(D));
		System.out.println(Arrays.toString(P));
		System.out.println("total="+Arrays.stream(D).sum());
	}
	
	

	public static void main(String[] args) {
		UndirectedGraph.WeightedAdjacencyList graph = weightedAdjList2();

		prim_pq(graph.getAdjList(), 0);
		
		//잘못된 메소드
		//prim_pq2(graph.getAdjList(), 0);
	}

	public static UndirectedGraph.WeightedAdjacencyList weightedAdjList1() {
		UndirectedGraph.WeightedAdjacencyList graph = new UndirectedGraph.WeightedAdjacencyList();
		graph.init(9);
		graph.AddEdge(0, 1, 4);
		graph.AddEdge(0, 6, 8);
		graph.AddEdge(1, 6, 11);
		graph.AddEdge(1, 2, 8);
		graph.AddEdge(2, 4, 2);
		graph.AddEdge(4, 6, 2);
		graph.AddEdge(6, 7, 1);
		graph.AddEdge(7, 8, 2);
		graph.AddEdge(4, 7, 6);
		graph.AddEdge(2, 8, 4);
		graph.AddEdge(2, 3, 7);
		graph.AddEdge(3, 8, 14);
		graph.AddEdge(3, 5, 9);
		graph.AddEdge(5, 8, 10);
		return graph;
	}

	//total : 37 경로는 2가지 가능. (1-2, 0-6의 가중치가 8로 중복)
	//0-1, 0-6, 6-7, 7-8, 8-2, 2-4, 2-3, 3-5 / 0-1, 0-2, 2-4, 2-8, 8-7, 8-6, 2-3, 3-5
	public static UndirectedGraph.WeightedAdjacencyList weightedAdjList2() {
		UndirectedGraph.WeightedAdjacencyList graph = new UndirectedGraph.WeightedAdjacencyList();
		graph.init(9);
		graph.AddEdge(0, 1, 4);
		graph.AddEdge(0, 6, 8);
		graph.AddEdge(1, 6, 11);
		graph.AddEdge(1, 2, 8);
		graph.AddEdge(2, 4, 2);
		graph.AddEdge(4, 6, 7);
		graph.AddEdge(6, 7, 1);
		graph.AddEdge(7, 8, 2);
		graph.AddEdge(4, 7, 6);
		graph.AddEdge(2, 8, 4);
		graph.AddEdge(2, 3, 7);
		graph.AddEdge(3, 8, 14);
		graph.AddEdge(3, 5, 9);
		graph.AddEdge(5, 8, 10);
		return graph;
	}

	public static UndirectedGraph.WeightedAdjacencyList weightedAdjList3() {
		UndirectedGraph.WeightedAdjacencyList graph = new UndirectedGraph.WeightedAdjacencyList();
		graph.init(6);
		graph.AddEdge(0, 1, 6);
		graph.AddEdge(0, 2, 3);
		graph.AddEdge(1, 2, 2);
		graph.AddEdge(1, 3, 5);
		graph.AddEdge(2, 3, 3);
		graph.AddEdge(2, 4, 4);
		graph.AddEdge(3, 4, 2);
		graph.AddEdge(3, 5, 3);
		graph.AddEdge(4, 5, 5);
		return graph;
	}
}
