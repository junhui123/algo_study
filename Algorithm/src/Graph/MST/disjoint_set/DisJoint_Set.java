package Graph.MST.disjoint_set;

public class DisJoint_Set {

	private int[] parent;
	private int max_length = 0;
	public DisJoint_Set(int max_length) {
		this.max_length = max_length;
		this.parent = new int[max_length];
	}
	
	/**
	 * 유일한 멤버 x를 가지는 집합 생성 
	 * @param x
	 */
	public void make_set(int x) {
		parent[x] = x;
	}
	
	/**
	 * x가 속한 집합의 root 검색
	 * @param x
	 * @return
	 */
	public int find_set(int x) {
		if(x==parent[x]) {
			return parent[x];
		}
		return parent[x] = find_set(parent[x]);
	}
	
	/**
	 * 높이가 낮은 트리를 큰 트리의 자식으로 합치는 연산
	 * 하나의 집합으로 변경
	 * @param u
	 * @param v
	 */
	public void union(int u, int v) {
		parent[find_set(v)] = find_set(u);
	}
	
	/**
	 * u,v가 같은 집합에 속해 있는지 확인
	 * @param u
	 * @param v
	 * @return
	 */
	public boolean isUnion(int u, int v) {
		if(find_set(u) != find_set(v)) 
			return false;
		
		return true;
	}
	
	private int[] size = new int[max_length];
	/**
	 * u, v가 속한 트리 중 노드 개수가 작은 트리를 큰 트리의 자식으로 합칩
	 * u, v가 속한 집합 중 노드 개수가 많은 집합으로 합침
	 * @param u
	 * @param v
	 */
	public void Weighted_Union(int u, int v) {
		int x = find_set(u);
		int y = find_set(v);
		if (size[x] > size[y]) {
			parent[x] = y;
			size[x] = size[x] + size[y];
		} else {
			parent[y] = x;
			size[y] = size[y] + size[x];
		}
	}
	
	/**
	 * Find-Set(x) 연산을 진행하면서 집합 트리의 높이를 반으로 만드는 연산
	 * @param x
	 * @return
	 */
	public int find_set_Pah_Compression(int x) {
		while (x != parent[x]) {
			parent[x] = parent[parent[x]];
			x = parent[x];
		}
		return parent[x];
	}
}
