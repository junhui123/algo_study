package Graph.Shortest_Problem;

import java.util.Arrays;
import java.util.LinkedList;

import Graph.DirectedGraph;

/**
 * 단일 시작점->다른 정점까지 최단 경로
 * 음의 가중치 허용. 음의 사이클은 허용하지 않음 
 * 음의 사이클로 허용시 무한 반복하므로 최단거리 없음
 */
public class Bellman_Ford {
	
	public static void BellmanFord_ShortestPath(int[][] adjMatrix, int s) {
		int nV = adjMatrix.length;
		int[] D = new int[nV];
		int[] P = new int[nV];
		for(int i=0; i<adjMatrix.length; i++) {
			D[i]=Integer.MAX_VALUE;
			P[i]=-1;
		}
		D[s] = 0;
		
		LinkedList<EdgeUVW> edgeList = getEdgeList(adjMatrix);
		for(int i=0; i<nV-1; i++) {  //최악의 경우에도 n-1번 반복으로 경로를 찾을 수 있음
			for (EdgeUVW e : edgeList) {
				int newDistance = D[e.u]+e.w ; 
				if(newDistance > Integer.MIN_VALUE && D[e.v] > newDistance) {
					D[e.v] = D[e.u]+e.w;
					P[e.v] = e.u;
				}
			}
		}
		
		//사이클 검사 Relaxtion이 가능하다는것은 최단 경로가 아님을 의미
		//음수 사이클이 존재함을 의미
		for (EdgeUVW e : edgeList) {
			if(D[e.v] > D[e.u]+e.w) {
				System.out.println(adjMatrix+" has a negative Cycle");
				break;
			}
		}
		System.out.println(Arrays.toString(D));
		System.out.println(Arrays.toString(P));
		
		//시작 지점에서 N의 최단 거리
		//for(int i=0; i<nV; i++) {
		//	System.out.println("Short Distance Between "+s+","+i+"="+D[i]);
		//}
	}
	
	public static LinkedList<EdgeUVW> getEdgeList(int[][] adjMatrix) {
		LinkedList<EdgeUVW> edgeList = new LinkedList<>();
		for (int u = 0; u < adjMatrix.length; u++) {
			for (int v = 0; v < adjMatrix[u].length; v++) {
				if (adjMatrix[u][v] != 0) { //벨만-포드는 음수,양수 모두 가능
					EdgeUVW e = new EdgeUVW(u, v, adjMatrix[u][v]);
					edgeList.add(e);
				}
			}
		}
		return edgeList;
	}
	
	static class EdgeUVW {
		int u,v,w;
		public EdgeUVW(int u, int v, int w) {
			this.u = u;
			this.v = v;
			this.w = w;
		}
	}
	
	public static void main(String[] args) {
		DirectedGraph.WeightedAdjacentArray dgraph = dweightedAdjArray1();
		BellmanFord_ShortestPath(dgraph.getAdjMatrix(), 0);
		System.out.println();
		
		DirectedGraph.WeightedAdjacentArray dgraph2 = dweightedAdjArray2();
		System.out.println(dgraph2.getAdjMatrix());
		BellmanFord_ShortestPath(dgraph2.getAdjMatrix(), 0);
		System.out.println();
		
	}

	//D=[0, 2, 4, 7, -2], P=[-1, 2, 3, 0, 1]
	public static DirectedGraph.WeightedAdjacentArray dweightedAdjArray1() {
		DirectedGraph.WeightedAdjacentArray graph = new DirectedGraph.WeightedAdjacentArray();
		graph.init(5, 5);
		graph.addEdge(0, 1, 6);
		graph.addEdge(0, 3, 7);
		graph.addEdge(1, 2, 5);
		graph.addEdge(1, 3, 8);
		graph.addEdge(1, 4, -4);
		graph.addEdge(2, 1, -2);
		graph.addEdge(3, 2, -3);
		graph.addEdge(3, 4, 9);
		graph.addEdge(4, 0, 2);
		graph.addEdge(4, 2, 7);
		return graph;
	}
	
	//D=[0, 5, 1, 5, 2147483647], P=[-1, 2, 0, 0, -1] //4로 진행하는 정점이 없으므로 4는 무한대 상태
	public static DirectedGraph.WeightedAdjacentArray dweightedAdjArray2() {
		DirectedGraph.WeightedAdjacentArray graph = new DirectedGraph.WeightedAdjacentArray();
		graph.init(5, 5);
		graph.addEdge(0, 2, 1);
		graph.addEdge(0, 3, 5);
		graph.addEdge(1, 2, -2);
		graph.addEdge(2, 1, 4);
		graph.addEdge(2, 3, 4);
		graph.addEdge(3, 1, 3);
		graph.addEdge(3, 0, -1);
		graph.addEdge(4, 0, 1);
		graph.addEdge(4, 2, -1);
		return graph;
	}	
}
