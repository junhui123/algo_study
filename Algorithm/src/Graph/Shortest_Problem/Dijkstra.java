package Graph.Shortest_Problem;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.atomic.AtomicInteger;

import Graph.DirectedGraph;
import Graph.UndirectedGraph;

/**
 * 단일 시작점 - 다른 정점까지 최단 경로
 * 시작점에서 가까운 정점을 선택 (탐욕적 방법)
 * 음의 가중치를 허용하지 않음. 음의 가중치가 존재 시 탐욕적 선택이 안전하지 않음
 * 방향 그래프, 무방향 그래프 상관없음
 */
public class Dijkstra {
	
	public static void Dijkstra_Array(int[][] adjMatrix, int s) {
		final int nV = adjMatrix.length;
		boolean[] visited = new boolean[nV]; //노드 방문 확인
		int[] D = new int[nV]; //최단 거리
		int[] P = new int[nV]; //이전 방문 노드
		int MAX = (int) Double.POSITIVE_INFINITY;
		for(int i=0; i<nV; i++) {
			D[i] = MAX;
			P[i] = -1;
		}
		D[s] = 0;
		
		for(int count=0; count<nV; count++) {
			int u = minDistance(D, visited);
			for(int v=0; v<nV; v++) {
				//아직 처리되지 않은 노드 && u,v 엣지 존재 && s 부터 u까지 경로가 존재 && 기존 v까지 최단거리 보다 새로운 계산 결과가 더 짧은 경우
				if( visited[v]==false && adjMatrix[u][v] > 0 && D[v] > D[u] + adjMatrix[u][v] ) {
					D[v] = D[u] + adjMatrix[u][v];
					P[v] = u;
				}
			}
			visited[u] = true;
		}

		System.out.println(Arrays.toString(D));
		System.out.println(Arrays.toString(P));
	}
	
	public static int minDistance(int[] D, boolean[] visited) {
		int min = (int) Double.POSITIVE_INFINITY;
		int min_idx = 0;
		for(int i=0; i<D.length; i++) {
			if(visited[i]==false && min > D[i]) {
				min_idx = i;
				min = D[i];
			}
		}
		return min_idx;
	}
	
	
	public static void Dijkstra_PQ(int[][] adjMatrix, int s) {
		PriorityQueue<Integer> nodePQ = new PriorityQueue<>();
		int nV = adjMatrix.length;
		LinkedList<Integer> S = new LinkedList<>(); //선택된 정점 집합
		LinkedList<Integer> V = new LinkedList<>(); //모든 정점
		int[] D = new int[nV]; //시작점->각 정점 최단 경로 거리 
		int[] P = new int[nV]; //각 지점의 이전 출발 지점. 최단 경로 저장
		boolean[] visited = new boolean[nV]; //방문한 정점 표시
		for(int v=0; v<nV; v++) {
			D[v] = Integer.MAX_VALUE; //거리 초기화
			P[v] = -1; //경로 초기화
			V.add(v);
			nodePQ.add(v);
		}
		D[s] = 0; 
		
		while(!nodePQ.isEmpty()) {
			int u = extractMinByDistance(nodePQ, D, visited);
			S.add(u);
			visited[u] = true;
			for(int v=0; v<adjMatrix[u].length; v++) {
				//VrelativeComplementS = V-S (차집합)
				LinkedList<Integer> VrelativeComplementS = (LinkedList<Integer>) V.clone();
				VrelativeComplementS.removeAll(S);
				if(VrelativeComplementS.contains(v) && D[u]+adjMatrix[u][v] < D[v] && adjMatrix[u][v]>0) {
					D[v] = D[u] + adjMatrix[u][v];
					P[v] = u;
				}
			}
		}
		System.out.println(Arrays.toString(D));
		System.out.println(Arrays.toString(P));
	}
	
	private static int extractMinByDistance(PriorityQueue<Integer> nodePQ, int[] D, boolean[] visited) {
		final int[] cD = D.clone();
		int smallestIdx = 0; // D[] 최소인 인덱스 = 노드값
		int small = (int) Double.POSITIVE_INFINITY; // Integer.MAX_VALUE
		for (int i = 0; i < D.length; i++) {
			if (visited[i] == false && small > D[i]) {
				smallestIdx = i;
				small = D[i];
			}
		}
		nodePQ.remove(smallestIdx);
		return smallestIdx;
	}
	
	public static void Dijkstra_PQ2(int[][] matrix, int s) {
		PriorityQueue<Pair> pq = new PriorityQueue<>();
		int[] D = new int[matrix.length];
		int[] P = new int[matrix.length];
		for (int i = 0; i < matrix.length; i++) {
			D[i] = Integer.MAX_VALUE;
			P[i] = -1;
		}
		D[s] = 0;
		pq.add(new Pair(s, D[s]));

		while (!pq.isEmpty()) {
			int u = pq.poll().vertex;
			for (int v = 0; v < matrix[u].length; v++) {
				if ( D[v] > D[u] + matrix[u][v] && matrix[u][v] > 0) {
					D[v] = D[u] + matrix[u][v];
					P[v] = u;
					pq.add(new Pair(v, D[v]));
				}
			}
		}
		System.out.println(Arrays.toString(D));
		System.out.println(Arrays.toString(P));
	}
	
	public static void main(String[] args) {
		UndirectedGraph.WeightedAdjacentArray graph = weightedAdjArray4();
		Dijkstra_Array(graph.getAdjMatrix(), 0);
		Dijkstra_PQ(graph.getAdjMatrix(), 0);
		Dijkstra_PQ2(graph.getAdjMatrix(), 0);
//		System.out.println();

//		DirectedGraph.WeightedAdjacentArray dgraph = dweightedAdjArray2();
//		Dijkstra_Array(dgraph.getAdjMatrix(), 0);
//		Dijkstra_PQ(dgraph.getAdjMatrix(), 0);
//		Dijkstra_PQ2(dgraph.getAdjMatrix(), 0);
	}
	
	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray1() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(9, 9);
		graph.addEdge(0, 1, 4);
		graph.addEdge(0, 6, 8);
		graph.addEdge(1, 6, 11);
		graph.addEdge(1, 2, 8);
		graph.addEdge(2, 4, 2);
		graph.addEdge(4, 6, 2);
		graph.addEdge(6, 7, 1);
		graph.addEdge(7, 8, 2);
		graph.addEdge(4, 7, 6);
		graph.addEdge(2, 8, 4);
		graph.addEdge(2, 3, 7);
		graph.addEdge(3, 8, 14);
		graph.addEdge(3, 5, 9);
		graph.addEdge(5, 8, 10);
		return graph;
	}

	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray2() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(9, 9);
		graph.addEdge(0, 1, 4);
		graph.addEdge(0, 6, 8);
		graph.addEdge(1, 6, 11);
		graph.addEdge(1, 2, 8);
		graph.addEdge(2, 4, 2);
		graph.addEdge(4, 6, 7);
		graph.addEdge(6, 7, 1);
		graph.addEdge(7, 8, 2);
		graph.addEdge(4, 7, 6);
		graph.addEdge(2, 8, 4);
		graph.addEdge(2, 3, 7);
		graph.addEdge(3, 8, 14);
		graph.addEdge(3, 5, 9);
		graph.addEdge(5, 8, 10);
		return graph;
	}

	// D=[0, 5, 3, 6, 7, 9], P=[-1, 2, 0, 2, 2, 3]
	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray3() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(6, 6);
		graph.addEdge(0, 1, 6);
		graph.addEdge(0, 2, 3);
		graph.addEdge(1, 2, 2);
		graph.addEdge(1, 3, 5);
		graph.addEdge(2, 3, 3);
		graph.addEdge(2, 4, 4);
		graph.addEdge(3, 4, 2);
		graph.addEdge(3, 5, 3);
		graph.addEdge(4, 5, 5);
		return graph;
	}
	
	public static UndirectedGraph.WeightedAdjacentArray weightedAdjArray4() {
		UndirectedGraph.WeightedAdjacentArray graph = new UndirectedGraph.WeightedAdjacentArray();
		graph.init(8, 8);
		graph.addEdge(0, 1, 2);
		graph.addEdge(0, 2, 2);
		graph.addEdge(1, 3, 3);
		graph.addEdge(1, 4, 5);
		graph.addEdge(2, 5, 1);
		graph.addEdge(3, 6, 4);
		graph.addEdge(4, 5, 2);
		graph.addEdge(4, 6, 1);
		graph.addEdge(5, 7, 6);
		graph.addEdge(6, 7, 2);
		return graph;
	}
	
	//D=[0, 11, 7, 19, 3, 19, 37], P=[-1, 0, 4, 4, 0, 2, 5]
	public static DirectedGraph.WeightedAdjacentArray dweightedAdjArray1() {
		DirectedGraph.WeightedAdjacentArray graph = new DirectedGraph.WeightedAdjacentArray();
		graph.init(7, 7);
		graph.addEdge(0, 1, 11);
		graph.addEdge(0, 2, 9);
		graph.addEdge(0, 4, 3);
		graph.addEdge(1, 4, 6);
		graph.addEdge(1, 6, 30);
		graph.addEdge(2, 4, 19);
		graph.addEdge(2, 5, 12);
		graph.addEdge(3, 6, 21);
		graph.addEdge(4, 2, 4);
		graph.addEdge(4, 3, 16);
		graph.addEdge(4, 5, 21);
		graph.addEdge(4, 6, 44);
		graph.addEdge(5, 6, 18);
		return graph;
	}
	
	//D=[0, 8, 9, 11, 15, 9, 14], P=[-1, 0, 0, 5, 1, 1, 5]
	public static DirectedGraph.WeightedAdjacentArray dweightedAdjArray2() {
		DirectedGraph.WeightedAdjacentArray graph = new DirectedGraph.WeightedAdjacentArray();
		graph.init(7, 7);
		graph.addEdge(0, 1, 8);
		graph.addEdge(0, 2, 9);
		graph.addEdge(1, 2, 3);
		graph.addEdge(1, 4, 7);
		graph.addEdge(1, 5, 1);
		graph.addEdge(2, 3, 3);
		graph.addEdge(4, 3, 4);
		graph.addEdge(4, 5, 3);
		graph.addEdge(5, 3, 2);
		graph.addEdge(5, 6, 5);
		return graph;
	}
}

class Pair implements Comparable<Pair> {
    int vertex;
    double distance;

    Pair(int v, double d) {
        vertex = v;
        distance = d;
    }

    @Override
    public int compareTo(Pair o) {
        return (int) (distance - o.distance);
    }
}
