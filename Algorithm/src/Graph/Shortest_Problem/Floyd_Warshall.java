package Graph.Shortest_Problem;

import Graph.DirectedGraph;

/**
 * 모든 정점쌍에 대한 경로 판정
 * 동적 계획법 적용 
 * 음수 가중치는 허용. 음수 사이클은 허용하지 않음
 */
public class Floyd_Warshall {
	
	private static int MAX = 99999;
	
	public static void FloydWarshall_ShortestPath1(int[][] adjMatrix) {
		int nV = adjMatrix.length;
		int[][] D = new int[nV][nV];
		int[][] P = new int[nV][nV];
		for(int i=0; i<nV; i++) {
			for(int j=0; j<nV; j++) {
				D[i][j] = adjMatrix[i][j];
				P[i][j] = Integer.MIN_VALUE;
			}
		}
		
		for(int k=0; k<nV; k++) {
			for(int i=0; i<nV; i++) {
				for(int j=0; j<nV; j++) {
					if(D[i][j] > D[i][k]+D[k][j]) {
						D[i][j] = D[i][k]+D[k][j];
						P[i][j] = k;
					}
				}
			}
		}
		Print_Array(D, "Array D");
		Print_Array(P, "Array P");
		
		print_all_path(P);
	}
	
	public static void FloydWarshall_ShortestPath2(int[][] adjMatrix) {
		int nV = adjMatrix.length;
		int[][] D = new int[nV][nV];
		int[][] P = new int[nV][nV];
		for(int i=0; i<nV; i++) {
			for(int j=0; j<nV; j++) {
				if( i!=j && adjMatrix[i][j] == 0) {
					D[i][j] = MAX;
				} else {
					D[i][j] = adjMatrix[i][j];
				}
				P[i][j] = Integer.MIN_VALUE;
			}
		}
		
		for(int i=0; i<nV; i++) {
			for(int j=0; j<nV; j++) {
				if( D[i][j] != MAX && D[i][j] != 0 ) {
					P[i][j] = i;
				} 
			}
		}
		
		for (int k = 0; k < nV; k++) {
			for (int i = 0; i < nV; i++) {
				if (k != i) {
					for (int j = 0; j < nV; j++) {
						if(k!=j && i!=j) {
							int tmp = D[i][j];
							D[i][j] = Math.min(D[i][j], D[i][k]+D[k][j]);
							if(tmp != D[i][j])
								P[i][j] = k;
						}
					}
				}
			}
		}	
		Print_Array(D, "Array D");
		Print_Array(P, "Array P");
		
		print_all_path(P);
	}

	public static void print_all_path(int[][] P) {
		for(int i=0; i<P.length; i++) {
			for(int j=0; j<P[i].length; j++) {
				if(i!=j) {
					System.out.print(i+"->"+j+" 까지 경로 : "+i+" ");
					print_path(i, j, P);
					System.out.println(+j);
				}
			}
		}
	}
	
	public static void print_path(int s, int t, int[][] P) {
		if (P[s][t] == Integer.MIN_VALUE) {
			return;
		}
		print_path(s, P[s][t], P);
		System.out.print(P[s][t] + " ");
		print_path(P[s][t], t, P);
	}
	
	public static void Print_Array(int[][] matrix, String arrayName) {
		System.out.println(arrayName);
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				System.out.print(String.format("%13d", matrix[i][j]));
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		DirectedGraph.WeightedAdjacentArray dgraph = dweightedAdjArray1();
		FloydWarshall_ShortestPath1(dgraph.getAdjMatrix());
		System.out.println();
		//경로 출력시  OOM(Out of Memory)
		//FloydWarshall_ShortestPath2(dgraph.getAdjMatrix());
	}
	
	public static DirectedGraph.WeightedAdjacentArray dweightedAdjArray1() {
		DirectedGraph.WeightedAdjacentArray graph = new DirectedGraph.WeightedAdjacentArray();
		int[][] adjMatrix = new int[5][5];
		for(int i=0; i<5; i++) {
			for(int j=0; j<5; j++) {
				if(i==j) {
					adjMatrix[i][j] = 0;
				} else {
					adjMatrix[i][j] = MAX;
				}
			}
		}
		graph.init(adjMatrix);
		graph.addEdge(0, 1, 3);
		graph.addEdge(0, 2, 8);
		graph.addEdge(0, 4, -4);
		graph.addEdge(1, 3, 1);
		graph.addEdge(1, 4, 7);
		graph.addEdge(2, 1, 4);
		graph.addEdge(3, 0, 2);
		graph.addEdge(3, 2, -5);
		graph.addEdge(4, 3, 6);
		return graph;
	}
	
	public static DirectedGraph.WeightedAdjacentArray dweightedAdjArray2() {
		DirectedGraph.WeightedAdjacentArray graph = new DirectedGraph.WeightedAdjacentArray();
		int[][] adjMatrix = new int[4][4];
		for(int i=0; i<4; i++) {
			for(int j=0; j<4; j++) {
				if(i==j) {
					adjMatrix[i][j] = 0;
				} else {
					adjMatrix[i][j] = 9999;
				}
			}
		}
		graph.init(adjMatrix);
		graph.addEdge(0, 1, 2);
		graph.addEdge(1, 0, 5);
		graph.addEdge(1, 2, 4);
		graph.addEdge(1, 3, 3);
		graph.addEdge(2, 0, -1);
		graph.addEdge(2, 3, 4);
		graph.addEdge(3, 1, 1);
		graph.addEdge(3, 2, 7);
		return graph;
	}
}