package Graph;

import java.util.LinkedList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class UndirectedGraph {
	public static class AdjacentArray {
		private int[][] adjMatrix;
		private int nV = 0;
		private int nE = 0;

		public void init(int nV, int nE) {
			this.nV = nV;
			this.nE = nE;
			adjMatrix = new int[nV][nE];
			for (int i = 0; i < adjMatrix.length; i++)
				Arrays.fill(adjMatrix[i], 0);
		}

		public void addEdge(int i, int j) {
			if (i >= nV || j >= nE) {
				System.out.println("Add Edge Fail");
				return;
			}
			adjMatrix[i][j] = 1;
			adjMatrix[j][i] = 1;
		}

		public void printAdjcent(int v) {
			for (int i = 0; i < nE; i++) {
				if (adjMatrix[v][i] == 1)
					System.out.println(v + " Adjcent " + i);
			}
		}

		public void print() {
			for (int i = 0; i < adjMatrix.length; i++) {
				for (int j = 0; j < adjMatrix[0].length; j++) {
					System.out.print(String.format("%5d", adjMatrix[i][j]));
				}
				System.out.println();
			}
		}

		public int[][] getAdjMatrix() {
			return adjMatrix;
		}

		public int getnV() {
			return nV;
		}
	}
	
	public static class WeightedAdjacentArray {
		private int[][] adjMatrix;
		private int nV = 0;
		private int nE = 0;

		public void init(int nV, int nE) {
			this.nV = nV;
			this.nE = nE;
			adjMatrix = new int[nV][nE];
			for (int i = 0; i < adjMatrix.length; i++)
				Arrays.fill(adjMatrix[i], 0);
		}

		public void addEdge(int i, int j, int w) {
			if (i >= nV || j >= nE) {
				System.out.println("Add Edge Fail");
				return;
			}
			adjMatrix[i][j] = w;
			adjMatrix[j][i] = w;
		}

		// 가중치가 있으므로 검사 기준 또는 배열 형태를 Edge[]로 변경 필요.
		// public void printAdjcent(int v) {
		// for (int i = 0; i < nE; i++) {
		// if (adjMatrix[v][i] == 1)
		// System.out.println(v + " Adjcent " + i);
		// }
		// }

		public void print() {
			for (int i = 0; i < adjMatrix.length; i++) {
				for (int j = 0; j < adjMatrix[0].length; j++) {
					System.out.print(String.format("%5d", adjMatrix[i][j]));
				}
				System.out.println();
			}
		}

		public int[][] getAdjMatrix() {
			return adjMatrix;
		}

		public int getnV() {
			return nV;
		}		
	}
	
	public static class AdjacentList {
		private LinkedList[] adjList;
		private int nV = 0;
		
		public LinkedList[] getAdjList() {
			return adjList;
		}

		public int getnV() {
			return nV;
		}

		public void init(int nV) {
			this.nV = nV;
			adjList = new LinkedList[nV];
			for(int i=0; i<nV; i++) {
				adjList[i] = new LinkedList<Integer>(); 
			}
		}
		
		public void AddEdge(int i, int j) {
			if (i >= nV || j >= nV  ) {
				System.out.println("Add Edge Fail.("+i+","+j+")");
				return;
			}
			adjList[i].add(j);
			adjList[j].add(i);
		}
		
		public void print() {
			AtomicInteger integer = new AtomicInteger(0);
			Arrays.stream(adjList).forEach((list)->{
				System.out.println(integer.getAndIncrement()+"->"+list.toString());
			});
		}
		
		public void printAdjacent(int v) {
			if(v>=nV) {
				System.out.println("Adjacent Does't Exsist");
				return;
			}
			LinkedList<Integer> list = adjList[v];
			System.out.println(v+"->"+list.toString());
		}
	}
	
	public static class WeightedAdjacencyList {
		private LinkedList[] adjList;
		private int nV = 0;
		
		public LinkedList[] getAdjList() {
			return adjList;
		}

		public int getnV() {
			return nV;
		}

		public void init(int nV) {
			this.nV = nV;
			adjList = new LinkedList[nV];
			for(int i=0; i<nV; i++) {
				adjList[i] = new LinkedList<Edge>(); 
			}
		}
		
		public void AddEdge(int i, int j, int w) {
			if (i >= nV || j>=nV) {
				System.out.println("Add Edge Fail.("+i+","+j+")");
				return;
			}
			adjList[i].add(new Edge(j, w));
			adjList[j].add(new Edge(i, w));
		}
		
		public void print() {
			AtomicInteger integer = new AtomicInteger(0);
			Arrays.stream(adjList).forEach((list)->{
				System.out.println(integer.getAndIncrement()+"->"+list.toString());
			});
		}
		
		public void printAdjacent(int v) {
			if(v>=nV) {
				System.out.println("Adjacent Does't Exsist");
				return;
			}
			LinkedList<Integer> list = adjList[v];
			System.out.println(v+"->"+list.toString());
		}
	}
	
	public static class Edge {
		public int v, w;
		public Edge(int v, int w) {
			this.v = v; this.w=w;
		}
		@Override
		public String toString() {
			return "Edge [v=" + v + ", w=" + w + "]";
		}
	}

	public static void main(String[] args) {
		//adjArray();
		System.out.println();
//		adjList();
		System.out.println();
		weightedAdjList();
		weightedAdjArray();
	}
	
	public static void adjArray() {
		AdjacentArray graph = new AdjacentArray();
		graph.init(4, 4);
		graph.addEdge(5, 5);
		graph.addEdge(1, 0);
		graph.print();
		graph.printAdjcent(0);
		graph.printAdjcent(2);
	}
	
	public static void weightedAdjArray() {
		WeightedAdjacentArray graph = new WeightedAdjacentArray();
		graph.init(5, 5);
		graph.addEdge(0, 1, 1);
		graph.addEdge(0, 3, 5);
		graph.addEdge(1, 2, 3);
		graph.addEdge(2, 4, 4);
		graph.addEdge(3, 4, 6);
		graph.print();
	}	
	
	public static void adjList() {
		AdjacentList graph = new AdjacentList();
		graph.init(8);
		
		graph.AddEdge(1, 1);
		graph.AddEdge(1, 3);
		graph.AddEdge(2, 0);
		graph.AddEdge(3, 2);
		graph.AddEdge(4, 1);
		graph.AddEdge(1, 0);
		graph.print();
		System.out.println();
		
		graph.printAdjacent(0);
		graph.printAdjacent(3);
	}
	
	public static void weightedAdjList() {
		WeightedAdjacencyList graph = new WeightedAdjacencyList();
		graph.init(4);
		
		graph.AddEdge(5, 5, 1);
		graph.AddEdge(2, 0, 2);
		graph.AddEdge(3, 2, 3);
		graph.AddEdge(4, 1, 3);
		graph.AddEdge(1, 0, 4);
		graph.print();
		System.out.println();
		
		graph.printAdjacent(0);
		graph.printAdjacent(3);	
	}
}
