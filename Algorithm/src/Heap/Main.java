package Heap;


public class Main {
	public static void main(String[] args) {
		Test_Min_Heap();
		System.out.println();
		Test_Max_Heap();
	}
	
	private static void Test_Max_Heap() {
		MaxHeap heap = new MaxHeap(10);
		heap.insert(1);
		heap.insert(5);
		heap.insert(2);
		heap.insert(4);
		heap.insert(3);
		System.out.println("Max Heap");
		heap.print();
		
		System.out.println("Insert 8");
		heap.insert(8);
		heap.print();
		
		System.out.println("\nExtract Max");
		for(int i=0; i<10; i++) {
			System.out.print("Root = "+heap.extract_Max()+" => ");
			heap.print();			
		}
		
	}
	
	private static void Test_Min_Heap() {
		MinHeap heap = new MinHeap(10);
		heap.insert(0);
		heap.insert(1);
		heap.insert(2);
		heap.insert(3);
		heap.insert(4);
		heap.insert(5);
		heap.insert(6);
		heap.insert(7);
		heap.insert(8);
		System.out.println("Min Heap");
		heap.print();
		
//		System.out.println("\nInsert 0");
//		heap.insert(0);
//		heap.print();
		
		System.out.println("\nExtract Min");
		for(int i=0; i<10; i++) {
			System.out.print("Root = "+heap.extract_Min()+" => ");
			heap.print();			
		}
	}
	
}
