package Heap;

/**
 * Root > Child
 * 인덱스 0번째 부터 시작 => 왼쪽 : 2i+1, 오른쪽 : 2i+2 
 * 저장된 값 자체를 우선순위로 보고 우선순위 큐로 사용 가능
 */
public class MaxHeap {
	private int[] heap;
	private int capacity;
	private int currentSize = 0;

	public MaxHeap(int size) {
		this.capacity = size;
		this.heap = new int[size];
	}
	
	public void insert(int v) {
		//1. heap 꽉차 있는지 검사
		if(isFull()) {
			return;
		}
		
		//2. heap 끝에 마지막에 삽입
		heap[currentSize] = v;
		int insertPosition = currentSize;
		currentSize++;
		
		//3. 마지막 노드와 그 부모 노드의 값을 비교. 부모는 자식보다 커야 함. 자식이 더 커지는 경우 종료
		//   조건 만족할때까지 반복
		while(insertPosition > 0 && heap[insertPosition/2] < heap[insertPosition]) {
			Swap(insertPosition/2, insertPosition);
			insertPosition = insertPosition/2;
		}
	}
	
	public int extract_Max() {
		int item = Integer.MIN_VALUE;
		//1. 루트 제거
		if(!isEmpty()) {
			//2. 마지막 인덱스 노드를 루트로 옮김
			item = heap[0];
			heap[0] = heap[--currentSize];
			//3. 마지막 인덱스 제거 
			heap[currentSize] = 0;
		} else {
			System.out.println("Heap Array is Empty");
			return Integer.MIN_VALUE;
		}
		//4. heapfiy(루트)
		maxheapify(0);
		
		return item;
	}

	//자식이 더 크다면 자식과 부모를 교환 하는것을 반복하여 힙의 구조를 변경
	public void maxheapify(int i) {
		int left = 2 * i + 1;
		int right = 2 * i + 2;
		if (left >= currentSize || right >= currentSize) {
			return;
		}

		int large = Integer.MAX_VALUE; 
		if (heap[left] > heap[i]) { //부모보다 왼쪽 자식이 크 다면
			large = left;
		} else {
			large = i;
		}

		if (heap[right] < heap[large]) { //부모와 왼족 자식보다 오른쪽 자식이 크다면
			large = right;
		}

		if (large != i) {
			Swap(i, large); //자식과 부모의 자리를 교환
			maxheapify(large); //강등된 노드의 위치부터 다시 힙 성질 만족하도록 구조 변경
		}
	}
	
	public void print() {
		for (int i = 0; i < currentSize; i++) {
			System.out.print(heap[i] + " ");
		}
		System.out.println();
	}

	public void Swap(int a, int b) {
		int tmp = heap[a];
		heap[a] = heap[b];
		heap[b] = tmp;
	}

	public boolean isFull() {
		return currentSize == capacity;
	}

	public boolean isEmpty() {
		return currentSize == 0;
	}
}
