package Heap;

/**
 * Root < Child
 * 인덱스 0번째 부터 시작 => 왼쪽 : 2i+1, 오른쪽 : 2i+2 
 * 저장된 값 자체를 우선순위로 보고 우선순위 큐로 사용 가능
 */
public class MinHeap {

	private int[] heap;
	private int capacity;
	private int currentSize = 0;

	public MinHeap(int size) {
		this.capacity = size;
		this.heap = new int[size];
	}

	/**
	 * root < child
	 */
	public void insert(int n) {
		if (isFull()) {
			System.out.println("Heap Array Full");
			return;
		}

		// 마지막 위치에 노드 삽입
		heap[currentSize] = n;
		int insertPos = currentSize;
		currentSize++;

		//삽입된 노드의 키 값이 부모 노드의 키값보다 크다면 부모와 자식 교환
		while (insertPos > 0 && heap[insertPos] < heap[insertPos / 2]) {
			Swap(insertPos / 2, insertPos);
			insertPos = insertPos / 2;
		}
	}
	
	public boolean contain(int n) {
		for (int i = 0; i < currentSize; i++) {
			if(heap[i] == n ) {
				return true;
			}
		}
		return false;
	}

	public int extract_Min() {
		int item = Integer.MIN_VALUE;
		if (!isEmpty()) {
			item = heap[0];
			heap[0] = heap[--currentSize];
			heap[currentSize] = 0;
		} else {
			System.out.println("Heap Array is Empty");
			return Integer.MIN_VALUE;
		}

		minheapify(0);

		return item;
	}

	/**
	 * 힙 규칙에 맞도록 다시 힙을 구성
	 * 
	 * @param i = 시작 인덱스
	 */
	public void minheapify(int i) {
		int left = 2 * i + 1;
		int right = 2 * i + 2;
		if (left >= currentSize || right >= currentSize) {
			return;
		}

		int small = Integer.MIN_VALUE;
		if (left <= currentSize && heap[left] < heap[i]) {
			small = left;
		} else {
			small = i;
		}

		if (right <= currentSize && heap[right] < heap[small]) {
			small = right;
		}

		if (small != i) {
			Swap(i, small);
			minheapify(small);
		}
	}

	public void print() {
		for (int i = 0; i < currentSize; i++) {
			System.out.print(heap[i] + " ");
		}
		System.out.println();
	}

	public void Swap(int a, int b) {
		int tmp = heap[a];
		heap[a] = heap[b];
		heap[b] = tmp;
	}

	public boolean isFull() {
		return currentSize == capacity;
	}

	public boolean isEmpty() {
		return currentSize == 0;
	}
}
