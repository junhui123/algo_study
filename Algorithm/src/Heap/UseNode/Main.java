package Heap.UseNode;


public class Main {
	public static void main(String[] args) {
		Test_MinHeap();
		System.out.println();
		Test_MaxHeap();
	}
	
	static void Test_MinHeap() {
		MinHeap heap = new MinHeap(10);
		heap.insert(1);
		heap.insert(2);
		heap.insert(3);
		heap.insert(4);
		heap.print();
		
		heap.insert(0);
		heap.print();
		
		heap.extract_Min();
		heap.print();
		heap.extract_Min();
		heap.print();
	}
	
	static void Test_MaxHeap() {
		MaxHeap heap = new MaxHeap(10);
		heap.insert(1);
		heap.insert(5);
		heap.insert(2);
		heap.insert(4);
		heap.insert(3);
		heap.print();
		
		heap.insert(8);
		heap.print();
		
		heap.extract_Max();
		heap.print();
		
		heap.extract_Max();
		heap.print();
		
	} 	
}
