package Heap.UseNode;

public class MaxHeap {
	private Node[] heap;
	private int capacity;
	private int currentSize = 0;

	public MaxHeap(int size) {
		this.capacity = size;
		this.heap = new Node[size];
	}
	
	public void insert(int v) {
		if(isFull()) {
			System.out.println("Heap is Full");
			return;
		}
		
		heap[currentSize] = new Node(v, null, null);
		int insertPosition = currentSize;
		currentSize++;
		
		//root > child 되어야함. 자식이 더 크다면 부모와 자식을 교체
		while(insertPosition > 0 && heap[insertPosition].value > heap[insertPosition/2].value) { 
			Swap(insertPosition, insertPosition/2);
			insertPosition = insertPosition/2;
		}
	}
	
	public Node extract_Max() {
		Node tmp = null;
		if(isEmpty()) {
			System.out.println("Heap is Empty");
			return null;
		}	
		
		//1. 루트를 제거
		tmp = heap[0];
		//2. 마지막 자식을 루트로 이동
		heap[0] = heap[--currentSize]; 
		heap[currentSize] = null;
		//3. heapify
		maxheapify(0);
		
		return tmp;
	}
	
	private void maxheapify(int i) {
		int left = 2 * i + 1;
		int right = 2 * i + 2;
		if (left >= currentSize || right >= currentSize) {
			return;
		}

		int large = 0;
		if (heap[left].value > heap[i].value) {
			large = left;
		} else {
			large = i;
		}

		if (heap[right].value < heap[large].value) {
			large = right;
		}
		
		if(large != i) {
			Swap(large, i);
			maxheapify(large);
		}

	}

	public void print() {
		for (int i = 0; i < currentSize; i++) {
			System.out.print(heap[i].value + " ");
		}
		System.out.println();
	}

	public void Swap(int a, int b) {
		Node tmp = heap[a];
		heap[a] = heap[b];
		heap[b] = tmp;
	}
	
	public boolean isFull() {
		return currentSize == capacity;
	}
	
	public boolean isEmpty() {
		return currentSize == 0;
	}
}
