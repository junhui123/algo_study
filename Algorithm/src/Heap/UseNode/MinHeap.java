package Heap.UseNode;

public class MinHeap {

	private Node[] heap;
	private int capacity;
	private int currentSize = 0;

	public MinHeap(int size) {
		this.capacity = size;
		this.heap = new Node[size];
	}

	/**
	 * root <= child, root >= child 
	 */
	public void insert(int n) {
		if(isFull())  {
			System.out.println("Heap Array Full");
			return;
		}
		
		//마지막 위치에 노드 삽입
		heap[currentSize] = new Node(n, null, null);
		int inPos = currentSize;
		currentSize++;
		
		//삽입된 노드와 부모 노드의 키값을 비교. 삽입된 노드의 키 값이 부모 노드의 키값보다 크다면 부모와 자식 교환
		while(inPos > 0 && heap[inPos].value < heap[inPos/2].value) {
			Swap(inPos, inPos/2);
			inPos = inPos/2;
		}
	}
	
	public Node extract_Min() {
		Node tmp = null;
		if(!isEmpty()) {
			//1. 루트 노드 제거
			tmp = heap[0];
			//2. 마지막 인덱스 노드 루트 인덱스로 이동
			heap[0] = heap[--currentSize];
			heap[currentSize] = null;
		} else {
			System.out.println("Heap is Empty");
			return tmp;
		}
		
		//3. heapify
		minheapify(0);
		
		return tmp;
	}

	//Min Heap heapify
	public void minheapify(int i) {
		int left = 2*i;
		int right = 2*i+1;
		if(left>=currentSize || right>=currentSize) {
			return;
		}
		
		int smaller = 0;
		if (left <= currentSize && heap[left].value < heap[i].value) {
			smaller = left;
		} else {
			smaller = i;
		}

		if (right <= currentSize && heap[right].value < heap[smaller].value) {
			smaller = right;
		}
		
		if(smaller != i) {
			Swap(smaller, i);
			minheapify(smaller);
		}
	}
	
	public void print() {
		for (int i = 0; i < currentSize; i++) {
			System.out.print(heap[i].value + " ");
		}
		System.out.println();
	}

	public void Swap(int a, int b) {
		Node tmp = heap[a];
		heap[a] = heap[b];
		heap[b] = tmp;
	}
	
	public boolean isFull() {
		return currentSize == capacity;
	}
	
	public boolean isEmpty() {
		return currentSize == 0;
	}
	
	public void removeChild(Node n) {
		n.left = null;
		n.right = null;
	}

}
