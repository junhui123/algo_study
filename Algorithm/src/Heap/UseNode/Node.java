package Heap.UseNode;

public class Node implements Comparable<Node> {
	int value;
	Node left;
	Node right;
	
	public Node(int value, Node left, Node right) {
		this.value = value;
		this.left = left;
		this.right = right;
	}

	@Override
	public String toString() {
		return ""+value;
	}

	@Override
	public int compareTo(Node o) {
		if (value > o.value) {
			return 1;
		} else {
			return 0; 
		}
	}
}
