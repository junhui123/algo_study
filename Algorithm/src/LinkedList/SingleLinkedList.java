package LinkedList;

import java.util.Stack;

public class SingleLinkedList<T> {

	private T data;
	private SingleLinkedList<T> next;

	public SingleLinkedList(T data) {
		this.data = data;
	}
	
	public SingleLinkedList() {
		
	}

	public SingleLinkedList<T> next() {
		return next;
	}

	public void setNext(SingleLinkedList<T> elem) {
		next = elem;
	}

	public void setValue(T value) {
		data = value;
	}

	public T value() {
		return data;
	}
	
	public void printIter(SingleLinkedList<T> list) {
		SingleLinkedList<T> current = list;
		while(current != null) {
			System.out.print(current.value()+", ");
			current = current.next;
		}
		System.out.println("");
	}
	
	public void printRecur(SingleLinkedList<T> list) {
		if(list == null) {
			System.out.println("");
			return;
		}
		
		System.out.print(list.value()+", ");
		printRecur(list.next);
	}
	
	public void printReverseIter(SingleLinkedList<T> list) {
		Stack<SingleLinkedList<T>> stack = new Stack<>();
		SingleLinkedList<T> current = list;
		while(current != null) {
			stack.add(current);
			current = current.next;
		}
		
		while(!stack.isEmpty()) {
			System.out.print(stack.pop().value()+", ");
		}
		System.out.println("");
	}
	
	public void printReverseRecur(SingleLinkedList<T> list) {
		Stack<SingleLinkedList<T>> stack = new Stack<>();
		printReverseRecur(list, stack);
		
		while(!stack.isEmpty()) {
			System.out.print(stack.pop().value()+", ");
		}
		System.out.println("");
	}
	
	private void printReverseRecur(SingleLinkedList<T> list, Stack<SingleLinkedList<T>> stack) {
		if(list == null) {
			return;
		}
		
		stack.add(list);
		printReverseRecur(list.next, stack);
	}
}
