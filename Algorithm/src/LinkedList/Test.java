package LinkedList;

public class Test {
	public static void main(String[] args) {
		SingleLinkedList<Integer> singleLinkedList = new SingleLinkedList<Integer>(1);
		
		SingleLinkedList<Integer> lastNode = singleLinkedList;
		for(int i=2; i<=10; i++) {
			SingleLinkedList<Integer> newNode = new SingleLinkedList<Integer>(i);
			lastNode.setNext(newNode);
			if(i==10) {
				newNode.setNext(null);
			} else {
				lastNode = newNode;
			}
		}
		
		SingleLinkedList<Integer> printList = new SingleLinkedList<>();
		printList.printIter(singleLinkedList);
		printList.printRecur(singleLinkedList);
		printList.printReverseIter(singleLinkedList);
		printList.printReverseRecur(singleLinkedList);
	}
}
