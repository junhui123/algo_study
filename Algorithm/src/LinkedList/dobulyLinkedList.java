package LinkedList;

public class dobulyLinkedList<T> {
	private T data;
	private dobulyLinkedList<T> next;
	private dobulyLinkedList<T> prev;

	public T value() {
		return data;
	}

	public void setValue(T value) {
		data = value;
	}

	public dobulyLinkedList<T> getNext() {
		return next;
	}

	public dobulyLinkedList<T> getPrev() {
		return prev;
	}

	public void setNext(dobulyLinkedList<T> elem) {
		next = elem;
	}

	public void setPrev(dobulyLinkedList<T> elem) {
		prev = elem;
	}
}
