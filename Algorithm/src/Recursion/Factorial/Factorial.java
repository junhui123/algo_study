package Recursion.Factorial;

public class Factorial {
	private static int[] TEMP = new int[100];
	public static void main(String[] args) {
		int n = 10, acc = 1;
		System.out.println("iter  	  : "+n+"! = "+iter(n));
		System.out.println("recur	  : "+n+"! = "+recur(n));
		System.out.println("tailRecur : "+n+"! = "+tailRecur(n, acc));
		System.out.println("DPrecur	  : "+n+"! = "+DPrecur(n));
	}
	//반복
	public static int iter(int n) {
		int sum = 1;
		for(int i=2; i<=n; i++) {
			sum *= i;
		}
		return sum;
	}
	
	//단순 재귀
	public static int recur(int n ) {
		if(n==1) return 1;
		return n*recur(n-1);
	}
	
	//꼬리 재귀
	public static int tailRecur(int n, int acc) {
		if(n==1) return acc;
		return tailRecur(n-1, acc*n);
	}
	
	//동적 프로그래밍
	public static int DPrecur(int n) {
		if (n == 0 || n == 1) {
			return 1;
		} else {
			if (TEMP[n] != 0)
				return TEMP[n];
			else
				return TEMP[n] = (n * DPrecur(n - 1));
		}
	}
}
