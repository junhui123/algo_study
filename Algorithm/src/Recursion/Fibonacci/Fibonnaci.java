package Recursion.Fibonacci;

/**
 * 피보나치 수열  = 1,1,2,3,4,7,13,2,1,34,55,89,144,233,377,...
 * 처음 두 항은 1 이고 그 다음 항부터 앞의 두항을 더하여 만든다.
 *  
 * n>=1
 * f1=1, f2=2, fn+2=fn+1 + fn
 */
public class Fibonnaci {
	public static void main(String[] args) {
//		int n=20;
		int n=50;
		System.out.println(fibonnaci_recur_memoization(n));
		System.out.println(finbonnaci_iter(n));
		System.out.println(fibonnaci_recur(n));
	}
	
	public static long fibonnaci_recur(int n) {
		if(n==1 || n==2) return 1;
//		return fibonnaci_recur(n+1)+fibonnaci_recur(n);
		return fibonnaci_recur(n-2)+fibonnaci_recur(n-1);
	}
	
	public static int SIZE = 200;
	public static long[] MEMO = new long[SIZE];
	public static long fibonnaci_recur_memoization(int n) {
		if (MEMO[n] > 0) {
			return MEMO[n];
		}

		if (n == 1 || n == 2) {
			return 1;
		} else {
			MEMO[n] = fibonnaci_recur_memoization(n - 1) + fibonnaci_recur_memoization(n - 2);
		}
		return MEMO[n];
	}
	
	public static long finbonnaci_iter(int n) {
		long i, f_i, f_i_1, t;
		
		if(n==1 || n==2) return 1;
		
		f_i_1=1;
		f_i=1;
		
		for (i = 3; i <= n; i++) {
			t = f_i;
			f_i = f_i_1 + f_i;
			f_i_1 = t;
		}
		return f_i;
	}
	
	
}
