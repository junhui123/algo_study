package Recursion;

/**
 * 금액 맞추기 
 * 
 * 100만원 지불 방법
 * 
 * 50만원으로 2장
 * 50만원 1장, 1,2,5,10,20으로 50만원 
 * 50만원 0장, 1,2,5,10,20으로 100만원
 * 
 * pay(m,n) 
 *  n=1 이면 m % biils[0] == 0 이면 1 아니면 0
 *  n>=2 pay(m-bills[n-1]*i, n-1)
 *  
 * @author QWER
 */
public class NumberOfBills {
	public static void main(String[] args) {
		int money = 100;
		int numOfBills = 6;
		int[] bills = new int[] {1,2,5,10,20,50};
		System.out.println(payMoney_Recursion(money, bills, numOfBills));
		System.out.println(payMoney_Memoization(money, bills, numOfBills));
	}
	
	public static int payMoney_Recursion(int money, int[] bills, int n) {
		int count = 0, i, t;
		
		if(n==1) {
			if(money % bills[0] == 0) {
				return 1;
			} else {
				return 0;
			}
		}
		
		t = money / bills[n-1];
		for(i=0; i<=t; i++) {
			count += payMoney_Recursion(money-bills[n-1]*i, bills, n-1);
		}
		System.out.print(count+",");
		return count;
	}
	
	public static int[][] MEMO = new int[1000][1000];
	public static int payMoney_Memoization(int money, int[] bills, int n ) {
		
		if(MEMO[n][money] != 0) {
			return MEMO[n][money]; 
		}		
		
		int count = 0, i, t;
		
		if(n==1) {
			if(money % bills[0] == 0) {
				return 1;
			} else {
				return 0;
			}
		}
		
		t = money / bills[n-1];
		for(i=0; i<=t; i++) {
			count += payMoney_Memoization(money-bills[n-1]*i, bills, n-1);
		}
		MEMO[n][money]=count;
		System.out.print(count+",");
		return count;	
	}
}
