package Recursion;

import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * 수분할
 * 5 수분할 = 5/5 수분할
 * 
 * 1. 순서 상관 없는 수분할
 * 1+1+1+1+1
 * 2+1+1+1
 * 2+2+1
 * 3+1+1
 * 3+2
 * 4+1
 * 5
 *  
 * 5/2 수분할
 * 1+1+1+1+1 => 1+ 4/1 분할
 * 2+1+1 => 2+3/2 분할
 * 2+2+1 => 2+3/2 분할
 * 
 * 1<=i<=m 일 때 i+(n-i)/i
 * 
 * 2. 순서 있는 수분할
 * 3 수분할 = 3/3 수분할
 * 1+1+1
 * 1+2 (≠2+1)
 * 2+1 (≠1+2)
 * 3
 *  
 */
public class NumberPartition {
	public static void main(String[] args) {
		// int n = 200, m = 200;
		int n = 5, m = 3;
		System.out.println(partition_memo(n, m));
		System.out.println();

		System.out.println(partition(n, m));
		System.out.println();

		int[] arr = new int[200];
		int arr_len = 0;
		System.out.println(partition_print(n, m, arr, arr_len));

		int z = 34;
		test(z, NumberPartition::partition2_memo);
		test(z, NumberPartition::partition2);
	}

	public static void test(int z, Function<Integer, Long> f) {
		long start = System.nanoTime();
		for (int i = 1; i <= 1; i++) {
			System.out.println(f.apply(z).longValue());
		}
		System.out.println(TimeUnit.SECONDS.convert(System.nanoTime() - start, TimeUnit.NANOSECONDS) + " sec");
	}

	public static int partition(int n, int m) {
		int count = 0;

		if (n < m)
			m = n;
		if (n == 0)
			return 1;

		for (int i = 1; i <= m; i++) {
			count += partition(n - i, i);
		}

		return count;
	}

	public static int partition_print(int n, int m, int arr[], int arr_len) {
		int count = 0, i;

		if (n < m)
			m = n;
		
		if (n == 0) {
			print_arr(arr, arr_len);
			return 1;
		}

		for (i = 1; i <= m; i++) {
			arr[arr_len] = i;
			count += partition_print(n - i, i, arr, arr_len + 1);
		}
		return count;
	}

	static void print_arr(int arr[], int arr_len) {
		for (int i = 0; i < arr_len; i++)
			System.out.print(arr[i]);
		System.out.println();
	}

	private static int[][] MEMO = new int[300][300];
	public static int partition_memo(int n, int m) {
		if (MEMO[n][m] != 0)
			return MEMO[n][m];

		int count = 0, i;

		if (n < m)
			m = n;
		
		if (n == 0) {
			MEMO[n][m] = 1;
			return MEMO[n][m];
		}

		for (i = 1; i <= m; i++) {
			count += partition_memo(n - i, i);
		}
		MEMO[n][m] = count;
		return MEMO[n][m];
	}

	// 순서 있는 수분할
	static long partition2(int n) {
		long count = 0, i;
		for (i = 1; i <= n - 1; i++)
			count += partition2((int) (n - i));
		return count + 1;
	}

	private static long[] MEMO2 = new long[300];
	public static long partition2_memo(int n) {
		if (MEMO2[n] != 0)
			return MEMO2[n];

		long count = 0, i;
		for (i = 1; i <= n - 1; i++) {
			count += partition2_memo((int) (n - i));
		}
		MEMO2[n] = count + 1;
		return MEMO2[n];
	}
}
