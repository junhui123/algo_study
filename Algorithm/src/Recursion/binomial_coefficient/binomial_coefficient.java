package Recursion.binomial_coefficient;

/**
 * 이항 계수 = nCr
 * 4C2 = 4x3/2 = 6
 * 
 * 점화식
 * nC0 = nCn = 1
 * r>0 nCr = n-1Cr-1+n-1Cr
 * 
 */
public class binomial_coefficient {
	public static void main(String[] args) {
		int n=50, r=20;
//		int n=30, r=10;
		System.out.println(binomial_Memoization(n, r));
		System.out.println(binomial(n, r));
	}
	

	public static int binomial(int n, int r) {
		if (r == 0 || r == n) {
			return 1;
		} else { 
			return binomial(n - 1, r - 1) + binomial(n - 1, r); 
		}
	}
	
	public static int SIZE = 200;
	public static int[][] memo = new int[SIZE][SIZE];
	public static int binomial_Memoization(int n, int r) {
		if(memo[n][r] > 0) {
			return memo[n][r];
		} 
		
		if (r == 0 || r == n) {
			return 1;
		} else {
			memo[n][r] = binomial_Memoization(n - 1, r - 1) + binomial_Memoization(n - 1, r); 
			return memo[n][r];
		}
	}
	
}
