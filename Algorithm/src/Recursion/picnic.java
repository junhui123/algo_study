package Recursion;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

/**
 * 문제 ID : PICNIC, 알고스팟
 * 0     1  2  3 = 4 
 * 보라돌이 뚜비 나나 뽀
 * 0, 1, 1, 2, 2, 3, 3, 0, 0, 2, 1, 3 = 6
 * 
 * 보라돌이 뚜비, 나나 보 (0 1, 2 3)
 * 뚜비 뽀, 보라돌이 나나 (1 3, 0 2)
 * 나나 뚜비, 뽀 보라돌이 (2 1, 3 0)
 */
public class picnic {
	public static void main(String[] args) throws IOException {
		// int n = 4, m = 6;
		// int[] FRIENDS = {0, 1, 1, 2, 2, 3, 3, 0, 0, 2, 1, 3};
		// countParings(n, m, FRIENDS);

		Scanner sc = new Scanner(System.in);
		int count = sc.nextInt();
		for (int j = 0; j < count; j++) {
			n = sc.nextInt();
			int friendCount = sc.nextInt();
			areFriends = new boolean[10][10];
			for (int i = 0; i < friendCount; i++) {
				int a = sc.nextInt();
				int b = sc.nextInt();
				areFriends[a][b] = areFriends[b][a] = true;
			}
			boolean[] data = new boolean[10];
			System.out.println(countParingsBook(data));
		}
	}
	public static int countParings(int n, int m, int[] FRIENDS) {
		
		if(Arrays.stream(FRIENDS).anyMatch(t->t==n) == true) {
			return 0;
		}
		
		ArrayList<int[]> friendslist = new ArrayList<>(m);
		for(int i=0; i<FRIENDS.length; i=i+2) {
			int[] tmp = new int[] {FRIENDS[i], FRIENDS[i+1]};
			friendslist.add(tmp);
		}
		
		int first = 0;
		ArrayList<int[]> result = new ArrayList<>();
		for (int[] is : friendslist) {
			if(is[0] == first || is[1] == first) {
				result.add(is);
			}
		}
		
		for (int[] is : result) {
			if(is[0] == first || is[1] == first) {
				
			}
		}
		
		return 0;
	}
	
	//{0,1} == {1,0} 순서만 다를 뿐 같은 학생 집합 
	static int n;
	static boolean[][] areFriends = new boolean[10][10];
	
	//taken[i] = i번재 학생이 짝을 찾으면 true, 아니면 false
	public static int countParingsBook(boolean taken[]) {
		//남은 학생 중 번호가 가장 빠른 학생
		int firstFree = -1;
		for(int i=0; i<n; ++i) {
			if(!taken[i]) {
				firstFree = i;
				break;
			}
		}
		
		//BaseCase
		if(firstFree == -1) return 1;
		
		//짝 지을 학생 결정
		int ret = 0;
		for(int pairWith = firstFree+1; pairWith < n; ++pairWith) {
			if(!taken[pairWith] && areFriends[firstFree][pairWith]) {
				taken[firstFree] = taken[pairWith] = true;
				ret += countParingsBook(taken);
				taken[firstFree] = taken[pairWith] = false;
			}
		}
		return ret;
	}
}
