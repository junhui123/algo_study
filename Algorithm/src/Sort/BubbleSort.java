package Sort;

import java.util.Arrays;

public class BubbleSort {
	public static void main(String[] args) {
		int[] arrs = new int[] { 17, 35, 6, 28, 2, 40, 38, 48, 4, 7 };
//		int[] arrs = new int[] { 29,10,14,37,14 };
//		int[] arrs = new int[] { 28,30,15,15,24,35,47,15,34,49,28,26 };
//		int[] arrs = new int[] { 6,29,17,13,19,35,49,26,7,1,22 };
		
		//순차 정렬...
		sort2(arrs);
		
		System.out.println("=====================================================");
		
		bubbleSort(new int[] {17, 35, 6, 28, 2, 40, 38, 48, 4, 7});
//		bubbleSort(new int[] {28,30,15,15,24,35,47,15,34,49,28,26});
//		bubbleSort(new int[] {29,10,14,37,14});
//		bubbleSort(new int[] {6,29,17,13,19,35,49,26,7,1,22});
		
	}

	//정렬은 되나...순차 정렬...버블 소트가 아님..버블은 인접한 숫자와 비교 해야 ..
	public static void sort(int[] arrs) {
		for (int i = 0; i < arrs.length; i++) {
			for (int j = i + 1; j < arrs.length; j++) {
				System.out.println("arrs["+i+"]="+arrs[i]+", arrs["+j+"]="+arrs[j]);
				if (arrs[i] > arrs[j]) {
					int tmp = arrs[i];
					arrs[i] = arrs[j];
					arrs[j] = tmp;
				}
			}
		}
	}
	
	//17, 35, 6, 28, 2, 40, 38, 48, 4, 7
	public static void sort2 (int[] arrs) {
		//배열 길이 만큼 반복
		for (int i = 0; i < arrs.length; i++) {
			//숫자 비교 스왑 
			for (int j = 1; j < arrs.length-i; j++) {
//				System.out.println("arrs["+j+"]="+arrs[j]+", arrs["+(j-1)+"]="+arrs[j-1]);
				if (arrs[j] < arrs[j-1]) {
					int tmp = arrs[j];
					arrs[j] = arrs[j-1];
					arrs[j-1] = tmp;
					
					System.out.println(Arrays.toString(arrs));
					
				}
			}
		}
		System.out.println("End ========> "+Arrays.toString(arrs));
	}

	public static void bubbleSort(int[] arr) {
		int temp = 0;
		for (int i = 0; i < arr.length; i++) {
			for (int j = 1; j < arr.length - i; j++) {
//				System.out.println("arrs["+j+"]="+arr[j]+", arrs["+(j-1)+"]="+arr[j-1]);
				if (arr[j] < arr[j - 1]) {
					temp = arr[j - 1];
					arr[j - 1] = arr[j];
					arr[j] = temp;
					System.out.println(Arrays.toString(arr));

				}
			}
		}
		System.out.println("End ========> "+Arrays.toString(arr));
	}
}
