package Sort;

import java.util.Arrays;
import java.util.StringJoiner;

/**
 * 삽입정렬 = 첫번째는 정렬된 상태로 가정, 두번째 인덱스 부터 시작해서 앞의 원소 모두와 비교해서 교체 하는 작업
	arr 
	[3] 7 2 5 1 4
	3 [7] 2 5 1 4
	3 7 [2] 5 1 4
	2 3 7 [5] 1 4
	2 3 5 7 [1] 4
	1 2 3 5 7 [4]
	1 2 3 4 5 7
	
	arr
	[31] 25 12 22 11
	31 [25] 12 22 11
	25 31 [12] 22 11
	12 25 31 [22] 11
	12 22 25 31 [11]
	11 12 22 25 31
 * 
 */
public class InsertionSort {
	public static void main(String[] args) {
//		int[] data = sort(new int[] { 31, 25, 12, 22, 11 });
//		for (int a : data) {
//			System.out.println(a);
//		}
		
//		System.out.println("========================================");
//		
//		int[] data2 = sort2(new int[] { 31, 25, 12, 22, 11 });
//		for (int a : data2) {
//			System.out.println(a);
//		}
		
//		System.out.println("========================================");
		
		int[] data3 = sort3(new int[] { 31, 25, 12, 22, 11 });
		for (int a : data3) {
			System.out.println(a);
		}
		
		
	}

	public static int[] sort(int[] data) {
		for (int i = 1; i < data.length; i++){
		    for (int j = i; j > 0 && data[j-1] > data[j]; j--){
		        int temp = data[j];
		        data[j] = data[j-1];
		        data[j-1] = temp;
		    }
		}
		return data;
	}

	public static int[] sort2(int[] data) {
		for (int i = 1; i < data.length; i++) {
			int val = data[i];
			int dataj = 0;
			for (int j = 0; j < i; j++) {
				dataj = data[j];
				if (dataj > val) {
//					System.out.println(String.valueOf(j)+","+String.valueOf(j+1)+","+String.valueOf(i-j));
					System.arraycopy(data, j, data, j + 1, i - j);
//					System.out.println(Arrays.toString(data));
					data[j] = val;
					break;
				}
			}
		}
		return data;
	}
	
	public static int[] sort3(int[] arr) {
		for (int index = 1; index < arr.length; index++) {
			int temp = arr[index];
			int aux = index - 1;
			
			while ( (aux >= 0) && (arr[aux] > temp) ) {
				arr[aux + 1] = arr[aux];
				aux--;
			}
			arr[aux + 1] = temp;
		}
		return arr;
	}
}
