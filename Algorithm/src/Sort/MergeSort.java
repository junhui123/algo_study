package Sort;

import java.util.Arrays;

public class MergeSort {
	public static void main(String[] args) {
		// int[] arrs = new int[] { 17, 35, 6, 28, 2, 40, 38, 48, 4, 7 };
		int[] arrs = new int[] { 40, 38, 48, 4, 7 };
		sort(arrs);
		System.out.println(Arrays.toString(arrs));
		
	}

	public static int[] sort(int[] data) {

		partion(data);

		return data;
	}

	public static int[] partion(int[] data) {

		// 분할 과정을 거쳐 1개가 남을 경우 다 분할된 상태
		if (data.length < 2) {
			return data;
		}

		// 배열을 분할
		int mid = data.length / 2;
		int[] left = new int[mid];
		int[] right = new int[data.length - mid];

		System.arraycopy(data, 0, left, 0, left.length);
		System.arraycopy(data, mid, right, 0, right.length);
		System.out.println("copy  : data=" + Arrays.toString(data) + ", left=" + Arrays.toString(left) + ", right="
				+ Arrays.toString(right));

		partion(left);
		partion(right);

		merge(data, left, right);
		System.out.println("merge : data=" + Arrays.toString(data) + ", left=" + Arrays.toString(left) + ", right="
				+ Arrays.toString(right));

		return data;
	}

	public static int[] merge(int[] dest, int[] left, int[] right) {

		int dind = 0;
		int lind = 0;
		int rind = 0;

		while (lind < left.length && rind < right.length) {
			if (left[lind] <= right[rind]) {
				dest[dind++] = left[lind++];
			} else {
				dest[dind++] = right[rind++];
			}
		}

		while (lind < left.length) {
			dest[dind++] = left[lind++];
		}

		while (rind < right.length) {
			dest[dind++] = right[rind++];
		}

		return dest;
	}
}
