package Sort;

public class QuickSort {
	public static void main(String[] args) {
		int[] arrs = new int[] { 17, 35, 6, 28, 2, 40, 38, 48, 4, 7 };

//		quickSort(arrs, 0, arrs.length - 1);

		// simpleQuick(arrs);

		// quickSort2(arrs);

		 quickSort3(arrs, 0, arrs.length - 1);
		
		for (int a : arrs) {
			System.out.print(a + " ");
		}
	}

	/**
	 * 
	 * @param arr 정렬할 배열
	 * @param left 정렬 시작 인덱스
	 * @param right 마지막 인덱스 
	 * @return
	 */
	public static int[] quickSort(int[] arr, int left, int right) {
	    int i, j, pivot, tmp;
	    if (left < right) {
	        i = left;
	        j = right;
	        pivot = arr[left];
	        //분할 과정
	        while (i < j) {
	            while (arr[j] > pivot) j--;
	            // 이 부분에서 arr[j-1]에 접근해서 익셉션 발생가능함
	            while (i < j && arr[i] <= pivot) i++;

	            tmp = arr[i];
	            arr[i] = arr[j];
	            arr[j] = tmp;
	        }
	        arr[left] = arr[i];
	        arr[i] = pivot;
	        //정렬 과정
	        quickSort(arr, left, i - 1);
	        quickSort(arr, i + 1, right);
	    }
		return arr;
	}
	
	public static int[] simpleQuick(int[] data) {
		
		//배열에 원소가 1개 인 경우 정렬 필요 없음
		if(data.length < 2) {
			return data;
		}
		
		int pivotIndex = 0;
		int pivotValue = data[pivotIndex];
		
		//피벗 보다 작은 원소 갯수 확인
		int leftCount = 0;
		for(int i = 0 ; i < data.length; i++ ) {
			if (data[i] < pivotValue )
				leftCount++;
		}
		
		//피벗을 기준으로 분할 할 집합 설정
		int[] left = new int[leftCount]; //피벗 보다 작은 값 (왼쪽)
		int[] right = new int[data.length - leftCount - 1]; //피벗 보다 큰 값 (오른쪽)
		int l = 0;
		int r = 0; 
		
		for(int i=0; i<data.length; i++) {
			if ( i == pivotIndex ) continue;
			
			//현재 배열 값 
			int val = data[i];
			
			//피벗보다 작은 값 left 배열에 집어 넣음 큰 값은 right 배열
			if ( val < pivotValue) 
				left[l++] = val;
			else 
				right[r++] = val;
		}
		
		//더 이상 쪼갤 부분집합이 없을 때 까지 각각의 부분집합에 대해 쪼개기 작업을 수행
		left = simpleQuick(left);
		right = simpleQuick(right);
		
		//최종적으로 정렬된 left,right 배열을 하나로 합침
		System.arraycopy(left, 0, data, 0, left.length);
		data[left.length] = pivotValue;
		System.arraycopy(right, 0, data, left.length+1, right.length);
		
		return data;
	}
	


	
	/**
	 * 퀵 소트 메소드
	 * 
	 * @param nums
	 * @param start
	 * @param end
	 */
//	1) 피벗보다 우선순위가 낮은 값이 나올 때까지 low를 오른쪽으로 이동한다.
//	2) 피벗보다 우선순위가 높은 값이 나올 때까지 high를 왼쪽으로 이동한다.
//	3-1) 만약, low와 high가 교차되었다면, 피벗의 값과 high가 가리키는 값을 교환한다.
//	3-2) 그렇지 않다면, low와 high가 가리키는 값을 서로 바꾼다.
	private static void quickSort3(final int[] NUMS, int start, int end) {
		if (start < end) {
			int low = start;
			int high = end;
			int pivot = NUMS[start];

			while (low < high) {
				//피벗보다 작은 값 이면서, 오른쪽의 값 보다 작은 경우 
				//작은 값 인덱스를 증가 시킴
				while (NUMS[low] < pivot && low < high) {
					low++;
				}
				
				//피벗보다 큰 값이면서 low index 보다 작은 값이면 
				//높은 값 인덱스 감소 
				while (NUMS[high] > pivot && low < high) {
					high--;
				}

				//인덱스가 교차 하면 스왑 실행
				if (low < high) {
					swap(NUMS, low, high);
				}
			}

			quickSort3(NUMS, start, low - 1);
			quickSort3(NUMS, low + 1, end);
		}
	}

	/**
	 * 배열의 두 요소의 값을 바꾸는 메소드
	 * 
	 * @param nums
	 * @param aIdx
	 * @param bIdx
	 */
	private static void swap(final int[] NUMS, final int aIdx, final int bIdx) {
		int tmp = NUMS[aIdx];
		NUMS[aIdx] = NUMS[bIdx];
		NUMS[bIdx] = tmp;
	}
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	public static void Swap(int[] array, int index1, int index2) {
		int iTemp = array[index1];
		array[index1] = array[index2];
		array[index2] = iTemp;
	}
	
	public static int[] partition(int[] data, int[] partition, int srcPos, int destPos, int length) {
		System.arraycopy(data, srcPos, partition, destPos, length);
		return partition;
	}
	
	
	/**
	 for each (unsorted) partition
		set first element as pivot
		  storeIndex = pivotIndex + 1
		  for i = pivotIndex + 1 to rightmostIndex
		    if element[i] < element[pivot]
		      swap(i, storeIndex); storeIndex++
		  swap(pivot, storeIndex - 1)
	 */
	
	//잘못됨...피벗을 기준으로 먼저 분할을 해야 함....정렬 후 분할을 시도 했으므로 실패..?
	public static int[] quickSort2(int[] data) {
		if(data.length < 2) return data;
		
		int pivotIndex = 0;
		int storeIndex = 0;
		int pivot = 0;
		for(int n=0; n<data.length; n++) {
			pivot = data[n];
			pivotIndex = n;
			storeIndex = pivotIndex+1;
			
			for(int i = pivotIndex+1; i<data.length; i++) {
				int t1 = data[i];
				int t2 = data[pivotIndex];
				if ( t1 < t2 ) {
					Swap(data, i, storeIndex);
					storeIndex++;
				}
			}
			
			Swap(data, pivotIndex, storeIndex-1);
			quickSort2(data);
		}
//		int[] left = new int[storeIndex-1];
//		partition(data, left, pivotIndex, 0, left.length);
//		quickSort2(left);
//		
//		int[] right = new int[data.length-left.length];
//		partition(data, right, left.length+1, 0, right.length);
//		quickSort2(right);
//		
//		System.arraycopy(left, 0, data, 0, left.length);
//		data[left.length] = pivot;
//		System.arraycopy(right, 0, data, left.length+1, right.length);
		
		return data;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
