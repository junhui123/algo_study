package Sort;

/**
 * 선택 정렬 = 한 번 순회 후 최저값 찾은 뒤 첫번째 원소와 교체 두 번째 원소 부터 다시 최저값 찾은 후 두번째 원소와 교체 이를 반복
	array				Min
	[9,1,6,8,4,3,2,0]	0
	[0,1,6,8,4,3,2,9]	1
	[0,1,6,8,4,3,2,9]	2
	[0,1,2,8,4,3,6,9]	3
	[0,1,2,3,4,8,6,9]	4
	[0,1,2,3,4,8,6,9]	6
	[0,1,2,3,4,6,8,9]	8
 */
public class SelectionSort {
	public static void main(String[] args) {
		int[] data = { 66, 10, 1, 99, 5 };
		int[] result = sort(data);

//		for (int a : result) {
//			System.out.println(a);
//		}
		
		int[] result2 = recursionSort(new int[] { 66, 10, 1, 99, 5 }, 0);
		for (int a : result2) {
			System.out.println(a);
		}
		
		
	}

	public static int[] sort(int[] data) {
		for (int i = 0; i < data.length; i++) {
			// 최저값 인덱스
			int minIndex = i;
			for (int z = i; z < data.length; z++) {
				if (data[minIndex] > data[z]) {
					minIndex = z;
				}
			}

			int min = data[minIndex];
			data[minIndex] = data[i];
			data[i] = min;

		}
		return data;
	}
	
	
	////// 재귀  //////
	public static int getMinNumIndex(int[] data, int start) {
		int min = start;
		for (int z = start+1; z < data.length; z++) {
			if (data[min] > data[z]) {
				min = z;
			}
		}

		return min;
	}
	
	public static int[] recursionSort(int[] data, int start) {
		if (start < data.length - 1) {
			int minIndex = getMinNumIndex(data, start);
			int minNum = data[minIndex];
			
			int beforeFirst = data[start];
			data[start] = minNum;
			data[minIndex] = beforeFirst;
			
			recursionSort(data, start + 1);
		}
		return data;
	}
}
