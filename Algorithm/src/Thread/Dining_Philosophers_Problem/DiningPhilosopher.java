package Thread.Dining_Philosophers_Problem;

/*
philosophers[i] = new Philosopher(leftFork, rightFork);
===> DeadLock 
Philosophers 2 10154550179315: Thinking
Philosophers 1 10154550179315: Thinking
Philosophers 3 10154550300872: Thinking
Philosophers 5 10154550507759: Thinking
Philosophers 4 10154550488976: Thinking
Philosophers 1 10154551121445: Picked up left fork 
Philosophers 2 10154551122786: Picked up left fork 
Philosophers 5 10154551126275: Picked up left fork 
Philosophers 4 10154551126275: Picked up left fork 
Philosophers 3 10154551131105: Picked up left fork 
 * 
 */

public class DiningPhilosopher {
	public static void main(String[] args) {

		Philosopher[] philosophers = new Philosopher[5];
		Object[] forks = new Object[philosophers.length];

		for (int i = 0; i < forks.length; i++) {
			forks[i] = new Object();
		}

		for (int i = 0; i < philosophers.length; i++) {
			Object leftFork = forks[i];
			Object rightFork = forks[(i + 1) % forks.length];
			
			if( i== philosophers.length-1) {
				philosophers[i] = new Philosopher(i+1, rightFork, leftFork);
			} else {
				philosophers[i] = new Philosopher(i+1, leftFork, rightFork);
			}

			Thread t = new Thread(philosophers[i], "Philosophers " + (i + 1));
			t.start();
		}
	}
}
