package Thread.Dining_Philosophers_Problem;

public class Philosopher implements Runnable {

	private int id;
	private Object leftFork;
	private Object rightFork;
	private boolean isEat = false;
	
	public Philosopher(int id, Object leftFork, Object rightFork) {
		super();
		this.id = id;
		this.leftFork = leftFork;
		this.rightFork = rightFork;
	}

	private void doAction(String action) throws InterruptedException {
		System.out.println(Thread.currentThread().getName() + " " + action);
		Thread.sleep((int) Math.random() * 100);
	}

	@Override
	public void run() {
		try {
			while (isEat == false) {
				doAction(System.nanoTime() + ": Thinking");
				synchronized (leftFork) {
					doAction(System.nanoTime() + ": Picked up left fork ");	
					synchronized (rightFork) {
						doAction(System.nanoTime() + ": Picked up right fork ");	
						doAction(System.nanoTime() + ": "+id+" Eating");	
						doAction(System.nanoTime() + ": Picked down right fork ");	
					}
					doAction(System.nanoTime() + ": Picked down left fork. Back to Thinking ");
					isEat = true;	
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
