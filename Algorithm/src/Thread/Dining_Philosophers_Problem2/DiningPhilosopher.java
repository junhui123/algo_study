package Thread.Dining_Philosophers_Problem2;

import java.util.concurrent.Semaphore;

public class DiningPhilosopher {

	Semaphore[] chopsticks = new Semaphore[5];
	Philosopher[] philosophers = new Philosopher[5];
	
	public DiningPhilosopher() {
		
		for(int i = 0 ; i < chopsticks.length ; i++) {
			Semaphore s = new Semaphore(1, false);
			chopsticks[i] = s;
		}
	}
	
	public void DoDining()  throws InterruptedException {
		for (int i=0 ; i< philosophers.length ; i++) {
			Philosopher p = new Philosopher(i, chopsticks[i], chopsticks[(i+1) % 5]);
			System.out.println("i = "+i+ ", (i+1) % 5 = "+(i+1) % 5);
			philosophers[i] = p;
			System.out.println("Philosopher " + i + " start to eat.");
			
			philosophers[i].start();
		}
		
		// 10초가 수행 
		Thread.sleep(10000);
		
		for (int i=0 ; i< philosophers.length ; i++) {
			philosophers[i].StopEating();
			philosophers[i].join();
		}
		
		// 굶어죽은 철학자가 없는지 확인 
		for (int i=0 ; i< philosophers.length ; i++) {
			System.out.println("Did Philosopher " + philosophers[i].id + " eat dinner? : " + philosophers[i].isEat);
		}
		
		System.out.println("Done.");
	}
	
	public static void main(String[] args) {
		
		try {
			DiningPhilosopher d = new DiningPhilosopher();
			d.DoDining();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
