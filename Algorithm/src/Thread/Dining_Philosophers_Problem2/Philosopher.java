package Thread.Dining_Philosophers_Problem2;

import java.util.Random;
import java.util.concurrent.Semaphore;


/*
	Java 1.4.x 까지는 Sychronized 키워드를 사용하는 모니터와 wait(), notify() 같은 메서드만 제공 되었지만
	Java 5부터는 java.util.concurrent 네임스페이스에 쓸만한 그러나 이해를 좀 해야하는 클래스들이 제공 Semaphore
	세마포어와 뮤텍스는 큰차이는 없어 보이는데 구현할 때 명확히 차이가 보입니다. Mutex는 한개 Semaphore는 여러개의 permit
	
	acquire: lock과 동일하나 N개의 thread가 진입할 수 있다.
	release: acquire로 얻은 semaphore(lock) 반납.
	tryAcquire: semaphore의 성질에 의해 진입할 수 없는 경우, false등을 반환하여 lock획득에 실패했음을 알리고 즉시 리턴. (semaphore를 획득할 수 있으면 획득, 아니면 즉시 리턴)
	timedTryAcquire: tryAcquire와 동일하나 semaphore 획득을 위해 일정시간 대기한다.

	생서자에서 리소스의 수(permits)를 지정
	acquire() - 리소스를 확보. 리소스에 빈자리가 생겼을 경우 바로 쓰레드가 acquire 메소드로부터 곧바로 돌아오게 되고 세마포어 내부에서는
	                      리소스의 수가 하나 감소. 리소스에 빈자리가 없을 경우 쓰레드는 acquire 메소드 내부에서 블록.
	release() - 리소스를 해제. 세마포어 내부에서는 리소스가 하나 증가. acquire 메소드 안에서 대기중인 쓰레드가 있으면
	                      그 중 한 개가 깨어나 acquire 메소드로부터 돌아올 수 있다.

	세마포 변수란? 세마포는 가장 오래된 동기화 프리미티브이다. 
	- acquire() 호출되면 1감소 /  값이 0 이면 쓰레드는 세미포 대기큐에 들어가 block 상태가 된다. 
	- release() 호출되면, 먼저 세마포 대기큐를 검사해서 하나를 풀어준다, 대기큐에 없으면 변수값 1증가 
	
	활용법 : 어떤 임계영역에 제한된 수의 쓰레드만 진입가능하게 설정
*/
public class Philosopher extends Thread {
	
	public int id;
	private Semaphore leftChopstick;
	private Semaphore rightChopstick;
	private boolean stopEating;
	
	public boolean isEat;
	
	Random rand;

	public Philosopher(int id, Semaphore left, Semaphore right) {
		this.id = id;
		this.leftChopstick = left;
		this.rightChopstick = right;
		
		stopEating = false;
		rand = new Random();
		isEat = false;
	}
	
	private void Think() throws InterruptedException  {
		System.out.println("Philosopher " + id + " is THINKING.");
		sleep(rand.nextInt(1000));
	}
	
	private void Eat() throws InterruptedException {
		leftChopstick.acquire();
		rightChopstick.acquire();
		
		System.out.println("Philosopher " + id + " is EATING.");
		sleep(rand.nextInt(1000));
		isEat = true;
		
		leftChopstick.release();
		rightChopstick.release();
	}
	
	public void StopEating() {
		stopEating = true;
	}
	
	
	@Override
	public void run() {
		
		while(!stopEating) {
			try {
				this.Think();
				this.Eat();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
