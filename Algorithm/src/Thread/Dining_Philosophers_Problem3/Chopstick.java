package Thread.Dining_Philosophers_Problem3;

///////////////////////////////////////////////////////////
//젓가락(세마포어)
public class Chopstick {
	// Chopstick == Semaphore
	int id;			 // 젓가락 번호
	boolean isUsing; // 젓가락이 사용중인지 확인하는 값

	Chopstick(int id) {
		this.id 	 = id;
		this.isUsing = false;
	}

	synchronized void p() throws InterruptedException {
		while (isUsing) { // 미리 사용중이라면 대기.
			this.wait();
		}
		
		// 사용중이 아니라면 젓가락 사용
		// critical section
		System.out.println(id + " 번 젓가락 들음"); 
		isUsing = true;
	}

	synchronized void v() {
		// 사용중이라면 젓가락 내려놓기
		if(isUsing) {
			System.out.println(id + " 번 젓가락 내려놓음");
			this.notifyAll();
			isUsing = false;
		}
		
	}
}