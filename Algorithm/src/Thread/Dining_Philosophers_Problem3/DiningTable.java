package Thread.Dining_Philosophers_Problem3;

public class DiningTable {
	final static int NUMBER_OF_PEOPLE = 4; 					  // 4명의 철학자
	Philosopher[] person = new Philosopher[NUMBER_OF_PEOPLE]; //철학자 배열
	Chopstick[] s 		 = new Chopstick[NUMBER_OF_PEOPLE];   // 젓가락 배열

	// 생성자
	DiningTable() {
		for (int i = 0; i < s.length; i++) {
			s[i] = new Chopstick(i + 1);
		}
		for (int i = 0; i < person.length; i++) {
			person[i] = new Philosopher(i + 1, s[i], s[(i + 1)
					% NUMBER_OF_PEOPLE]);
		}
	}

	void doEat() {
		for (int i = 0; i < person.length; i++) {
			System.out.println((i + 1) + " : " + person[i].left.id + ", "
					+ person[i].right.id);
		}

		
		// /////////////////////////////////////////////////////////
		// 식사 시작
		for (Philosopher ph : person) {
			ph.start();
		}

		try {
			Thread.sleep(500); // 식사 시간

			// 종료
			for (int i = 0; i < person.length; i++) {
				person[i].stopEat();
				person[i].join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		for (int i = 0; i < person.length; i++) {
			System.out.println((i + 1) + ". 식사 확인 : " + person[i].isEating);

		}
	}

	public static void main(String[] args) {
		DiningTable t = new DiningTable(); // 식사테이블 객체 생성
		t.doEat(); // 식사 시작 메소드

		System.out.println("식사종료");
	}
}
