package Thread.Dining_Philosophers_Problem3;

import java.util.Random;

///////////////////////////////////////////////////////////
//철학자 클래스
public class Philosopher extends Thread {
	int    	  id;			// 철학자 식별번호
	Random    r;			// 식사, 생각하는 랜던 시간
	Chopstick right, left;  // 젓가락

	boolean isEating;		// 식사중인지 확인하는 값
	boolean isStop;			// 식사를 멈출것인지 확인하기 위한 값

	public Philosopher(int id, Chopstick right, Chopstick left) {
		this.id = id;
		this.right = right;
		this.left = left;
		
		r = new Random();
		this.isEating = false;
		this.isStop = false;
	}

	// 젓가락을 듬.
	void st() throws InterruptedException {
		left.p(); 			 // 왼쪽 젓가락 듦
		synchronized(this) { // 오른쪽 젓가락을 드는 동시에 식사시작하기 때문에 동기화
			right.p(); 		 // 오른쪽 젓가락 듦
			System.out.println("철학자" + id + " 식사 시작");
		}
	}

	// 젓가락을 내려놓음
	void end() {
		synchronized (this) { // 왼쪽 젓가락을 내려놓음과 동시에 식사마침 때문에 동기화
			System.out.println(id + " 식사 마침");
			left.v();
		}
		
		synchronized(this) { // 오른쪽 젓가락까지 마저 내려놓음과 동시에 생각 시작 때문에 동기화
			right.v();
			System.out.println("철학자" + id + " 생각 중.");
		}
	}

	private synchronized void thinking() throws InterruptedException {
		sleep(r.nextInt(1000)); // 생각하는 시간
	}

	private void eating() throws InterruptedException {
		isEating = true;		// 식사를 했다는 값
		st(); 			 		// 젓가락 듦.
		sleep(r.nextInt(1000)); // 식사시간
		end();					// 젓가락 내려놓음.
	}

	public void stopEat() { // 식사를 멈추기 위한 메소드
		isStop = true;
	}

	public void run() {
		while (!isStop) { // 멈추는 메소드 사용전에 무한 루프
			try {
				thinking(); // 생각중
				eating();	// 식사중
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

}
