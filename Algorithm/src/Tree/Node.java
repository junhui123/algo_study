package Tree;

public class Node {
	private int value;
	private Node Left;
	private Node right;
	private Node parent;

	public Node(int value, Node left, Node right) {
		super();
		this.value = value;
		Left = left;
		this.right = right;
	}
	
	public Node(int value) {
		super();
		this.value=value;
	}
	
	public int getvalue() {
		return value;
	}

	public void setvalue(int value) {
		this.value = value;
	}

	public Node getLeft() {
		return Left;
	}

	public void setLeft(Node left) {
		Left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
	
	public Node addNewNode(String LR, int value) {
		Node newNode = new Node(value);
		newNode.setParent(this);
		if(LR.equals("L")) {
			this.setLeft(newNode);
		} else { //R
			this.setRight(newNode);
		}
		return newNode;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}
}
