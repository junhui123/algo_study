package Tree;

import java.util.Stack;

import algorithm_InterView.CtCILibrary.TreeNode;

//BST
public class TreeAddRemove {
	public static void main(String[] args) {
		TreeNode root = new TreeNode(20);
		TreeNode n1 = new TreeNode(8);
		TreeNode n2 = new TreeNode(22);
		TreeNode n3 = new TreeNode(4); 
		TreeNode n4 = new TreeNode(12);
		TreeNode n5 = new TreeNode(21);
		TreeNode n6 = new TreeNode(23);
		TreeNode n7 = new TreeNode(1); 
		TreeNode n8 = new TreeNode(5); 
		root.left = n1;
		root.right = n2;
		n1.left = n3;
		n1.right = n4;
		n2.left = n5;
		n2.right = n6;
		n3.left = n7;
		n3.right = n8;

		TreeNode searchNode = searchNode(root, 5);
		if (searchNode != null)
			System.out.println(searchNode.data);
		else {
			addNode(root, 11);
			System.out.println("add node");
		}
		
		searchNode = search(root, 5) ;
		if (searchNode != null)
			System.out.println(searchNode.data);
		
		
		//Delete(root, 20);
		root.print();
		
		Delete2(root, 20);
		root.print();
		
//		System.out.println("Find Min / Max");
//		System.out.println("Min : "+searchMin(n3).data);
//		System.out.println("Max : "+searchMax(n3).data);
//		
//		System.out.println("Min : "+findMin(n3).data);
//		System.out.println("Max : "+findMax(n3).data);
	}

	public static void addNode(TreeNode root, int value) {
		if (Integer.valueOf(value) == null || root == null)
			return;

		TreeNode current = root;
		while (true) {
			if (value < current.data) {
				if (current.left != null) {
					current = current.left;
				} else {
					current.setLeftChild(new TreeNode(value));
					break;
				}
			} else if (value > current.data) {
				if (current.right != null) {
					current = current.right;
				} else {
					current.setRightChild(new TreeNode(value));
					break;
				}
			}
		}
	}

	public static TreeNode findMin(TreeNode root) {
		TreeNode current = root;
		while (current.left != null)
			current = current.left;
		return current;
	}

	public static TreeNode findMax(TreeNode root) {
		TreeNode current = root;
		while (current.right != null)
			current = current.right;
		return current;
	}

	public static TreeNode Delete(TreeNode root, int data) {
		if (root == null) {
			return root;
		} else if (data < root.data) {
			root.setLeftChild(Delete(root.left, data));
		} else if (data > root.data) {
			root.setRightChild(Delete(root.right, data));
		} else {
			// We found the number we want to delete
			// case 1: no child
			if (root.left == null && root.right == null) {
				root = null;
			}
			// case 2: one child
			// node with right child
			else if (root.left == null) {
				root = root.right;
				root.setRightChild(null);
			}
			// node with left child
			else if (root.right == null) {
				root = root.left;
				root.setLeftChild(null);
			}
			// case 3: with two child
			else {
				TreeNode temp = searchMin(root.right);
				root.data = temp.data;
				root.setRightChild(Delete(root.right, temp.data));
			}
		}
		return root;
	}

	public static TreeNode searchMin(TreeNode root) {

		if (root == null) {
			return root;
		} else if (root.left == null) {
			return root;
		} else {
			return searchMin(root.left);
		}
	}

	public static TreeNode searchMax(TreeNode root) {
		if (root == null) {
			return root;
		} else if (root.right == null) {
			return root;
		} else {
			return searchMax(root.right);
		}
	}

	public static TreeNode searchNode(TreeNode root, int value) {
		Stack<TreeNode> s1 = new Stack<>();
		s1.push(root);

		TreeNode tmp = root;
		TreeNode peekNode = null;
		while (tmp.left != null && tmp.right != null) {
			peekNode = s1.peek();
			tmp = peekNode;

			if (peekNode.right != null) {
				s1.push(peekNode.right);
			}

			if (peekNode.left != null) {
				s1.push(peekNode.left);
			}
		}

		while (s1.size() > 0) {
			TreeNode n = s1.pop();
			if ((n.left != null && n.right != null) && ((n.left != tmp && n.right != tmp) && n.parent != null)) {
				s1.push(n);
				s1.push(n.right);
				s1.push(n.left);
			} else {
				if (Integer.valueOf(value) != null) {
					if (n.data == value)
						return n;
				}
				tmp = n;
			}
		}
		return null;
	}

	public static TreeNode search(TreeNode n, int key) {
		while (n != null) {
			if (key == n.data)
				return n;
			if (key < n.data)
				n = n.left;
			else
				n = n.right;
		}
		return null;
	}

	/**
	 * 루트 삭제시 정상 처리 안됨
	 * @param root
	 * @param childValue
	 * @return
	 */
	@Deprecated
	public static boolean Delete2(TreeNode root, int childValue) {
		TreeNode parent = root;
		TreeNode current = root;
		boolean isLeftChild = false;
		// 삭제할 자식이 루트의 왼쪽/오른쪽 어디에 있는지 확인
		while (current.data != childValue) {
			parent = current;
			if (current.data > childValue) {
				isLeftChild = true;
				current = current.left;
			} else {
				isLeftChild = false;
				current = current.right;
			}
			if (current == null)
				return false;
		}
		// Case 1: 자식노드가 없는 경우
		if (current.left == null && current.right == null) {
			if (current == root)
				root = null;
			if (isLeftChild == true) {
				parent.setLeftChild(null);
			} else {
				parent.setRightChild(null);
			}
		}
		// Case 2 : 하나의 자식을 갖는 경우
		else if (current.right == null) {
			if (current == root) {
				root = current.left;
			} else if (isLeftChild) {
				parent.setLeftChild(current.left);
			} else {
				parent.setRightChild(current.left);
			}
		} else if (current.left == null) {
			if (current == root) {
				root = current.right;
			} else if (isLeftChild) {
				parent.setLeftChild(current.right);
			} else {
				parent.setRightChild(current.right);
			}
		}
		// Case 3 : 두개의 자식을 갖는 경우
		else if (current.left != null && current.right != null) {
			TreeNode successor = getSuccessor(current); // 오른쪽 최소값을 찾음
			if (current == root) {
				root = successor;
			} else if (isLeftChild) {
				parent.setLeftChild(successor);
			} else {
				parent.setRightChild(successor);
			}
			successor.setLeftChild(current.left);
		}
		return true;
	}

	private static TreeNode getSuccessor(TreeNode current) {
		if (current.right != null) {
			TreeNode minimum = current.right;
			while (minimum.left != null) {
				minimum = minimum.left;
			}
			return minimum;
		} else {
			TreeNode y = current.parent;
			while (y != null && current == y.right) {
				current = y;
				y = y.parent;
			}
			return y;
		}
	}		
}
