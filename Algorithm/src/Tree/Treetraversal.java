package Tree;

import java.util.ArrayList;
import java.util.LinkedList;

public class Treetraversal {
	public static void main(String[] args) {
		Node root = new Node(100);
		Node n1 = root.addNewNode("L", 50);
		Node n2 = root.addNewNode("R", 150);
		Node n3 = n1.addNewNode("L", 25);
		Node n4 = n1.addNewNode("R", 75);
		Node n5 = n2.addNewNode("L", 125);
		Node n6 = n2.addNewNode("R", 175);
		Node n7 = n5.addNewNode("L", 110);

		System.out.println("Pre");
		getPredOrder(root);
		System.out.println("");
		System.out.println("");
		System.out.println("In");
		getInOrder(root);
		System.out.println("");
		System.out.println("");
		System.out.println("Post");
		getPostOrder(root);

		System.out.println("===================================================");
		getPredOrderNoRecursion(root);

	}

	// 전위 = 루트 처리 - 왼쪽 처리 - 오른쪽 처리
	public static void getPredOrder(Node node) {
		if (node == null)
			return;
		System.out.println(node.getvalue());
		getPredOrder(node.getLeft());
		getPredOrder(node.getRight());
	}

	// 중위 = 왼쪽 처리 - 루트 처리 - 오른쪽 처리
	public static void getInOrder(Node node) {
		if (node == null)
			return;
		getInOrder(node.getLeft());
		System.out.println(node.getvalue());
		getInOrder(node.getRight());
	}

	// 후위 = 왼쪽 처리 - 오른쪽 처리 - 루트 처리
	public static void getPostOrder(Node node) {
		if (node == null)
			return;
		getPostOrder(node.getLeft());
		getPostOrder(node.getRight());
		System.out.println(node.getvalue());
	}

	public static void getPredOrderNoRecursion(Node node) {
		if (node == null)
			return;

		LinkedList<Node> stack = new LinkedList<>();
		System.out.println(node.getvalue());
		stack.push(node.getRight());
		stack.push(node.getLeft());

		while (stack.size() > 0) {
			Node curr = stack.pop();
			System.out.println(curr.getvalue());
			
			//스택 = Last In First Out, 왼쪽이 먼저 출력되어야 하므로 전위 순회의 경우 오른쪽을 먼저 넣고 왼쪽을 나중에 넣으면 마지막에 넣은 왼쪽부터 출력
			Node n = curr.getRight();
			if (n != null)
				stack.push(n);
			n = curr.getLeft();
			if (n != null)
				stack.push(n);

		}
	}

}
