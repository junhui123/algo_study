package Tree;

import Tree.Node;

public class treeHeight {

	public static void main(String[] args) {

		Node root = new Node(5);
		Node n1 = root.addNewNode("L", 3);
		Node n2 = root.addNewNode("R", 10);
		Node n3 = n1.addNewNode("L", 1);
		Node n4 = n1.addNewNode("R", 4);
		Node n5 = n2.addNewNode("L", 7);
		Node n6 = n2.addNewNode("R", 12);
		Node n7 = n5.addNewNode("L", 6);
		Node n8 = n5.addNewNode("R", 8);
		Node n9 = n8.addNewNode("L", 99); 
		
		System.out.println(getTreeHeight(root, 1));
//		System.out.println(getTreeHeight(root));

	}

	public static int getTreeHeight(Node node, int height) {
		if (node == null) {
			return 0;
		}

		if (node.getLeft() != null || node.getRight() != null) {
			height++;
			//Math.max = 두 요소중 가장 큰 값 찾는 함수
			//왼쪽과 오른쪽 높이가 다를 수 있으므로...
			return Math.max(  getTreeHeight(node.getLeft(), height),  getTreeHeight(node.getRight(), height)  );
		} else {
			return height;
		}
	}
	
	public static void getTreeH(Node node) {
		
		
//		1. 왼쪽부터 찾음 + 숫자 ++, 방문한 노드를 리스트에 저장
//		2. 자식이 없으면 리스트의 저장된 마지막 노드로 복귀 
//		3. 2에서 복귀한 노드 부터 오른쪽 순회 
//		4. 자식이 있으면 숫자 ++
//		5. 1~4 반복 
		
	}
	

	/**
	 * //right 시작 시 left의 마지막 노드에서 시작
	 * static int lheight = 1; static int rheight =1;
	 * static Node root = null; 
	 * public static int getTreeHeight(Node node) {
	 * if(root == null) { root = node; }
	 * 
	 * if (node == null) { return 0; }
	 * 
	 * if (node.getLeft() != null) { return lheight =
	 * lheight+getTreeHeight(node.getLeft()); }
	 * 
	 * if(node.getRight() != null) { return rheight =
	 * rheight+getTreeHeight(node.getRight()); }
	 * 
	 * if (lheight > rheight) return lheight; else return rheight; }
	 */
}
