package Tree;

public class unbalancedTree {
	public static void main(String[] args) {
		Node root = new Node(6);
		Node n1 = root.addNewNode("L", 4);
		Node n2 = root.addNewNode("R", 7);
		Node n3 = n1.addNewNode("L", 2);
		Node n4 = n1.addNewNode("R", 5);
		Node n5 = n3.addNewNode("L", 1);
		Node n6 = n3.addNewNode("R", 3);
		
		//균형 잡힌 트리는 노드 절반은 오른쪽 절반은 왼쪽에 위치
		//예제의 경우 1~7 사이의 불균형 트리를 균형으로 해야 되므로 루트값을 중앙값 4로 잡고 진행하도록 해야 한다.
		root=changeBalanced(root);
	}

	
	//균형 잡힌 트리는 노드 절반은 오른쪽 절반은 왼쪽에 위치 
	//루트보다 큰 값은 오른쪽 작은값은 왼쪽
	public static Node changeBalanced(Node oldRoot) {
		Node newRoot = oldRoot.getLeft();  //4
		oldRoot.setLeft(newRoot.getRight()); //6 
		newRoot.setRight(oldRoot);
		newRoot.setParent(null);
		return newRoot;
	}
}
