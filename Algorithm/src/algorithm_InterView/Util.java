package algorithm_InterView;

import java.util.ArrayList;
import java.util.List;
import java.util.LongSummaryStatistics;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;
import java.util.function.Function;

public class Util {
	public static int count = 500;
	
	public static void print(int[][] arr) {
		
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<arr[0].length; j++) {
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}
	
	public static <T, R> void test(Function<T, R> f, T t) {
		List<Long> durationList_nsec = new ArrayList<>();
		List<Long> durationList_sec = new ArrayList<>();
		
		Long lastNanoSec = 0L;
		Long lastSec = 0L;
		for(int i=0; i<=count; i++) {
			long start = System.nanoTime();
			f.apply(t);
			long end = System.nanoTime();
			long elapsed = end-start;
			long duration = TimeUnit.SECONDS.convert(elapsed, TimeUnit.NANOSECONDS);
			lastNanoSec = elapsed;
			lastSec = duration;
			durationList_nsec.add(elapsed);
			durationList_sec.add(duration);
		}
		
		LongSummaryStatistics ls = durationList_nsec.stream().mapToLong(x->x).summaryStatistics();
		double averageNano = ls.getAverage();
		System.out.println("Average Nsec = " + averageNano+", last Nsec = "+lastNanoSec);
		LongSummaryStatistics is = durationList_sec.stream().mapToLong(x->x).summaryStatistics();
		double averageSec = is.getAverage();
		System.out.println("Average Sec = " + averageSec+", last Sec = "+lastSec);
				
	}
	
	public static <T, U, R> void test(BiFunction<T, U, R> f, T t1, U t2) {
		int count = 500;
		
		List<Long> durationList_nsec = new ArrayList<>();
		List<Long> durationList_sec = new ArrayList<>();
		
		Long lastNanoSec = 0L;
		Long lastSec = 0L;
		for(int i=0; i<=count; i++) {
			long start = System.nanoTime();
			f.apply(t1, t2);
			long end = System.nanoTime();
			long elapsed = end-start;
			long duration = TimeUnit.SECONDS.convert(elapsed, TimeUnit.NANOSECONDS);
			lastNanoSec = elapsed;
			lastSec = duration;
			durationList_nsec.add(elapsed);
			durationList_sec.add(duration);
		}
		
		LongSummaryStatistics ls = durationList_nsec.stream().mapToLong(x->x).summaryStatistics();
		double averageNano = ls.getAverage();
		System.out.println("Average Nsec = " + averageNano+", last Nsec = "+lastNanoSec);
		LongSummaryStatistics is = durationList_sec.stream().mapToLong(x->x).summaryStatistics();
		double averageSec = is.getAverage();
		System.out.println("Average Sec = " + averageSec+", last Sec = "+lastSec);
		
	}
}
