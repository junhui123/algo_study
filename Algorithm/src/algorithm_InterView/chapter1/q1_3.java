package algorithm_InterView.chapter1;

import java.util.Arrays;

/**
 * 문자열 2개를 받아 하나가 다른 하나의 순열여부 판단
 * 대소문자, 공백 중요, god_ != dog
 * 
 *  1. 정렬 후 비교
 *  2. 문자열 출현 횟수 비교 
 *
 */
public class q1_3 {

	public static void main(String[] args) {
		String s1 = "god ";
		String s2 = "dog";
		String s3 = "god";
		
		System.out.println(permuntation(s1, s2));
		System.out.println(permuntation(s3, s2));
		
	}
	
	public static boolean permuntation(String s1, String s2) {
		s1 = sort(s1);
		s2 = sort(s2);
		
		if(s1.equals(s2)) {
			return true;
		}
		
		return false;
	}
	
	public static String sort(String s) {
		char[] content = s.toCharArray();
		Arrays.sort(content);
		return String.valueOf(content);
	}
}
