package algorithm_InterView.chapter1;


/**
 * 
 * NxN 행렬 90도 회전
 *  
 *   1. 가장자리 배열을 복사 후 옮기는 방법 O(N) 공간 필요
 *   2. 인덱스별로 교체
 *	for(i=0 to N)
 *		tmp = top[i]
 *		top[i] = left[i]
 *		left[i] = bottom[i]
 *		bottom[i] = right[i]
 *		right[i] = tmp   	
 */

public class q1_6 {
	public static void main(String[] args) {

		int n = 4;
		int[][] arr = { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 }, { 13, 14, 15, 16 } };
		
//		int n = 5;
//		int[][] arr = { { 1, 2, 3, 4, 5 }, { 6, 7, 8, 9, 10 }, { 11, 12, 13, 14, 15 }, { 16, 17, 18, 19, 20 }, { 21, 22, 23, 24, 25 } };

		rotate(arr, n);
	}

	public static void rotate(int[][] arr, int n) {
		for (int layer = 0; layer < n / 2; ++layer) {
			int first = layer;
			int last = n-1-layer;
			
			print(arr);
			System.out.println();
			
			for (int i = first; i < last; ++i) {
				int offset = i - first;
				System.out.println("layer="+layer+", first="+first+", last="+last+", offset="+offset);
				
				int top = arr[first][i];
				
				arr[first][i] = arr[last-offset][first];
				
				arr[last-offset][first] = arr[last][last-offset];
				
				arr[last][last-offset] = arr[i][last];
				
				arr[i][last] = top;
				
				print(arr);
				System.out.println();
			}
		}
	}
	
	public static void print(int[][] arr) {
		
		for(int i=0; i<arr.length; i++) {
			for(int j=0; j<arr[0].length; j++) {
				System.out.print(arr[i][j]+" ");
			}
			System.out.println();
		}
	}
}
