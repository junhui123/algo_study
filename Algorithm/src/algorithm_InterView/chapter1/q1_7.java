package algorithm_InterView.chapter1;

import java.util.ArrayList;
import java.util.List;

import algorithm_InterView.Util;

/**
 *  MN 행렬에서 0인 원소가 속한  행, 열의 값을 0으로 설정
 */
public class q1_7 {
	public static void main(String[] args) {
		
//		int m = 4, n = 3;
//		int[][] arr = new int[][] { { 1, 2, 3 }, { 4, 0, 6 }, { 7, 8, 9 }, { 10, 11, 12 } };
//
//		int m = 3, n = 4;
//		int[][] arr = new int[][] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 0, 11 } };

		int m = 4, n = 4;
		int[][] arr = new int[][] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 0, 11 }, { 12, 13, 14, 15 } };

		setZero(arr, m, n);
		Util.print(arr);
	}

	public static void setZero(int[][] arr, int m, int n) {
		List<String> list = new ArrayList<>();
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[0].length; j++) {
				if (arr[i][j] == 0) {
					findZeroRowCol(list, i, j, m, n);
				}
			}
		}

		for (String s : list) {
			String[] point = s.split(",");
			arr[Integer.parseInt(point[0])][Integer.parseInt(point[1])] = 0;
		}
	}

	public static void findZeroRowCol(List<String> list, int i, int j, int m, int n) {
		for (int z = 0; z < n; z++) {
			list.add(i + "," + z);
		}

		for (int z = 0; z < m; z++) {
			list.add(z + "," + j);
		}
	}
	
	
}
