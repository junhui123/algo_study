package algorithm_InterView.chapter1;

public class q1_8 {
	public static void main(String[] args) {
		String s1 = "waterbottle";
		String s2 = "erbottlewat";
//		System.out.println(isSubstring(s1, s2));
		
		System.out.println(isRotationEqualString(s1, s2));
	}
	
	// 실패
	// s1 문자열을 순환하여 회전 시켰을때 s2와 같은지 확인
	// waterbottle -> aterbottlew ->
	// terbottlewa -> erbottlewat 
	public static boolean isSubstring(String s1, String s2) {
		int s1Length = s1.length();
		for(int c=0; c<s1Length; c++) {
			char[] tmp = new char[c+1];
			for(int b=0; b<=c; b++) {
				char t = s1.charAt(b);
				tmp[b] = t;
			}
			int last = 0;
			int next = c;
			char[] s1Chars = s1.toCharArray();
			for(int i=c+1; i<s1Length; i++) {
				 s1Chars[i] = s1Chars[i-c+1];
				if(i==s1Length-1) 
					last = i-next;
			}
			
			int j=0;
			for(int i=last; i<s1Length; i++) {
				s1Chars[i] = tmp[j];
				j++;
			}
			
			if(String.valueOf(s1Chars).equals(s2)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isRotationEqualString(String s1, String s2) {
		if(s1.length() == s2.length() && s1.length() > 0) {
			String s1s1 = s1+s1;
			if(s1s1.contains(s2)) {
				return true;
			}
		}
		return false;
	}
}
