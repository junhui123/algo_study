package algorithm_InterView.chapter2;

import java.util.function.BiFunction;

import algorithm_InterView.CtCILibrary.LinkedListNode;

/**
 * x 값을 갖는 노드 기준으로 연결 리스트 분할
 * 보다 작은 값을 갖는 노드가 x와 같거나 더 큰 값을 갖는 노드들 보다 앞에 
 * 
 * small < x <= big, x= 7
 * 0->1->2->3->4->5->6->7->8->8->7->6->5->4->3->2->1->0
 * 0->1->2->3->4->5->6->6->5->4->3->2->1->0->7->8->8->7
 */
public class q2_4 {
	public static void main(String[] args) {
		int x = 5;
		test(q2_4::partition, x);
		System.out.println();
		test(q2_4::partition2, x);
	}
	
	static void test(BiFunction<LinkedListNode, Integer, LinkedListNode> f, int x) {
		/* Create linked list */
		int[] vals = {1, 3, 7, 5, 2, 9, 4};
		LinkedListNode head = new LinkedListNode(vals[0], null, null);
		LinkedListNode current = head;
		for (int i = 1; i < vals.length; i++) {
			current = new LinkedListNode(vals[i], null, current);
		}
		System.out.println(head.printForward());
		
		LinkedListNode h = f.apply(head, x);
		
		System.out.println(h.printForward());		
	}

	public static LinkedListNode partition(LinkedListNode node, int i) {
		LinkedListNode smallStart = null;
		LinkedListNode smallEnd= null;
		LinkedListNode bigStart = null;
		LinkedListNode bigEnd = null;
		
		while(node != null) {
			LinkedListNode next = node.next;
			node.next = null; 
			if(node.data < i) {
				if(smallStart == null) {
					smallStart = node;
					smallEnd = node;
				} else {
					smallEnd.next = node;
					smallEnd = node;
				}
			} else {
				if(bigStart == null ) {
					bigStart = node;
					bigEnd = node;
				} else {
					bigEnd.next = node;
					bigEnd = node;
				}
			}
			node = next;
		}
		
		if(smallStart == null) {
			return bigStart;
		}
		
		smallEnd.next = bigStart;
		return smallStart;
	}
	
	public static LinkedListNode partition2(LinkedListNode node, int i) {
		LinkedListNode small = null;
		LinkedListNode big = null;
		
		while(node != null) {
			LinkedListNode next = node.next;
			if(node.data < i ) {
				node.next = small;
				small = node;
			} else {
				node.next = big;
				big = node;
			}
			node = next;
		}
		
		if(small == null ) {
			return big;
		}
		
		LinkedListNode head = small;
		while(small.next != null) {
			small = small.next;
		}
		small.next = big;
		
		return head;
	}
}
