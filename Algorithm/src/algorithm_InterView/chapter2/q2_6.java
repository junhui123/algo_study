package algorithm_InterView.chapter2;

import algorithm_InterView.CtCILibrary.LinkedListNode;

/**
 * 순환 연결 리스트에서 순환하는 부분의 첫 노드 반환
 */
public class q2_6 {
	public static void main(String[] args) {
		int list_length = 100;
		int k = 10;
		
		// Create linked list
		LinkedListNode[] nodes = new LinkedListNode[list_length];
		for (int i = 0; i < list_length; i++) {
			nodes[i] = new LinkedListNode(i, null, i > 0 ? nodes[i - 1] : null);
		}
		
		// Create loop;
		nodes[list_length - 1].next = nodes[list_length - k];
		
		System.out.println(findLoopFirstNode(nodes[0]).data);
	}
	
	
	public static LinkedListNode findLoopFirstNode(LinkedListNode node) {
		LinkedListNode fast = node;
		LinkedListNode slow = node;
		
		//while (fast != null && fast.next != null) { //순환 연결 리스트가 아닌 경우 
		while (fast != null && slow != null) { //순환 연결 리스트 
			fast = fast.next.next;
			slow = slow.next;
			System.out.println("fast="+fast.data+", slow="+slow.data);
			if(fast.data == slow.data) {
				return fast;
			}
		}
		return node;
	}
	
	public static void print(LinkedListNode node) {
		int i = 200;
		while(i>0) {
			System.out.print(node.data+"->");
			node = node.next;
			i--;
		}
	}
}
