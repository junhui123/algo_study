package algorithm_InterView.chapter2;

import java.util.OptionalInt;
import java.util.Stack;

import algorithm_InterView.Util;
import algorithm_InterView.CtCILibrary.LinkedListNode;

/**
 * 주어진 연결 리스트가 회문(palindrome) 인지 검사
 * 회문 : 0->1->2->3->4->4->3->2->1->0
 */
public class q2_7 {
	public static void main(String[] args) {
		int length = 10;
		LinkedListNode[] nodes = new LinkedListNode[length];
		for (int i = 0; i < length; i++) {
			nodes[i] = new LinkedListNode(i >= length / 2 ? length - i - 1 : i, null, null);
		}
		
		for (int i = 0; i < length; i++) {
			if (i < length - 1) {
				nodes[i].setNext(nodes[i + 1]);
			}
			if (i > 0) {
				nodes[i].setPrevious(nodes[i - 1]);
			}
		}
		// nodes[length - 2].data = 9; // Uncomment to ruin palindrome
		
		LinkedListNode head = nodes[0];
		System.out.println(head.printForward());
		System.out.println(isPalindrome(head));
		System.out.println(isPalindromeBookSolution(head));
		
		LinkedListNode createTestNode1 = createTestNode1();
		LinkedListNode createTestNode2 = createTestNode2();
		createTestNode1.last.next = createTestNode2;
		
		System.out.println(createTestNode1.printForward());

		System.out.println(isPalindrome(createTestNode1));
		System.out.println(isPalindromeBookSolution(createTestNode1));
		
		System.out.println("");
		Util.test(q2_7::isPalindromeBookSolution, head);
		System.out.println("");
		Util.test(q2_7::isPalindrome, head);
		
		System.out.println();
		Util.test(q2_7::isPalindromeBookSolution, createTestNode1);
		System.out.println();
		Util.test(q2_7::isPalindrome, createTestNode1);
		
	}
	
	
	private static LinkedListNode createTestNode1() {
		LinkedListNode custome0 = new LinkedListNode(3, null, null);
		LinkedListNode custome1 = new LinkedListNode(6, null, null);
		LinkedListNode custome2 = new LinkedListNode(9, null, null);
		custome0.next = custome1;
		custome1.next = custome2;
		
		custome1.prev = custome0;
		custome2.prev = custome1;
		
		custome0.last = custome2;
		
		return custome0;
	}
	
	private static LinkedListNode createTestNode2() {
		LinkedListNode custome0 = new LinkedListNode(9, null, null);
		LinkedListNode custome1 = new LinkedListNode(6, null, null);
		LinkedListNode custome2 = new LinkedListNode(3, null, null);
		LinkedListNode custome3 = new LinkedListNode(1, null, null);
		custome0.next = custome1;
		custome1.next = custome2;
		custome2.next = custome3;
		
		custome1.prev = custome0;
		custome2.prev = custome1;
		custome3.prev = custome2;
		
		custome0.last = custome3;
		
		return custome0;
	}
	
	public static LinkedListNode lastNode(LinkedListNode node) {
		while(node.next != null) {
			node = node.next;
		}
		return node;
	}
	
	//실패=> 0-1-2-3-4는 회문 아님. true로 출력
	public static boolean isPalindromeDoublyLinkedList(LinkedListNode node) {
		LinkedListNode fast = node; 
		LinkedListNode slow = node;
		
		while(node != null) {
			LinkedListNode next = node.next;
			fast = node.next.next;
			slow = node.next;
			if(fast.prev.data == slow.data) {
				return true;
			}
			node = next;
		}
		return false;
	}
	
	public static boolean isPalindrome(LinkedListNode node) {
		Stack<LinkedListNode> stack = new Stack<>();
		LinkedListNode compareNode=null;
		while(node.next != null) {
			stack.push(node);
			if(node.data == node.next.data) {
				compareNode = node.next;
				break;
			} 
			node = node.next;
		}
		
		while (compareNode != null) {
			OptionalInt optCompareData = OptionalInt.of(compareNode.data);
			if(stack.isEmpty() && optCompareData.isPresent() ) {
				return false;
			}
			int popdata = stack.pop().data;
				
			if ( optCompareData.getAsInt() != popdata) {
				return false;
			}
			compareNode = compareNode.next;
		}
		
		return true;
	}
	
	//2000nsec 정도 더 빠름 
	public static boolean isPalindromeBookSolution(LinkedListNode head) {
		LinkedListNode fast = head;
		LinkedListNode slow = head;
		
		Stack<Integer> stack = new Stack<Integer>();
		
		while (fast != null && fast.next != null) {
			stack.push(slow.data);
			slow = slow.next;
			fast = fast.next.next;			
		}
		
		/* 홀수 길이 리스트인 경우 가운데 원소 통과 */
		if (fast != null) { 
			slow = slow.next;
		}
		
		while (slow != null) {
			int top = stack.pop().intValue();
			if (top != slow.data) {
				return false;
			}
			slow = slow.next;
		}
		return true;
	}
}
