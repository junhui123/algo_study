package algorithm_InterView.chapter3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

/**
 * SetOfStacks 구현
 * 일정 길이 이상으로 스택이 쌓이면 새로운 스택을 생성하여 사용
 */
public class q3_3 {
	
	private static class SetOfStacks {
		private ArrayList<Stack<Integer>> stacks;
		private int MAX_STACK = 5;
		
		public SetOfStacks(ArrayList<Stack<Integer>> stacks) {
			this.stacks = stacks;
		}

		public void push(int v) {
			if(stacks.isEmpty()) {
				stacks.add(new Stack<Integer>());
			}
			
			Stack<Integer> last = stacks.get(stacks.size()-1);
			if(last.size() == MAX_STACK) {
				Stack<Integer> newStk = new Stack<>();
				newStk.push(v);
				stacks.add(newStk);
			} else {
				last.push(v);
			}
		}
		
		public int pop() {
			if(stacks.isEmpty()) {
				return Integer.MIN_VALUE;
			} 

			int index = stacks.size()-1;
			Stack<Integer> last = stacks.get(index);
			while(last.isEmpty()) {
				stacks.remove(last);
				index--;
				if(!stacks.isEmpty()) 
					last = stacks.get(index);
				else if(stacks.isEmpty())
					return Integer.MIN_VALUE;
			}
			
			return last.pop();
		}
		
		public int popAt(int index) {
			if(index != 0 ) 
				index--;
			
			int m=0, n=index;
			if(MAX_STACK < index ) {
				m = MAX_STACK / index;
				n = MAX_STACK % index;
			}
			
			if(m > stacks.size()) {
				return Integer.MIN_VALUE;
			}
			
			return popAt(stacks.get(m), n);
		}
		
		private int popAt(Stack<Integer> stack, int index) {
			 int result = 0;
			 Stack<Integer> tmpStack = new Stack<>();
			 while(!stack.isEmpty()) {
				 tmpStack.push(stack.pop());
			 }
			 
			int j = 0;
			while (!tmpStack.isEmpty()) {
				if (j == index) {
					result = tmpStack.pop();
				} else {
					stack.push(tmpStack.pop());
				}
				j++;
			}
			
			for(int i=1; i<=stacks.size()-1; i++) {
				Stack<Integer> s1 = stacks.get(i-1);
				Stack<Integer> s2 = stacks.get(i);
				shift(s1, s2);
			}
			
			return result;
		}
		
		private void shift(Stack<Integer> s1, Stack<Integer> s2) {
			int moveCount = 0;
			if(s1.size() < MAX_STACK) {
				moveCount = MAX_STACK-s1.size();
			}
			
			for(int i=0; i<moveCount; i++) {
				s1.push(s2.firstElement());
			}
			
			for(int i=0; i<moveCount; i++) {
				s2.remove(0);
			}
		}
		
		public void Print() {
			if(stacks.isEmpty()) {
				System.out.println("Stacks is Empty");
				return;
			}
			
			for(Stack<Integer> s : stacks) {
				System.out.println(s.toString());
			}
			System.out.println();
		}
	}
	
	public static void main(String[] args) {
		SetOfStacks setOfStacks = new SetOfStacks(new ArrayList<Stack<Integer>>());
		for(int i=1; i<=13; i++) { 
			setOfStacks.push(i);
		}
		setOfStacks.Print();
		
		for(int i=1; i<=5; i++) {
			System.out.print(setOfStacks.pop()+" ");
		}
		setOfStacks.Print();
		
		setOfStacks.push(100);
		setOfStacks.Print();
		
		System.out.println(setOfStacks.popAt(5));
		System.out.println(setOfStacks.popAt(0));
		setOfStacks.Print();
		
	}
	
}
