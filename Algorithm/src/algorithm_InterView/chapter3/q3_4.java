package algorithm_InterView.chapter3;

import java.util.Stack;

/**
 * 하노이 탑
 *  한 번에 원판 하나만 이동 가능.
 *  탑의 맨 꼭대기에 있는 원판은 옆에 있는 탑으로 옮길 수 있다.
 *  원판은 자기보다 지름이 큰 원판 위로만 옮길 수 있다.
 */
public class q3_4 {

	public static Stack<Circle> createCircle(int n) {
		// 이 부분 자바8 스트림 람다로 대체 가능한지 확인
		Stack<Circle> top1 = new Stack<>();
		for (int i = n; i > 0; i--) {
			Circle c = new Circle(i);
			top1.add(c);
		}
		return top1;
	}

	public static void start(int n) {
		Stack<Circle> top1 = createCircle(n);
		System.out.println(top1.toString());
		Stack<Circle> top2 = new Stack<>();
		Stack<Circle> top3 = new Stack<>();

		move(top1, top2, top3);

		System.out.println(top3.toString());
	}

	public static void move(Stack<Circle> top1, Stack<Circle> top2, Stack<Circle> top3) {
		top3.push(top1.pop());

		int circleCount = 1;
		while (!top1.isEmpty()) {
			circleCount = top3.size();

			if (top2.isEmpty()) {
				top2.push(top1.pop());
			}

			while (!top3.isEmpty()) {
				top1.push(top3.pop());
			}

			top3.push(top2.pop());

			while (circleCount != 0) {
				top3.push(top1.pop());
				--circleCount;
			}
		}
	}

	public static void main(String[] args) {
		start(4);
	}
}

class Circle {
	int r;

	public Circle(int r) {
		this.r = r;
	}

	@Override
	public String toString() {
		return "Circle r=" + r;
	}
}
