package algorithm_InterView.chapter3;

import java.util.Stack;

/**
 * 2개의 스택으로 queue 구현 
 */
public class q3_5_Earger{
	public static void main(String[] args) {
		MyQueue<Something> q = new MyQueue<>();
		q.enqueue(new Something(4));
		q.enqueue(new Something(1));
		q.enqueue(new Something(2));
		q.enqueue(new Something(3));
		
		System.out.println(q.dequeue()+","+q.size());
		System.out.println(q.dequeue()+","+q.size());
		System.out.println(q.dequeue()+","+q.size());
		System.out.println(q.dequeue()+","+q.size());
	}
	
	private static class Something{
		int a;
		public Something(int a) {
			this.a = a;
		}
		@Override
		public String toString() {
			return String.valueOf(a);
		}
	}

	//Queue = First in First Out
	private static class MyQueue<T> {
		private Stack<T> ENQ = new Stack<>();  
		private Stack<T> DEQ = new Stack<>();  
		
		public void enqueue(T t) {
			ENQ.push(t);
		}
		
		//dequeue()가 여러변 호출되면 성능이 떨어짐
		public T dequeue() {
			while(!ENQ.isEmpty()) {
				DEQ.push(ENQ.pop());
			}
			T t = DEQ.pop();
			
			while(!DEQ.isEmpty()) {
				ENQ.push(DEQ.pop());
			}
			
			return t;
		}
		
		public int size() {
			return ENQ.size();
		}
		
	}
}


