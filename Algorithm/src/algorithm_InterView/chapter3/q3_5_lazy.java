package algorithm_InterView.chapter3;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * 2개의 스택으로 queue 구현 
 */
public class q3_5_lazy{
	public static void main(String[] args) {
		Queue<Something> q = new LinkedList<>();
		q.offer(new Something(1));
		q.offer(new Something(2));
		q.offer(new Something(3));
		
		System.out.println(q.poll());
		System.out.println(q.poll());
		q.offer(new Something(6));
		System.out.println(q.poll());
		q.offer(new Something(9));
		System.out.println(q.poll());
		System.out.println(q.poll());
		q.offer(new Something(10));
		q.offer(new Something(11));
		q.offer(new Something(12));
		System.out.println(q.toString());
		
		System.out.println("====================");
		
		MyQueue<Something> mq = new MyQueue<>();
		mq.enqueue(new Something(1));
		mq.enqueue(new Something(2));
		mq.enqueue(new Something(3));
		
		System.out.println(mq.dequeue());
		System.out.println(mq.dequeue());
		mq.enqueue(new Something(6));
		System.out.println(mq.dequeue());
		mq.enqueue(new Something(9));
		System.out.println(mq.dequeue());
		System.out.println(mq.dequeue());
		mq.enqueue(new Something(10));
		mq.enqueue(new Something(11));
		mq.enqueue(new Something(12));
		System.out.println(mq.toString());
	}
}

class Something{
	int a;
	public Something(int a) {
		this.a = a;
	}
	@Override
	public String toString() {
		return String.valueOf(a);
	}
}

//Queue = First in First Out
class MyQueue<T> {
	private Stack<T> ENQ = new Stack<>();  
	private Stack<T> DEQ = new Stack<>();  

	private boolean isDEQEmpty() {
		return DEQ.isEmpty();
	}
	
	public void enqueue(T t) {
		ENQ.push(t);
	}
	
	//lazy evaluation 적용. DEQ가 비어있을때만 ENQ의 내용을 옮기도록
	public T dequeue() {
		if(isDEQEmpty()==true) {
			while(!ENQ.isEmpty()) {
				DEQ.push(ENQ.pop());
			}
		}
		T t = DEQ.pop();
		return t;
	}
	
	public int size() {
		return ENQ.size()+DEQ.size();
	}

	@Override
	public String toString() {
		if(ENQ.isEmpty()) {
			return DEQ.toString();
		} 
		
		if(DEQ.isEmpty()) {
			return ENQ.toString();
		}
		
		String enq = ENQ.toString();
		String deq = DEQ.toString();
		return enq+" "+deq;
	}
}
