package algorithm_InterView.chapter4;

import algorithm_InterView.CtCILibrary.AssortedMethods;
import algorithm_InterView.CtCILibrary.TreeNode;

/**
 * 주어진 이진 트리 균형 이진 트리 인지 판별
 */
public class q4_1 {
	public static void main(String[] args) {
		TreeNode tree1 = AssortedMethods.randomBST(5, 1, 20);
		tree1.print();
		System.out.println(BalancedTree.isBalancedBinaryTree(tree1));
		
		TreeNode tree2 = AssortedMethods.randomBST(5, 1, 20);
		tree2.right = null;
		tree2.print();
		System.out.println(BalancedTree.isBalancedBinaryTree(tree2));
		
		TreeNode tree3 = AssortedMethods.randomBST(5, 1, 20);
		tree3.left = null;
		tree3.print();
		System.out.println(BalancedTree.isBalancedBinaryTree(tree3));
		
		TreeNode tree4 = new TreeNode(1);
		TreeNode tree4_2 = new TreeNode(2);
		TreeNode tree4_3 = new TreeNode(3);
		TreeNode tree4_4 = new TreeNode(4);
		TreeNode tree4_5 = new TreeNode(5);
		TreeNode tree4_6 = new TreeNode(6);
		TreeNode tree4_7 = new TreeNode(7);
		tree4.left = tree4_2;
		tree4.right = tree4_3;
		tree4_2.left = tree4_4;
		tree4_2.right = tree4_5;
		tree4_3.left = tree4_6;
		tree4_3.right = tree4_7;
		tree4.print();
		System.out.println(BalancedTree.isBalancedBinaryTree(tree4));
	}
}

class BalancedTree {
	
	private static int leftCount = 0;
	private static int rightCount = 0;
	public static boolean isBalancedBinaryTree(TreeNode root) {
		leftCount = 0;
		rightCount = 0;
		
		shift(root, "left");
		shift(root, "right");
		
		if(leftCount == rightCount) {
			return true;
		} else {
			return false;
		}
	}
	
	private static void shift(TreeNode root, String leftOrRight) {
		if(root == null) {
			return;
		}

		if(leftOrRight.equals("left")) {
			leftCount++;
			shift(root.left, leftOrRight);
		} else {
			rightCount++;
			shift(root.right, leftOrRight);
		}
	}
}
