package algorithm_InterView.chapter4;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import algorithm_InterView.CtCILibrary.Graph;
import algorithm_InterView.CtCILibrary.Node;

public class q4_2 {
	public static void main(String[] args) {
		Graph g = createNewGraph();
		Node[] n = g.getNodes();
		Node start = n[3];
		Node end = n[5];
		System.out.println(isRoute_DFS_Recursion(g, start, end));
		System.out.println(isRoute_DFS(g, start, end));
		System.out.println(isRoute_BFS(g, start, end));
	}
	
	/**
	 * DFS
	 * 스택에 정점을 넣음 
	 * 정점을 pop 하여 인접한 정점을 스택에 넣음
	 * 다시 pop하여 인접한 정점을 스택에 넣음
	 * 모든 노드에 대하여 반복
	 */
	public static boolean isRoute_DFS(Graph g, Node start, Node end) {
		Stack<Node> cycleCheck = new Stack<>();
		Stack<Node> dfs = new Stack<>();
		dfs.push(start);
		cycleCheck.push(start);
		
		while(!dfs.isEmpty()) {
			Node top = dfs.pop();
			Node[] adjacent = top.getAdjacent();
			for(Node n : adjacent) {
				if(n==end) {
					return true;
				}
				dfs.push(n);
				cycleCheck.push(n);
			}
		}
		return false;
	}
	
	
	public static boolean isRoute_DFS_Recursion(Graph g, Node start, Node end) {
		Node[] visited = new Node[g.count];
		DFS_Recursion(start, visited, 0);
		for(Node n : visited) {
			if(n==end) {
				return true;
			}
		}
		return false;
	}
	
	public static void DFS_Recursion(Node start, Node[] visited, int visitedCount) {
		visited[visitedCount] = start;
		visitedCount++;
		if(visitedCount == visited.length) {
			return ;
		}
		
		for(Node n : start.getAdjacent()) {
			DFS_Recursion(n, visited, visitedCount);
		}
	}
	
	
	
	/**
	 * BFS
	 * 큐에 시작점 넣고
	 * 큐에서 하나 꺼내서 
	 * 방문하지 않은 경우에 인접한 정점을 큐에 추가
	 * 반복
	 */
	public static boolean isRoute_BFS(Graph g, Node start, Node end) {
		Node[] visited = new Node[g.count];
		Queue<Node> q = new LinkedList<>();
		q.offer(start);
		visited[0] = start;
		
		int visitedCnt = 0;
		while (!q.isEmpty()) {
			Node poll = q.poll();
			Node[] adjacent = poll.getAdjacent();
			for (Node n : adjacent) {
				if(visited[visitedCnt] == n) {
					continue;
				}
				q.offer(n);
				visitedCnt++;
				visited[visitedCnt] = n;
			}
		}
		
		for(Node n : visited) {
			if(n != null && n==end) {
				return true;
			}
		}
		
		return false;
	}	
	
	public static Graph createNewGraph() {
		Graph g = new Graph();
		Node[] temp = new Node[6];

		temp[0] = new Node("a", 3);
		temp[1] = new Node("b", 0);
		temp[2] = new Node("c", 0);
		temp[3] = new Node("d", 1);
		temp[4] = new Node("e", 1);
		temp[5] = new Node("f", 0);

		temp[0].addAdjacent(temp[1]);
		temp[0].addAdjacent(temp[2]);
		temp[0].addAdjacent(temp[3]);
		temp[3].addAdjacent(temp[4]);
		temp[4].addAdjacent(temp[5]);
		for (int i = 0; i < 6; i++) {
			g.addNode(temp[i]);
		}
		return g;
	}
}
