package algorithm_InterView.chapter4;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.stream.Collectors;

import algorithm_InterView.Util;
import algorithm_InterView.CtCILibrary.TreeNode;

public class q4_3 {
	private static ExecutorService execService = Executors.newCachedThreadPool();

	public static void main(String[] args)  {
		int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		
		TreeNode root = createBST(array, 0, array.length-1);
		System.out.println(isBST_inOrder(root, array));
		System.out.println(isBallanced(root));
		System.out.println(isBallanced_SingleThread(root));
		
		BiFunction<TreeNode, int[], Boolean> f1  = q4_3::isBST_inOrder;
		Function<TreeNode, Boolean> f2  = q4_3::isBallanced_SingleThread;
		Function<TreeNode, Boolean> f3  = q4_3::isBallanced;
		
		Util.count = 500;
		Util.test(f1, root, array);
		Util.test(f2, root);
		Util.test(f3, root);
		
		execService.shutdown();
	}
	
	public static TreeNode createBST(int[] array, int start, int end) {
		if(start>end)
			return null;
		
		int midIdx = (start+end) / 2;
		TreeNode root = new TreeNode(array[midIdx]);
		root.left = createBST(array, start, midIdx-1); 
		root.right = createBST(array, midIdx+1, end);
		return root;
	}
	
	public static boolean isBST_inOrder(TreeNode root, int[] array) {
		ArrayList<TreeNode> inOrder = inOrder(root, new ArrayList<TreeNode>());
		List<TreeNode> arrayToList = Arrays.stream(array).mapToObj(i->new TreeNode(i)).collect(Collectors.toList());
		if(inOrder.size() == arrayToList.size()) {
			return true;
		}
		return false;
	}
	
	private static void printList(List<TreeNode> l) {
		for(TreeNode n : l) {
			System.out.println(n.data+" ");
		}
		System.out.println();
	}
	
	private static ArrayList<TreeNode> inOrder(TreeNode root, ArrayList<TreeNode> inOrderList) {
		if(root == null) {
			return inOrderList;
		}
		
		inOrder(root.left, inOrderList);
		inOrderList.add(root);
		inOrder(root.right, inOrderList);
		return inOrderList;
	}
	
	public static boolean isBallanced(TreeNode root)  {
		boolean isBallance = false;
		Callable<Integer> c1 = () -> shift(root.left, 0);
		Callable<Integer> c2 = () -> shift(root.right, 0);

		Future<Integer> future1 = execService.submit(c1);
		Future<Integer> future2 = execService.submit(c2);
		
		try {
			int leftLevel = future1.get();
			int rightLevel = future2.get();
			if (leftLevel == rightLevel) {
				isBallance=true;
			}
		} catch (InterruptedException | ExecutionException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return isBallance;
	}
	
	public static boolean isBallanced_SingleThread(TreeNode root) {
		int leftLevel = shift(root.left, 0);
		int rightLevel = shift(root.right, 0);
		if(leftLevel == rightLevel) {
			return true;
		}
		return false;
	}
	
	private static int shift(TreeNode root, int level) {
		if(root == null)
			return level;
		
		return shift(root.left, ++level);
	}
}
