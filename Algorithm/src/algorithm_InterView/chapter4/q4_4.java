package algorithm_InterView.chapter4;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;

import algorithm_InterView.CtCILibrary.AssortedMethods;
import algorithm_InterView.CtCILibrary.TreeNode;

public class q4_4 {
	public static void main(String[] args) {
		int[] nodes_flattened = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		TreeNode root = AssortedMethods.createTreeFromArray(nodes_flattened);
		ArrayList<Queue<TreeNode>> list = treeLevelNodeList(root);
		
		for(Queue<TreeNode> q : list) {
			System.out.println(q.toString());
		}
		
	}
	
	public static ArrayList<Queue<TreeNode>> treeLevelNodeList(TreeNode root) {
		//ArrayList<? extends LinkedList<TreeNode>> list = new ArrayList<>();
		ArrayList<Queue<TreeNode>> list = new ArrayList<>();
		Queue<TreeNode> q = new LinkedList<>();
		q.add(root);
		
		while(!q.isEmpty()) {
			list.add(q);
			Queue<TreeNode> parent = q;
			 q = new LinkedList<>();
			for(TreeNode n : parent) {
				if( n.left != null) {
					q.offer(n.left);
				}
				if( n.right != null) {
					q.offer(n.right);
				}
			}
		}
		return list;
	}
	
	private static void addMap(HashMap<Integer, ArrayList<TreeNode>> map, int level, TreeNode t) {
		ArrayList<TreeNode> l=null;
		if(map.containsKey(level)) {
			l = map.get(level);
		} else {
			l = new ArrayList<>();
			map.put(level, l);
		}
		l.add(t);
	}
}
