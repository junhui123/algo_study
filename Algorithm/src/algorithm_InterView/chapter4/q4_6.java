package algorithm_InterView.chapter4;

import algorithm_InterView.CtCILibrary.TreeNode;

public class q4_6 {
	public static void main(String[] args) {
		int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		TreeNode root = TreeNode.createMinimalBST(array);
		
		TreeNode node = root.find(array[6]);
		System.out.println(predecessor(node).data+" -> "+node.data+" -> "+successor(node).data);
		TreeNode n1 = root.find(array[6]);
		TreeNode n2 = root.find(array[9]);
		System.out.println(getCommonAncestor(root, n1, n2).data);
	}
	
	public static TreeNode successor(TreeNode n) {
		if(n.right != null) {
			TreeNode m = getMinimum(n.right);
			return  m;
		}
		TreeNode y = n.parent;
		while (y != null && n == y.right) {
			n = y;
			y = y.parent;
		}
		return y;
	}
	
	private static TreeNode getMinimum(TreeNode n ) {
		if(n.left == null) {
			return n;
		}
		return getMinimum(n.left);
	}
	
	
	public static TreeNode predecessor(TreeNode n) {
		if(n.left != null) {
			TreeNode m = getMaximum(n.left);
			return  m;
		}
		
		TreeNode y = n.parent;
		while(y!=null && n == y.left) {
			n = y;
			y = y.parent;
		}
		return y;
	}

	private static TreeNode getMaximum(TreeNode n) {
		if(n.right == null) {
			return n;
		}
		return getMaximum(n.right);
	}
	
	public static TreeNode getCommonAncestor(TreeNode root, TreeNode n1, TreeNode n2) {
		TreeNode node = root;
		while(node!=null) {
			if(n1.data < node.data && n2.data < node.data) {
				node = node.left;
			} else if(n1.data > root.data && n2.data > node.data) {
				node = node.right;
			} 
		}
		return node;
	}
}
