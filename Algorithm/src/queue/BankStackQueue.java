package queue;

import java.util.Arrays;
import java.util.Scanner;

//배열로 큐 작성
public class BankStackQueue {
	private static int SIZE = 8;
	private static int[] BANK_QUEUE = new int[SIZE];
	private static int enqueue_idx = 0;
	
	public static void main(String[] args) {
		Arrays.fill(BANK_QUEUE, -1);
		Scanner scan = new Scanner(System.in);
		while(true) {
			int inNum = scan.nextInt();
			if(inNum == 0 ) {
				//배열의 0번째 값 반환
				System.out.println("["+dequeue()+"]");
			} else if(inNum == -1) {
				System.out.println("Bye");
				scan.close();
				break;
			} else {
				enqueue(inNum);
			}
		}
	}
	
	public static boolean enqueue(int n) {
		if(enqueue_idx < SIZE) {
			BANK_QUEUE[enqueue_idx] = n;
			enqueue_idx++;
			System.out.println(Arrays.toString(BANK_QUEUE));
			return true;
		} else {
			System.out.println("Queue is Full");
			return false;
		}
	}
	
	public static int dequeue() {
		int firstValue = BANK_QUEUE[0];
		
		if(firstValue == -1) {
			System.out.println("Queue is Empty");
			return -1;
		}
		
		for(int i=1; i<SIZE-1; i++) {
			BANK_QUEUE[i-1] = BANK_QUEUE[i]; 
		}
		enqueue_idx--;
		
		return firstValue;
	}
}
