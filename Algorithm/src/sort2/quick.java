package sort2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class quick {
	public static void main(String[] args) {
		//2 4 6 7 17 28 35 38 40 48 
		int[] arrs = new int[] { 17, 35, 6, 28, 2, 40, 38, 48, 4, 7 };
		System.out.println(Arrays.toString(Lomuto_qsort(arrs, 0, arrs.length-1)));
		
		int[] arrs2 = new int[] { 17, 35, 6, 28, 2, 40, 38, 48, 4, 7 };
		System.out.println(Arrays.toString(Hoare_qsort(arrs2, 0, arrs2.length-1)));
		
		int[] arrs3 = new int[] { 17, 35, 6, 28, 2, 40, 38, 48, 4, 7 };
		sort(arrs3);
	}
	
	//1. Lomuto
	/**
	 * algorithm quicksort(A, lo, hi) is
	    if lo < hi then
	        p := partition(A, lo, hi)
	        quicksort(A, lo, p - 1 )
	        quicksort(A, p + 1, hi)
	
		algorithm partition(A, lo, hi) is
		    pivot := A[hi]
		    i := lo - 1    
		    for j := lo to hi - 1 do
		        if A[j] < pivot then
		            i := i + 1
		            swap A[i] with A[j]
		    if A[hi] < A[i + 1] then
		        swap A[i + 1] with A[hi]
		    return i + 1
	 */
	public static int[] Lomuto_qsort(int[] arrs, int low, int high) {
		if(low<high) {
//			int i = low - 1;
//			for (int j = low; j < high; j++){
//				if (arrs[j] < arrs[high]){	// arr[high]:피봇
//					swap(arrs, ++i, j);
//				}
//			}
//			swap(arrs, ++i, high);
//			qsort(arrs, low, i-1);
//			qsort(arrs, i+1, high);
			
			int p = Lomuto_partition(arrs, low, high);
			Lomuto_qsort(arrs, low, p-1);
			Lomuto_qsort(arrs, p+1, high);
		}
		return arrs;
	}
	
	public static int Lomuto_partition(int[] arrs, int low, int high) {
		int pivot = arrs[high];
		
		int i = low-1;
		for(int j=low; j<high; j++) {
			if(arrs[j] < pivot ) {
				i = i+1;
				swap(arrs, i, j);
			}
		}
		
		if(pivot < arrs[i+1])
			swap(arrs, i+1, high);
		return i+1;
	}
	
	public static void swap(int[] arrs, int iIdx, int jIdx) {
		int tmp = arrs[iIdx];
		arrs[iIdx]=arrs[jIdx];
		arrs[jIdx]=tmp;
	}
	
	//2. Hoare
	public static int[] Hoare_qsort(int[] arrs, int low, int high) {
		if(low<high) {
			int p = hoare_partition(arrs, low, high);
			Hoare_qsort(arrs, low, p-1);
			Hoare_qsort(arrs, p+1, high);
		}
		return arrs;
	}


	private static int hoare_partition(int[] arr, int lo, int hi) {
		int i = lo;
		int j = hi;
		while (i < j)
		{
			while (i <= hi && arr[lo] >= arr[i]) 
				i++;
			
			while (arr[lo] < arr[j]) 
				j--;

			if (i < j){
				swap(arr, i, j);
			}
		}
		swap(arr, lo, j);
		
		return j;
	}
	
	public static void sort(int[] arr) {
		//boxed() = int, long, double primitive type == Boxing ==> Integer, Long, Double reference type
		List<Integer> arrList = Arrays.stream(arr).boxed().collect(Collectors.toList());
		System.out.println(sort(arrList));
	}
	public static List<Integer> sort(List<Integer> arr) {
		if(arr.size()<2) {
			return arr;
		}
		
		int pivot = arr.get(0);
		List<Integer> less = new ArrayList<>();
		List<Integer> greater = new ArrayList<>();
		
		for(int i=1; i<arr.size(); i++) {
			if(arr.get(i) <= pivot) {
				less.add(arr.get(i));
			}
		}
		
		for(int i=1; i<arr.size(); i++) {
			if(arr.get(i) > pivot) {
				greater.add(arr.get(i));
			}
		}
		
		less = sort(less);
		greater = sort(greater);
		
		List<Integer> result = new ArrayList<>();
		result.addAll(less);
		result.add(pivot);
		result.addAll(greater);
		return result;
	}
}
