package Bit;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

/**
 * 두 개의 32비트 수 N,M 비트 위치 i,j
 * M을 N에 삽입하는 메소드 구현
 * M은 N의 j번째 비트에서 시작 하여 i번째 비트에서 끝
 * (i~j 사이 공간은 M을 담기 충분하다고 가정)
 * 
 * N = 10000000000 M = 10011, i = 2, j = 6
 * N = 10001001100
 */
public class q5_1 {
	public static void main(String[] args) {
		String N = "10000000000"; 	// int foo1 = Integer.parseInt(N, 2); <==> Integer.toBinaryString(foo1)
		String M = "10011";			// int foo2 = Integer.parseInt(M, 2);
	    String ans = "10001001100";
	    int nN = Integer.parseInt(N, 2);
	    int nM = Integer.parseInt(M, 2);
		int result1 = addMtoNControlBit(nM, nN, 6, 2);
//		System.out.println(AssortedMethods.toFullBinaryString(result1));

		// String myans = addMtoN(M, N, 6, 2);
		// System.out.println(ans.equals(myans));
	    
		int a = 103217;
//		System.out.println(AssortedMethods.toFullBinaryString(a));
		int b = 13;
//		System.out.println(AssortedMethods.toFullBinaryString(b));
		int reuslt2 = addMtoNControlBit(b, a, 12, 4);
		System.out.println(AssortedMethods.toFullBinaryString(reuslt2));
		

	}

	//오답....비트 조작이 아닌 배열 조작
	public static String addMtoN(String M, String N, int j, int i) {
		if( M.length() > 32 && N.length() > 32) {
			return "";
		}
		
		char[] arrN = N.toCharArray();
		char[] arrM = M.toCharArray();
		int arrMIndex = 0;
		
		int tmp = arrM.length-1 <= j ? arrM.length-1 : 0;
		if(tmp == 0 ) {
			return "";
		}
		
		for (int start = arrM.length - 1; start <= arrN.length - i; start++) {
			if (arrMIndex <= arrM.length - 1)
				arrN[start] = arrM[arrMIndex++];
		}
		return String.valueOf(arrN);
	}
	
	public static int addMtoNControlBit(int M, int N, int j , int i ) {
		/*
		1. N의 j부터 i를 0으로 (32bit int 전부 1로 채우고 j 부터 i번째만 0인 비트)
		 1-1 전부 1로 = ~0
		 1-2 j 다음부터  0 앞에는 1
		 1-3 i 앞부터 0 뒤에는 1
		 1-4 1-2/1-3 이용 마스크 생성 |연산 이용 (하나라도 1이면 1 아니면 0) 
		2. M을 시프트 (j부터 i번 비트 자리에 오도록)
		 2-1 N을 j~i까지 0으로 만듬 (j~i 사이  제외 나머지 1 이되어야 하므로 | 연산 이용 )
		 2-2 M을 쉬프트 i 까지 ( i번째 비트에서 끝나므로)
		3. M과 N을 합침 (& 연산 시 자리가 자리가 변경되거나 M의 값이 변경될 수 있으므로 | 연산 사용)
		*/
		
		//1
		//1-1
		int allOne = ~0;
		//1-2 
		int left = allOne << (j+1);
//		System.out.println(AssortedMethods.toFullBinaryString(allOne << (j-1)));
		System.out.println(AssortedMethods.toFullBinaryString(allOne << (j)));
		System.out.println(AssortedMethods.toFullBinaryString(allOne << (j+1)));

		//1-3
		int right = ((1 << i) -1 );
		System.out.println(AssortedMethods.toFullBinaryString((1 << i)));
		System.out.println(AssortedMethods.toFullBinaryString(right));
		//1-4
		int mask = left | right;
		
		//2
		//2-1
		int n_clear = N & mask;
		//2-2
		int m_shift = M << i;
		
		//3
		int result = n_clear | m_shift;
		return result;
	}
}
