package Bit;

/**
 * 0과 1 사이의 double 타입이 주어진 경우
 * 그 값을 2진수 형태로 출력 
 * (길이가 32개 이하 문자열로 출력할 수 없는 경우 ERROR 출력)
 */
public class q5_2 {
	public static void main(String[] args) {
		double input = .125;
		String bs = printBinary(input);
		System.out.println(bs);
		
		bs = printBinary(0.2);
		System.out.println(bs);
		
		bs = printBinary(0.25);
		System.out.println(bs);
		
		bs = printBinary(0.375);
		System.out.println(bs);
		
		bs = printBinary(0.5);
		System.out.println(bs);
		
		bs = printBinary(0.625);
		System.out.println(bs);
	}

	//10진수 소수점을 2진수 1이 될 때 까지 2 곱한다 
	//1초과시 나머지 부분 다시 2 곱한다 
	//1미만이면 0 
	private static String printBinary(double d) {
		if (d >= 1 || d <= 0) {
			return "ERROR";
		}
		
		StringBuffer sBuf = new StringBuffer();
		sBuf.append(".");
		
		while(true) {
			if(sBuf.length() == 32) {
				sBuf.setLength(0);
				sBuf.append("ERROR");
				break;
			}
			
			d = d * 2;
			if (d == 1) {
				sBuf.append("1");
				break;
			} else if (d > 1) {
				d = d - (int) d;
				sBuf.append("1");
			} else if (d < 1) {
				sBuf.append("0");
			}
		}
		
		return sBuf.toString();
	}
	
	
}
