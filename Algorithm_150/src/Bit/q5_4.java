package Bit;

/**
 * ( (n & n-1) == 0 ) 의미?
 *    => 2^n 확인
 * @author QWER
 *
 */
public class q5_4 {
	public static void main(String[] args) {
		int num = 16;
		int min1 = num-1;
		System.out.println(Integer.toBinaryString(num));
		System.out.println(Integer.toBinaryString(min1));
		System.out.println(Integer.toBinaryString(num & min1));
		System.out.println((num & min1) == 0);
		System.out.println("");
		
		num = 17;
		min1 = num-1;
		System.out.println(Integer.toBinaryString(num));
		System.out.println(Integer.toBinaryString(min1));
		System.out.println(Integer.toBinaryString(num & min1));
		System.out.println((num & min1) == 0);
		System.out.println("");
		
		num = 32;
		min1 = num-1;
		System.out.println(Integer.toBinaryString(num));
		System.out.println(Integer.toBinaryString(min1));
		System.out.println(Integer.toBinaryString(num & min1));
		System.out.println((num & min1) == 0);
		System.out.println("");
		
		num = 45;
		min1 = num-1;
		System.out.println(Integer.toBinaryString(num));
		System.out.println(Integer.toBinaryString(min1));
		System.out.println(Integer.toBinaryString(num & min1));
		System.out.println((num & min1) == 0);
		System.out.println("");
		
		num = 64;
		min1 = num-1;
		System.out.println(Integer.toBinaryString(num));
		System.out.println(Integer.toBinaryString(min1));
		System.out.println(Integer.toBinaryString(num & min1));
		System.out.println((num & min1) == 0);
	}
}
