package Bit;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

/**
 * 정수 A를 B로 변환하기 위해 바꿔야 하는 비트 개수 
 */
public class q5_5 {
	public static void main(String[] args) {
		int a = 23432;
		int b = 512132;
		System.out.println(a + ": " + AssortedMethods.toFullBinaryString(a));
		System.out.println(b + ": " + AssortedMethods.toFullBinaryString(b));
		
		long n1 = System.nanoTime();
		int xor1 = bitSwapRequired(a, b);
		System.out.println(System.nanoTime()-n1);
		
		n1 = System.nanoTime();
		int xor2 = bitSwapRequired(a, b);
		System.out.println(System.nanoTime()-n1);
		
		n1 = System.nanoTime();
		int my = MybitSwapRequired(a, b);
		System.out.println(System.nanoTime()-n1);
		
		System.out.println(xor1+","+xor2+","+my);
	}

	//비트에 1이 몇 개 인지 확인
	public static int bitCount1(int n) {
		if (n == 0)
			return 0;
		return n % 2 + bitCount1(n / 2);
	}
	
	//비트 연산을 사용한 경우와 66배 속도 차이 남....
	public static int MybitSwapRequired(int a, int b) {
		String sa = AssortedMethods.toFullBinaryString(a);
		String sb = AssortedMethods.toFullBinaryString(b);
		
		int count = 0;
		for(int i=0; i<sb.length(); i++) {
			char cb = sb.charAt(i);
			char ca = sa.charAt(i);
			if(cb != ca ) {
				count++;
			}
		}
		return count;
	}
	
	//XOR 사용
	//서로 같으면 0 다르면 1 
	public static int bitSwapRequired(int a, int b) {
		int count = 0;
		for(int c = a^b; c != 0 ; c = c >> 1) {
			count += c & 1;
		}
		return count;
	}

	//LSB = 이진 정수 에서 짝수/홀수 결정
	//LSB(최하위 비트)를 검사 0으로 변경 해가면서 c가 0이 되는 순간의 count
	public static int bitSwapRequired2(int a, int b){
		int count = 0;
		for (int c = a ^ b; c != 0; c = c & (c-1)) {
			count++;
		}
		return count;
	}
}
