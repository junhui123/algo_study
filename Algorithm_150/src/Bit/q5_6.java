package Bit;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

/**
 * 주어진 정수의 짝수 번째 비트값과 홀수 번째 비트값을 바꾸는 프로그램
 * 가능한 적은 횟수로 변경 ( ex) 0<->1, 2<->3 ... )
 */
public class q5_6 {
	public static void main(String[] args) {
		int a = 103217;
		System.out.println(a + ": " + AssortedMethods.toFullBinaryString(a));
		int b = swapOddEvenBits(a);
		System.out.println(b + ": " + AssortedMethods.toFullBinaryString(b));
	}

	private static int swapOddEvenBits(int x) {
		int tmp = 103217;
		System.out.println(AssortedMethods.toFullBinaryString(0xaaaaaaaa));
		System.out.println(AssortedMethods.toFullBinaryString(tmp & 0xaaaaaaaa));
		System.out.println(AssortedMethods.toFullBinaryString((tmp & 0xaaaaaaaa) >> 1));
		
		System.out.println(AssortedMethods.toFullBinaryString(0x55555555));
		System.out.println(AssortedMethods.toFullBinaryString(tmp & 0x55555555));
		System.out.println(AssortedMethods.toFullBinaryString((tmp & 0x55555555) << 1));
		
		tmp = (tmp & 0xaaaaaaaa) >> 1 | (tmp & 0x55555555) << 1;
		System.out.println(tmp + ": "+AssortedMethods.toFullBinaryString(tmp));

		return ( ((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1) ); 
	}
}
