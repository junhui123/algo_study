package Ctci.Question10_02_Group_Anagrams;

import java.util.Arrays;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;


public class Question {
	public static void main(String[] args) {
		String[] array = {"apple", "banana", "carrot", "ele", "duck", "papel", "tarroc", "cudk", "eel", "lee", "banana"};
		System.out.println(AssortedMethods.stringArrayToString(array));
		long t1 = System.nanoTime();
		Arrays.sort(array, new AnagramComparator());
		System.out.println(System.nanoTime()-t1);
		System.out.println(AssortedMethods.stringArrayToString(array));
	}
}
