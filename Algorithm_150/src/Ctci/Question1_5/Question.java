package Ctci.Question1_5;

public class Question {

	public static int setChar(char[] array, char c, int index, int count) {
		array[index] = c;
		index++;
		char[] cnt = String.valueOf(count).toCharArray();
		for (char x : cnt) {
			array[index] = x;
			index++;
		}
		return index;
	}
	
	public static int countCompression(String str) {
		if (str == null || str.isEmpty()) return 0;
		char last = str.charAt(0);
		int size = 0;
		int count = 1;
		for (int i = 1; i < str.length(); i++) {
			if (str.charAt(i) == last) {
				count++;
			} else {
				last = str.charAt(i);
				size += 1 + String.valueOf(count).length();
				count = 1;
			} 
		}
		size += 1 + String.valueOf(count).length();
		return size;
	}
	
	public static String compressBad(String str) {
		long start = System.nanoTime();
		int size = countCompression(str);
		if (size >= str.length()) {
			System.out.print(System.nanoTime()-start+" ");
			return str;
		}
		String mystr = "";
		char last = str.charAt(0);
		int count = 1;
		for (int i = 1; i < str.length(); i++) {
			if (str.charAt(i) == last) {
				count++;
			} else {
				mystr += last + "" + count;
				last = str.charAt(i);
				count = 1;
			}
		}
		String result = mystr + last + count;
		System.out.print(System.nanoTime()-start+" ");
		return result;
	}
	
	public static String compressBetter(String str) {
		long start =System.nanoTime();
		int size = countCompression(str);
		if (size >= str.length()) {
			System.out.print(System.nanoTime()-start+" ");
			return str;
		}
		StringBuffer mystr = new StringBuffer();
		char last = str.charAt(0);
		int count = 1;
		for (int i = 1; i < str.length(); i++) {
			if (str.charAt(i) == last) {
				count++;
			} else {
				mystr.append(last);
				mystr.append(count);
				last = str.charAt(i);
				count = 1;
			}
		}
		mystr.append(last);
		mystr.append(count);
		System.out.print(System.nanoTime()-start+" ");
		return mystr.toString();
	}	
	
	public static String compressAlternate(String str) {
		long start = System.nanoTime();
		int size = countCompression(str);
		if (size >= str.length()) {
			System.out.print(System.nanoTime()-start+" ");
			return str;
		}
		char[] array = new char[size];
		int index = 0;
		char last = str.charAt(0);
		int count = 1;
		for (int i = 1; i < str.length(); i++) {
			if (str.charAt(i) == last) {
				count++;
			} else {
				index = setChar(array, last, index, count);
				last = str.charAt(i);
				count = 1;
			}
		}
		index = setChar(array, last, index, count);
		System.out.print(System.nanoTime()-start+" ");
		return String.valueOf(array);
	}
	
	public static String getZipStr(String str) {
		
		//문자열 반복 하면서 지금 인덱스와 다음 인덱스의 문자가 같은 것을 찾는다.
		//같다면 카운트 값을 올리고 반환 문자열에 추가한다.
		//다음 인덱스는 위에서 찾음 다음 인덱스 부터 시작 하도록 한다.
		
		long start = System.nanoTime();
		String result = "";
		int count = 1;
		for(int i=0; i<str.length()-1; i++) {
			char c1 = str.charAt(i);
			char c2 = str.charAt(i+1);
			
			if(c1==c2) {
				count++;
			} else {
				result += c1+String.valueOf(count);
				count = 1;
			}
			
			if(str.length()-1 == i+1) {
				result += c2+String.valueOf(count);
			}
		}
		
//		//압축 결과 원래 문자열 보다 긴 경우 원래 문자열 반환
		if(str.length() < result.length()) {
			System.out.print(System.nanoTime()-start+" ");
			return str;
		}
		
		System.out.print(System.nanoTime()-start+" ");
		
		return result;
	}
	
	public static String getZipStrUseStrBuf(String str) {
		long start = System.nanoTime();

		StringBuffer result = new StringBuffer();
		int count = 1;
		for(int i=0; i<str.length()-1; i++) {
			char c1 = str.charAt(i);
			char c2 = str.charAt(i+1);
			
			if(c1==c2) {
				count++;
			} else {
				result.append(c1+String.valueOf(count));
				count = 1;
			}
			
			if(str.length()-1 == i+1) {
				result.append(c2+String.valueOf(count));
			}
		}
		
		//압축 결과 원래 문자열 보다 긴 경우 원래 문자열 반환
		if(str.length() < result.length()) {
			System.out.print(System.nanoTime()-start+" ");
			return str;
		}
		
		System.out.print(System.nanoTime()-start+" ");
		
		return result.toString();
	}
	
	public static String getZipStrChangeAlgo(String str) {
		long start = System.nanoTime();
		StringBuffer result = new StringBuffer();
		char last = str.charAt(0);
		int count = 1;
		for(int i=1; i<str.length(); i++) {
			if(str.charAt(i) == last) {
				count++;
			} else {
				result.append(last);
				result.append(String.valueOf(count));
				last = str.charAt(i);
				count = 1;
			}
		}
		
		result.append(last);
		result.append(count);
		String resultStr=result.toString();
		if(resultStr.length() > str.length()) {
			resultStr=str;
		}
		System.out.print(System.nanoTime()-start+" ");
		return resultStr;
	}
	
	public static void main(String[] args) {
		String str = "bacbbcc";
//		String str = "aaaaaabbbbbbccccczzzbbaaabbcccciiaaiiioooovvnmnnbbbvvvxsddddiiiiiioojgfgggggggggzzxcqqqqfsfsfssssplplplpllllllllllpopppppppppiiiiziibnkjlkkkkkllassaaaafffqzzzzbvbbbvvvvvvmmmmmmnnnnnnbbbbbkkkkkcccccllllcllllllaaaaaasssssdddqqqqwwwwewwwweeeeeebbbbvvvvbbbbzzzzxxkkkxxxxxxxiiiiiiieeeeeuuuuuuuuyyyyyuuuyyyyrrrrrrrrrzzaazzffggggggjjjjjjjkkkkkttttttttttuuuuututututurrrdddghggggghhhhhhhvvvvbbbbbbbbpppppppppppzzzazzz";
//		String str = "aabccccccccaaa";
		System.out.println("old    : " + str);
		System.out.println("me1    : " + getZipStr(str));
		System.out.println("me2    : " + getZipStrUseStrBuf(str));
		System.out.println("me3    : " + getZipStrChangeAlgo(str));
		System.out.println("bad    : " + compressBad(str));
		System.out.println("better : " + compressBetter(str));
		System.out.println("alter  : " + compressAlternate(str));
	}
	
	
}
