package Ctci.Question1_6;

import Ctci.CtCILibrary.CtCILibrary.*;

public class Question {

	public static void rotate(int[][] matrix, int n) {
		for (int layer = 0; layer < n / 2; ++layer) {
			int first = layer;
			int last = n - 1 - layer;
			for(int i = first; i < last; ++i) {
				int offset = i - first;
				int top = matrix[first][i]; // save top

				// left -> top
				matrix[first][i] = matrix[last-offset][first]; 			

				// bottom -> left
				matrix[last-offset][first] = matrix[last][last - offset]; 

				// right -> bottom
				matrix[last][last - offset] = matrix[i][last]; 

				// top -> right
				matrix[i][last] = top; // right <- saved top
			}
		}
	}
	
	public static void main(String[] args) {
//		int[][] matrix = AssortedMethods.randomMatrix(10, 10, 0, 9);
		int[][] matrix = new int[][] {
			{1,5,1,5,2,9,7,5,6,9},
			{6,9,5,8,9,3,4,9,7,8},
			{1,9,1,9,5,3,8,8,7,3},
			{5,1,1,6,6,7,2,1,8,1},
			{3,2,1,9,8,4,3,6,2,2},
			{1,6,6,3,8,7,2,1,4,6},
			{7,6,8,9,2,2,2,1,8,4},
			{2,6,1,5,9,8,3,6,7,5},
			{5,2,1,5,3,8,9,4,7,8},
			{7,5,9,3,7,6,1,2,5,7}
		};
		AssortedMethods.printMatrix(matrix);
		rotate(matrix, 10);
		System.out.println();
		AssortedMethods.printMatrix(matrix);
	}

}
