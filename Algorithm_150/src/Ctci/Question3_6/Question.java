package Ctci.Question3_6;

import java.util.Stack;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;


public class Question {
	static int c = 0;
	public static Stack<Integer> mergesort(Stack<Integer> inStack) {
		if (inStack.size() <= 1) {
			return inStack;
		}

		Stack<Integer> left = new Stack<Integer>();
		Stack<Integer> right = new Stack<Integer>();

		int count = 0;
		int stackHalf = inStack.size()/2;
		while(!inStack.isEmpty()) {
			if(count < stackHalf) {
				left.push(inStack.pop());
			} else {
				right.push(inStack.pop());
			}
			count++;
		}		
		
		left = mergesort(left);
		right = mergesort(right);

		while (left.size() > 0 || right.size() > 0) {
			if (left.size() == 0) {
				inStack.push(right.pop());
			} else if (right.size() == 0) {
				inStack.push(left.pop());
			} else if (right.peek().compareTo(left.peek()) <= 0) {
				inStack.push(left.pop());
			} else {
				inStack.push(right.pop());
			}
		}
		
		Stack<Integer> reverseStack = new Stack<Integer>();
		while (inStack.size() > 0) {
			reverseStack.push(inStack.pop());
		}

		return reverseStack;
	}
	
	public static Stack<Integer> sort(Stack<Integer> s) {
		Stack<Integer> r = new Stack<Integer>();
		while(!s.isEmpty()) {
			int tmp = s.pop();
			while(!r.isEmpty() && r.peek() > tmp) {
				s.push(r.pop());
			}
			r.push(tmp);
		}
		return r;
	}
		
	public static void main(String [] args) {
		Stack<Integer> s2 = new Stack<>();
		s2.push(582);
		s2.push(808);
		s2.push(84);
		s2.push(106);
		s2.push(556);
		s2.push(360);
		s2.push(996);
		s2.push(119);
		s2.push(749);
		s2 = mergesort(s2);
		System.out.println(s2.toString());
		
//		for (int k = 1; k < 100; k++) {
//			c = 0;
//			Stack<Integer> s = new Stack<Integer>();
//			for (int i = 0; i < 10 * k; i++) {
//				int r = AssortedMethods.randomIntInRange(0,  1000);
//				s.push(r);
//			}
//			s = mergesort(s);
//			int last = Integer.MAX_VALUE;
//			while(!s.isEmpty()) {
//				int curr = s.pop();
//				if (curr > last) {
//					System.out.println("Error: " + last + " " + curr);
//				}
//				last = curr;
//			}
//			System.out.println(c);
//		}
	}
}
