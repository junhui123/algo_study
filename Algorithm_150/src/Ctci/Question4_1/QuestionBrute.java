package Ctci.Question4_1;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;
import Ctci.CtCILibrary.CtCILibrary.TreeNode;

public class QuestionBrute {
	
	public static int getHeight(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return Math.max(getHeight(root.left), getHeight(root.right)) + 1;
	}
		
	public static boolean isBalanced(TreeNode root) {
		if (root == null) {
			return true;
		}
		
		int left = getHeight(root.left);
		int right = getHeight(root.right);
		int heightDiff = left - right;
		if (Math.abs(heightDiff) > 1) {
			return false;
		}
		else {
			return isBalanced(root.left) && isBalanced(root.right);
		}
	}
	
	public static int checkBalancedTree(TreeNode root) {
		if (root == null)
			return 0;
		int left = 0;
		int right = 0;
		if (root.left != null || root.right != null) {
			left += checkBalancedTree(root.left);
			if (left == -1)
				return -1;
			right += checkBalancedTree(root.right);
			if (right == -1)
				return -1;
		}
		int result = Math.abs(left - right) > 1 ? -1 : (left > right ? left + 1 : right + 1);
		return result;
	}

	
	public static void main(String[] args) {
		// Create balanced tree
		int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		TreeNode root = TreeNode.createMinimalBST(array);
		System.out.println("Root? " + root.data);
		System.out.println("Is balanced? " + isBalanced(root));
		boolean check = checkBalancedTree(root) >= 1 ? true : false;
		System.out.println("Is balanced? " + check);
		
		// Could be balanced, actually, but it's very unlikely...
		TreeNode unbalanced = new TreeNode(10);
		for (int i = 0; i < 10; i++) {
			unbalanced.insertInOrder(AssortedMethods.randomIntInRange(0, 100));
		}
		System.out.println("Root? " + unbalanced.data);
		System.out.println("Is balanced? " + isBalanced(unbalanced));
		check = checkBalancedTree(unbalanced) >= 1 ? true : false;
		System.out.println("Is balanced? " + check);
	}

}
