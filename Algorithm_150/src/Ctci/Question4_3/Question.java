package Ctci.Question4_3;

import Ctci.CtCILibrary.CtCILibrary.TreeNode;

public class Question {	
	public static void main(String[] args) {
		int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		
		// We needed this code for other files, so check out the code in the library
		TreeNode root = TreeNode.createMinimalBST(array);
//		TreeNode root = createMinimalBST(array);
		System.out.println("Root? " + root.data);
		System.out.println("Created BST? " + root.isBST());
		System.out.println("Height: " + root.height());
	}

	
	private static TreeNode createMinimalBST(int[] array) {
		TreeNode[] nodes = new TreeNode[array.length];
		for (int i = 0; i < array.length; i++) {
			nodes[i] = new TreeNode(array[i]);
		}
		
		for (int i = 0; i < nodes.length; i++) {
			int left = (2*i)+1;
			int right = (2*i)+2;
			nodes[i].setLeftChild(left >= nodes.length ? null : nodes[left]);
			nodes[i].setRightChild(right >= nodes.length ? null : nodes[right]);
		}
		
		TreeNode root = nodes[0];

		return root;
	}
}
