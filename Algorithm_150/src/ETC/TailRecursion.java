package ETC;

public class TailRecursion {
	public static void main(String[] args) {
		System.out.println(foo(5, 1));
		System.out.println(foo(5));
	}
	
	public static int foo(int n, int k) {
		
		if(n==0) return k;
		
		return foo(n-1, 2*k);
	}
	
	public static int foo(int n) {
		
		if(n==0) return 1;
		
		return 2*foo(n-1);
	}
}
