package ETC;

import java.util.Arrays;

public class deepAndShallowCopyEx01 {

	public static class IntWrap {
		public int value = 0;
	}
    public static void main(String[] args) {
    	
    	IntWrap[] data = new IntWrap[5];
    	for(int i=0; i<data.length; i++) {
    		data[i] = new IntWrap();
    		data[i].value = i;
    	}
    	
    	IntWrap[]  sCopy = null;
    	IntWrap[]  dCopy = null;
 
        sCopy = shallowCopy(data);
        dCopy = deepCopy(data);
 
        System.out.println("Original : " + Arrays.toString(data));
        System.out.println("Shallow : " + Arrays.toString(sCopy));
        System.out.println("Deep : " + Arrays.toString(dCopy));
        System.out.println();
 
        data[0].value = 5;
        System.out.println("data[0].value=5; 적용 후 결과");
        System.out.println();
 
        System.out.println("Original : " + Arrays.toString(data));
        System.out.println("Shallow : " + Arrays.toString(sCopy));
        System.out.println("Deep : " + Arrays.toString(dCopy));
 
    }
 
    // 얕은 복사
    private static IntWrap[] shallowCopy(IntWrap[] arr) {
        return arr;
    }
 
    // 깊은 복사
    private static IntWrap[] deepCopy(IntWrap[]  arr) {
        if (arr == null)
            return null;
 
        IntWrap[]  result = new IntWrap[arr.length];
 
        System.arraycopy(arr, 0, result, 0, arr.length);
        return result;
    }
}


