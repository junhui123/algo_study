package LinkedList;

import java.util.HashMap;
import java.util.HashSet;

import Ctci.CtCILibrary.CtCILibrary.LinkedListNode;

/**
 * 1. 비정렬 연결 리스트에서 중복 문자 제거 2. 추가적 임시 버퍼가 없다면?
 * 
 * @author QWER
 *
 */
public class q2_1 {
	public static void main(String[] args) {
		LinkedListNode first = new LinkedListNode(0, null, null);
		LinkedListNode head = first;
		LinkedListNode second = first;
		for (int i = 1; i < 8; i++) {
			second = new LinkedListNode(i % 2, null, null);
			first.setNext(second);
			second.setPrevious(first);
			first = second;
		}
		System.out.println(head.printForward());
		LinkedListNode clone = head.clone();
		System.out.println(removeDuplicate(head));
		System.out.println(head.printForward());
		removeDuplicateNoBuffer(clone);
		System.out.println(clone.printForward());
	}

	//O(n) 이므로 연결된 노드 갯수만큼 수행됨. 버퍼로 인한 추가 메모리 요구 있음
	public static boolean removeDuplicate(LinkedListNode head) {
		boolean isDuplicate = false;
		HashSet set = new HashSet();
		LinkedListNode prev = null;
		while (head != null) {
			if (set.contains(head.data)) {
				prev.next = head.next;
				isDuplicate = true;
			} else {
				set.add(head.data);
				prev = head;
			}
			head = head.next;
		}
		return isDuplicate;
	}

	//O(n^2) 시간 수행...느리지만 추가 메모리 요구 작음
	private static void removeDuplicateNoBuffer(LinkedListNode head) {
		if(head == null) return;
		
		LinkedListNode current = head;
		while(current != null) {
			LinkedListNode runner = current;
			while(runner.next != null) {
				if(runner.next.data == current.data ) {
					runner.next = runner.next.next;
				} else {
					runner = runner.next;
				}
			}
			current = current.next;
		}
	}
}
