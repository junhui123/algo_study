package LinkedList;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;
import Ctci.CtCILibrary.CtCILibrary.LinkedListNode;

/**
 * 단방향 연결리스트 뒤에서 k번째 원소
 * @author QWER
 *
 */
public class q2_2 {
	public static void main(String[] args) {
		int k = 3;
		LinkedListNode head = AssortedMethods.randomLinkedList(10, 0, 10);
		System.out.println(head.printForward());
		
		long t1 = System.nanoTime();
		findKNodeRecursion(head, k);
		System.out.println(System.nanoTime()-t1);
		
		t1 = System.nanoTime();
//		IntWrap iw = new IntWrap();
		int[] iw = new int[1];
		LinkedListNode n = findKNodeRecursion2(head, k, iw);
		System.out.println(n.data);
		System.out.println(System.nanoTime()-t1);

		t1 = System.nanoTime();
		LinkedListNode n1 = findKNodeEasy(head, k);
		System.out.println(n1.data);
		System.out.println(System.nanoTime()-t1);
		
		t1 = System.nanoTime();
		LinkedListNode n2 = findNodeIterative(head, k);
		System.out.println(n2.data);
		System.out.println(System.nanoTime()-t1);
	}
	
	//사이즈를 찾아내서 해결 쉬움
	public static LinkedListNode findKNodeEasy(LinkedListNode head, int k) {
		if(head==null || k<=0) return null;
		
		LinkedListNode findNode = null;
		LinkedListNode sizeTmp = head;
		int size = 0;
		while(sizeTmp.next != null) {
			size++;
			sizeTmp = sizeTmp.next;
		}
		
		int findNodeLoc = (size-k)+1;
		int count = 0;
		while(head.next != null) {
			head = head.next;
			count++;
			if(count == findNodeLoc) {
				findNode=head;
			}
		}
		return findNode;
	}
	
	public static int findKNodeRecursion(LinkedListNode head, int k) {
		if(head==null || k<=0)
			return 0;
		
		int i = findKNodeRecursion(head.next, k)+1;
		if(i==k) 
			System.out.println(head.data);
		return i;
	}
	
	private static class IntWrap{
		public int value = 0;
	}
	
	//pass by value(call by value) : 기본자료형, pass by reference(call by reference) : 객체, 배열, 값이 공유됨 (기본자료형의 Wrapper는 immutable로 디자인되어 불가능)
//	public static LinkedListNode findKNodeRecursion2(LinkedListNode head, int k, IntWrap z) {
	public static LinkedListNode findKNodeRecursion2(LinkedListNode head, int k, int[] z) {
		if(head==null || k<=0)
			return null;
		
		LinkedListNode node = findKNodeRecursion2(head.next, k, z);
//		z.value = z.value+1;
//		if(z.value==k) 
		z[0] = z[0]+1;
		if(z[0]==k) 
			return head;
		return node;
	}
	
	public static LinkedListNode findNodeIterative(LinkedListNode head, int k) {
		if(head==null || k<=0)
			return null;
		
		LinkedListNode node1=head;
		LinkedListNode node2=head;
		for(int i=0; i<k; i++) {
			node2 = node2.next;
		}
		
		while(node2 != null) {
			node1 = node1.next;
			node2 = node2.next;
		}
		
		return node1;
	}
}
