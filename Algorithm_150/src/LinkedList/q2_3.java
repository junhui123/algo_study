package LinkedList;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;
import Ctci.CtCILibrary.CtCILibrary.LinkedListNode;

/**
 * 단방향 연결 리스트 중간 노드 삭제
 * 삭제할 노드에 대한 접근만 가능에 유의
 * (head 접근 불가)
 * @author QWER
 *
 */
public class q2_3 {
	public static void main(String[] args) {
		LinkedListNode head = AssortedMethods.randomLinkedList(10, 0, 10);
		LinkedListNode clone = head.clone();
		
		System.out.println(head.printForward());
		deleteNode(head.next.next.next.next); // delete node 4
		System.out.println(head.printForward());
		
		System.out.println("");
		
		System.out.println(clone.printForward());
		deleteNodeUseHead(clone);
		System.out.println(clone.printForward());
	}
	
	//중간을 알고 있는 경우
	public static void deleteNode(LinkedListNode node) {
		if(node == null) return;
		
		LinkedListNode tmp = node.next;
		if(tmp != null) {
			node.data = tmp.data;
			node.next = tmp.next;
			return;
		}
	}
	
	//중간을 모를 경우
	public static void deleteNodeUseHead(LinkedListNode head) {
		if(head == null) return;
		
		int size = 0;
		LinkedListNode n = head;
		while(n != null) {
			n = n.next;
			size++;
		}
		
		size = size/2;
		int start = 0;
		LinkedListNode prev = null;
		while(head != null) {
			prev = head;
			head = head.next;
			start++;
			if(start == size) {
				prev.next = head.next;
				prev.data = head.data;
				return;
			}
		}
	}
}
