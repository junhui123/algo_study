package LinkedList;

import java.util.Arrays;

import Ctci.CtCILibrary.CtCILibrary.LinkedListNode;

/**
 * x 값을 갖는 노드 기준으로 연결 리스트 분할
 * 보다 작은 값을 갖는 노드가 x와 같거나 더 큰 값을 갖는 노드들 보다 앞에 
 * 
 * small <= x < big
 * 0->1->2->3->4->5->6->7->8->8->7->6->5->4->3->2->1->0
 * 0->1->2->3->4->5->6->6->5->4->3->2->1->0->7->8->8->7
 * @author QWER
 *
 */
public class q2_4 {
	public static void main(String[] args) {
//		/* Create linked list */
//		int[] vals = {1, 3, 7, 5, 2, 9, 4};
//		LinkedListNode head = new LinkedListNode(vals[0], null, null);
//		LinkedListNode current = head;
//		for (int i = 1; i < vals.length; i++) {
//			current = new LinkedListNode(vals[i], null, current);
//		}
//		System.out.println(head.printForward());
//		
//		/* Partition */
//		LinkedListNode h = partition(head, 5);
//		
//		/* Print Result */
//		System.out.println(h.printForward());
		
		int length = 20;
		LinkedListNode[] nodes = new LinkedListNode[length];
		for (int i = 0; i < length; i++) {
			nodes[i] = new LinkedListNode(i >= length / 2 ? length - i - 1 : i, null, null);
		}
		
		for (int i = 0; i < length; i++) {
			if (i < length - 1) {
				nodes[i].setNext(nodes[i + 1]);
			}
			if (i > 0) {
				nodes[i].setPrevious(nodes[i - 1]);
			}
		}
		
		LinkedListNode[] clones = new LinkedListNode[length];
//		not deep copy, shallow copy
//		clones = Arrays.copyOf(nodes, length);
		
//		not deep copy, shallow copy
//		System.arraycopy(nodes, 0, clones, 0, length);
		
		//deep copy
		clones = java.util.Arrays.stream(nodes).map(el -> el.clone()).toArray($ -> nodes.clone());
		
		LinkedListNode head = nodes[0];
		System.out.println(head.printForward());

		/* Partition */
		LinkedListNode h = partition(head, 5);
		
		/* Print Result */
		System.out.println(h.printForward());
		
		System.out.println(" ");
		
		
		LinkedListNode clone = clones[0];
		System.out.println(clone.printForward());
		/* Partition */
		h = paritionMemlow2(clone, 5);
		
		/* Print Result */
		System.out.println(h.printForward());

		
	}

	//연결을 위해 변수 5개 사용. 메모리사용 많음
	private static LinkedListNode partition(LinkedListNode head, int i) {
		if (head == null)
			return null;

		LinkedListNode smallStart = null;
		LinkedListNode small = null;
		LinkedListNode bigStart = null;
		LinkedListNode big = null;
		LinkedListNode iNode = null;

		while (head != null) {
			if (head.data == i && iNode == null) {
				iNode = head;
			} else if (head.data <= i) {
				if (small == null) {
					smallStart = head;
					small = head;
				} else {
					small.next = head;
					small = small.next;
				}
			} else if (head.data > i) {
				if (big == null) {
					bigStart = head;
					big = head;
				} else {
					big.next = head;
					big = big.next;
				}
			}
			head = head.next;
		}

		big.next = null;
		small.next = iNode;
		iNode.next = bigStart;

		return smallStart;
	}
	
	public static LinkedListNode partition2(LinkedListNode node, int x) {
		LinkedListNode beforeStart = null;
		LinkedListNode beforeEnd = null;
		LinkedListNode afterStart = null;
		LinkedListNode afterEnd = null;
		
		/* Partition list */
		while (node != null) {
			LinkedListNode next = node.next;
			node.next = null;
			if (node.data < x) {
				if (beforeStart == null) {
					beforeStart = node;
					beforeEnd = beforeStart;
				} else {
					beforeEnd.next = node;
					beforeEnd = node;
				}
			} else {
				if (afterStart == null) {
					afterStart = node;
					afterEnd = afterStart;
				} else {
					afterEnd.next = node;
					afterEnd = node;
				}
			}	
			node = next;
		}
		
		/* Merge before list and after list */
		if (beforeStart == null) {
			return afterStart;
		}
		
		beforeEnd.next = afterStart;
		return beforeStart;
	}

	//LinkedListNode 변수 2개만 사용
	private static LinkedListNode paritionMemlow(LinkedListNode node, int i ) {
		LinkedListNode head = node;
		LinkedListNode tail = node;
		
		/* Partition list */
		while (node != null) {
			LinkedListNode next = node.next;
			if (node.data < i) {
				/* Insert node at head. */
				node.next = head;
				head = node;
			} else {
				/* Insert node at tail. */
				tail.next = node;
				tail = node;
			}	
			node = next;
		}
		tail.next = null;
		
		return head;
	}
	
	public static LinkedListNode paritionMemlow2(LinkedListNode node, int x) {
		LinkedListNode beforeStart = null;
		LinkedListNode afterStart = null;
		
		/* Partition list */
		while (node != null) {
			LinkedListNode next = node.next;
			if (node.data < x) {
				/* Insert node into start of before list */
				node.next = beforeStart;
				beforeStart = node;	
			} else {
				/* Insert node into front of after list */
				node.next = afterStart;
				afterStart = node;
			}	
			node = next;
		}
		
		/* Merge before list and after list */
		if (beforeStart == null) {
			return afterStart;
		}
		
		LinkedListNode head = beforeStart;
		while (beforeStart.next != null) {
			beforeStart = beforeStart.next;
		}
		beforeStart.next = afterStart;
		return head;
	}
}
