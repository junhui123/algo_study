package LinkedList;

import java.util.Stack;

import Ctci.CtCILibrary.CtCILibrary.LinkedListNode;

/**
 * 연결 리스트로 표현된 두 숫자의 합
 * 자리숫들은 역순으로 배열. 연결리스트이 첫번째는 1의 자리 숫자.
 * 합을 연결 리스트로 반환 
 * @author QWER
 *
 */
public class q2_5 {
	public static void main(String[] args) {
		LinkedListNode lA1 = new LinkedListNode(7, null, null);
		LinkedListNode lA2 = new LinkedListNode(1, null, lA1);
		LinkedListNode lA3 = new LinkedListNode(6, null, lA2);
		LinkedListNode lA4 = new LinkedListNode(6, null, lA3);
		
		LinkedListNode lB1 = new LinkedListNode(5, null, null);
		LinkedListNode lB2 = new LinkedListNode(9, null, lB1);
		LinkedListNode lB3 = new LinkedListNode(2, null, lB2);	
		
//		LinkedListNode lA1 = new LinkedListNode(9, null, null);
//		LinkedListNode lA2 = new LinkedListNode(9, null, lA1);
//		LinkedListNode lA3 = new LinkedListNode(9, null, lA2);
//		
//		LinkedListNode lB1 = new LinkedListNode(1, null, null);
//		LinkedListNode lB2 = new LinkedListNode(0, null, lB1);
//		LinkedListNode lB3 = new LinkedListNode(0, null, lB2);	
		
//		LinkedListNode lA1 = new LinkedListNode(3, null, null);
//		LinkedListNode lA2 = new LinkedListNode(1, null, lA1);
//		LinkedListNode lA3 = new LinkedListNode(5, null, lA2); //513
//		
//		LinkedListNode lB1 = new LinkedListNode(5, null, null);
//		LinkedListNode lB2 = new LinkedListNode(9, null, lB1);
//		LinkedListNode lB3 = new LinkedListNode(1, null, lB2); //195
		
		LinkedListNode sumN = sum(lA1, lB1);
		System.out.println(sumN.printForward());
		
		LinkedListNode sumN2 = sumRecursion(lA1, lB1);
		System.out.println(sumN2.printForward());
		
		LinkedListNode list3 = addLists(lA1, lB1, 0);
		int l3 = linkedListToInt(list3);
		System.out.println(l3);
	}
	
	//Iterative
	public static LinkedListNode sum(LinkedListNode a1, LinkedListNode b1) {
		
		Stack<String> a1Stack = new Stack<>();		
		Stack<String> b1Stack = new Stack<>();
		
		while(a1 != null || b1 != null) {
			if(a1 != null) {
				a1Stack.push(String.valueOf(a1.data));
				a1 = a1.next == null ? null : a1.next;
			}
			
			if(b1 != null) {
				b1Stack.push(String.valueOf(b1.data));
				b1 = b1.next == null ? null : b1.next;
			}
		}
		
		String a1Str = "";
		String b1Str = "";
		
		while(a1Stack.size()>0) {
			a1Str += a1Stack.pop();
		}
		
		while(b1Stack.size()>0) {
			b1Str += b1Stack.pop();
		}
		
		int sum = Integer.valueOf(a1Str)+Integer.valueOf(b1Str);
		
		String tmp = String.valueOf(sum);
		
		LinkedListNode[] sumNodes = new LinkedListNode[tmp.length()];
		
		for(int i=0; i<tmp.length(); i++) {
			char c = tmp.charAt(i);
			if(i==0)
				sumNodes[i] = new LinkedListNode(Integer.valueOf(Character.getNumericValue(c)), null, null);
			else
				sumNodes[i] = new LinkedListNode(Integer.valueOf(Character.getNumericValue(c)), null, sumNodes[i-1]);
		}
		
		return sumNodes[0];
	}
	
	//Recursion
	public static LinkedListNode sumRecursion(LinkedListNode a1, LinkedListNode b1) {
		
		StringBuffer a1Buf = new StringBuffer();
		a1Buf = recursion(a1, a1Buf);
		String a1Str = a1Buf.reverse().toString();
		
		StringBuffer b1Buf = new StringBuffer();
		b1Buf = recursion(b1, b1Buf);
		String b1Str = b1Buf.reverse().toString();
		
		int sum = Integer.valueOf(a1Str)+Integer.valueOf(b1Str);
		String tmp = String.valueOf(sum);
		LinkedListNode[] sumNodes = new LinkedListNode[tmp.length()];
		for(int i=0; i<tmp.length(); i++) {
			char c = tmp.charAt(i);
			if(i==0)
				sumNodes[i] = new LinkedListNode(Integer.valueOf(Character.getNumericValue(c)), null, null);
			else
				sumNodes[i] = new LinkedListNode(Integer.valueOf(Character.getNumericValue(c)), null, sumNodes[i-1]);
		}
		
		return sumNodes[0];
	}
	
	public static StringBuffer recursion(LinkedListNode node, StringBuffer a1Buf) {
		if(node == null) return a1Buf.append("");
		
		a1Buf.append(String.valueOf(node.data));
		recursion(node.next, a1Buf);
		return a1Buf;
	}
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	private static LinkedListNode addLists(LinkedListNode l1, LinkedListNode l2, int carry) {
		if (l1 == null && l2 == null && carry == 0) {
             return null;
		}
		
		LinkedListNode result = new LinkedListNode();
		int value = carry;
		if (l1 != null) {
			value += l1.data;
		}
		if (l2 != null) {
			value += l2.data;
		}
		result.data = value % 10;
		if (l1 != null || l2 != null) {
			LinkedListNode more = addLists(l1 == null ? null : l1.next, 
										   l2 == null ? null : l2.next, 
										   value >= 10 ? 1 : 0);
			result.setNext(more);
		}
		return result;
	}
	
	public static int linkedListToInt(LinkedListNode node) {
		int value = 0;
		if (node.next != null) {
			value = 10 * linkedListToInt(node.next);
		}
		return value + node.data;
	}	
}
