package LinkedList;

import java.util.HashSet;

import Ctci.CtCILibrary.CtCILibrary.LinkedListNode;

/**
 * 순환 연결 리스트, 순환되는 부분의 첫 노드 반환
 * @author QWER
 *
 */
public class q2_6 {
	public static void main(String[] args) {
		int list_length = 100;
		int k = 10;
		
		// Create linked list
		LinkedListNode[] nodes = new LinkedListNode[list_length];
		for (int i = 0; i < list_length; i++) {
			nodes[i] = new LinkedListNode(i, null, i > 0 ? nodes[i - 1] : null);
		}
		
		// Create loop;
		nodes[list_length - 1].next = nodes[list_length - k];
		
		LinkedListNode loopUseSet = FindBeginning(nodes[0]);
		if (loopUseSet == null) {
			System.out.println("No Cycle.");
		} else {
			System.out.println(loopUseSet.data);
		}
		
		LinkedListNode loopUseRabbitTurtle = FindBeginningRabbitTurtle(nodes[0]);
		if (loopUseRabbitTurtle == null) {
			System.out.println("No Cycle.");
		} else {
			System.out.println(loopUseRabbitTurtle.data);
		}
	}

	//메모리 사용량이 많음.. 리스트가 커질수록 hashset 크기 커짐
	private static LinkedListNode FindBeginning(LinkedListNode head) {
		
		HashSet<LinkedListNode> dupl = new HashSet<>();
		
		LinkedListNode beginning = null;
		while(head.next != null) {
			if(!dupl.contains(head)) { 
				dupl.add(head);
			} else if(dupl.contains(head)) {
				beginning = head;
				break;
			}
			head = head.next;
		}
		
		return beginning;
	}
	
	//토끼 거북이 알고리즘 (Tortoise and Hare)
	//토끼는 거북이의 2배를 이동. 원형 연결 리스트인 경우 언젠가 만나는 지점이 발생함 단순 연결 리스트인 경우 토끼는 먼저 null로 도착
	//추가적인 공간 없이 O(n) 시간 소요
	private static LinkedListNode FindBeginningRabbitTurtle(LinkedListNode head) {
		
		LinkedListNode rabbit = head;
		LinkedListNode turtle = head;
		
		while(rabbit != null && rabbit.next != null ) {
			rabbit = rabbit.next.next;
			turtle = turtle.next;
			
			if(rabbit == turtle)
				return rabbit;
		}
		
		return null;
	}
}
