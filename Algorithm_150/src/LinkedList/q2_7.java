package LinkedList;

import java.util.LinkedList;
import java.util.Stack;

import Ctci.CtCILibrary.CtCILibrary.LinkedListNode;

/**
 * 연결리스트 회문(팰린드롬) 검사 
 * 회문 : 거꾸로 읽어도 앞에서 읽은것과 같은 구조
 * @author QWER
 *
 */
public class q2_7 {
	public static void main(String[] args) {
		int length = 11;
		LinkedListNode[] nodes = new LinkedListNode[length];
		for (int i = 0; i < length; i++) {
			nodes[i] = new LinkedListNode(i >= length / 2 ? length - i - 1 : i, null, null);
		}
		
		for (int i = 0; i < length; i++) {
			if (i < length - 1) {
				nodes[i].setNext(nodes[i + 1]);
			}
			if (i > 0) {
				nodes[i].setPrevious(nodes[i - 1]);
			}
		}
		// nodes[length - 2].data = 9; // Uncomment to ruin palindrome
		
		LinkedListNode head = nodes[0];
		System.out.println(head.printForward());
		
		System.out.println(isPalindrome(head));
		System.out.println(isPalindrome2(head));
		
		LinkedListNode reverse = reverseNoStack(head);
		System.out.println(reverse.printForward());
		
	}
	
	//스택 사용
	public static boolean isPalindrome(LinkedListNode head) {
		Stack<Integer> stack = new Stack<>();
		
		StringBuffer sBuf = new StringBuffer();
		while(head != null) {
			sBuf.append(head.data);
			stack.push(head.data);
			head = head.next;
		}
		
		StringBuffer stackBufStr = new StringBuffer();
		while(stack.size() > 0) {
			stackBufStr.append(stack.pop());
		}
		
		if(stackBufStr.toString().equals(sBuf.toString())) {
			return true;
		}
		
		return false;
	}
	
	//토끼와 거북이 알고리즘 사용
	//토끼가 끝나는 순간은 거북이는 리스트 절반 위치에 도착
	//거북이를 순회 하면서 스택에 하나씩 꺼내서 값이 같은지 비교 같다면 팰린드롬
	public static boolean isPalindrome2(LinkedListNode head) {
		Stack<Integer> stack = new Stack<>();
		
		LinkedListNode fast = head;
		LinkedListNode slow = head;
		
		while(fast!=null && fast.next != null) {
			stack.push(slow.data);
			fast = fast.next.next;
			slow = slow.next;
		}
		
		//홀수인 경우 fast가 아직 종료 지점에 도달하지 않았으므로
		if(fast != null) {
			slow = slow.next;
		}
		
		while(slow != null) {
			
			int top = stack.pop();
			
			if(top != slow.data) {
				return false;
			}
			slow = slow.next;
		}
		return true;
	}

	public static LinkedListNode reverseNoStack(LinkedListNode root) {
		LinkedListNode prev = null;
		LinkedListNode curr = root;
		while (curr != null) {
			LinkedListNode next = curr.next;
			curr.setNext(prev);
			prev = curr;
			curr = next;
		}
		root = prev;
		return root;
	}

}
