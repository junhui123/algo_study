package Recursion_and_DP;

/**
 * n개의 계단 
 * 한 번에 1개 / 2개 / 3개 오를수 있음
 * 계단을 오르는 방법?
 * 
 */
public class q9_1 {
	public static void main(String[] args) {
		for (int i = 0; i < 30; i++) {
			long t1 = System.currentTimeMillis();
			int[] map = new int[30 + 1];
			for (int j = 0; j < map.length; j++) {
				map[j] = -1;
			}
			int c1 = countWaysDP(i, map);
			long t2 = System.currentTimeMillis();
			long d1 = t2 - t1;
			
			long t3 = System.currentTimeMillis();
			int c2 = countWaysRecursive(i);
			long t4 = System.currentTimeMillis();
			long d2 = t4 - t3;			
			System.out.println(i + " " + c1 + " " + c2 + " " + d1 + " " + d2);
		}
	}

	/**
		1번째 칸에 올라가는 방법  : 1
		  0번째 칸에서 1계단 오르기
		  
		2번째 칸에 올라가는 방법  : 2
		  1번째 칸에서 1계단 오르기 
		  0번째 칸에서 출발 한번에 2계단 오르기
		 
		3번째 칸에 올라가는 방법  : 4
		  0번째 칸에서 3계단 오르기 
		  1번째 칸에서 2계단 오르기
		  0번째 칸에서 2계단 오르기 + 1계단 오르기 --| 2번째 칸에서   
		  1번째 칸에서 1계단 오르기 + 1계단 오르기 --| 올라가는 방법 가지수
	 */
	public static int countWaysDP(int n, int[] map) {
		if (n < 0) {
			return 0;
		} else if (n == 0) {
			return 1;
		} else if (map[n] > -1) {
			return map[n]; 
		} else {
			//memoization 활용 
			//재귀를 사용 시 중복된 호출이 여러번 발생하므로 이를 방지 하여 효율 향상
			map[n] = countWaysDP(n - 1, map) + 
					 countWaysDP(n - 2, map) + 
					 countWaysDP(n - 3, map);
			return map[n];
		}
	}

	//f(4) = f(4-1)+f(4-2)+f(4-3)   3  2  1  
	//f(3) = f(3-1)+f(3-2)+f(3-3)   2  1  0
	//f(2) = f(2-1)+f(2-2)+f(2-3)   1  0 -1 
	//f(1) = f(1-1)+f(1-2)+f(1-3)   0 -1 -2
	//=>중복 계산 발생
	public static int countWaysRecursive(int n) {
		if (n < 0) {
			return 0;
		} else if (n == 0) {
			return 1;
		} else {
			return countWaysRecursive(n - 1) + countWaysRecursive(n - 2) + countWaysRecursive(n - 3);
		}
	}
}
