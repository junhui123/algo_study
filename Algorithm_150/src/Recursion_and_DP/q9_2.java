package Recursion_and_DP;

import java.util.Arrays;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

// 문제 Pass....................................

/**
 *	XxY 그리드 왼쪽 상단 꼭짓점 로봇이 놓여 있다.
 *  이 로봇은 오른쪽 아니면 아래쪽만 이동 가능하다.
 *  로봇이 (0,0) 에서 (X,Y)로 이동하는 가능하 경로의 수는 몇 개 인가?
 */
public class q9_2 {
	public static int size = 4;
//	public static int[][] maze = new int[][] { { 1, 4, 4, 5 }, { 1, 5, 4, 5 }, { 3, 5, 3, 5 }, { 4, 2, 5, 2 } };
	public static int[][] path = new int[][] { { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 }, { 1, 1, 1, 1 } };
	public static int[][] maze = new int[size][size];
//	public static int[][] path = new int[size][size];
	public static void main(String[] args) {
//		maze = AssortedMethods.randomMatrix(size, size, 0, 5);
		maze = new int[][] { { 5, 0, 2, 0 }, { 4, 4, 5, 0 }, { 5, 0, 3, 3 }, { 3, 0, 5, 3 } };
		AssortedMethods.printMatrix(maze);
		System.out.println("=================================");
		System.out.println(getPathCount(size-1, size-1));
	}
	
	private static int getPathCount(int x, int y) {
		for (int i = 0; i <= x; i++) {
			for (int j = 0; j <= y; j++) {
				if (maze[i][j] == 0) {
					path[i][j] = 0;
				}
			}
		}

		for (int i = 1; i <= x; i++) {
			for (int j = 1; j <= y; j++) {
				try {
					if (maze[i][j] == 0) {
						path[i][j] = 0;
						continue;
					} 
					path[i][j] = path[i - 1][j] + path[i][j - 1];
				} catch (Exception e) {
					System.out.println(i + "," + j);
				}
			}
		}

		AssortedMethods.printMatrix(path);

		return path[x][y];
	}
}
 