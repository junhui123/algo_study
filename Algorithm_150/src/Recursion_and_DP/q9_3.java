package Recursion_and_DP;

import java.util.Arrays;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

/**
 * 배열 A[0....n-1] 에서 A[i]=i인 인덱스 i를 magic index
 * 정렬된 상태의 배열에서 magic index가 존재한다면 i를 찾아라
 * (중복은 없음)
 */
public class q9_3 {
	public static void main(String[] args) {
//		int size = AssortedMethods.randomIntInRange(5, 20);
//		int[] array = AssortedMethods.randomArray(size, -1 * size, size);
//		for (int i = 1; i < array.length; i++) {
//			if (array[i] == array[i-1]) {
//				array[i]++;
//			} else if (array[i] < array[i - 1]) {
//				array[i] = array[i-1] + 1;
//			}
//		}
		
		int[] array = new int[] {0, -7, 2, 4, 5};
		array = new int[] {-2, -1, 1, 3, 4, 5};
		Arrays.sort(array);
		System.out.println(Arrays.toString(array));
		getMagixInexBruteForce(array);
		magicFast(array);
	}
	
	private static int magicFast(int[] array, int start, int end) {
		if(start > end || start < 0) 
			return -1;
		
		int mid = (start+end)/2;
		if(array[mid] == mid) {
			return mid;
		} else if(array[mid] < mid) {
			return magicFast(array, mid+1, end);
		} else {
			return magicFast(array, start, mid);
		}
	}
	

	public static void getMagixInexBruteForce(int[] array) {
		for(int i = 0; i<array.length; i++) {
			if(i == array[i]) {
				System.out.println("array["+i+"]="+i);
			}
		}
	}
	
//	public static int magicFast(int[] array, int start, int end) {
//		if (end < start || start < 0 || end >= array.length) {
//			return -1;
//		}
//		int mid = (start + end) / 2;
//		if (array[mid] == mid) {
//			return mid;
//		} else if (array[mid] > mid){
//			return magicFast(array, start, mid - 1);
//		} else {
//			return magicFast(array, mid + 1, end);
//		}
//	}
	
	public static void magicFast(int[] array) {
		System.out.println(magicFast(array, 0, array.length - 1));
	}
}
