package Recursion_and_DP;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 주어진 집합의 모든 부분집합
 */
public class q9_4 {
	private static boolean[] include = null;
	@SuppressWarnings("rawtypes")
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i < 3; i++) {
			list.add(i);
		}
		ArrayList subsets1 = getSubsets1(list);
		System.out.println(subsets1.toString());
		
		ArrayList<ArrayList<Integer>> subsets2 = getSubsets2(list);
		System.out.println(subsets2.toString());
		
		include = new boolean[list.size()];
		ArrayList<ArrayList<Integer>> subSetList = new ArrayList<>();
		subSetList = powerSet(subSetList, list, list.size(), 0);
		System.out.println(subSetList.toString());
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static ArrayList getSubsets1(ArrayList<Integer> list) {
		ArrayList subSetList = new ArrayList();
		subSetList.add(new ArrayList<Integer>());
		
		for(int i=0; i<list.size(); i++) {
			ArrayList<Integer> subset = new ArrayList<>();
			subset.add(list.get(i));
			subSetList.add(subset);
			for(int j=i+1; j<list.size(); j++) {
				String tmp = String.valueOf(list.get(i))+", "+String.valueOf(list.get(j));
				ArrayList subset2 = new ArrayList();
				subset2.add(tmp);
				subSetList.add(subset2);
			}
		}
		subSetList.add(list);
		return subSetList;
	}
	
	private static ArrayList<ArrayList<Integer>> getSubsets2(ArrayList<Integer> list) {
		ArrayList<ArrayList<Integer>> subSetList = new ArrayList<>();
		subSetList.add(new ArrayList<Integer>());
		makeSubSet(subSetList, list, 0, list.size());
		subSetList.add(list);
		return subSetList;
	}
	
	
	private static ArrayList<ArrayList<Integer>>  makeSubSet(ArrayList<ArrayList<Integer>> subSetList, List<Integer> list, int fromIndex, int toIndex) {
		if(fromIndex == toIndex) {
			return subSetList;
		}
		
		int num1 = list.get(fromIndex);
		List<Integer> tmp = list.subList(fromIndex, toIndex);
		for(int i=0; i<tmp.size(); i++) {
			ArrayList<Integer> subSet = new ArrayList<>();
			int num2 = tmp.get(i);
			if(num1 == num2) {
				subSet.add(num2);
				subSetList.add(subSet);
				continue;
			}
			subSet.add(num1);
			subSet.add(num2);
			subSetList.add(subSet);
		}
		makeSubSet(subSetList, list, ++fromIndex, toIndex);
		
		return subSetList;
	}
	
	//https://www.youtube.com/watch?time_continue=1715&v=nkeMRRIVW9s
	public static ArrayList<ArrayList<Integer>> powerSet(ArrayList<ArrayList<Integer>> subSetList, List<Integer> list, int listLength, int k) {
		if(k == listLength) {
			ArrayList<Integer> tmp = new ArrayList<>();
			for(int i = 0 ; i<listLength; i++) {
				if(include[i]) {
					tmp.add(list.get(i));
				}
			}
			subSetList.add(tmp);
			return subSetList;
		}
		
		include[k] = false;
		powerSet(subSetList, list, listLength, k+1);
		include[k] = true;
		powerSet(subSetList, list, listLength, k+1);
		
		return subSetList;
	}
}
