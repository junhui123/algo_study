package Recursion_and_DP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *	중복된 문자가 없는 문자열의 모든 순열 (permutation) 찾는 메소드 작성 
 */
public class q9_5 {
	public static void main(String[] args) {
		ArrayList<String> list = getPerms("abcde");
		Collections.sort(list);
		System.out.println("There are " + list.size() + " permutations.");
		for (String s : list) {
			System.out.println(s);
		}
		
		System.out.println("");
		System.out.println("");
		
		ArrayList<String> list2 = (ArrayList<String>) getPermutations("abcde");
		Collections.sort(list2);
		System.out.println("There are " + list2.size() + " permutations.");
		for (String s : list2) {
			System.out.println(s);
		}
	}

	//5! = 120
	//nPr (n, r = 5) = n×(n−1)×(n−2)×⋯×(n−r+1) = n!/(n-r)!
	private static ArrayList<String> getPerms(String string) {
		if(string == null)
			return null;
		
		ArrayList<String> permutations = new ArrayList<String>();
		if (string.length() == 0) { // base case
			permutations.add("");
			return permutations;
		}
		
		char first = string.charAt(0);
		String remainder = string.substring(1);
		ArrayList<String> words = getPerms(remainder);
		for(String word : words) {
			for(int i=0; i<=word.length(); i++) {
				String start = word.substring(0, i);
				String end = word.substring(i);
				String combi = start + first + end;
				permutations.add(combi);				
			}
		}
		
		return permutations;
	}
	
	public static List<String> getPermutations(String s) {
		if (s == null)
			return null;
		// boolean 은 누구를 선택 했는지 판별 파라미터
		return permuRec(s, new boolean[s.length()], "", new ArrayList<String>());
	}

	private static List<String> permuRec(String s, boolean[] pick, String perm, ArrayList<String> result) {
		// 종료 조건
		if (perm.length() == s.length()) {
			result.add(perm);
			return result;
		}
		for (int i = 0; i < s.length(); i++) {
			if (pick[i])
				continue;
			pick[i] = true;
			permuRec(s, pick, perm + s.charAt(i), result);
			pick[i] = false;
		}
		return result;
	}
}
