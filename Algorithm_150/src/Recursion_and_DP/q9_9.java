package Recursion_and_DP;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 8x8 체스판 8개 퀸 놓는 방법
 * 같은 행,열,대각선 상에 퀸이 놓이면 안됨
 */
public class q9_9 {
	public static int GRID_SIZE = 8;
	public static int[][] BOARD = new int[GRID_SIZE][GRID_SIZE];
	private static int[] cols = new int [GRID_SIZE+1];
	private static int SOLUTION_CNT = 0;
	public static void main(String[] args) {
		Arrays.fill(cols, -1);
//		ArrayList<Integer[]> results = new ArrayList<>();
//		queens(0, results);
		
		long t1 = System.nanoTime();
		initBoard();
		for(int i=0; i<GRID_SIZE; i++) {
			clearRow(0);
			
			if (isOK(0, i) == 1) {
				BOARD[0][i] = 1;
				checkNext(1);
			}
		}
		System.out.println(System.nanoTime()-t1);
	}
	
	private static void initBoard() {
		for(int i=0; i<GRID_SIZE; i++) {
			for(int j=0; j<GRID_SIZE; j++) {
				BOARD[i][j] = 0;
			}
		}
	}
	
	private static int isOK(int r, int c) {
		// 위족 검사
		for (int i = 0; i < r; i++) {
			if (BOARD[i][c] == 1) {
				return 0;
			}
		}
		// 왼쪽 검사
		for (int i = 0; i < c; i++) {
			if (BOARD[r][i] == 1) {
				return 0;
			}
		}
		// 대각선 검사
		for (int i = 0; i < r; i++) {
			for (int j = 0; j < GRID_SIZE; j++) {
				if (Math.abs(i - r) == Math.abs(j - c) && BOARD[i][j] == 1) {
					return 0;
				}
			}
		}
		return 1;
	}
	
	private static void checkNext(int r) {
		for(int i=0; i<GRID_SIZE; i++) {
			clearRow(r);
			
			if(isOK(r, i) == 1) {
				BOARD[r][i] = 1;
				
				if(r == (GRID_SIZE-1)) {
					printSolution();
				} else {
					checkNext(r+1);
				}
			}
		}
	}
	
	private static void printSolution() {
		SOLUTION_CNT++;
		
		System.out.println("Solution Number = "+SOLUTION_CNT);
		for(int i=0; i<GRID_SIZE; i++) {
			for(int j=0; j<GRID_SIZE; j++) {
				System.out.print(BOARD[i][j]);
			}
			System.out.println("");
		}
	}

	private static void clearRow(int r) {
		for(int i=0; i<GRID_SIZE; i++) {
			BOARD[r][i] = 0;
		}
	}

	////////////////////////////////////////////////////////////////////////
	private static boolean queens(int level, ArrayList<Integer[]> results) {
		if(!promising(level)) {
			return false;
		} else if(level == GRID_SIZE) {
			return true;
		}
		for(int i=1; i<=GRID_SIZE; i++) {
			cols[level+1] = i;
			if(queens(level+1, results))
				return true;
			
		}
		return false;
	}

	private static boolean promising(int level) {
		
		for(int i=1; i<level; i++) { //같은 열에 놓여져 있는지 검사
			if(cols[i] == cols[level]) {
				return false;
			} else if(level-i == Math.abs(cols[level]-cols[i])) { //같은 대각선 검사
				return false;
			}
		}
		return true;
	}
	////////////////////////////////////////////////////////////////////////
}
