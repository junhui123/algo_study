package Sorting_and_Search;

import java.util.Arrays;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

/**
 * 정렬된 배열 A,B
 * A 뒤에는 B가 들어갈 만한 충분한 공간 있음
 * A와 B를 정렬된 상태로 병합
 *
 */
public class q10_1 {
	public static void main(String[] args) {
		int[] a = {2, 3, 4, 5, 6, 8, 10, 100, 0, 0, 0, 0, 0, 0};
		int[] b = {1, 4, 7, 6, 7, 7};
		
		long t1 = System.nanoTime();
		mergeMy(a, b);
		System.out.println(System.nanoTime()-t1);
		System.out.println(AssortedMethods.arrayToString(a));
		
		int[] a1 = {2, 3, 4, 5, 6, 8, 10, 100, 0, 0, 0, 0, 0, 0};
		long t2 = System.nanoTime();
		int lastB = b.length;
		int lastA = 0;
		for(int i=0; i<a1.length; i++) {
			if(a1[i] == 0 ) {
				lastA = i;
				break;
			}
		}
		merge(a1, b, lastA, lastB);
		System.out.println(System.nanoTime()-t2);
		System.out.println(AssortedMethods.arrayToString(a1));
		
	}

	//array copy 오버헤드
	private static void mergeMy(int[] a, int[] b) {
		int ai = 0;
		int bi = 0;
		while(true) {
			if(a[ai] >= b[bi]) {
				int destPos = 0;
				if(ai==0) 
					destPos = 1;
				else 
					destPos = ai+1;
				
				int copyCnt = a.length-destPos;
				//배열 A 1단계씩 뒤로 밀림
				System.arraycopy(a, ai, a, destPos, copyCnt);
				a[ai] = b[bi];
				bi++;
			} else {
				ai++;
			}
			if(bi==b.length)
				break;
		}
	}
	
	//마지막 0이 아닌 원소를 지정해줘야 함. 
	//마지막 원소들을 비교 하여 병합
	public static void merge(int[] a, int[] b, int lastA, int lastB) {
		int indexA = lastA - 1;
		int indexB = lastB - 1;
		int indexMerge = lastB + lastA - 1;

		while (indexB >= 0) {
			if (indexA >= 0 && a[indexA] > b[indexB]) { //a의 마지막 원소가 b의 마지막 원소 보다 크다.
				a[indexMerge] = a[indexA]; //a의 마지막 원소를 통합할 위치의 마지막으로 이동
				indexA--; //A의 다음값을 가리킬 전 인덱스의 앞의 인덱스로 이동
			} else { //a의 마지막 원소가 b의 마지막 원소보다 작다. (b의 마지막 원소가 a의 마지막 원소보다 크다.)
				a[indexMerge] = b[indexB]; //b의 마지막 원소를 통합할 위치의 마지막으로 이동
				indexB--; //B의 다음값을 가리킬 전 인덱스의 앞의 인덱스로 이동
			}
			indexMerge--; //통합할 인덱스의 앞의 인덱스로 이동
		}
	}
}
