package Sorting_and_Search;

import java.util.Arrays;
import java.util.Comparator;

/**
 * 철자 순서만 바꾼 문자열이 서로 인접하도록 문자열 배열을 정렬 
 * (Anagram)
 */
public class q10_2 {
	public static void main(String[] args) {
		String[] array2 = {"kcud", "apple", "banana", "carrot", "ele", "duck", "papel", "tarroc", "cudk", "eel", "lee", "banana" ,"ppale"};
		System.out.println(Arrays.toString(array2));
		
		long t2 = System.nanoTime();
		Arrays.sort(array2, new Comparator<String>() {
			private String sortChar(String s ) {
				char[] chars = s.toCharArray();
				Arrays.sort(chars);
				return String.valueOf(chars);
			}
			
			@Override
			public int compare(String o1, String o2) {
				return sortChar(o1).compareTo(sortChar(o2));
			}
		});
		System.out.println(Arrays.toString(array2));
		System.out.println(System.nanoTime()-t2);
		
		System.out.println(" ");
		
//		String[] array = {"kcud", "apple", "banana", "carrot", "ele", "duck", "papel", "tarroc", "cudk", "eel", "lee", "banana" ,"ppale"};
//		String[] array = {"apple", "banana", "carrot", "ele", "duck", "papel", "tarroc", "cudk", "eel", "lee", "banana"};
		String[] array = {"apple", "banana", "carrot", "ele", "duck", "papel", "tarroc", "cudk", "eel", "lee"};
		System.out.println(Arrays.toString(array));
		long t1 = System.nanoTime();
		sort(array, 0, 1);
		System.out.println(Arrays.toString(array));
		System.out.println(System.nanoTime()-t1);
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////////////
	public static void sort(String[] array, int start, int near) {
		if(near >= array.length) {
			return;
		}
		
		String value = array[start];
		char[] v1C = value.toCharArray();
		for(int i=start+1; i<array.length; i++ ) {
			String value2 = array[i];
			if(isContains(v1C, value2.toCharArray())) {
				if(near == array.length-1 || (i== array.length-1 && i == near)) {
					near-=1;
				} 
				
				String tmp = array[near];
				array[near] = value2;
				array[i] = tmp;
				
				near = near+1;
			}
		}
		start = near;
		near = start+1;
		sort(array, start, near);
	}
	
	private static boolean isContains(char[] value1, char[] value2) {
		Arrays.sort(value1);
		Arrays.sort(value2);
		
		if(value1.length != value2.length) 
			return false;
		
		for(int i=0; i<value1.length; i++) {
			if(value1[i] != value2[i]) 
				return false;
		}
		return true;
	}
}
