package Sorting_and_Search;

/**
 * n개의 정수로 구성된 정렬 상태의 배열 임의 횟수만큼 회전시켜 얻은 배열이 주어질 때 
 * 배열에서 특정한 원소를 찾는 알고리즘
 * (회전 이전에는 오름차순 정렬 상태)
 * 
 * ex) 입력 : arr={15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14}, find=5
 *     출력 : 8 (5의 인덱스)
 */
public class q10_3 {
	public static void main(String[] args) {
		int[] a = { 2, 3, 1, 2, 2, 2, 2, 2, 2, 2 };
//		int[] a = { 15, 16, 19, 20, 25, 1, 3, 4, 5, 7, 10, 14 };

//		System.out.println(searchMy(a, 2));
		System.out.println(searchMy(a, 3));  //1
		System.out.println(searchMy(a, 4));  //-1
		System.out.println(searchMy(a, 1));  //2
		System.out.println(searchMy(a, 8));  //-1
		
		System.out.println();
		
		System.out.println(findIndexLinearSearch(a, 3));
		System.out.println(findIndexLinearSearch(a, 4));
		System.out.println(findIndexLinearSearch(a, 1));
		System.out.println(findIndexLinearSearch(a, 8));
		
		System.out.println("");
		
		System.out.println(search(a, 3));
		System.out.println(search(a, 4));
		System.out.println(search(a, 1));
		System.out.println(search(a, 8));
		
	}
	//순차 탐색
	public static int findIndexLinearSearch(int[] arr, int find) {
		for(int i=0; i<arr.length; i++) {
			if(arr[i] == find) {
				return i;
			}
		}
		return -1;
	}
	
	//이진 탐색 (오름차순으로 정렬된 리스트에서 특정한 값의 위치를 찾는 알고리즘, 회전한 경우 정렬이 깨져있으므로 정상 작동하지 않음)
	public static int findIndexBinarySearch(int[] arr, int find, int low, int high) {
		if(high < low ) {
			return -1;
		}
		
		int mid = (high+low) / 2;
		if(arr[mid] > find) {
			return findIndexBinarySearch(arr, find, low, mid-1);
		} else if(arr[mid] < find) {
			return findIndexBinarySearch(arr, find, mid+1, high);
		} else {
			return mid;
		}
	}
	
	//이진 탐색 응용 (정렬된 부분과 회전된 부분 분리 해서 탐색)
	//오름차순 정렬된 분할의 경우 binarySearch 호출
	//아닌경우 LinearSearch 호출
	//너무 복잡.....안되는 경우도 있음..탈락...
	private static int searchMy(int[] arr, int i) {
		int mid = arr.length/2;
		
		//오름차순 정렬 이면서 범위 안에 i가 있는 경우
		if(arr[0] < arr[mid]) {
			if(arr[0] <= i && i<=arr[mid]) {
				int[] cpArr = new int[mid+1];
				System.arraycopy(arr, 0, cpArr, 0, mid);
				int result = findIndexBinarySearch(arr, i, 0, cpArr.length);
				return returnResult(arr.length, cpArr.length, result);
			} 
		} else if(arr[0] > arr[mid]) { 
			if( arr[mid] <= i && arr[0] >= i ) {
				int[] cpArr = new int[arr.length-mid];
				System.arraycopy(arr, mid, cpArr, 0, cpArr.length);
				int result = findIndexBinarySearch(cpArr, i, 0, cpArr.length);
				return returnResult(arr.length, cpArr.length, result);
			} else {
				int[] cpArr = new int[mid];
				System.arraycopy(arr, 0, cpArr, 0, cpArr.length);
				int result = findIndexLinearSearch(cpArr, i);
				return result;
			}
		} else if(arr[0] == arr[mid]) {
			if(arr[mid] < arr[arr.length-1]) {
				int[] cpArr = new int[arr.length-mid];
				System.arraycopy(arr, mid+1, cpArr, 0, cpArr.length);
				int result = findIndexBinarySearch(cpArr, i, 0, cpArr.length);
				return returnResult(arr.length, cpArr.length, result);
			} 
		} else {
			return findIndexLinearSearch(arr, i);
		}
		return -1;
	}
	
	private static int returnResult(int arrSize, int cpArrLength, int value) {
		if (value == -1) {
			return -1;
		} else {
			if (arrSize < value + cpArrLength) {
				return value;
			} else {
				return value + cpArrLength;
			}
		}
	}
	
	////////////////////////////////////////////////////////////////////////////////
	public static int search(int a[], int find) {
		return search(a, 0, a.length - 1, find);
	}

	public static int search(int a[], int left, int right, int find) {
		int mid = (left+right) / 2;
		if(find == a[mid]) {
			return mid;
		}
		
		//인덱스가 교차 
		if(right < left) {
			return -1;
		}
		
		//1. 왼쪽이 정렬 상태
		if(a[mid] > a[left]) {
			// 1-1 왼쪽 탐색 arr[left] <= find <= a[mid]
			if(a[left]<= find && find<=a[mid]) {
				return search(a, left, mid-1, find);
			} else {
			// 1-2 오른쪽 탐색 
				return search(a, mid+1, right, find);
			}
		//2. 오른쪽이 정렬 상태
		} else if(a[mid] < a[left]) {
			// 2-1 오른쪽 탐색 arr[mid] <= find <= arr[right]
			if(a[mid] <= find && find<= a[right]) {
				return search(a, mid+1, right, find);
			} else {
			// 2-2 왼쪽 탐색
				return search(a, left, mid-1, find);
			}
		//3. 왼쪽 절반이 같은 값 arr[mid] == arr[left]
		} else if(a[mid] == a[left]) {
			// 3-1 맨 오른쪽이 다른 값이면 오른쪽 탐색
			if(a[right] != a[mid]) {
				return search(a, mid+1, right, find);
			} else {
				// 3-2 그렇지 않으면 전부 탐색
				int result = search(a, left, mid-1, find);
				if(result == -1) {
					return search(a, mid+1, right, find);
				} else {
					return result;
				}
			}
		}
		return -1;
	}
	
}
