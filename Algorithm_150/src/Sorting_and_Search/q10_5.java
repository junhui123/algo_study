package Sorting_and_Search;

/**
 * 빈 문자열이 섞여 있는 정렬 상태의 배열이 주어졌을 때 특정한 문자열의 위치를 찾는 메서드를 작성하라
 * ex)
 *   입력 : arr={"at","","","","ball","","","car", "", "", "dad", "", ""}, find=ball
 *   출력 : 4 
 */
public class q10_5 {
	public static void main(String[] args) {
//		String[] stringList = { "apple", "", "", "banana", "", "", "", "carrot", "duck", "", "", "eel", "", "flower" };
//		System.out.println(search(stringList, "ac"));
		String[] stringList = {"at","","","","ball","","","car", "", "", "dad", "", ""};
		
		System.out.println(search(stringList, "ball"));
		System.out.println(search(stringList, "car"));
		System.out.println(search(stringList, "ac"));
		System.out.println(search(stringList, "dad"));
		
		System.out.println(searchMy(stringList, "ball"));
		System.out.println(searchMy(stringList, "car"));
		System.out.println(searchMy(stringList, "ac"));
		System.out.println(searchMy(stringList, "dad"));
		System.out.println("");
		
		
		long t1 = System.nanoTime();
		System.out.println(search(stringList, "ball"));
		System.out.println(search(stringList, "car"));
		System.out.println(search(stringList, "ac"));
		System.out.println(search(stringList, "dad"));
		System.out.println(System.nanoTime()-t1);
		
		System.out.println("");
		System.out.println("");
		
		long t2 = System.nanoTime();
		System.out.println(searchMy(stringList, "ball"));
		System.out.println(searchMy(stringList, "car"));
		System.out.println(searchMy(stringList, "ac"));
		System.out.println(searchMy(stringList, "dad"));
		System.out.println(System.nanoTime()-t2);
	}

	//""일 경우 mid + 1 
	private static int searchMy(String[] stringList, String string) {
		if(string == null || string.equals("") || stringList.length == 0 || stringList == null) {
			return -1;
		}
		
		return searchMy(stringList, string, 0, stringList.length-1);
	}
	
	private static int searchMy(String[] s, String string, int low, int high) {
		if (low > high)
			return -1;

		int mid = (low + high) / 2;

		if (s[mid].equals("")) {
			for (int i = mid; i < high; i++) {
				if (!s[i].equals("")) {
					mid = i;
					break;
				}
			}

			if (low == mid) {
				for (int i = high; i >= 0; i--) {
					if (!s[i].equals("")) {
						mid = i;
						break;
					}
				}
			}
		}

		if (s[mid].equals(string)) {
			return mid;
		} else if (s[mid].compareTo(string) == -1) { // -1 = string이 뒤에 있음
			return searchMy(s, string, mid + 1, high);
		} else if (s[mid].compareTo(string) >= 0) { // 0이면 같음 1이면 string이 앞에 있음
			return searchMy(s, string, 0, mid - 1);
		}
		return -1;
	}
	////////////////////////////////////////////////////////////////////////////////////////////////////
	public static int search(String[] strings, String str, int first, int last) {
		if (first > last) {
			return -1;
		}
		
		/* Move mid to the middle */
		int mid = (last + first) / 2;
		
		/* If mid is empty, find closest non-empty string. */
		if (strings[mid].isEmpty()) { 
			int left = mid - 1;
			int right = mid + 1;
			while (true) {
				if (left < first && right > last) {
					return -1;
				} else if (right <= last && !strings[right].isEmpty()) {
					mid = right;
					break;
				} else if (left >= first && !strings[left].isEmpty()) {
					mid = left;
					break;
				}
				right++;
				left--;
			}
		}
			
		/* Check for string, and recurse if necessary */
		if (str.equals(strings[mid])) { // Found it!
			return mid;
		} else if (strings[mid].compareTo(str) < 0) { // Search right
			return search(strings, str, mid + 1, last);
		} else { // Search left
			return search(strings, str, first, mid - 1);
		}
	}	
		
	public static int search(String[] strings, String str) {
		if (strings == null || str == null || str.isEmpty()) {
			return -1;
		}
		return search(strings, str, 0, strings.length - 1);
	}
}
