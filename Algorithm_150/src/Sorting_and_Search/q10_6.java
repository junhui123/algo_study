package Sorting_and_Search;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

/**
 * 각 행과열이 오름차순으로 정렬된 MxN 행렬에서 특정한 원소 찾는 메소드 작성
 */
public class q10_6 {
	public static void main(String[] args) {

//		int[][] matrix = {
//							{15,20,40,85},
//							{10,35,80,95},
//							{30,55,95,105},
//							{40,80,100,120},
//						 };
		
		int M = 200;
		int N = 200;
		int[][] matrix = new int[M][N];
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				matrix[i][j] = 10 * i + j;
			}
		}
		
		AssortedMethods.printMatrix(matrix);
		
//		
		System.out.println(findLinear(matrix, 100));
		System.out.println(findLinear(matrix, 1000));
		
		System.out.println();
		System.out.println();
		
		int findValue = 2005;
		
		long t1 = System.nanoTime();
		String s1 =findLinear(matrix, findValue);
		System.out.println(System.nanoTime()-t1);
		System.out.println(s1);
		
		long t2 = System.nanoTime();
		String s2 =	findElement(matrix, findValue);
		System.out.println(System.nanoTime()-t2);
		System.out.println(s2);
		
		long t3 = System.nanoTime();
		String s3 = findElemBinary(matrix, findValue);
		System.out.println(System.nanoTime()-t3);
		System.out.println(s3);
	}
	
	//Linear
	public static String findLinear(int[][] matrix, int findValue) {
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix[0].length; j++) {
				if(matrix[i][j] == findValue) {
					return i+","+j;
				}
			}
		}
		return -1+","+-1;
	}
	
	//Linear2
	public static String findElement(int[][] matrix, int elem) {

		int row = 0;
		int col = matrix[0].length-1;
		
		String reuslt = -1+","+-1;
		//정렬이 되어 있으므로 각 행의 마지막 열의 값을 비교
		//행과 열 값을 조정
		//행의 마지막열의 값 > elem 같은 행 앞의 열에 있는 수 이므로 col--;
		//행의 마지막열의 값 < elem 다음 행에 값이 있으므로 row++;

		//while(true) { //값이 없다면 row와 col이 무한히 증가 overflow
		while (row < matrix.length && col >= 0) {
			if(matrix[row][col]  == elem ) {
				reuslt = row+","+col;
				break;
			} else if(matrix[row][col] > elem) {
				col--;
			} else if(matrix[row][col] < elem) {
				row++;
			}
		}
		
		return reuslt;
	}
	
	public static String findElemBinary(int[][] matrix, int elem) {
		if(matrix==null || elem < 0) {
			return -1+","+-1;
		}
		
		int[] singleMatrix = new int[matrix[0].length];
		
		int index = 0;
		for(int i=0; i<matrix.length; i++) {
			for(int j=0; j<matrix[0].length; j++) {
				singleMatrix[index] = matrix[i][j];
				index++;
			}
			index=0;
			int searchR = findElemBinary(singleMatrix, elem, 0, matrix[0].length);
			if(searchR != -1) {
				return i+","+searchR;
			}
		}
		
		return -1+","+-1;
	}
	
	public static int findElemBinary(int[] matrix, int elem, int low, int high) {
		if(high <= low ) {
			return -1;
		}
		
		int mid = (high+low) / 2;
		if(matrix[mid] == elem) {
			return mid;
		} else if(matrix[mid] > elem) {
			return findElemBinary(matrix, elem, low, mid-1);
		} else if(matrix[mid] < elem) {
			return findElemBinary(matrix, elem, mid+1, high);
		} 
		
		return -1;
	}
	
}
