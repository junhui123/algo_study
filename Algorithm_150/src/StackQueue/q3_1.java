package StackQueue;

/**
 * 하나의 배열 3개 스택
 * 
 * @author QWER
 *
 */
public class q3_1 {
	
	private static final int STACK_LEN = 3;

	public static void main(String[] args) {

	}

	public static Object[] createStack(int statckLengh) {
		int arrTotalSize = statckLengh * statckLengh;
		Object[] arr = new Object[arrTotalSize];
		return arr;
	}

	//Push
	public static Object[] push(Object[] arrs, int stackNum, int value) {
		int tmp = stackNum * STACK_LEN;
		int inputCount = 0;
		for(int i=tmp; i<arrs.length; i++) {
			if(inputCount == STACK_LEN) break;
			inputCount++;
			
			if(arrs[i] == null) {
				arrs[i] = value; break;
			}
		}
		return arrs;
	}

	//Pop
	public static Object[] pop(Object[] arrs, int stackNum) {
		int tmp = stackNum * STACK_LEN;
		for(int i=tmp+STACK_LEN-1; i>0; i--) {
			if( arrs[i] == null ) {
				continue;
			} else {
				arrs[i] = null;
				break;
			}
		}
		return arrs;
	}

}