package StackQueue;

import java.util.Stack;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;
import Ctci.Question3_2.NodeWithMin;

/**
 * push, pop 이외 최솟값을 갖는 원소 반환 min 연산 갖춘 스택 구현 push, pop, min = O(1)
 * 
 * @author QWER
 *
 */
public class q3_2 {
	public static void main(String[] args) {
		StackWithMin stack = new StackWithMin();
		StackWithMin2 stack2 = new StackWithMin2();
		for (int i = 0; i < 15; i++) {
			int value = AssortedMethods.randomIntInRange(0, 100);
			stack.push(value);
			stack2.push(value);
			System.out.print(value + ", ");
		}
		System.out.println('\n');
		for (int i = 0; i < 15; i++) {
			System.out.println("Popped " + stack.pop().value + ", " + stack2.pop());
			System.out.println("New min is " + stack.min() + ", " + stack2.min());
		}
	}

	// 스택의 크기가 커지면 Min 값 저장으로 인해 추가 공간 필요
	public static class StackWithMin {
		Stack<NodeWithMin> stack = new Stack<>();
		NodeWithMin popNode = null;

		public void push(int value) {
			int min = value;
			if (!stack.isEmpty()) {
				NodeWithMin lastPeek = stack.peek();
				if (lastPeek.min < value) {
					min = lastPeek.min;
				}
			}
			stack.push(new NodeWithMin(value, min));
		}

		public int min() {
			if (stack.isEmpty()) {
				return Integer.MAX_VALUE;
			} else {
				return stack.peek().min;
			}
		}

		public NodeWithMin pop() {
			popNode = stack.lastElement();
			return stack.pop();
		}
	}

	// 스택을 2개 사용 하나는 최소값을 저장하는 스택, 나머지 하나는 값을 저장하는 스택
	public static class StackWithMin2 {

		Stack<Integer> stack1 = new Stack<>();
		Stack<Integer> stack2 = new Stack<>();

		public void push(int value) {
			stack1.push(value);
			if (!stack2.isEmpty()) {
				int min = Integer.MAX_VALUE;
				int lastPeekValue = stack2.peek();
				if(lastPeekValue > value) {
					stack2.push(value);
				}
			} else {
				stack2.push(value);
			}
		}
		
		public int min() {
			if (stack2.isEmpty()) {
				return Integer.MAX_VALUE;
			} else {
				return stack2.peek();
			}
		}

		public int pop() {
			int stack1Pop = stack1.pop();
			if(stack1Pop == min()) {
				stack2.pop();
			}
			return stack1Pop;
		}
	}

}
