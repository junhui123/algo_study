package StackQueue;

import java.util.ArrayList;
import java.util.Stack;

/**
 * SetOfStacks 구현
 * 여러 스택으로 구현 되며 이전 스택이 지정된 용량 초과 시 
 * 새로운 스택 생성
 * SetOfStacks.push / pop은 하나의 스택에서의 동작과 동일하게 구현
 * @author QWER
 *
 */
public class q3_3 {
	public static void main(String[] args) throws Exception {
		int capacity_per_substack = 5;
		SetOfStacks set = new SetOfStacks(capacity_per_substack);
		for (int i = 0; i < 34; i++) {
			set.push(i);
		}
		
//		for (int i = 0; i < 34; i++) {
//		for (int i = 0; i < 10; i++) {
//			System.out.println("Popped " + set.pop());
//		}	
		
		set.pop();
		set.pop();
		set.pop();
		set.popAt(3);
		
		for(int i=0; i<set.stackSet.size(); i++) {
			Stack<Integer> stk = set.stackSet.get(i);
			System.out.println("size = "+stk.size()+"=>"+stk.toString());
		}
	}
	
	private static class SetOfStacks {
		ArrayList<Stack<Integer>> stackSet = null;
		int capacity = 0;
		
		public SetOfStacks(int capacity_per_substack) {
			this.capacity = capacity_per_substack;
			stackSet = new ArrayList<>();
		}

		public int pop() {
			int lastIndex = stackSet.size()-1;
			Stack<Integer> oldStack = stackSet.get(lastIndex);
			int popValue = oldStack.pop();
			if(oldStack.isEmpty()) {
				stackSet.remove(oldStack);
			}
			return popValue;
		}

		public void push(int i) {
			if(stackSet.isEmpty()) {
				Stack<Integer> newStk = new Stack<>();
				newStk.push(i);
				stackSet.add(newStk);
			} else {
				int lastIndex = stackSet.size()-1;
				Stack<Integer> oldStack = stackSet.get(lastIndex);
				if(oldStack.size() == capacity) {
					Stack<Integer> newStk = new Stack<>();
					newStk.push(i);
					stackSet.add(newStk);
				} else {
					oldStack.push(i);
				}
			}
		}
	
		public int popAt(int index) throws Exception {
			int popAtValue = 0;
			if (!stackSet.isEmpty()) {
				int totalSize = (stackSet.size() * capacity) - (capacity - stackSet.get(stackSet.size() - 1).size());
				int stackCount = totalSize / capacity;

				int stackSetIndex = index / capacity;
				int stackIndex = index % capacity;

				if (stackSetIndex > stackCount) {
					throw new Exception("Index RangeOut");
				}

				Stack<Integer> oldStk = stackSet.get(stackSetIndex);
				popAtValue = oldStk.get(stackIndex);
				oldStk.removeElementAt(stackIndex);

				for (int i = stackSetIndex + 1; i<=stackCount; i++) {
					Stack<Integer> tmpStk1 = stackSet.get(i-1);
					if (tmpStk1.size() != capacity) {
						Stack<Integer> tmpStk2 = stackSet.get(i);
						int first = tmpStk2.firstElement();
						tmpStk1.push(first);
						tmpStk2.removeElement(first);
						if(tmpStk2.isEmpty()) {
							stackSet.remove(tmpStk2);
						}
					}
				}
			}
			return popAtValue;
		}
	}
}
