package StackQueue;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

/**
 * 두 개의 스택을 사용하여 Queue를 구현
 * 
 * @author QWER
 */
public class q3_5 {
	public static void main(String[] args) {

		MyQueue<Integer> my_queue = new MyQueue<Integer>();

		// Let's test our code against a "real" queue
		Queue<Integer> test_queue = new LinkedList<Integer>();

		for (int i = 0; i < 100; i++) {
			int choice = AssortedMethods.randomIntInRange(0, 10);
			if (choice <= 5) { // enqueue
				int element = AssortedMethods.randomIntInRange(1, 10);
				test_queue.add(element);
				my_queue.add(element);
				System.out.println("Enqueued " + element);
			} else if (test_queue.size() > 0) {
				int top1 = test_queue.remove();
				int top2 = my_queue.remove();
				if (top1 != top2) { // Check for error
					System.out.println("******* FAILURE - DIFFERENT TOPS: " + top1 + ", " + top2);
				}
				System.out.println("Dequeued " + top1);
			}

			if (test_queue.size() == my_queue.size()) {
				if (test_queue.size() > 0 && test_queue.peek() != my_queue.peek()) {
					System.out.println("******* FAILURE - DIFFERENT TOPS: " + test_queue.peek() + ", " + my_queue.peek()
							+ " ******");
				}
			} else {
				System.out.println("******* FAILURE - DIFFERENT SIZES ******");
			}
		}

	}

	// Queue -> FIFO
	// peek, pop 할 때 마다 stack1을 pop 하여 stack2로 push. stack2는 stack1의 반대가 되었으므로 peek,
	// pop 큐 처럼 가능
	// peek, pop이 끝나면 stack2를 pop 하면서 stack1으로 push
	// peek, pop 작업이 많아지면 오버헤드 증가
	// public static class MyQueue<T> {
	//
	// private Stack<T> stack1;
	// private Stack<T> stack2;
	//
	// public MyQueue() {
	// super();
	// this.stack1 = new Stack<>();
	// this.stack2 = new Stack<>();
	// }
	//
	// public int size() {
	// return stack1.size();
	// }
	//
	// public T peek() {
	// while (stack1.size() > 0) {
	// T num = (T) stack1.pop();
	// stack2.push(num);
	// }
	// T peekValue = stack2.peek();
	// stack1.removeAllElements();
	// while (stack2.size() > 0) {
	// T num = (T) stack2.pop();
	// stack1.push(num);
	// }
	// return peekValue;
	// }
	//
	// public void add(T value) {
	// stack1.push(value);
	// }
	//
	// public T remove() {
	// while (stack1.size() > 0) {
	// T num = (T) stack1.pop();
	// stack2.push(num);
	// }
	//
	// T removeValue = stack2.pop();
	// stack1.removeAllElements();
	// while (stack2.size() > 0) {
	// T num = (T) stack2.pop();
	// stack1.push(num);
	// }
	// return removeValue;
	// }
	// }

	// lazy 접근법 적용 - 반드시 뒤집어야 하는 경우 아니면 stack2에 그대로
	public static class MyQueue<T> {
		private Stack<T> stack1;
		private Stack<T> stack2;

		public MyQueue() {
			super();
			this.stack1 = new Stack<>();
			this.stack2 = new Stack<>();
		}

		public int size() {
			return stack1.size() + stack2.size();
		}

		public void add(T value) {
			stack1.push(value);
		}

		public T peek() {
			stackMove(stack1, stack2);
			return stack2.peek();
		}

		public T remove() {
			stackMove(stack1, stack2);
			return stack2.pop();
		}

		private void stackMove(Stack<T> stack1, Stack<T> stack2) {
			if (stack2.isEmpty()) {
				while (stack1.size() > 0) {
					stack2.push(stack1.pop());
				}
			}
		}
	}
}
