package StackQueue;

import java.util.Comparator;
import java.util.Stack;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

/**
 * 큰 값이 위에오는 스택 (오름차순 정렬)
 * 스택은 여벌로 추가적으로 하나 더  사용 가능. 보관된 요소 배열 등 다른 자료구조 복사 금지
 * 스택은 push, pop, peek, isEmpty 제공 (-> 내장 Stack 사용 가능)
 * @author QWER
 *
 */
public class q3_6 {
	static int c = 0;
	public static void main(String[] args) {
//		Stack<Integer> s2 = new Stack<>();
//		s2.push(582);
//		s2.push(808);
//		s2.push(84);
//		s2.push(106);
//		s2.push(556);
//		s2.push(360);
//		s2.push(996);
//		s2.push(119);
//		s2.push(749);
////		s2 = mergesort(s2);
//		s2 = sort(s2);
//		System.out.println(s2.toString());
		
		for (int k = 1; k < 100; k++) {
			c = 0;
			Stack<Integer> s = new Stack<Integer>();
			for (int i = 0; i < 10 * k; i++) {
				int r = AssortedMethods.randomIntInRange(0,  1000);
				s.push(r);
			}
			s = mergesort(s);
			int last = Integer.MAX_VALUE;
			while(!s.isEmpty()) {
				int curr = s.pop();
				if (curr > last) {
					System.out.println("Error: " + last + " " + curr);
				}
				last = curr;
			}
			System.out.println(c);
		}
	}
	
	/**
	 * 여벌 스택 하나만  추가적 사용
	 * 1.s에서 값을 pop
	 * 2.if(r is empty) r.push(s.pop)
	 * 3.r is not empty and r.peek > s.pop 인 경우  s.push(r.pop) 반복 //r은 작은값 부터 넣어야 되므로
	 * 4.r.peek < s.pop 인 경우 r에 존재하는 값은 s.pop 보다 작은 값들 이므로 r.push(s.pop)
	 * 
	 * @param s
	 * @return
	 */
	private static Stack<Integer> sort(Stack<Integer> s) {
		Stack<Integer> r = new Stack<>();
		
//		while(!s.isEmpty()) {
//			int s1peek = s.pop();
//			
//			if(r.isEmpty()) {
//				r.push(s1peek);
//			} else {
//				while (!r.isEmpty() && r.peek() > s1peek) {
//					s.push(r.pop());
//				}
//				r.push(s1peek);
//			}
//		}
		
		while (!s.isEmpty()) {
			int s1peek = s.pop();
			while (!r.isEmpty() && r.peek() > s1peek) {
				s.push(r.pop());
			}
			r.push(s1peek);
		}		
		
		return r;
	}
	
	
	/**
	 * 1. 리스트의 길이가 0 또는 1이면 이미 정렬된 것으로 본다. 그렇지 않은 경우에는
	 * 2. 정렬되지 않은 리스트를 절반으로 잘라 비슷한 크기의 두 부분 리스트로 나눈다.
	 * 3. 각 부분 리스트를 재귀적으로 합병 정렬을 이용해 정렬한다.
	 * 4. 두 부분 리스트를 다시 하나의 정렬된 리스트로 합병한다.
	 */
	//스택을 여러개 사용한다면 
	private static Stack<Integer> mergesort(Stack<Integer> s) {
		if(s.size() == 0 || s.size() == 1) {
			return s;
		}
		
		Stack<Integer> left = new Stack<>();
		Stack<Integer> right = new Stack<>();
		
		int count = 0;
		int stackHalf = s.size()/2;
		while(!s.isEmpty()) {
			if(count < stackHalf) {
				left.push(s.pop());
			} else {
				right.push(s.pop());
			}
			count++;
		}
		
		//divide
		left = mergesort(left);
		right = mergesort(right);
		
		//merge
		//left와 right 사이즈가 0 보다 큰 경우 순회 하면서 left.peek right.peek과 비교
		//left.peek > right.peek 인 경우 right.pop s에 삽입 => 틀림. 스택은 LIFO 이므로 작은값이 먼저 나오기 위해 큰 값을 먼저 넣어야함.
		//left.peek < right.peek 인 경우 left.pop s에 삽입 => 틀림. 스택은 LIFO 이므로 작은값이 먼저 나오기 위해 큰 값을 먼저 넣어야함.
		while (left.size() > 0 || right.size() > 0) {
			if (left.isEmpty()) {
				s.push(right.pop());
			} else if (right.isEmpty()) {
				s.push(left.pop());
			} else if (left.peek() > right.peek()) {
				s.push(left.pop());
			} else {
				s.push(right.pop());
			}
		}
		
		Stack<Integer> reverseStack = new Stack<Integer>();
		while (s.size() > 0) {
			c++;
			reverseStack.push(s.pop());
		}
		
		return reverseStack;
	}


}
