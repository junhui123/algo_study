package StackQueue;

import java.util.LinkedList;

import Ctci.Question3_7.Animal;
import Ctci.Question3_7.Cat;
import Ctci.Question3_7.Dog;

/**
 * 먼저 들어온 동물이 먼저 나가는 쉼터 구현
 * 
 * @author QWER
 *
 */
public class q3_7 {
	public static void main(String[] args) {
		AnimalQueue animals = new AnimalQueue();
		animals.enqueue(new Cat("Callie"));
		animals.enqueue(new Cat("Kiki"));
		animals.enqueue(new Dog("Fido"));
		animals.enqueue(new Dog("Dora"));
		animals.enqueue(new Cat("Kari"));
		animals.enqueue(new Dog("Dexter"));
		animals.enqueue(new Dog("Dobo"));
		animals.enqueue(new Cat("Copa"));
		
		System.out.println(animals.dequeueAny().name());	
		System.out.println(animals.dequeueAny().name());	
		System.out.println(animals.dequeueAny().name());	
		
		animals.enqueue(new Dog("Dapa"));
		animals.enqueue(new Cat("Kilo"));
		
		System.out.println("deque Cat :"+animals.dequeueCats().name);
		System.out.println("deque Dog :"+animals.dequeueDogs().name);
		System.out.println("deque Cat :"+animals.dequeueCats().name);
		System.out.println("deque Dog :"+animals.dequeueDogs().name);
		
		while (animals.size() != 0) {
			System.out.println(animals.dequeueAny().name());	
		}
	}
	
	private static class AnimalQueue {

		LinkedList<Dog> dog = new LinkedList<>();
		LinkedList<Cat> cat = new LinkedList<>();
		int order = 0;
		
		public int size() {
			return dog.size()+cat.size();
		}
		
		public void enqueue(Animal animal) {
			animal.setOrder(order);
			order++;
			
			if(animal instanceof Dog) {
				dog.addLast((Dog) animal);
			} else if(animal instanceof Cat) {
				cat.addLast((Cat) animal);
			}
		}

		public Dog dequeueDogs() {
			Dog firsDog = dog.getFirst();
			dog.removeFirst();
			return firsDog;
		}

		public Cat dequeueCats() {
			Cat firstCat = cat.getFirst();
			cat.removeFirst();
			return firstCat;
		}

		public Animal dequeueAny() {
			if (dog.isEmpty()) {
				return dequeueCats();
			} else if (cat.isEmpty()) {
				return dequeueDogs();
			} else {
				Dog firstDog = dog.peek();
				Cat firstCat = cat.peek();

				Animal any;
				if (firstDog.getOrder() > firstCat.getOrder()) {
					any = dequeueDogs();
				} else {
					any = dequeueCats();
				}

				return any;
			}
		}
	}
}
