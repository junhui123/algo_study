package String;

import java.util.HashSet;

/**
 * 문자열에 포함된 문자들이 전부 유일한지 검사하는 알고리즘. 다른 자료구조를 사용할 수 없는 상황이라면?
 * 
 * @author QWER
 *
 */
public class q1_1 {
	
	public static void main(String[] args) {
		String[] words = {"abcde", "hello", "apple", "kite", "padle"};
		for(String s : words) {
			System.out.println(s+" is unique (set) ? "+isAllCharUnique(s));
		}
		System.out.println("================");
		
		for(String s : words) {
			System.out.println(s+" is unique (array) ? "+isAllCharUniqueNotSet(s));
		}
	}

	//Set을 사용
	public static boolean isAllCharUnique(String s) {
		HashSet<Character> charSet = new HashSet<>();
		
		for(int i=0; i<s.length(); i++) {
			char tmp = s.charAt(i);
			if(!charSet.contains(tmp)) {
				charSet.add(s.charAt(i)); 
			} else {
				return false;
			}
		}
		return true;
	}
	
	public static boolean isAllCharUniqueNotSet(String s) {
		for (int i = 0; i < s.length() - 1; i++) {
			char c1 = s.charAt(i);
			char c2 = s.charAt(i + 1);

			if (c1 == c2) {
				return false;
			}
		}
		return true;
	}
}
