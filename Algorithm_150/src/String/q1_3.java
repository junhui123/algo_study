package String;

import java.util.Arrays;
import java.util.HashMap;

/**
 * 문자열 2개를 입력받아 그중 하나가 다른 하나의 순열인지 판단
 * 
 * 대소문자, 공백 구별 중요
 * @author QWER
 *
 */
public class q1_3 {
	public static void main(String[] args) {
		String[][] pairs = {{"apple", "papel"}, {"carrot", "tarroc"}, {"hello", "llloh"}};
		
		long start = System.nanoTime();
		boolean[] result1 = isPerm(pairs);
		for(int i=0; i<result1.length; i++) {
			System.out.println(pairs[i][0]+","+pairs[i][1]+" => "+result1[i]);
		}
		System.out.println(System.nanoTime()-start);
		System.out.println("");
		
		System.out.println("isPerm2");
		start = System.nanoTime();
		for (String[] pair : pairs) {
			String word1 = pair[0];
			String word2 = pair[1];
			boolean anagram = isPerm2(word1, word2);
			System.out.println(word1 + ", " + word2 + ": " + anagram);
		}
		System.out.println(System.nanoTime()-start);
		System.out.println("");

		start = System.nanoTime();
		boolean[] result2 = isPermCharCount(pairs);
		for(int i=0; i<result2.length; i++) {
			System.out.println(pairs[i][0]+","+pairs[i][1]+" => "+result2[i]);
		}
		System.out.println(System.nanoTime()-start);
		System.out.println("");
		
		System.out.println("isPermCount2");
		start = System.nanoTime();
		for (String[] pair : pairs) {
			String word1 = pair[0];
			String word2 = pair[1];
			boolean anagram = isPermCharCount2(word1, word2);
			System.out.println(word1 + ", " + word2 + ": " + anagram);
		}
		System.out.println(System.nanoTime()-start);
		System.out.println("");
		
		
		start = System.nanoTime();
		for (String[] pair : pairs) {
			String word1 = pair[0];
			String word2 = pair[1];
			boolean anagram = permutation(word1, word2);
			System.out.println(word1 + ", " + word2 + ": " + anagram);
		}
		System.out.println(System.nanoTime()-start);
		System.out.println("");
		
		start = System.nanoTime();
		for (String[] pair : pairs) {
			String word1 = pair[0];
			String word2 = pair[1];
			boolean anagram = anagram(word1, word2);
			System.out.println(word1 + ", " + word2 + ": " + anagram);
		}
		System.out.println(System.nanoTime()-start);
		System.out.println("");
		
		start = System.nanoTime();
		for (String[] pair : pairs) {
			String word1 = pair[0];
			String word2 = pair[1];
			boolean anagram = permutation2(word1, word2);
			System.out.println(word1 + ", " + word2 + ": " + anagram);
		}
		System.out.println(System.nanoTime()-start);
		System.out.println("");
		

		
	}
	
	//정렬을 사용. 실용적 효율성이 중요하다면 다른 방법 사용
	public static boolean[] isPerm(String[][] pairs) {
		boolean[] result = new boolean[pairs.length];
		for(int i=0; i<pairs.length; i++) {
			String s1 = pairs[i][0];
			String s2 = pairs[i][1];
			
			//공백 => 특정문자료 변경 후 비교
			if(s1.contains(" ")) {
				s1.replaceAll(" ", "_");	
			}
			
			if(s2.contains(" ")) {
				s2.replaceAll(" ", "_");	
			}
			
			//대소문자 =>전체 대문자/소문자  변경 후 비교
			s1.toUpperCase();
			s2.toUpperCase();
			
			//정렬 사용 후 값 비교
			char[] s1toCharArr= s1.toCharArray();
			char[] s2toCharArr= s2.toCharArray();
			Arrays.sort(s1toCharArr);
			Arrays.sort(s2toCharArr);
			s1 = Arrays.toString(s1toCharArr);
			s2 = Arrays.toString(s2toCharArr);
			
			if(!s1.equals(s2)) {
				result[i] = false;
			} else {
				result[i] = true;
			}
		}
		return result;
	}
	
	public static boolean isPerm2(String s1, String s2) {
			//공백 => 특정문자료 변경 후 비교
			if(s1.contains(" ")) {
				s1.replaceAll(" ", "_");	
			}
			
			if(s2.contains(" ")) {
				s2.replaceAll(" ", "_");	
			}
			
			//대소문자 =>전체 대문자/소문자  변경 후 비교
			s1.toUpperCase();
			s2.toUpperCase();
			
			//정렬 사용 후 값 비교
			char[] s1toCharArr= s1.toCharArray();
			char[] s2toCharArr= s2.toCharArray();
			Arrays.sort(s1toCharArr);
			Arrays.sort(s2toCharArr);
			s1 = Arrays.toString(s1toCharArr);
			s2 = Arrays.toString(s2toCharArr);
			
			if(!s1.equals(s2)) {
				return false;
			} else {
				return true;
			}
	}

	//문자 카운트 중복되는 횟수만큼 비교 
	public static boolean[] isPermCharCount(String[][] pairs) {
		boolean[] result = new boolean[pairs.length];

		for(int i=0; i<pairs.length; i++) {
			String s1 = pairs[i][0];
			String s2 = pairs[i][1];
			
			if(s1.length() != s2.length()) {
				continue;
			}
			
			HashMap<Character, Integer> s1Map = new HashMap<>();
			HashMap<Character, Integer> s2Map = new HashMap<>();
			char[] s1toCharArr=s1.toCharArray();
			char[] s2toCharArr=s2.toCharArray();
			for(int z=0; z<s1toCharArr.length; z++) {
				char c1 = s1toCharArr[z];
				char c2 = s2toCharArr[z];
				
				if(!s1Map.containsKey(c1)) {
					s1Map.put(c1, 1);
				} else {
					s1Map.put(c1, (s1Map.get(c1))+1);
				}
				
				if(!s2Map.containsKey(c2)) {
					s2Map.put(c2, 1);
				} else {
					s2Map.put(c2, (s2Map.get(c2))+1);
				}
			}
			
			boolean flag = false;
			for(int z=0; z<s1toCharArr.length; z++) {
				char c = s1toCharArr[z];
				try {
					int c1Count = s1Map.get(c);
					if(!s2Map.containsKey(c)) {
						flag=false;
						break;
					}
					int c2Count = s2Map.get(c);
					if(c1Count != c2Count) {
						flag = false;
						break;
					}  else {
						flag = true;
					}
				} catch(NullPointerException e ) {
					e.printStackTrace();
				}
			}
			result[i] = flag;
		}
		return result;
	}
	
	public static boolean isPermCharCount2(String s1, String s2) {
			if(s1.length() != s2.length()) {
				return false;
			}
			
			HashMap<Character, Integer> s1Map = new HashMap<>();
			HashMap<Character, Integer> s2Map = new HashMap<>();
			char[] s1toCharArr=s1.toCharArray();
			char[] s2toCharArr=s2.toCharArray();
			for(int z=0; z<s1toCharArr.length; z++) {
				char c1 = s1toCharArr[z];
				char c2 = s2toCharArr[z];
				
				if(!s1Map.containsKey(c1)) {
					s1Map.put(c1, 1);
				} else {
					s1Map.put(c1, (s1Map.get(c1))+1);
				}
				
				if(!s2Map.containsKey(c2)) {
					s2Map.put(c2, 1);
				} else {
					s2Map.put(c2, (s2Map.get(c2))+1);
				}
			}
			
			boolean flag = false;
			for(int z=0; z<s1toCharArr.length; z++) {
				char c = s1toCharArr[z];
				try {
					int c1Count = s1Map.get(c);
					if(!s2Map.containsKey(c)) {
						flag=false;
						break;
					}
					int c2Count = s2Map.get(c);
					if(c1Count != c2Count) {
						flag = false;
						break;
					}  else {
						flag = true;
					}
				} catch(NullPointerException e ) {
					e.printStackTrace();
				}
			}
			return flag;
	}
	
	public static String sort(String s) {
		char[] content = s.toCharArray();
		java.util.Arrays.sort(content);
		return new String(content);
	}
	
	public static boolean permutation(String s, String t) {
		return sort(s).equals(sort(t));
	}
	
	public static boolean anagram(String s, String t) {
		if (s.length() != t.length())
			return false;
		int[] letters = new int[128];
		int num_unique_chars = 0;
		int num_completed_t = 0;
		char[] s_array = s.toCharArray();
		for (char c : s_array) { // count number of each char in s.
			if (letters[c] == 0)
				++num_unique_chars;
			++letters[c];
		}
		for (int i = 0; i < t.length(); ++i) {
			int c = (int) t.charAt(i);
			if (letters[c] == 0) { // Found more of char c in t than in s.
				return false;
			}
			--letters[c];
			if (letters[c] == 0) {
				++num_completed_t;
				if (num_completed_t == num_unique_chars) {
					// it�s a match if t has been processed completely
					return true;
					//return i == t.length() - 1;
				}
			}
		}
		return false;
	}	
	
	public static boolean permutation2(String s, String t) {
		if (s.length() != t.length()) {
			return false;
		}
		
		int[] letters = new int[128]; //가정
		 
		char[] s_array = s.toCharArray();
		for (char c : s_array) { // count number of each char in s.
			letters[c]++;  
		}
		  
		for (int i = 0; i < t.length(); i++) {
			int c = (int) t.charAt(i);
		    if (--letters[c] < 0) {
		    	return false;
		    }
		}
		  
		return true;
	}
}
