package String;

/**
 * 모든 공백을 %20으로 바꾸는 메소드 
 * 문자열 끝에 추가로 필요한 문자 더 할 공간 있다고 가정
 * 공백을 포함하는 문자열 길이 함께 주어짐
 * 문자 배열 사용하여 필요한 연산 각 문자에 바로 적용 가능 하도록 
 * @author QWER
 *
 */
public class q1_4 {
	public static void main(String[] args) {
		long start = System.nanoTime();
		String str = "abc d e f";
		System.out.println(replaceSpaces(str, str.length()));
		System.out.println(System.nanoTime()-start);

		start = System.nanoTime();
		String str1 = "abc d e f";
		System.out.println(replaceBlank(str1.length(), str1));
		System.out.println(System.nanoTime()-start);
	}
	
	public static String replaceBlank(int n, String str) {
		System.out.print("find blank time : ");
		long start = System.nanoTime();
		char[] charArr = str.toCharArray();
		int blankCount = 0;
		for(int i=0; i<n; i++) {
			if( charArr[i] == ' ') {
				blankCount++;
			}
		}
		
		char[] newCharArr = new char[n-blankCount+(blankCount*3)];
		int charrIndex = 0;
		for(int i=0; i<newCharArr.length; i++) {
			char c = charArr[charrIndex++];
			if( c == ' ') {
				newCharArr[i] = '%';
				newCharArr[i+1] = '2';
				newCharArr[i+2] = '0';
				i = i+2;
			} else {
				newCharArr[i] = c;
			}
		}
		System.out.println(System.nanoTime()-start);
		
		return String.valueOf(newCharArr);
	}
	
	public static String replaceSpaces(String s, int length) {
		System.out.print("find blank time : ");
		long start = System.nanoTime();
		char[] arr = s.toCharArray();
		int spaceCount = 0, index, i = 0;
		for (i = 0; i < length; i++) {
			if (arr[i] == ' ') {
				spaceCount++;
			}
		}
		
		char[] str = new char[s.length() + 3 * 2 + 1];
		for (i = 0; i < s.length(); i++) {
			str[i] = s.charAt(i);
		}
		
		index = length + spaceCount * 2;
		str[index] = '\0'; //\0 == null
		for (i = length - 1; i >= 0; i--) {
			if (str[i] == ' ') {
				str[index - 1] = '0';
				str[index - 2] = '2';
				str[index - 3] = '%';
				index = index - 3;
			} else {
				str[index - 1] = str[i];
				index--;
			}
		}
		System.out.println(System.nanoTime()-start);
		
		
		return String.valueOf(str);
	}
	
}
