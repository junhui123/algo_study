package String;

/**
 * 문자열 압축
 * 압축 결과 원래 문자열 보다 긴 경우 원래 문자열 반환
 * @author QWER
 *
 */
public class q1_5 {
	public static void main(String[] args) {
		String str = "aabccccccccaaa";
		String zipStr = "a2b1c8a3";
//		String str = "bacbbcc";
//		String zipStr = "b1a1c1b2c2";
		
		String result = getZipStr(str);
		System.out.println(zipStr.equals(result)); 
		
		String result2 = getZipStrUseStrBuf(str);
		System.out.println(zipStr.equals(result2)); 
		
	}
	
	//O(n^2)
	public static String getZipStr(String str) {
		
		//문자열 반복 하면서 지금 인덱스와 다음 인덱스의 문자가 같은 것을 찾는다.
		//같다면 카운트 값을 올리고 반환 문자열에 추가한다.
		//다음 인덱스는 위에서 찾음 다음 인덱스 부터 시작 하도록 한다.
		
		long start = System.nanoTime();
		
		String result = "";
		int count = 1;
		for(int i=0; i<str.length()-1; i++) {
			char c1 = str.charAt(i);
			char c2 = str.charAt(i+1);
			
			if(c1==c2) {
				count++;
			} else {
				result += c1+String.valueOf(count);
				count = 1;
			}
			
			if(str.length()-1 == i+1) {
				result += c1+String.valueOf(count);
			}
		}
		
		//압축 결과 원래 문자열 보다 긴 경우 원래 문자열 반환
		if(str.length() < result.length()) {
			System.out.println(System.nanoTime()-start);
			return str;
		}
		
		System.out.println(System.nanoTime()-start);
		
		return result;
	}
	
	//O(n^2)
	public static String getZipStrUseStrBuf(String str) {
		long start = System.nanoTime();

		StringBuffer result = new StringBuffer();
		int count = 1;
		for(int i=0; i<str.length()-1; i++) {
			char c1 = str.charAt(i);
			char c2 = str.charAt(i+1);
			
			if(c1==c2) {
				count++;
			} else {
				result.append(c1+String.valueOf(count));
				count = 1;
			}
			
			if(str.length()-1 == i+1) {
				result.append(c2+String.valueOf(count));
			}
		}
		
		//압축 결과 원래 문자열 보다 긴 경우 원래 문자열 반환
		if(str.length() < result.length()) {
			System.out.println(System.nanoTime()-start);
			return str;
		}
		
		System.out.println(System.nanoTime()-start);
		
		return result.toString();
	}
	
	//스트링 버퍼 사용하는 알고리즘 수정 O(n)이 되도록
	public static String getZipStrChangeAlgo(String str) {
		long start = System.nanoTime();
		StringBuffer result = new StringBuffer();
		char last = str.charAt(0);
		int count = 1;
		for(int i=1; i<str.length(); i++) {
			if(str.charAt(i) == last) {
				count++;
			} else {
				result.append(last);
				result.append(String.valueOf(count));
				last = str.charAt(i);
				count = 1;
			}
		}
		
		result.append(last);
		result.append(count);
		String resultStr=result.toString();
		if(resultStr.length() > str.length()) {
			resultStr=str;
		}
		System.out.println(System.nanoTime()-start);
		return resultStr;
	}
}
