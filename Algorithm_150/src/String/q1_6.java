package String;

/**
 * NxN 이미지 행렬 회전 
 * 픽셀은 4바이트, 90도 회전. 부가적인 행렬 사용하지 않고서?
 * @author QWER
 *
 */
public class q1_6 {
	public static void main(String[] args) {
//		int[][] matrix = randomMatrix(10, 0, 9);
		int[][] matrix = new int[][] {
			{1,5,1,5,2,9,7,5,6,9},
			{6,9,5,8,9,3,4,9,7,8},
			{1,9,1,9,5,3,8,8,7,3},
			{5,1,1,6,6,7,2,1,8,1},
			{3,2,1,9,8,4,3,6,2,2},
			{1,6,6,3,8,7,2,1,4,6},
			{7,6,8,9,2,2,2,1,8,4},
			{2,6,1,5,9,8,3,6,7,5},
			{5,2,1,5,3,8,9,4,7,8},
			{7,5,9,3,7,6,1,2,5,7}
		};
		printMatrix(matrix);
		System.out.println("");
		rotate(matrix);
//		printMatrix(matrix);
		printMatrixIndex(matrix);
	}
	
	public static int[][] randomMatrix(int N, int min, int max) {
		int[][] matrix = new int[N][N];
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < N; j++) {
				matrix[i][j] = (int) (Math.random() * max + 1 - min)+min;
			}
		}
		return matrix;
	}
	
	public static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			int[] row = matrix[i];
			for (int j = 0; j < row.length; j++) {
				if(j==row.length-1)
					System.out.print(row[j]);
				else
					System.out.print(row[j]+" ");
					
			}
			System.out.println("");
		}
	}
	
	public static void printMatrixIndex(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			int[] row = matrix[i];
			for (int j = 0; j < row.length; j++) {
				if(j==row.length-1)
					System.out.print("["+i+"]["+j+"]");
				else
					System.out.print("["+i+"]["+j+"] ");
			}
			System.out.println("");
		}
	}
	
	public static void rotate(int[][] matrix) {

	}
}
