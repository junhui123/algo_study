package String;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;

/**
 * MxN 행렬 한 원소가 0 인 경우 해당 원소가 속한 행과 열의 모든 원소를 0으로
 * @author QWER
 *
 */
public class q1_7 {
	public static void main(String[] args) {
		int nrows = 10;
		int ncols = 15;
		int[][] matrix = AssortedMethods.randomMatrix(nrows, ncols, 0, 100);	
//		int[][] matrix = {
//				{4,4,5,3,8,4,7,5,8,2},
//				{1,9,1,5,2,6,8,7,0,0},
//				{1,6,2,8,9,6,4,9,4,8},
//				{1,2,9,7,8,0,7,4,0,8},
//				{8,8,4,5,4,7,7,6,1,3},
//				{2,4,8,0,3,0,1,5,6,4},
//				{5,3,2,0,3,0,5,4,3,5},
//				{6,9,0,8,7,8,1,2,1,7},
//				{2,9,1,7,9,6,4,9,4,2},
//				{4,8,1,3,8,4,0,9,3,5}
//		};
		
		printMatrix(matrix);
		System.out.println("");
		changeRowCol(nrows, ncols, matrix);
		System.out.println("");
		printMatrix(matrix);
	}
	
	public static int[][] randomMatrix(int M, int N, int min, int max) {
		int[][] matrix = new int[M][N];
		for (int i = 0; i < M; i++) {
			for (int j = 0; j < N; j++) {
				matrix[i][j] =(int) ((Math.random() * (max + 1 - min))+min); 
			}
		}
		return matrix;
	}
	
	public static void printMatrix(int[][] matrix) {
		for (int i = 0; i < matrix.length; i++) {
			int[] row = matrix[i];
			for (int j = 0; j < row.length; j++) {
				if(j==row.length-1)
					System.out.print(row[j]);
				else
					System.out.print(row[j]+" ");
					
			}
			System.out.println("");
		}
	}
	
	public static void changeRowCol( int nrows, int ncols, int[][] matrix) {
		
		int row = matrix.length;
		int col = matrix[matrix.length-1].length;
		int[][] tmp = new int[row][col];
		
		for(int i=0; i<tmp.length; i++) {
			for(int j=0; j<tmp[i].length; j++) {
				if(matrix[i][j] == 0 ) {
					tmp[i][j] = -1; 
				}
			}
		}
		
		for(int i=0; i<tmp.length; i++) {
			for(int j=0; j<tmp[i].length; j++) {
				if(tmp[i][j] == -1 ) {
					changeZero(i, j, nrows, ncols, matrix);
				} 
			}
		}
	}
	
	private static void changeZero(int row, int col, int nrows, int ncols, int[][] tmp) {
		for(int z=0; z<nrows; z++) {
			tmp[z][col] = 0;
		}
		
		for(int z=0; z<ncols; z++) {
			tmp[row][z] = 0;
		}
	}
}
