package String;
/**
 * 한 단어가 다른 단어에 포함된 문자열 인지 판별 isSubString 메서드 
 * s1, s2 스트링 있을때 s2가 s1을 회전시킨 결과인지 판별하는 코드 isSubString 한 번만 호출 해서 작동
 * waterbottle - erbottlewat
 * @author QWER
 *
 */
public class q1_8 {
	public static void main(String[] args) {
//		String s1 = "waterbottle";
//		String s2 = "erbottlewat";
//		String s1 = "whatsup";
//		String s2 = "upwhats";
//		String s1 = "uestionQ";
//		String s2 = "Question";

		String[][] pairs = {{"apple", "pleap"}, {"waterbottle", "erbottlewat"}, {"camera", "macera"}, {"whatsup", "upwhats"}, {"uestionQ", "Question"}, {"abbb", "ddda"}};
		for(int i=0; i<pairs.length; i++) {
			System.out.println(isSubString(pairs[i][0], pairs[i][1]));
		}
		System.out.println("========================");
		for (String[] pair : pairs) {
			String word1 = pair[0];
			String word2 = pair[1];
			boolean is_rotation = isRotation(word1, word2);
			System.out.println(word1 + ", " + word2 + ": " + is_rotation);
		}
		
	}
	
	//문자열 분할 후 결합해서 같은지 확인
	public static boolean isSubString(String s1, String s2) {
		long start = System.nanoTime();
		
		char s1start = s1.charAt(0);
		
		int endindex = 0;
		for(int idxS2=0; idxS2<s1.length(); idxS2++) {
			char c = s2.charAt(idxS2);
			if(c==s1start) {
				endindex = idxS2;
				break;
			}
		}
		
		String s2Sub1 = s2.substring(0, endindex);
		String s2Sub2 = s2.substring(endindex, s2.length());
		String tmp = s2Sub2+s2Sub1;
		
		if(s1.equals(tmp)) {
			System.out.println(System.nanoTime()-start);
			return true;
		}
		System.out.println(System.nanoTime()-start);
		return false;
	}
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public static boolean isSubstring(String big, String small) {
		if (big.indexOf(small) >= 0) {
			return true;
		} else {
			return false;
		}
	}
	
	//문자열 s1+s1에 s2가 속하는지 검사.
	//x = wat, y == erbottle
	//xy = waterbottle, yx = erbottlewat
	//xyxy = waterbottlewatbottle 은 부분집합 yx = erobttlewat가 속함
	public static boolean isRotation(String s1, String s2) {
		long start = System.nanoTime();
	    int len = s1.length();
	    /* check that s1 and s2 are equal length and not empty */
	    if (len == s2.length() && len > 0) { 
	    	/* concatenate s1 and s1 within new buffer */
	    	String s1s1 = s1 + s1;
	    	boolean result = isSubstring(s1s1, s2);
	    	System.out.println(System.nanoTime()-start);
	    	return result;
	    }
	    System.out.println(System.nanoTime()-start);
	    return false;
	}
}
