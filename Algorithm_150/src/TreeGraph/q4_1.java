package TreeGraph;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;
import Ctci.CtCILibrary.CtCILibrary.TreeNode;

/**
 * 주어진 이진 트리가 균형 이진 트리인지 판별하는 함수 구현
 * (4.1의 이진 트리는 어떤 노드의 두 자식 트리 깊이가 하나 이상 차이 나지 않는다)
 * 1) 왼쪽 높이 - 오른쪽 높이 <=1 ( 균형 트리 중 AVL 트리)
 * 2) BST의 겨우 중위 순회 하여 값이 순차 정렬 될 경우 균형 이진 트리
 * @author QWER
 *
 */
public class q4_1 {
	public static void main(String[] args) {
		// Create balanced tree
		int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		TreeNode root = TreeNode.createMinimalBST(array);
		System.out.println("Root? " + root.data);
		System.out.println("Is balanced? " + isBalanced(root));
		System.out.println("Is balanced? " + isBalanced2(root));

		TreeNode unbalanced = new TreeNode(10);
		for (int i = 0; i < 10; i++) {
			unbalanced.insertInOrder(AssortedMethods.randomIntInRange(0, 100));
		}
		System.out.println("Root? " + unbalanced.data);
		System.out.println("Is balanced? " + isBalanced(unbalanced));
		System.out.println("Is balanced? " + isBalanced2(unbalanced));
		
	}
	
	public static boolean isBalanced2(TreeNode root) {
		if (root == null) {
			return true;
		}
		boolean check = checkBalancedTree(root) >= 1 ? true : false;
		return check;
	}
	
	//왼쪽 높이 - 오른쪽 높이 <=1 ( 균형 트리 중 AVL 트리)
	//O(n)
	public static int checkBalancedTree(TreeNode root) {
		if (root == null)
			return 0;
		int left = 0;
		int right = 0;
		if (root.left != null || root.right != null) {
			left += checkBalancedTree(root.left);
			if (left == -1)
				return -1;
			right += checkBalancedTree(root.right);
			if (right == -1)
				return -1;
		}
		int result = Math.abs(left - right) > 1 ? -1 : (left > right ? left + 1 : right + 1);
		return result;
	}

	//절대값(왼쪽 높이 - 오른쪽 높이) > 1 ( 균형 트리 중 AVL 트리)
	//O(nlogN)
	public static int getHeight(TreeNode root) {
		if (root == null) {
			return 0;
		}
		return Math.max(getHeight(root.left), getHeight(root.right)) + 1;
	}
		
	public static boolean isBalanced(TreeNode root) {
		if (root == null) {
			return true;
		}
		
		int left = getHeight(root.left);
		int right = getHeight(root.right);
		int heightDiff = left - right;
		if (Math.abs(heightDiff) > 1) {
			return false;
		}
		else {
			return isBalanced(root.left) && isBalanced(root.right);
		}
	}
}
