package TreeGraph;

import java.util.LinkedList;
import Ctci.Question4_2.Node;
import Ctci.Question4_2.Question.State;
import Ctci.Question4_2.Graph;

/**
 * 주어진 방향 그래프에서 특정 두 노드간의 경로 존재하는지 판별
 * 
 * @author QWER
 *
 */
public class q4_2 {
	
	public static void main(String[] args) {
		Graph g = createNewGraph();
		Node[] n = g.getNodes();
		Node start = n[3];
		Node end = n[5];
		System.out.println(search(g, start, end));
	}
	
	//어떻게 하면 그래프의 모든 정점을 방문할 것 인가!
	//그래프 탐색 - DFS, BFS 사용
	//BFS 사용 search
	public static boolean search(Graph g, Node start, Node end) {
		LinkedList<Node> queue = new LinkedList<>();

		for (Node u : g.getNodes()) {
			u.state = State.Unvisited;
		}
		start.state = State.Visited;
		queue.add(start);
		Node u;
		while (!queue.isEmpty()) {
			u = queue.removeLast();
			if (u != null) {
				for (Node new_v : u.getAdjacent()) {
					if (new_v.state == State.Unvisited) {
						if (new_v == end) {
							return true;
						} else {
							new_v.state = State.Visited;
							queue.add(new_v);
						}
					}
				}
				u.state = State.Visited;
			}
		}
		return false;
	}

	public static Graph createNewGraph() {
		Graph g = new Graph();
		Node[] temp = new Node[6];

		temp[0] = new Node("a", 3);
		temp[1] = new Node("b", 0);
		temp[2] = new Node("c", 0);
		temp[3] = new Node("d", 1);
		temp[4] = new Node("e", 1);
		temp[5] = new Node("f", 0);

		temp[0].addAdjacent(temp[1]);
		temp[0].addAdjacent(temp[2]);
		temp[0].addAdjacent(temp[3]);
		temp[3].addAdjacent(temp[4]);
		temp[4].addAdjacent(temp[5]);
		for (int i = 0; i < 6; i++) {
			g.addNode(temp[i]);
		}
		return g;
	}
}
