package TreeGraph;

import java.util.ArrayList;

import Ctci.CtCILibrary.CtCILibrary.TreeNode;

/**
 * 오름차순 정렬 배열로 높이가 가장 낮은 이진 탐색 트리 생성
 * (배열내 원소는 유일한 값)
 * 최소 높이 트리 생성
 * 
 * BST = 오른쪽은 항상 부모 노드 보다 큰 값, 왼쪽은 작은 값
 *
 * @author QWER
 *
 */
public class q4_3 {
	public static void main(String[] args) {
		
		int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		
		// We needed this code for other files, so check out the code in the library
		TreeNode root = createMinimalBST(array, 0, array.length-1);
		System.out.println("Root? " + root.data);
		System.out.println(isBST(root, array));
		
		root = createBalancedHeapTree(array);
		System.out.println("Root? " + root.data);
		System.out.println(isBST(root, array));
		
	}
	
	private static TreeNode createMinimalBST(int[] array, int start, int end) {
//		
//		StringBuffer sbuf = new StringBuffer();
//		sbuf.append("f(");
//		sbuf.append(start+",");
//		sbuf.append(end + ") = ");
//		if(start > end)
//			sbuf.append("null");
//		else
//			sbuf.append(array[(start+end)/2]);
//		
//		System.out.println(sbuf.toString());
//		
		if(start > end) 
			return null;
		
		int mid = (start+end)/2;
		TreeNode root = new TreeNode(array[mid]);
		TreeNode left = createMinimalBST(array, start, mid-1);
		TreeNode right = createMinimalBST(array, mid+1, end);
		root.setLeftChild(left);
		root.setRightChild(right);
		return root;
	}

	//Balanced Binary Heap Tree.... 
	private static TreeNode createBalancedHeapTree(int[] array) {
		TreeNode[] nodes = new TreeNode[array.length];
		for (int i = 0; i < array.length; i++) {
			nodes[i] = new TreeNode(array[i]);
		}
		
		for (int i = 0; i < nodes.length; i++) {
			int left = (2*i)+1;
			int right = (2*i)+2;
			nodes[i].setLeftChild(left >= nodes.length ? null : nodes[left]);
			nodes[i].setRightChild(right >= nodes.length ? null : nodes[right]);
		}
		
		TreeNode root = nodes[0];

		return root;
	}

	public static boolean isBST(TreeNode root, int[] array) {
		ArrayList<TreeNode> inOrderList = new ArrayList<>();
		inOrder(root, inOrderList);
		
		int listIndex = 0;
		for(int i : array) {
			TreeNode n = inOrderList.get(listIndex);
			if(n.data != i) {
				return false;
			}
			listIndex++;
		}
		
		return true;
	}
	
	private static ArrayList<TreeNode> inOrder(TreeNode root, ArrayList<TreeNode> inOrderList) {
		if(root == null) {
			return inOrderList;
		}
		
		inOrder(root.left, inOrderList);
		inOrderList.add(root);
		inOrder(root.right, inOrderList);
		
		return inOrderList;
	}
}
