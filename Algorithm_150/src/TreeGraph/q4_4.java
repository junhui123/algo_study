package TreeGraph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;
import Ctci.CtCILibrary.CtCILibrary.TreeNode;

/**
 * 주어진 이진 트리 깊이별 연결 리스트 만들어 내는 알고리즘 작성
 * (깊이 값 = D 인 경우 얀결 리스트 생성 D개 생성) 
 * @author QWER
 *
 */

public class q4_4 {
	public static void main(String[] args) {
		int[] nodes_flattened = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		TreeNode root = AssortedMethods.createTreeFromArray(nodes_flattened);
//		System.out.println(getHeight(root)); //4
//		ArrayList<LinkedList<TreeNode>> dfslist = createLevelLinkedList(root, "DFS");
//		printArrayList(dfslist);
		
		ArrayList<LinkedList<TreeNode>> bfslist = createLevelLinkedList(root, "BFS");
		printArrayList(bfslist);
	}

	private static ArrayList<LinkedList<TreeNode>> createLevelLinkedList(TreeNode root, String mode) {
		ArrayList<LinkedList<TreeNode>> list = new ArrayList<>();
		
		
		int height = 0;
		if (mode.equals("DFS")) {
			list = createModeDFS(root, list, height);
		} else {
			list = createModeBFS(root);
		}
		
		
		Object obj = list.indexOf(list.get(0));
		Object obj2 = list.indexOf(null);
		obj.toString();

		
		return list;
	}

	private static ArrayList<LinkedList<TreeNode>> createModeDFS(TreeNode root, ArrayList<LinkedList<TreeNode>> list, int height) {
		if(root == null) return list;
		
		LinkedList<TreeNode> linkedList = null;
		if(list.size() == height) {
			linkedList = new LinkedList<>();
			list.add(linkedList);
		} else {
			linkedList = list.get(height);
		}
		
		linkedList.add(root);
		height++;
		createModeDFS(root.left, list, height);
		createModeDFS(root.right, list, height);
		 
		return list;
	}
	
	/**
	 * BFS - 같은 높이 부터 탐색
	 */
	private static ArrayList<LinkedList<TreeNode>> createModeBFS(TreeNode root) {
		ArrayList<LinkedList<TreeNode>> result = new ArrayList<LinkedList<TreeNode>>();
		
		/* "Visit" the root */
		LinkedList<TreeNode> current = new LinkedList<TreeNode>();
		if (root != null) {
			current.add(root);
		}
		
		while (current.size() > 0) {
			result.add(current); // Add previous level
			LinkedList<TreeNode> parents = current; // Go to next level
			current = new LinkedList<TreeNode>(); 
			for (TreeNode parent : parents) {
				/* Visit the children */
				if (parent.left != null) {
					current.add(parent.left);
				}
				if (parent.right != null) {
					current.add(parent.right);
				}
			}
		}
		return result;
	}
	
	private static int getHeight(TreeNode root) {
		if(root == null) return 0;
		return 1+Math.max(getHeight(root.left), getHeight(root.right));
	}
	
	private static void printArrayList(ArrayList<LinkedList<TreeNode>> list) {
		for(int i=0; i<list.size(); i++) {
			LinkedList<TreeNode> tmp = list.get(i);
			StringBuffer sBuf = new StringBuffer();
			for(int z=0; z<tmp.size(); z++) {
				sBuf.append(tmp.get(z).data+" ");
			}
			System.out.println(sBuf.toString());
		}
	}
}
