package TreeGraph;

import java.util.ArrayList;

import Ctci.CtCILibrary.CtCILibrary.TreeNode;
import Ctci.Question4_5.*;
/**
 * 어떤 이진 트리가 이진 탐색 트리 인지 판별
 * @author QWER
 *
 */
public class q4_5 {
	public static void main(String[] args) {
		/* Simple test -- create one */
		int[] array = {Integer.MIN_VALUE, 3, 5, 6, 10, 13, 15, Integer.MAX_VALUE};
		TreeNode node = TreeNode.createMinimalBST(array);
		//node.left.data = 6; // "ruin" the BST property by changing one of the elements
//		node.print();
		
		boolean isBst = isBST(node);
		System.out.println(isBst);
		isBst = isBST2(node);
		System.out.println(isBst);
		
		/* More elaborate test -- creates 100 trees (some BST, some not) and compares the outputs of various methods. */
		for (int i = 0; i < 100; i++) {
			TreeNode head = QuestionB.createTestTree();
			
			// Compare results 
			boolean isBst1 = Question.checkBST(head);
			boolean isBst2 = QuestionB.checkBSTAlternate(head);
			boolean isBstMy1 = isBST(head);
			boolean isBstMy2 = isBST2(head);
			
			if (isBstMy1 != isBstMy2 ) {
				System.out.println("*********************** ERROR ******************* ==>" + isBst1 + " | " + isBst2 +" ==> My "+isBstMy1+" | " +isBstMy2);
				isBST(head);
				isBST2(head);
				break;
			} else {
				System.out.println(isBstMy1 + " | " + isBstMy2 + " ==> My "+isBstMy1+" | " +isBstMy2);
			}
		}
		
	}
	
	//중위순회 결과 = 트리 오름차순 정렬된것과 같음. 정렬이 옵바른지 확인 
	public static boolean isBST(TreeNode root) {
		ArrayList<TreeNode> inOrderList = new ArrayList<>();
		inOrder(root, inOrderList);
		
		int inOrderMin = inOrderList.get(0).data;
		for(int i=1; i<inOrderList.size(); i++) {
			TreeNode n = inOrderList.get(i);
			if(inOrderMin > n.data) {
				return false;
			}
			inOrderMin = n.data;
		}
		return true;
	}
	
	private static ArrayList<TreeNode> inOrder(TreeNode root, ArrayList<TreeNode> inOrderList) {
		if(root == null) {
			return inOrderList;
		}
		
		inOrder(root.left, inOrderList);
		inOrderList.add(root);
		inOrder(root.right, inOrderList);
		
		return inOrderList;
	}
	
	//BST 특성 이용 - 왼쪽 서브 트리는 루트 보다 작거나 같고 오른쪽 서브 트리는 루트보다 크거나 같다
	//부모 및 부모의 부모 상위 전부 보다 작거나 커야 함..!!
	//이진 트리 특성도 가지고 있으므로 최대 자식은 2개까지 가능. 자식이 없어도 BST 가능
	public static boolean isBST2(TreeNode root) {
		if(root==null || root.left == null || root.right == null) {
			return true;
		}
		
		isBST2(root.left);
		Integer rootData = root.data;
		Integer leftData = root.left.data;
		Integer rightData = root.right.data;
		if( !(leftData<=rootData && rootData<=rightData) ) {
			return false;
		} 
		isBST2(root.right);
		return true;
	}
}
