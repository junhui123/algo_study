package TreeGraph;

import Ctci.CtCILibrary.CtCILibrary.TreeNode;

/**
 * 중위순회. BST 안의 한 노드가 주어지면 그 노드의 다음 노드 찾아내는 코드 각 노드는 부모 링크 정보 가지고 있음
 * ==>Successor 문제
 * 
 * @author QWER
 *
 */
public class q4_6 {
	public static void main(String[] args) {
		int[] array = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
		TreeNode root = TreeNode.createMinimalBST(array);
		for (int i = 0; i < array.length; i++) {
			TreeNode node = root.find(array[i]);
			TreeNode next = inorderSucc(node);
			if (next != null) {
				System.out.println(node.data + "->" + next.data);
			} else {
				System.out.println(node.data + "->" + null);
			}
		}
	}

	private static TreeNode inorderSucc(TreeNode node) {
		if (node.right != null) {
			return minNode(node.right);
		}

		TreeNode y = node.parent;
		while (y != null && node == y.right) {
			node = y;
			y = y.parent;
		}
		return y;

	}

	private static TreeNode minNode(TreeNode node) {
		if (node == null)
			return null;

		while (node.left != null) {
			node = node.left;
		}

		return node;
	}
}
