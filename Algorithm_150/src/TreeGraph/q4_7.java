package TreeGraph;

import Ctci.CtCILibrary.CtCILibrary.TreeNode;

/**
 * 공통 조상 
 * (자료구조 내에 부가적인 노드 저장 금지)
 * @author QWER
 *
 */
public class q4_7 {
	public static void main(String[] args) {
		int[] array = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
		TreeNode root = TreeNode.createMinimalBST(array);
		TreeNode n3 = root.find(1);
		TreeNode n7 = root.find(3);
		TreeNode ancestor1 = commonAncestorNodeKnowParent(root, n3, n7);
		System.out.println(ancestor1.data);
		TreeNode ancestor2 = commonAncestor(root, n3, n7);
		System.out.println(ancestor2.data);
	}

	//노드가 부모를 알고 있는 경우만 사용 가능
	private static TreeNode commonAncestorNodeKnowParent(TreeNode root, TreeNode n1, TreeNode n2) {
		if(n1 == null || n2 == null) 
			return null;

		TreeNode prevN1 = null;
		TreeNode currN1 = null;
		TreeNode currN2 = null;
		if(n1.parent == n2.parent) {
			return n1.parent;
		} else {
			if(prevN1 == null && currN1 == null) {
				prevN1 = n1.parent;
				currN1 = n1.parent;
			}
			
			if(currN2 == null) {
				currN2 = n2.parent;
			}
			
			while(prevN1 != currN2) {
				prevN1 = currN1;
				if(currN1 != null)
					currN1 = currN1.parent;
				if(currN2 != null)
					currN2 = currN2.parent;
				
				if((prevN1==null && currN2 == null )) {
					return root;
				} else if( prevN1 == currN2 ) {
					return currN2;
				} else if( currN1 == currN2) {
					return currN2;
				}
			}
		}
		return currN2;
	}
	
	private static TreeNode commonAncestor(TreeNode root, TreeNode n1, TreeNode n2) {
		TreeNode curr = root;
		while(curr != null) {
			int currValue = curr.data;
			
			if(n1.data < currValue && n2.data < currValue) {
				curr = curr.left;
			}
			
			else if(n1.data > currValue && n2.data > currValue) {
				curr = curr.right;
			}
			else {
				return curr;
			}
		}
		return null;
	}
	
}
