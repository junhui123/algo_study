package TreeGraph;

import java.util.ArrayList;

import Ctci.CtCILibrary.CtCILibrary.AssortedMethods;
import Ctci.CtCILibrary.CtCILibrary.TreeNode;

/**
 * 트리 T1, T2가 있을때 T2가 T1의 하위 트리인지 판별
 * T1안에 노드 n이 있어 n의 하위 트리가 T2와 동일하다면 하위 트리 이다. n 부터 시작하는 서브 트리를 분할 하면 T2와 같다.
 * @author QWER
 */
public class q4_8 {
	public static void main(String[] args) {
		int[] array1 = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13 };
		int[] array2 = { 2, 4, 5, 8, 9, 10, 11 };

		TreeNode t1 = AssortedMethods.createTreeFromArray(array1);
		TreeNode t2 = AssortedMethods.createTreeFromArray(array2);

		if (containsTreeUseInOrderTraversal(t1, t2))
			System.out.println("t2 is a subtree of t1");
		else
			System.out.println("t2 is not a subtree of t1");

		// t4 is not a subtree of t3
		int[] array3 = { 1, 2, 3 };
		TreeNode t3 = AssortedMethods.createTreeFromArray(array1);
		TreeNode t4 = AssortedMethods.createTreeFromArray(array3);

		if (containsTreeUseInOrderTraversal(t3, t4))
			System.out.println("t4 is a subtree of t3");
		else
			System.out.println("t4 is not a subtree of t3");
		
		System.out.println("\n//////////////////////////////////////////////////////////////////////\n");
		
		if (containsTree(t1, t2))
			System.out.println("t2 is a subtree of t1");
		else
			System.out.println("t2 is not a subtree of t1");

		// t4 is not a subtree of t3
		if (containsTree(t3, t4))
			System.out.println("t4 is a subtree of t3");
		else
			System.out.println("t4 is not a subtree of t3");
	}

	private static boolean containsTree(TreeNode t1, TreeNode t2) {
		
		if(t1 == null) return false; //큰 트리가 비어 있으면 하위 트리 찾을 수 없음
		if(t2 == null) return true; //빈 트리는 언제나 하위 트리 
		
		return subTree(t1, t2);
	}

	private static boolean subTree(TreeNode t1, TreeNode t2) {
		if(t1 == null) return false; //Base Case, 큰 트리가 비어 있으면 하위 트리 찾을 수 없음
		
		if(t1.data == t2.data) { 
			if(matchTree(t1, t2)) {
				return true;
			}
		}
		
		return (subTree(t1.left, t2) || subTree(t1.right, t2));
	}

	//노드의 왼쪽/오른쪽 자식을 자식이 없는 리프 노드(자식 없는 노드)가 나올때까지 비교 
	private static boolean matchTree(TreeNode t1, TreeNode t2) {
		
		if(t1==null && t2==null) { //둘 다 비어있는 경우 (빈 트리는 언제나 하위 트리)
			return true;
		}
		
		if(t1==null || t2==null) { //둘 중 하나만 비어 있는 경우
			return false;
		}
		
		if(t1.data != t2.data) {
			return false;
		}
		
		return ( matchTree(t1.left, t2.left) || matchTree(t1.left, t2.left) );
	}

	/** 
	 *     N1    N1
	 *    /		  \
	 *  N2          N2
	 *  위의 2경우는 서로 다른 트리이지만 
	 *  중위 순회 결과 N1-N2, N1-N2로 같다. 
	 *  이런 경우 문제가 된다.
	 *  
	 *  중위 순회 하면서 root가 null인 경우도 리스트에 추가 함으로써 위와 같은 문제는 해결 가능
	 *  if(root == null) return list; 
	 *    ↓
	 *  if(root == null) {
			TreeNode nullNode = new TreeNode(0);
			list.add(nullNode);
			return list;
		}
		
		트리가 크면 클 수록 아래와 같은 방법은 메모리 사용량이 커짐.
	 */
	private static boolean containsTreeUseInOrderTraversal(TreeNode t1, TreeNode t2) {
		
		ArrayList<TreeNode> t1InOrder = new ArrayList<>();
		t1InOrder = inOrder(t1, t1InOrder);
		ArrayList<TreeNode> t2InOrder = new ArrayList<>();
		t2InOrder = inOrder(t2, t2InOrder);

		StringBuffer t1Buf = new StringBuffer();
		StringBuffer t2Buf = new StringBuffer();
		
		for(TreeNode n : t1InOrder) {
			t1Buf.append(n.data+" ");
		}
		
		for(TreeNode n : t2InOrder) {
			t2Buf.append(n.data+" ");
		}
		
		if(t1Buf.toString().contains(t2Buf.toString()))
			return true;
		
		return false;
	}
	
	
	public static ArrayList<TreeNode> inOrder(TreeNode root, ArrayList<TreeNode> list)  {
		if(root == null) {
			TreeNode nullNode = new TreeNode(0);
			list.add(nullNode);
			return list;
		}
		
		inOrder(root.left, list);
		list.add(root);
		inOrder(root.right, list);
		
		return list;
	}
}
