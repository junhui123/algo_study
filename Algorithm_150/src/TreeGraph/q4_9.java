package TreeGraph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Stack;

import Ctci.CtCILibrary.CtCILibrary.TreeNode;

/**
 * 이진 트리 하나와 값 n이 주어졌을때 n과 같은 값을 갖는 모든 경로를 찾아라.  
 * 경로의 값은 그 경로에 포함된 모든 노드의 값의 합. 경로는 트리내 아무 위치에서나 시작 하고 종료 가능
 * n = 경로의 값(경로에 포함된 모든 노드의 값의 합)
 * @author QWER
 *
 */
public class q4_9 {
	public static void main(String[] args) {
		TreeNode root = new TreeNode(5);
		root.left = new TreeNode(3);
		root.right = new TreeNode(1);
		root.left.left = new TreeNode(4);
		root.left.right = new TreeNode(8);
		root.right.left = new TreeNode(2);
		root.right.right = new TreeNode(6);
		
		//루트값 합에 포함 시키는 경우는 가능 
		//5-3,8,5-1-2 만족
		findSum(root, 8, 0);
		//5-3-4,5-1-6 에서 3-4, 1-6 합은 7이지만 출력 안됨
		findSum(root, 7, 0);
		
		findSum(root, 7);
		
	}
	public static void findSum(TreeNode node, int sum, int[] path, int level) {
		if (node == null) {
			return;
		}
		
		/* Insert current node into path */
		path[level] = node.data; 
		
		//path[]에 존재하는 값이 sum과 같은지 확인
		int t = 0;
		for (int i = level; i >= 0; i--){
			t += path[i];
			if (t == sum) {
				print(path, i, level);
			}
		}
		
		findSum(node.left, sum, path, level + 1);
		findSum(node.right, sum, path, level + 1);
		
		/* Remove current node from path. Not strictly necessary, since we would
		 * ignore this value, but it's good practice.
		 */
		path[level] = Integer.MIN_VALUE; 
	}
	
	public static int depth(TreeNode node) {
		if (node == null) {
			return 0;
		} else {
			return 1 + Math.max(depth(node.left), depth(node.right));
		}
	}
	
	public static void findSum(TreeNode node, int sum) {
		int depth = depth(node);
		int[] path = new int[depth];
		findSum(node, sum, path, 0);
	}
	
	private static void print(int[] path, int start, int end) {
		for (int i = start; i <= end; i++) {
			System.out.print(path[i] + " ");
		}
		System.out.println();
	}

	//경로는 트리내 아무 위치에서나 시작 하고 종료 가능 조건을 만족하지 못함
	//합 7이 되는 경로를 찾는 경우 5에서 시작 시 5-3-4 경로 중 3-4가 만족, 5-1-6 또한 1-6이 만족
	//아래 풀이는 루트값까지 포함하는 경로에 대해서만 작동
	private static Stack<TreeNode> STACK = new Stack<>();
	private static void findSum(TreeNode root, int sum, int prevSum) {
		if(root == null) return;
		
		STACK.push(root);
		sumNode(root.left, sum, root.data);
		
		if(!STACK.isEmpty()) {
			STACK.removeAllElements();
		}
		STACK.push(root);
		sumNode(root.right, sum, root.data);
	}
	
	private static void sumNode(TreeNode root, int sum, int prevSum) {
		if(root == null) return;
		
		STACK.push(root);
		prevSum += root.data;
		if(root.data == sum) {
			System.out.println(" "+root.data);
			prevSum = 0;
			STACK = new Stack<>();
		} else {
			if(prevSum == sum) {
				StringBuffer sBuf = new StringBuffer();
				while(!STACK.isEmpty()) {
					sBuf.append(STACK.pop().data+" ");
				}
				System.out.println(sBuf.reverse().toString());
				prevSum = 0;
				STACK = new Stack<>();
			}
		}	
		sumNode(root.left, sum,prevSum);
		sumNode(root.right, sum, prevSum);
	}
	
	//미완성
	private static void findSumUseBFS(TreeNode root, int sum) {
		ArrayList<LinkedList<TreeNode>> result = new ArrayList<LinkedList<TreeNode>>();

		/* "Visit" the root */
		LinkedList<TreeNode> current = new LinkedList<TreeNode>();
		if (root != null) {
			current.add(root);
		}

		Stack stk = new Stack();
		while (current.size() > 0) {
			result.add(current); // Add previous level
			LinkedList<TreeNode> parents = current; // Go to next level
			current = new LinkedList<TreeNode>();
			for (TreeNode parent : parents) {
				/* Visit the children */
				if (parent.left != null) {
					current.add(parent.left);
				}
				if (parent.right != null) {
					current.add(parent.right);
				}
			}
		}
	}
}
