package Array.ArrayBaseStack;

public class ArrayBaseStack {
	private static int STACK_LEN = 0;
	public static void main(String[] args) {
		STACK_LEN = 3;
		Object[] arrs = createStack(STACK_LEN);
		
		push(arrs, 0, 1);
		push(arrs, 0, 2);
		push(arrs, 0, 3);
		push(arrs, 1, 4);
		push(arrs, 1, 5);
		push(arrs, 1, 6);
		push(arrs, 2, 7);
		push(arrs, 2, 8);
		push(arrs, 2, 9);
		
		pop(arrs, 0);
		pop(arrs, 1);
		pop(arrs, 2);
		pop(arrs, 2);
		
	}

public static Object[] createStack(int statckLengh) {
	int arrTotalSize = statckLengh * statckLengh;
	Object[] arr = new Object[arrTotalSize];
	return arr;
}

public static Object[] push(Object[] arrs, int stackNum, int value) {
	int tmp = stackNum * STACK_LEN;
	int inputCount = 0;
	for(int i=tmp; i<arrs.length; i++) {
		if(inputCount == STACK_LEN) {
			break;
		}
		inputCount++;
		
		if(arrs[i] == null) {
			arrs[i] = value;
			break;
		}
		
	}
	return arrs;
}

public static Object[] pop(Object[] arrs, int stackNum) {
	int tmp = stackNum * STACK_LEN;
	for(int i=tmp+STACK_LEN-1; i>0; i--) {
		if( arrs[i] == null ) {
			continue;
		} else {
			arrs[i] = null;
			break;
		}
	}
	return arrs;
}
}
 