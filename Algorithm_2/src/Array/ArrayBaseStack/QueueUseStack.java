package Array.ArrayBaseStack;

import java.util.Stack;

public class QueueUseStack {
	public static void main(String[] args) throws Exception {
		
		MyQueue queue = new MyQueue();
		queue.enqueue(1);
		queue.enqueue(2);
		queue.enqueue(3);
		queue.enqueue(4);

		int size = queue.size();
		for(int i=0; i<5; i++) {
			System.out.println(queue.dequeue());
		}
		
	}

	static class MyQueue {
		private int size = 0; 
		private Stack stack_enqueue = new Stack();
		private Stack stack_dequeue = new Stack();

		public void enqueue(int number) {
			stack_enqueue.push(number);
		}

		public int dequeue() throws Exception {
			while (stack_enqueue.size() > 0) {
				int num = (int) stack_enqueue.pop();
				stack_dequeue.push(num);
			}
			
			if(stack_dequeue.isEmpty()) {
				throw new Exception("Empty Queue Exception");
			} else {
				int result = (int) stack_dequeue.pop();
				return result;
			}
			
		}
		
		public int size() {
			size = stack_enqueue.size() > stack_dequeue.size() ? stack_enqueue.size() : stack_dequeue.size();
			return size; 
		}
	}
}
