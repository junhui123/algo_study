package Array.ArrayBaseStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class SetOfStack {

	public static void main(String[] args) {
		StackList stackList = new StackList(3);
		
		for(int i=1; i<9; i++) {
			stackList.push(i);
		}
		
		for(int i=1; i<9; i++) {
			stackList.pop();
		}
	}
}

class StackList {
	private List stackList;
	private int each_stack_size = 3;

	public StackList(int num) {
		each_stack_size = num;
	}

	public StackList() {
	}

	public void push(int value) {
		if (stackList == null || stackList.isEmpty()) {
			stackList = new ArrayList();
			Stack newStack = new Stack();
			newStack.push(value);
			stackList.add(newStack);
		} else {
			Stack lastStack = getLastStack();

			if (lastStack.size() < each_stack_size) {
				lastStack.push(value);
			} else {
				Stack newStack = new Stack();
				newStack.push(value);
				stackList.add(newStack);
			}
		}
	}

	public void pop() {
		Stack lastStack = getLastStack();
		if (lastStack != null) {
			if (lastStack.isEmpty()) {
				stackList.remove(lastStack);
			} else {
				lastStack.pop();
			}
		}
	}

	public Stack getLastStack() {
		return (Stack) stackList.get(stackList.size() - 1);
	}
}

