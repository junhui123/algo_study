package Array;

import java.util.Stack;

/**
 * push, pop, min O(1) 시간복잡도 연산
 * 
 * @author QWER
 *
 */
public class PushPopMin {

	public static int MIN = 0;
	public static void main(String[] args) {
		Stack stack = new Stack();
		Stack minStack = new Stack();
		push(stack, minStack, 7);
		push(stack, minStack, 4);
		push(stack, minStack, 5);
		push(stack, minStack, 3);
		System.out.println(min());
		
		pop(stack, minStack);
		pop(stack, minStack);
		System.out.println(min());
		pop(stack, minStack);
		
		System.out.println(min());
		
	}
	
	public static void push(Stack stack, Stack minStack, int num) {
		stack.push(num);
		
		if(minStack.size() > 0 ) {
			int min = (int) minStack.peek();
			if(min > num ) {
				minStack.push(num);
				MIN = num;
			}
		} else {
			minStack.push(num);
		}
	}
	
	public static void pop(Stack stack, Stack minStack) {
		stack.pop();
		MIN = (int) minStack.pop();
	}
	
	public static int min() {
		return MIN;
	}
}
