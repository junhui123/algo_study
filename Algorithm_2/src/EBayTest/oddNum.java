package EBayTest;

import java.util.ArrayList;
import java.util.Arrays;

public class oddNum {
	public static void main(String[] args) {
		
		int[] r = oddNumbers(3, 9);
		System.out.println(Arrays.toString(r));

	}
	
    public static int[] oddNumbers(int l, int r) {
    	
    	ArrayList<Integer> tmpList = new ArrayList<>();
        for(int start=l; start<=r; l++) {
        	if(start % 2 == 0) {
        		tmpList.add(start);
        	}
        }
        
        int[] result = new int[tmpList.size()];
        int index = 0;
        for(int n : tmpList) {
        	result[index++] = n;
        }
        
        return result;

    }
}
