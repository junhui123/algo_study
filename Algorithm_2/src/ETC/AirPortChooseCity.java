package ETC;

public class AirPortChooseCity {
	public int chooseCity(int n, int[][] city) {
		int answer = 0;

		int[] distanceArr = new int[city.length];
		int distance = 0;
		for (int i = 0; i < city.length; i++) {
			int[] cityInfo = city[i];

			for (int z = 0; z < city.length; z++) {
				if (i == z) {
					continue;
				}
				distance += Math.abs((city[z][0] - cityInfo[0])) * city[z][1];
			}
			distanceArr[i] = distance;
			distance = 0;
		}

		int minIndex = 0;
		for (int a = 0; a < distanceArr.length; a++) {
			if (distanceArr[minIndex] > distanceArr[a]) {
				minIndex = a;
			}
		}

		answer = city[minIndex][0];

		return answer;
	}

	public int chooseCity2(int n, int[][] city) {
		int answer = 0;

		int[] distanceArr = new int[city.length];
		int distance = 0;
		for (int i = 0; i < city.length; i++) {
			int z = n - 1;
			while (z >= 0) {
				int cityz = city[z][0];
				int cityzP = city[z][1];
				int cityi = city[i][0];

				if (cityz == cityi) {
					z--;
					continue;
				}

				distance += Math.abs(cityz - cityi) * cityzP;
				z--;
			}
			distanceArr[i] = distance;
			distance = 0;
		}

		int minIndex = 0;
		for (int a = 0; a < distanceArr.length; a++) {
			if (distanceArr[minIndex] > distanceArr[a]) {
				minIndex = a;
			}
		}

		answer = city[minIndex][0];

		return answer;
	}

	public static void main(String[] args) {
		AirPortChooseCity test = new AirPortChooseCity();

		long start = System.nanoTime();
		int tn2 = 4;
		int[][] tcity2 = { { 1, 5 }, { 3, 2 }, { 5, 3 }, { 8, 4 } };
		System.out.println(test.chooseCity(tn2, tcity2));
		System.out.println(System.nanoTime() - start);

		start = System.nanoTime();
		int tn = 3;
		int[][] tcity = { { 1, 5 }, { 2, 2 }, { 3, 3 } };
		System.out.println(test.chooseCity(tn, tcity));
		System.out.println(System.nanoTime() - start);

		start = System.nanoTime();
		System.out.println(test.chooseCity2(tn2, tcity2));
		System.out.println(System.nanoTime() - start);

		start = System.nanoTime();
		System.out.println(test.chooseCity2(tn, tcity));
		System.out.println(System.nanoTime() - start);

	}
}
