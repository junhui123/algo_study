package ETC;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

public class CollectionsSortTest {
	public static void main(String[] args) {
		ArrayList<int[] > tmpList = new ArrayList<>();
		tmpList.add(new int[] {1,2,3});
		tmpList.add(new int[] {2,1,3});
		tmpList.add(new int[] {3,2,1});
		tmpList.add(new int[] {1,3,2});
		tmpList.add(new int[] {3,1,2});
		tmpList.add(new int[] {2,3,1});
		
		tmpList.sort(new Comparator<int[]>() {

			@Override
			public int compare(int[] o1, int[] o2) {
				
				String v1 = Arrays.toString(o1);
				String v2 = Arrays.toString(o2);
				
//				return v1.compareTo(v2); //오름차순 (작은것->큰것)
				return v2.compareTo(v1); //내림차순 (큰것->작은것)
			}
		});
		
		for(int i=0; i<tmpList.size(); i++) {
			int[] arr = tmpList.get(i);
			System.out.println(Arrays.toString(arr));
		}
	}
}
