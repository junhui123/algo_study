package ETC;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

class LineCombination {
	public int[] setAlign(int n, long k) {
		int[] answer = {};

		int[] peoples = new int[n];
		for (int i = 0; i < n; i++) {
			peoples[i] = i + 1;
		}

//		answer = perm(peoples, answer, 0, k);

		
		perm2(peoples, 0, peoples.length, peoples.length, k);

		return answer;
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	public int[] perm(int[] peoples, int[] answer, int n, long count) {

		if (n == peoples.length) {
			if(count==0) {
				answer = peoples.clone();
				return peoples;
			} else {
				return peoples;
			}
		}

		for (int i = n; i < peoples.length; i++) {
			int tmp = peoples[n];
			peoples[n] = peoples[i];
			peoples[i] = tmp;

			perm(peoples, answer, n+1, count--);

			tmp = peoples[n];
			peoples[n] = peoples[i];
			peoples[i] = tmp;
		}

		return peoples;
	}

	public void perm2(int[] arr, int depth, int n, int k, long count) {
		if (depth == k) { // 한번 depth 가 k로 도달하면 사이클이 돌았음. 출력함.
			System.out.println(Arrays.toString(arr));
			return;
		}
		for (int i = depth; i < n; i++) {
			swap(arr, i, depth);
			perm2(arr, depth + 1, n, k, --count);
			swap(arr, i, depth);
		}
	} // 자바에서는 포인터가 없기 때문에 아래와 같이, 인덱스 i와 j를 통해 바꿔줌.

	public void swap(int[] arr, int i, int j) {
		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}

	public void print(int[] arr, int k) {
		for (int i = 0; i < k; i++) {
			if (i == k - 1)
				System.out.println(arr[i]);
			else
				System.out.print(arr[i] + ",");
		}
	}

	// 아래는 테스트로 출력해 보기 위한 코드입니다.
	public static void main(String[] args) {
		LineCombination lc = new LineCombination();
		System.out.println(Arrays.toString(lc.setAlign(3, 5)));
	}
}
