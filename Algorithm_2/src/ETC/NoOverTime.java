package ETC;

public class NoOverTime {
	public int noOvertime(int no, int[] works) {
		int result = 0;
		// 야근 지수를 최소화 하였을 때의 야근 지수는 몇일까요?

		while (no > 0) {
			no--;
			int maxIndex = maxValueIndex(works);
			works[maxIndex] = works[maxIndex] - 1;
		}

		for (int i = 0; i < works.length; i++) {
			int worksDouble = works[i] * works[i];
			result += worksDouble;
		}

		return result;
	}
	
	public int maxValueIndex(int[] works) {
		int maxValueIndex = 0;
		for(int i=0; i<works.length; i++) {
			if(works[maxValueIndex] < works[i]) {
				maxValueIndex = i;
			}
		}
		return maxValueIndex;
	}

	public static void main(String[] args) {
		NoOverTime c = new NoOverTime();
//		int[] test = { 4, 3, 3 };
//		System.out.println(c.noOvertime(4, test));
		int[] test = { 10, 8, 7 };
		System.out.println(c.noOvertime(5, test));
	}
}