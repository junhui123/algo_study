package List.Circle.Double;

public class Node {

	private int val;
	private Node next;
	private Node prev;

	public Node(int val) {
		super();
		this.val = val;
		this.next = null;
		this.prev = null;
	}

	public Node(int val, Node next, Node prev) {
		super();
		this.val = val;
		this.next = next;
		this.prev = prev;
	}

	public int getVal() {
		return val;
	}

	public void setNext(Node next) {
		this.next = next;
	}

	public Node getNext() {
		return next;
	}

	public void setPrev(Node prev) {
		this.prev = prev;
	}

	public Node getPrev() {
		return prev;
	}
}
