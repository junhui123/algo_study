package List.Circle.Single;

public class Test {
	public static void main(String[] args) {
		
		SingleLinkedList list = new SingleLinkedList();
		list.add(1, 0);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(5);
		list.add(6);
		list.add(7);
		
		System.out.println(list.toString());

		Node n1 = list.getNodeByIndex(-1);
		System.out.println(n1.getVal());
		
		list.remove();
		System.out.println(list.toString());
		
		list.remove();
		System.out.println(list.toString());
		
		list.remove(0);
		System.out.println(list.toString());
		
		list.remove(2);
		System.out.println(list.toString());

		System.exit(0);
	}
}
