package List.Double;

import java.util.Arrays;

public class DoubleLinkedList {

	private Node head;
	private Node tail;

	public DoubleLinkedList() {
		super();
		this.head = null;
		this.tail = null;
	}

	public void add(int number) {
		if (head == null) {
			Node node = new Node(number);
			head = node;
			tail = node;
		} else {
			Node node = new Node(number);
			node.setPrev(tail);
			tail.setNext(node);
			tail = node;
		}
	}

	public void add(int number, int index) {
		if (index < 0) {
			System.out.println("index invalid");
			return;
		}

		if (index == 0) {
			Node tmp = new Node(number);
			tmp.setNext(head);
			head = tmp;
		} else {
			Node tmp = getNode(index);
			Node tmpPrev = tmp.getPrev();
			Node newNode = new Node(number);
			newNode.setNext(tmp);
			newNode.setPrev(tmpPrev);
			tmpPrev.setNext(newNode);
			tmp.setPrev(newNode);
		}
	}

	public Node getNode(int index) {

		int start = 0;
		Node tmp = head;
		while (tmp.getNext() != null && start < index) {
			tmp = tmp.getNext();
			start++;
		}

		return tmp;
	}

	public void remove() {
		Node tailPrev = tail.getPrev();
		tailPrev.setNext(null);
		tail = tailPrev;
	}

	public void remove(int index) {
		Node tmp = getNode(index);

		if (tmp == tail) {
			remove();
			return;
		} 
		else if(tmp == head) {
			Node tmpHeadNext = head.getNext();
			head.setNext(null);
			head = tmpHeadNext;
		}
		else {
			Node tmpPrev = tmp.getPrev();
			Node tmpNext = tmp.getNext();
			tmpNext.setPrev(tmpPrev);
			tmpPrev.setNext(tmpNext);
		}
	}

	public int indexOf(int index) {
		return getNode(index) != null ? 0 : -1;
	}

	public String toString(Node node) {
		return String.valueOf(node.getVal());
	}

	public String toString() {
		int arrSize = 0;
		Node tmp = head;
		while (tmp.getNext() != null) {
			tmp = tmp.getNext();
			arrSize++;
		}

		int[] arrs = new int[arrSize+1];
		arrs[0] = head.getVal();
		int arrStarIndex = 1;
		tmp = head;
		while (tmp.getNext() != null) {
			tmp = tmp.getNext();
			arrs[arrStarIndex++] = tmp.getVal();
		}
		return String.valueOf(Arrays.toString(arrs));
	}
}
