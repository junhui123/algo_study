package List.Double;

public class Test {
	public static void main(String[] args) {
		
		DoubleLinkedList list = new DoubleLinkedList();
		
		list.add(0);
		list.add(1);
		list.add(2);
		list.add(3);
		list.add(4);
		list.add(6);
		list.add(5, 5);
		list.add(7, 3);
		
		System.out.println(list.toString());
		
		list.remove();
		System.out.println(list.toString());
		list.remove();
		System.out.println(list.toString());
		
		list.remove(0);
		System.out.println(list.toString());

		list.remove(2);
		System.out.println(list.toString());
		
		
		System.exit(0);
	}
}
