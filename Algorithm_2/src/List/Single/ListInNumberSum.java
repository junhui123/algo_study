package List.Single;

import java.util.Stack;

public class ListInNumberSum {
	public static void main(String[] args) {
		Node a_7 = new Node(7, null);
		Node a_1 = new Node(1, null);
		Node a_6 = new Node(6, null);
		a_7.setNext(a_1);
		a_1.setNext(a_6);
		
		Node b_5 = new Node(5, null);
		Node b_9 = new Node(9, null);
		Node b_2 = new Node(2, null);
		b_5.setNext(b_9);
		b_9.setNext(b_2);
		
		System.out.println(sum(a_7, b_5));
	}
	
	public static int sum(Node a, Node b) {
		
		Stack<Integer> aS = new Stack<>();
		Stack<Integer> bS = new Stack<>();
		aS.push(a.getVal());
		while(a.getNext() != null) {
			a = a.getNext();
			aS.push(a.getVal());
		}
		
		bS.push(b.getVal());
		while(b.getNext() != null) {
			b = b.getNext();
			bS.push(b.getVal());
		}
		
		String tmpStrA = "";
		String tmpStrB = "";
		
		while(aS.size()>0) {
			tmpStrA += aS.pop();
		}
		
		while(bS.size()>0) {
			tmpStrB += bS.pop();
		}
		
		int numA  = Integer.valueOf(tmpStrA);
		int numB = Integer.valueOf(tmpStrB);
		int result = numA+numB;
		
		return result;
	}
}
