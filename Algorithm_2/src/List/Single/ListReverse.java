package List.Single;

import java.util.Stack;

public class ListReverse {
	public static void main(String[] args) {
		Node a_7 = new Node(7, null);
		Node a_1 = new Node(1, null);
		Node a_6 = new Node(6, null);
		a_7.setNext(a_1);
		a_1.setNext(a_6);

		Node newroot = reverseNoStack(a_7);
	}

	public static Node reverse(Node root) {
		Stack<Node> nStack = new Stack<>();
		nStack.push(root);

		while (root.getNext() != null) {
			root = root.getNext();
			nStack.push(root);
		}

		Node reverse = nStack.pop();
		Node newRoot = reverse;
		while (nStack.size() > 0) {
			Node tmp = nStack.pop();
			reverse.setNext(tmp);
			reverse = tmp;
		}
		reverse.setNext(null);

		return newRoot;
	}

	public static Node reverseNoStack(Node root) {
		Node prev = null;
		Node curr = root;
		while (curr != null) {
			Node next = curr.getNext();
			curr.setNext(prev);
			prev = curr;
			curr = next;
		}
		root = prev;
		return root;
	}
}
