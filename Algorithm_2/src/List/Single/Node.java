package List.Single;

public class Node {

	private int val;
	private Node next;

	public Node() {
		super();
	}

	public Node(int val, Node next) {
		super();
		this.val = val;
		this.next = next;
	}

	public int getVal() {
		return val;
	}
	
	public void setNext(Node next) {
		this.next = next;
	}
	
	public Node getNext() {
		return next;
	}

}
