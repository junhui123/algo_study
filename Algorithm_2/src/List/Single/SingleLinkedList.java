package List.Single;

import java.util.Arrays;

public class SingleLinkedList {
	private Node head;
	
	public SingleLinkedList() {
		this.head = null;
	}

	public void add(int number, int index) {
		if(index < 0) {
			System.out.println("index invalid");
			return;
		}
		
		//시작에 넣는 경우
		if( index == 0 ) {
			Node n = new Node(number, null);
			if(head != null)
				n.setNext(head);
			head = n;
		} else {
			Node current = getNodeByIndex(index);
			Node current_prev = getNodeByIndex(index-1);
			Node n = new Node(number, current);
			current_prev.setNext(n);
		}
	}
	
	public void add(int number) {
		Node tempHead = head;
		while(tempHead.getNext() != null) {
			tempHead = tempHead.getNext();
		}
		tempHead.setNext(new Node(number, null));
	}

	public void remove(int index) {
		if(index < 0) {
			System.out.println("index invalid");
			return;
		}
		
		if(index == 0 ) {
			Node newFirst = head.getNext();
			head = newFirst;
		} else {
			Node current = getNodeByIndex(index);
			Node current_prev = getNodeByIndex(--index);
			current_prev.setNext(current.getNext());
			current.setNext(null);
		}
	}
	
	public void remove() {
		Node tempHead = head;
		Node targetBefore = null;
		while(tempHead.getNext() != null) {
			targetBefore = tempHead;
			tempHead = tempHead.getNext();
		}
		targetBefore.setNext(null);
	}

	public int indexOf(int index) {
		return getNodeByIndex(index) != null ? 0 : -1;
	}
	
	public Node getNodeByIndex(int index) {
		Node tempHead = head;
		int start = 0;
		while(tempHead.getNext() != null && start<index) {
			tempHead = tempHead.getNext();
			start++;
		}
		return tempHead;
	}
	
	public String toString() {
		int arrSize = 1;
		Node temp = head;
		while(temp.getNext() != null) {
			temp = temp.getNext();
			arrSize++;
		}
		
		int[] arrs = new int[arrSize];
		arrs[0] = head.getVal();
		int index = 1;
		temp = head;
		while(temp.getNext() != null) {
			temp = temp.getNext();
			arrs[index++]  = temp.getVal();
		}
		
		return String.valueOf(Arrays.toString(arrs));
	}
	
	public String toStringNode(Node node) {
		return String.valueOf(node.getVal());
	}
	
	public String toStringLastNode() {
		Node tempHead = head;
		while(tempHead.getNext() != null) {
			tempHead = tempHead.getNext();
		}
		return String.valueOf(tempHead.getVal());
	}

}

