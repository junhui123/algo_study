package List.Single;

public class Test {
	public static void main(String[] args) {
		
		SingleLinkedList list = new SingleLinkedList();
		list.add(1, 0);
		System.out.println(list.toString());
		
		list.add(0, -1);
		System.out.println(list.toString());
		
		list.add(3);
		System.out.println(list.toString());
		
		list.add(4);
		System.out.println(list.toString());
		
		list.add(5);
		System.out.println(list.toString());
		
		list.add(6);
		System.out.println(list.toString());
		
		list.add(2, 1);
		System.out.println(list.toString());
		
		list.add(-1, 0);
		System.out.println(list.toString());
		
		list.remove();
		System.out.println(list.toString());
		
		list.remove();
		System.out.println(list.toString());
		
		list.remove(0);
		System.out.println(list.toString());
		
		list.remove(3);
		System.out.println(list.toString());
		
		System.exit(0);
	}
}
