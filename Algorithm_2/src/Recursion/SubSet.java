package Recursion;

import java.util.ArrayList;

public class SubSet {
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		ArrayList result1 = geSubSets(list, 0);
		System.out.println(result1);
		
		
		ArrayList<ArrayList<String>> result = new ArrayList<>();
		String[] Set = new String[] {"1", "2", "3"};
		subSet(0, Set, result);
		System.out.println(result);
	}
	
	public static ArrayList<ArrayList<String>> subSet(int n, String[] Set, ArrayList<ArrayList<String>> result) {
		if (n < Set.length) {
			if (n == 0) {
				ArrayList<ArrayList<String>> allsubsets = null;
				allsubsets = new ArrayList();
				result.add(new ArrayList<String>());
				
				ArrayList<String> setO = new ArrayList<String>();
				setO.add(Set[0]);
				result.add(setO);
			} else {
				String item = Set[n];
				ArrayList<ArrayList<String>> moreSubSets = new ArrayList<>();
				for (ArrayList<String> subset : result) {
					ArrayList<String> newsubSet = new ArrayList<>();
					newsubSet.addAll(subset);
					newsubSet.add(item);
					moreSubSets.add(newsubSet);
				}
				result.addAll(moreSubSets);
			}
			return subSet(n + 1, Set, result);
		}
		return result;
	}

	public static ArrayList<ArrayList<Integer>> geSubSets(ArrayList<Integer> set, int index) {
		ArrayList<ArrayList<Integer>> allsubsets;
		if(set.size() == index) {
			allsubsets = new ArrayList();
			allsubsets.add(new ArrayList<Integer>());
		}
		else {
			allsubsets = geSubSets(set, index+1);
			int item = set.get(index);
			ArrayList<ArrayList<Integer>> moreSubSets = new ArrayList<>();
			for(ArrayList<Integer> subset : allsubsets) {
				ArrayList<Integer> newsubSet = new ArrayList<>();
				newsubSet.addAll(subset);
				newsubSet.add(item);
				moreSubSets.add(newsubSet);
			}
			allsubsets.addAll(moreSubSets);
		}
		
		return allsubsets;
	}
	

}
