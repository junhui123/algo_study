package Recursion;

public class UpStairs {
	public static void main(String[] args) {
		System.out.println(countWay(5));
	}

	public static int countWay(int n) {
		if (n < 0)
			return 0;
		if (n == 0)
			return 1;
		else
			return countWay(n - 1) + countWay(n - 2) + countWay(n - 3);
	}

}
