package Sort.Selection;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 삽입 정렬 = 첫번째 원소는 정렬된 상태로 가정, 두번째 원소와 첫번재 원소 비교 두번째 < 첫번째 인 경우 첫번째 원소 인덱스에 두번째
 * 원소 삽입, 두번째 원소 인덱스에 첫번째 원소 삽입 세번째는 첫번재, 두번째와 비교 후 더 작은 값과 교체 ... n-1 번째 까지 반복
 * 진행
 * 
 * @author QWER
 */
public class InsertionSort {
	public static void main(String[] args) {
//		int[] data = new int[] { 31, 25, 12, 22, 11 };

		long start = System.nanoTime();
		int[] arr_sotred1 = sort(new int[]{ 31, 25, 12, 22, 11 } );
		System.out.println(Arrays.toString(arr_sotred1));
		System.out.println(System.nanoTime()-start);
		
		start = System.nanoTime();
		int[] arr_sotred2 = sort2(new int[]{ 31, 25, 12, 22, 11 });
		System.out.println(Arrays.toString(arr_sotred2));
		System.out.println(System.nanoTime()-start);
		
		start = System.nanoTime();
		int[] arr_sotred3 = sort3(new int[]{ 31, 25, 12, 22, 11 });
		System.out.println(Arrays.toString(arr_sotred3));
		System.out.println(System.nanoTime()-start);
		
		start = System.nanoTime();
		int[] arr_sotred4 = sort4(new int[]{ 31, 25, 12, 22, 11 });
		System.out.println(Arrays.toString(arr_sotred4));
		System.out.println(System.nanoTime()-start);

	}

	public static int[] sort(int[] data) {
		int insert_index = 0;
		for (int i = 1; i < data.length; i++) {
			for (int z = i - 1; z >= 0; z--) {
				if (data[z] > data[i]) {
					insert_index = z;
				}
			}
			
			ArrayList<Integer> list = new ArrayList<>(data.length);
			for(int b=0; b<data.length; b++) {
				if(data[b] != data[i]) {
					list.add(b, data[b]);
				} else {
					list.add(insert_index, data[i]);
				}
			}
			data = list.stream().mapToInt(listIndex -> listIndex).toArray();
		}

		return data;
	}
	
	public static int[] sort2(int[] data) {
		for (int i = 1; i < data.length; i++) {
			int val = data[i];
			for (int j = 0; j < i; j++) {
				if (data[j] > val) {
					System.arraycopy(data, j, data, j + 1, i - j);
					data[j] = val;
					break;
				}
			}
		}
		return data;
	}
	
	public static int[] sort3(int[] data) {
		for (int i = 1; i < data.length; i++){
		    for (int j = i; j > 0 && data[j-1] > data[j]; j--){
		        int temp = data[j];
		        data[j] = data[j-1];
		        data[j-1] = temp;
		    }
		}
		return data;
	}
	
	public static int[] sort4(int[] arr) {
		for (int index = 1; index < arr.length; index++) {
			int temp = arr[index];
			int aux = index - 1;
			
			while ( (aux >= 0) && (arr[aux] > temp) ) {
				arr[aux + 1] = arr[aux];
				aux--;
			}
			arr[aux + 1] = temp;
		}
		return arr;
	}
}
