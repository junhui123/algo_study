package Sort.Selection;

import java.util.Arrays;
import java.util.Stack;

/**
  1) 피벗 다음 인덱스를 lowIndex로 지정 오른쪽으로 이동
  2) 피벗보다 작은 값을 만나면 lowIndex와 교환
  3) 다음 인덱스 값을 lowIndex로 지정 오른쪽으로 이동
          피벗보다 작은 값을 만나면 lowIndex와 교환 1~3 반복
  4) 피벗보다 더 이상 작은 값이 없으면 피벗 교환 필요.
          피벗 보다 작은 값 중 가장 오른쪽에 있는 값과 교환 후 1~4 반복

for each (unsorted) partition
set first element as pivot
  storeIndex = pivotIndex + 1
  for i = pivotIndex + 1 to rightmostIndex
    if element[i] < element[pivot]
      swap(i, storeIndex); storeIndex++
  swap(pivot, storeIndex - 1)

 * @author QWER
 */
public class QuickSort {
	public static void main(String[] args) {
		int[] arrs = new int[] { 17, 35, 6, 28, 2, 40, 38, 48, 4, 7 };
		
		Long start = System.nanoTime();
		int[] arr_Sorted1= sort(new int[] { 17, 35, 6, 28, 2, 40, 38, 48, 4, 7 });
		System.out.println(Arrays.toString(arr_Sorted1));
		System.out.println(System.nanoTime()-start);
		
		start = System.nanoTime();
		int[] arr_Sorted2= simpleQuick(new int[] { 17, 35, 6, 28, 2, 40, 38, 48, 4, 7 });
		System.out.println(Arrays.toString(arr_Sorted2));
		System.out.println(System.nanoTime()-start);
		
	}
	
	public static int[] sort(int[] arrs) {
		int pivotIndex = 0;
		for(int lowIndex=0; lowIndex<arrs.length; lowIndex++) {
			int pivot = arrs[pivotIndex];
			
			int storeIndex = pivotIndex+1;
			for(int highIndex=pivotIndex+1; highIndex<arrs.length; highIndex++) {
				if(pivot > arrs[highIndex]) {
					int right = arrs[highIndex];
					arrs[highIndex] = arrs[storeIndex]; 
					arrs[storeIndex] = right;
					storeIndex++;
				}
			}
			
			if(storeIndex-1 == pivotIndex) {
				pivotIndex = lowIndex+1;
				if(pivotIndex == arrs.length)
					--pivotIndex;
				
				continue;
			}
			
			arrs[pivotIndex] = arrs[storeIndex-1];
			arrs[storeIndex-1] = pivot;
		}
		
		return arrs;
	}
	
	public static int[] simpleQuick(int[] data) {
		
		//배열에 원소가 1개 인 경우 정렬 필요 없음
		if(data.length < 2) {
			return data;
		}
		
		int pivotIndex = 0;
		int pivotValue = data[pivotIndex];
		
		//피벗 보다 작은 원소 갯수 확인
		int leftCount = 0;
		for(int i = 0 ; i < data.length; i++ ) {
			if (data[i] < pivotValue )
				leftCount++;
		}
		
		//피벗을 기준으로 분할 할 집합 설정
		int[] left = new int[leftCount]; //피벗 보다 작은 값 (왼쪽)
		int[] right = new int[data.length - leftCount - 1]; //피벗 보다 큰 값 (오른쪽)
		int l = 0;
		int r = 0; 
		
		for(int i=0; i<data.length; i++) {
			if ( i == pivotIndex ) continue;
			
			//현재 배열 값 
			int val = data[i];
			
			//피벗보다 작은 값 left 배열에 집어 넣음 큰 값은 right 배열
			if ( val < pivotValue) 
				left[l++] = val;
			else 
				right[r++] = val;
		}
		
		//더 이상 쪼갤 부분집합이 없을 때 까지 각각의 부분집합에 대해 쪼개기 작업을 수행
		left = simpleQuick(left);
		right = simpleQuick(right);
		
		//최종적으로 정렬된 left,right 배열을 하나로 합침
		System.arraycopy(left, 0, data, 0, left.length);
		data[left.length] = pivotValue;
		System.arraycopy(right, 0, data, left.length+1, right.length);
		
		return data;
	}
}
