package Sort.Selection;

import java.util.Arrays;

/**
 * 1. 순회 후 최저값 검색
 * 2. 최저값과 기준값(첫번째 값) 비교 후 최저값 < 기준값, Swap
 * 3. 기준값을 다음 인덱스로 변경 후 다시 최저값 검색 후 교체 반복
 * 
 * @author QWER
 */
public class SelectionSort {
	public static void main(String[] args) {
		int[] data = { 66, 10, 1, 99, 5 };
		
		int[] sorted = sort(data);
		System.out.println(Arrays.toString(sorted));
		
//		int[] sorted2 = sortRecursion(data, 0);
//		System.out.println(Arrays.toString(sorted2));
	}
	
	public static int[] sort(int[] data) {
		int[] arr_Sorted = data.clone();
		
		int base_index = 0;
		int base = arr_Sorted[base_index];
		for(int arr_min=1; arr_min < arr_Sorted.length; arr_min++) {

			int min_most = arr_Sorted[arr_min];
			int min_most_index = arr_min;
			
			for(int arr_min2=arr_min+1; arr_min2 < arr_Sorted.length; arr_min2++) {
				if(min_most > arr_Sorted[arr_min2]) {
					min_most = arr_Sorted[arr_min2];
					min_most_index = arr_min2;
				}
			}
			
			if(min_most < base) {
				arr_Sorted[base_index] = min_most;
				arr_Sorted[min_most_index] = base;
				base_index++;
			}
		}
		
		return arr_Sorted;
	}
	
	public static int[] sortRecursion(int[] data, int index) {
		if(index > data.length-1 || index < 0)
			return data;
		
		int base = data[index];
		int minIndex = index;
		for(int i=index; i<data.length; i++) {
			if(data[minIndex] > data[i]) {
				minIndex = i;
			}
		}
		
		swap(data, index, minIndex);
		
		
		return sortRecursion(data, ++index);
	}
	
	public static void swap(int[] data, int index1, int index2) {
		int data_index2 = data[index2];
		data[index2] = data[index1];
		data[index1] = data_index2;
	}
}
