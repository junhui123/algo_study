package Tree;

import java.util.Stack;

//BST
public class TreeAddRemove {
	public static void main(String[] args) {
		Node root = new Node(20);
		Node n1 = root.addNewNode("L", 8);
		Node n2 = root.addNewNode("R", 22);
		Node n3 = n1.addNewNode("L", 4);
		Node n4 = n1.addNewNode("R", 12);
		Node n5 = n2.addNewNode("L", 21);
		Node n6 = n2.addNewNode("R", 23);
		Node n7 = n3.addNewNode("L", 1);
		Node n8 = n3.addNewNode("R", 5);

		// Node root = new Node(5);
		// Node n1 = root.addNewNode("L", 3);
		// Node n2 = root.addNewNode("R", 10);
		// Node n3 = n1.addNewNode("L", 1);
		// Node n4 = n1.addNewNode("R", 4);
		// Node n5 = n2.addNewNode("L", 7);
		// Node n6 = n2.addNewNode("R", 12);
		// Node n7 = n5.addNewNode("L", 6);
		// Node n8 = n5.addNewNode("R", 8);

		// Node root = new Node(9);
		// Node n1 = root.addNewNode("L", 1);
		// Node n2 = root.addNewNode("R", 5);
		// Node n3 = n1.addNewNode("L", 3);
		// Node n4 = n1.addNewNode("R", 6);
		// Node n5 = n2.addNewNode("L", 7);
		// Node n6 = n2.addNewNode("R", 10);
		// Node n7 = n3.addNewNode("L", 11);
		// Node n8 = n3.addNewNode("R", 2);
		// Node n9 = n4.addNewNode("L", 4);
		// Node n10 = n4.addNewNode("R", 8);

		long t1 = System.nanoTime();
		Node searchNode = searchNode(root, 5);
		System.out.println(System.nanoTime()-t1);
		if (searchNode != null)
			System.out.println(searchNode.getvalue());
		else {
			addNode(root, 11);
			System.out.println("add node");
		}
		
		t1 = System.nanoTime();
		searchNode = search(root, 5) ;
		System.out.println(System.nanoTime()-t1);
		if (searchNode != null)
			System.out.println(searchNode.getvalue());
		
		Node newN = Delete(root, 20);
		System.out.println(newN.getvalue());
//		System.out.println("delete");
		
//		removeNode(root, 5);
//		System.out.println("delete");
//		
//		removeNode(root, 4);
//		System.out.println("delete");
		
//		System.out.println("Find Min / Max");
//		System.out.println("Min : "+searchMin(n3).getvalue());
//		System.out.println("Max : "+searchMax(n3).getvalue());
//		
//		System.out.println("Min : "+findMin(n3).getvalue());
//		System.out.println("Max : "+findMax(n3).getvalue());
		
	}

	public static void addNode(Node root, int value) {
		if(Integer.valueOf(value) == null || root == null)
			return; 
		
		Node current = root;
		while(true) {
			if(value < current.getvalue()) {
				if(current.getLeft() != null) {
					current = current.getLeft();
				} else {
					current.setLeft(new Node(value));
					break;
				}
			} else if(value > current.getvalue()) {
				if(current.getRight() != null) {
					current = current.getRight();
				} else {
					current.setRight(new Node(value));
					break;
				}
			}
		}
	}

	/**
	  	1.	자식이 없는 경우
			-그냥 삭제
		2.	자식이 하나만 있는 경우
			-삭제 대상 노드의 자식을 삭제 대상 노드의 부모의 오른쪽 자식으로 연결
		3.	자식이 2개 다 있는 경우
			-왼쪽에서 가장 큰 노드를 삭제 노드 위치에 이동 또는 오른쪽에서 가장 작은 노드를 삭제 노드 위치에 이동 중 선택
	 * @param root
	 * @param value
	 */
	public static void removeNode(Node root, int value) {
		if(Integer.valueOf(value) == null || root == null)
			return;
		
		Node n = searchNode(root, value);
		Node parent = n.getParent();
		
		//자식이 없는 겨우
		if(n.getLeft() == null && n.getRight() == null) {
			if(parent.getLeft() == n ) {
				parent.setLeft(null);
			} else {
				parent.setRight(null);
			}
		//자식이 하나만 있는 겨우
		} else if(n.getLeft() == null || n.getRight() == null) {
			if(n.getLeft() == null) {
				Node right = n.getRight();
				parent.setRight(right);
				right.setParent(parent);
				n.setRight(null);				
			} else if(n.getRight() == null ) {
				Node left = n.getLeft();
				parent.setLeft(left);
				left.setParent(parent);
				n.setLeft(null);				
			}
		//자식이 2개 다 있는 경우
		} else if(n.getLeft() != null && n.getRight() != null) {
			//왼쪽에서 가장 큰 노드를 삭제 노드로 이동 
			Node leftMost = searchMax(n);
			if(parent.getLeft() == n ) {
				parent.setLeft(leftMost);
			} else {
				parent.setRight(leftMost);
			}
			leftMost.setParent(parent);
			
			leftMost.setLeft(n.getLeft());
			n.getLeft().setParent(leftMost);
		}
	}
	
	public static Node findMin(Node root) {
		Node current = root;
		while (current.getLeft() != null)
			current = current.getLeft();
		return current;
	}

	public static Node findMax(Node root) {
		Node current = root;
		while (current.getRight() != null)
			current = current.getRight();
		return current;
	}
	
	
	public static Node Delete(Node root , int data) {
		if(root == null) {
			return root;
		}
		else if(data < root.getvalue()){
			root.setLeft(Delete(root.getLeft() , data));
		}
		else if(data > root.getvalue()){
			root.setRight(Delete(root.getRight() , data));
		}
		else{ 
			//case 1: no child
			if(root.getLeft() == null && root.getRight() == null){
				root = null;
			}
			//case 2: one child 
			//node with right child
			else if(root.getLeft() == null){
				root = root.getRight();
				root.setRight(null);
			}
			//node with left child
			else if(root.getRight() == null) {
				root = root.getLeft();
				root.setLeft(null);
			}
			//case 3: with two child
			else{
				//오른쪽의 가장 작은 노드 또는 왼쪽의 가장 큰 노드를 삭제 위치로 이동
				Node temp = searchMin(root.getRight());
				root.setvalue(temp.getvalue()); 
				root.setRight(Delete(root.getRight() , temp.getvalue()));
			}
		}
		return root;
	}
	

	
	public static Node searchMin(Node root) {

		if (root == null) {
			return root;
		} else if (root.getLeft() == null) {
			return root;
		} else {
			return searchMin(root.getLeft());
		}
	}
	
	public static Node searchMax(Node root) {
		if (root == null) {
			return root;
		} else if (root.getRight() == null) {
			return root;
		} else {
			return searchMax(root.getRight());
		}
	}

	public static Node searchNode(Node root, int value) {
		Stack<Node> s1 = new Stack<>();
		s1.push(root);

		Node tmp = root;
		Node peekNode = null;
		while (tmp.getLeft() != null && tmp.getRight() != null) {
			peekNode = s1.peek();
			tmp = peekNode;

			if (peekNode.getRight() != null) {
				s1.push(peekNode.getRight());
			}

			if (peekNode.getLeft() != null) {
				s1.push(peekNode.getLeft());
			}
		}

		while (s1.size() > 0) {
			Node n = s1.pop();
			if ((n.getLeft() != null && n.getRight() != null) && ((n.getLeft() != tmp && n.getRight() != tmp) && n.getParent() != null)) {
				s1.push(n);
				s1.push(n.getRight());
				s1.push(n.getLeft());
			} else {
				if (Integer.valueOf(value) != null) {
					if (n.getvalue() == value) return n;
				}
				tmp = n;
			}
		}
		return null;
	}
	
	public static Node search(Node n, int key) {
		while (n != null) {
			if (key == n.getvalue())
				return n;
			if (key < n.getvalue())
				n = n.getLeft();
			else
				n = n.getRight();
		}
		return null;
	}
}
