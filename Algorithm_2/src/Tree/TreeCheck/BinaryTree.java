package Tree.TreeCheck;

class Node {
	private int value;
	private Node Left;
	private Node right;
	private Node parent;

	public Node(int value, Node left, Node right) {
		super();
		this.value = value;
		Left = left;
		this.right = right;
	}
	
	public Node(int value) {
		super();
		this.value=value;
	}
	
	public int getvalue() {
		return value;
	}

	public void setvalue(int value) {
		this.value = value;
	}

	public Node getLeft() {
		return Left;
	}

	public void setLeft(Node left) {
		Left = left;
	}

	public Node getRight() {
		return right;
	}

	public void setRight(Node right) {
		this.right = right;
	}
	
	public Node addNewNode(String LR, int value) {
		Node newNode = new Node(value);
		newNode.setParent(this);
		if(LR.equals("L")) {
			this.setLeft(newNode);
		} else { //R
			this.setRight(newNode);
		}
		return newNode;
	}

	public Node getParent() {
		return parent;
	}

	public void setParent(Node parent) {
		this.parent = parent;
	}
}

public class BinaryTree
{
    // Root of the Binary Tree
    Node root;
 
    // To keep tract of previous node in Inorder Traversal
    Node prev;
 
    boolean isBST()  {
        prev = null;
        return isBST(root);
    }
 
    /* Returns true if given search tree is binary
       search tree (efficient version) */
    boolean isBST(Node node)
    {
        // traverse the tree in inorder fashion and
        // keep a track of previous node
        if (node != null)
        {
            if (!isBST(node.getLeft()))
                return false;
 
            // allows only distinct values node
            if (prev != null && node.getvalue() <= prev.getvalue() )
                return false;
            prev = node;
            return isBST(node.getRight());
        }
        return true;
    }
    
	public boolean isValid(Node root) {
		return isValidBST(root, Integer.MIN_VALUE, Integer.MAX_VALUE);
	}
    
    private boolean isValidBST(Node node, int l, int h) {
        if(node == null)
            return true;
        return node.getvalue() > l 
            && node.getvalue() < h
            && isValidBST(node.getLeft(), l, node.getvalue())
            && isValidBST(node.getRight(), node.getvalue(), h);
    }
    
    
	public int checkBalancedTree(Node root) {
		if(root==null) return 0;
		
		int left = 0;
		int right = 0;
		if(root.getLeft() != null || root.getRight() != null) {
			left += checkBalancedTree(root.getLeft());
			if(left == -1)
				return -1;
			
			right += checkBalancedTree(root.getRight());
			if(right == -1)
				return -1;
		}
		
		int result = Math.abs(left-right) > 1 ? -1 : (left > right ? left+1 : right+1);

		return result;
	}
 
    /* Driver program to test above functions */
    public static void main(String args[])
    {
//    	BST
//		Node root = new Node(8);
//		Node n1 = root.addNewNode("L", 3);
//		Node n2 = root.addNewNode("R", 10);
//		Node n3 = n1.addNewNode("L", 1);
//		Node n4 = n1.addNewNode("R", 6);
//		Node n5 = n2.addNewNode("R", 14);
//		Node n6 = n5.addNewNode("L", 13);
//		Node n7 = n4.addNewNode("L", 4);
//		Node n8 = n4.addNewNode("R", 7);
    	
		//Not BST
		Node root = new Node(9);
		Node n1 = root.addNewNode("L", 1);
		Node n2 = root.addNewNode("R", 5);
		Node n3 = n1.addNewNode("L", 3);
		Node n4 = n1.addNewNode("R", 6);
		Node n5 = n2.addNewNode("L", 7);
		Node n6 = n2.addNewNode("R", 10);
		Node n7 = n3.addNewNode("L", 11);
		Node n8 = n3.addNewNode("R", 2);
		Node n9 = n4.addNewNode("L", 4);
		Node n10 = n4.addNewNode("R", 8);
		
        BinaryTree tree = new BinaryTree();
        tree.root = root;
 
        if (tree.isBST())
            System.out.println("IS BST");
        else
            System.out.println("Not a BST");
        
        
        if (tree.isValid(root))
            System.out.println("IS BST");
        else
            System.out.println("Not a BST");
        
        if(tree.checkBalancedTree(root) == -1) {
        	System.out.println("Un Balanced");
        } else {
        	System.out.println("Balanced");
        }
    }
}