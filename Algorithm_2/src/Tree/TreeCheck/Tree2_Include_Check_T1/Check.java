package Tree.TreeCheck.Tree2_Include_Check_T1;

import java.util.Stack;

import Tree.Node;

public class Check {
	public static void main(String[] args) {
		Node t1 = new Node(9);
		Node t1_n1 = t1.addNewNode("L", 1);
		Node t1_n2 = t1.addNewNode("R", 5);
		Node t1_n3 = t1_n1.addNewNode("L", 3);
		Node t1_n4 = t1_n1.addNewNode("R", 6);
		Node t1_n5 = t1_n2.addNewNode("L", 7);
		Node t1_n6 = t1_n2.addNewNode("R", 10);
		
		Node t2 = t1_n6.addNewNode("L", 8);
		Node t2_n2 = t2.addNewNode("L", 3);
		Node t2_n3 = t2.addNewNode("R", 10);
		Node t2_n4 = t2_n2.addNewNode("L", 1);
		Node t2_n5 = t2_n2.addNewNode("R", 6);
		
		boolean isCompare = compareTree(t1, t2);
		System.out.println(isCompare);
		
	}
	
	public static boolean compareTree(Node t1, Node t2) {
		boolean isResult = false;
		Stack nodeStack1 = new Stack();
		Stack nodeStack2 = new Stack();
		inOrder(t1, t2);
		if(equalNode == null) {
			return false;
		} else {
			inOrder(equalNode, nodeStack1);
			inOrder(t2, nodeStack2);
			
			if(nodeStack1.size() != nodeStack2.size()) {
				return false;
			} else {
				for(int i = 0; i<nodeStack1.size(); i++) {
					Node n1 = (Node) nodeStack1.pop();
					Node n2 = (Node) nodeStack2.pop();
					if(n1!=n2) {
						isResult=false;
						break;
					} else {
						isResult=true;
					}
				}
			}
			
		}
		return isResult;
	}
	
	private static Node equalNode = null;
	public static void inOrder(Node t1, Node t2) {
		if (t1 == null)
			return;

		inOrder(t1.getLeft(), t2);
		if(t1.getvalue() == t2.getvalue()) {
			equalNode = t1;
			return;
		}
		
		inOrder(t1.getRight(), t2);
	}
	
	public static Stack inOrder(Node node, Stack nodeStack) {
		if (node == null)
			return nodeStack;

		inOrder(node.getLeft(), nodeStack);
		nodeStack.push(node);
		inOrder(node.getRight(), nodeStack);
		
		return nodeStack;
	}
}
