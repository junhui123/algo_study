package Tree;

public class TreeHeight {

	public static void main(String[] args) {

		Node root = new Node(5);
		Node n1 = root.addNewNode("L", 3);
		Node n2 = root.addNewNode("R", 10);
		Node n3 = n1.addNewNode("L", 1);
		Node n4 = n1.addNewNode("R", 4);
		Node n5 = n2.addNewNode("L", 7);
		Node n6 = n2.addNewNode("R", 12);
		Node n7 = n5.addNewNode("L", 6);
		Node n8 = n5.addNewNode("R", 8);
		Node n9 = n8.addNewNode("L", 99);
		Node n10 = n9.addNewNode("L", 98);
		Node n11 = n10.addNewNode("L", 96);
		Node n12 = n10.addNewNode("R", 97);
		Node n13 = n11.addNewNode("L", 94);

		System.out.println(getHeight(root));

	}

	public static int getHeight(Node root) {
		//노드가 마지막인 경우 하위에 더 이상 자식이 없으므로 0을 반환  
		if(root==null) return 0;
		
		int left = 0;
		int right = 0;
		if(root.getLeft() != null || root.getRight() != null) {
			left += getHeight(root.getLeft());
			right += getHeight(root.getRight());
		}
		
		//왼쪽,오른쪽 서브 트리 높이가 다를 수 있으므로 더 높이가 큰 쪽의 값으로 설정 
		int result = left > right ? left : right;

		//하위에 노드가 있을 때 마다 높이가 1씩 늘어남
		return result+1;
	}
}
