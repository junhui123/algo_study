package Tree;

import java.util.Comparator;
import java.util.Stack;


public class TreeTraversal {
	public static void main(String[] args) {
		// Node root = new Node(5);
		// Node n1 = root.addNewNode("L", 3);
		// Node n2 = root.addNewNode("R", 10);
		// Node n3 = n1.addNewNode("L", 1);
		// Node n4 = n1.addNewNode("R", 4);
		// Node n5 = n2.addNewNode("L", 7);
		// Node n6 = n2.addNewNode("R", 12);
		// Node n7 = n5.addNewNode("L", 6);
		// Node n8 = n5.addNewNode("R", 8);

//		Node root = new Node(20);
//		Node n1 = root.addNewNode("L", 8);
//		Node n2 = root.addNewNode("R", 22);
//		Node n3 = n1.addNewNode("L", 4);
//		Node n4 = n1.addNewNode("R", 12);
//		Node n5 = n2.addNewNode("L", 21);
//		Node n6 = n2.addNewNode("R", 23);
//		Node n7 = n3.addNewNode("L", 1);
//		Node n8 = n3.addNewNode("R", 5);
		
		//Not BST
		Node root = new Node(9);
		Node n1 = root.addNewNode("L", 1);
		Node n2 = root.addNewNode("R", 5);
		Node n3 = n1.addNewNode("L", 3);
		Node n4 = n1.addNewNode("R", 6);
		Node n5 = n2.addNewNode("L", 7);
		Node n6 = n2.addNewNode("R", 10);
		Node n7 = n3.addNewNode("L", 11);
		Node n8 = n3.addNewNode("R", 2);
		Node n9 = n4.addNewNode("L", 4);
		Node n10 = n4.addNewNode("R", 8);
		
		//BST
//		Node root = new Node(8);
//		Node n1 = root.addNewNode("L", 3);
//		Node n2 = root.addNewNode("R", 10);
//		Node n3 = n1.addNewNode("L", 1);
//		Node n4 = n1.addNewNode("R", 6);
//		Node n5 = n2.addNewNode("R", 14);
//		Node n6 = n5.addNewNode("L", 13);
//		Node n7 = n4.addNewNode("L", 4);
//		Node n8 = n4.addNewNode("R", 7);

		System.out.print(" pre order = ");
		preOrder(root);
		System.out.println();

		System.out.print(" pre order = ");
		preOrderWithOutRecursion(root);
		System.out.println();

		System.out.print("  in order = ");
		inOrder(root);
		System.out.println();

		System.out.print("  in order = ");
		inOrderWithOutRecursion(root);
		System.out.println();
		
		System.out.print(" in order2 = " + inOrder2(root));
		System.out.println();
		
		System.out.print(" in order3 = ");
		System.out.println(checkBalnacneTree(root));

		long beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		Long l1 = System.nanoTime();
		System.out.print("post order = ");
		postOrder(root);
		System.out.println();
		System.out.println(System.nanoTime()-l1);
		long afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		System.out.println("actualMemUsed = " + (afterUsedMem-beforeUsedMem));

		beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		l1 = System.nanoTime();
		System.out.print("post order = ");
		postOrderWithOutRecursion(root);
		System.out.println();
		System.out.println(System.nanoTime()-l1);
		afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		System.out.println("actualMemUsed = " + (afterUsedMem-beforeUsedMem));

		beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		l1 = System.nanoTime();
		System.out.print("post order = ");
		postOrderWithOutRecursionOneStack(root);
		System.out.println();
		System.out.println(System.nanoTime()-l1);
		afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		System.out.println("actualMemUsed = " + (afterUsedMem-beforeUsedMem));

		beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		l1 = System.nanoTime();
		System.out.print("post order = ");
		postorderIter(root);
		System.out.println();
		System.out.println(System.nanoTime()-l1);
		afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		System.out.println("actualMemUsed = " + (afterUsedMem-beforeUsedMem));
		
		beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		l1 = System.nanoTime();
		System.out.print("post order = ");
		postOrderTraversal(root);
		System.out.println();
		System.out.println(System.nanoTime()-l1);
		afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		System.out.println("actualMemUsed = " + (afterUsedMem-beforeUsedMem));
		
		beforeUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		l1 = System.nanoTime();
		System.out.print("post order = ");
		postOrderTraversal2(root);
		System.out.println();
		System.out.println(System.nanoTime()-l1);
		afterUsedMem=Runtime.getRuntime().totalMemory()-Runtime.getRuntime().freeMemory();
		System.out.println("actualMemUsed = " + (afterUsedMem-beforeUsedMem));
	}

	// 전위 순회 - 루트 - 왼쪽 서브트리 - 오른쪽 서브트리
	public static void preOrder(Node root) {
		if (root == null)
			return;

		System.out.print(root.getvalue() + " ");
		preOrder(root.getLeft());
		preOrder(root.getRight());
	}

	// 중위 순회 - 왼쪽 서브트리 - 루트 - 오른쪽 서브트리
	public static void inOrder(Node root) {
		if (root == null)
			return;

		inOrder(root.getLeft());
		System.out.print(root.getvalue() + " ");
		inOrder(root.getRight());
	}
	
	private static Node prev;
	public static boolean inOrder2(Node root) {
		if (root != null) {
			if(!inOrder2(root.getLeft()))
				return false;

			if (prev != null && root.getvalue() <= prev.getvalue())
				return false;
			prev = root;

			return inOrder2(root.getRight());
		}
		return true;
	}
	
	public static boolean checkBalnacneTree(Node root) {
		Stack inOrderStack = new Stack();
		inOrder3(root, inOrderStack);
		String tmp = inOrderStack.toString();
		inOrderStack.sort(new Comparator<Integer>() {
			@Override
			public int compare(Integer o1, Integer o2) {
				return o1 > o2 ? 1 : (o1==o2 ? 0 : -1);
			}
		});
		String tmp2 = inOrderStack.toString();
		
		return tmp.equals(tmp2); 
	}
	
	public static void inOrder3(Node root, Stack inOrderStack) {
		if (root == null)
			return;

		inOrder3(root.getLeft(), inOrderStack);
		inOrderStack.push(root.getvalue());
		inOrder3(root.getRight(), inOrderStack);
	}

	// 후위 순회 - 왼쪽 서브트리 - 오른쪽 서브트리 - 루트
	public static void postOrder(Node root) {
		if (root == null)
			return;

		postOrder(root.getLeft());
		postOrder(root.getRight());
		System.out.print(root.getvalue() + " ");
	}

	// 전위 순회 - 루트 - 왼쪽 서브트리 - 오른쪽 서브트리
	public static void preOrderWithOutRecursion(Node root) {

		Stack<Node> stack = new Stack<Node>();
		stack.push(root);

		while (stack.size() > 0) {
			Node curr = stack.pop();
			System.out.print(curr.getvalue() + " ");

			Node n = curr.getRight();
			if (n != null)
				stack.push(n);

			n = curr.getLeft();
			if (n != null)
				stack.push(n);
		}
	}

	// 중위 순회 - 왼쪽 서브트리 - 루트 - 오른쪽 서브트리
	public static void inOrderWithOutRecursion(Node root) {
		Stack<Node> stack = new Stack<Node>();

		Node node = root;
		while (node != null) {
			stack.push(node);
			node = node.getLeft();
		}

		while (stack.size() > 0) {
			node = stack.pop();
			System.out.print(node.getvalue() + " ");
			if (node.getRight() != null) {
				node = node.getRight();

				// the next node to be visited is the leftmost
				while (node != null) {
					stack.push(node);
					node = node.getLeft();
				}
			}
		}
	}

	// 후위 순회 - 왼쪽 서브트리 - 오른쪽 서브트리 - 루트
	public static void postOrderWithOutRecursion(Node root) {
		Stack<Node> s1 = new Stack<Node>();
		Stack<Node> s2 = new Stack<Node>();
		// push the root node into first stack.
		s1.push(root);
		while (s1.isEmpty() == false) {
			// take out the root and insert into second stack.
			Node temp = s1.pop();
			s2.push(temp);
			// now we have the root, push the left and right child of root into
			// the first stack.
			if (temp.getLeft() != null) {
				s1.push(temp.getLeft());
			}
			if (temp.getRight() != null) {
				s1.push(temp.getRight());
			}
		}

		// once the all node are traversed, take out the nodes from second stack and
		// print it.
		while (s2.isEmpty() == false) {
			System.out.print(s2.pop().getvalue() + " ");
		}
	}

	public static void postOrderWithOutRecursionOneStack(Node root) {
		Stack<Node> S = new Stack<Node>();

		Node node = root;
		S.push(node);
		Node prev = null;
		while (!S.isEmpty()) {
			Node current = S.peek();

			/*
			 * go down the tree in search of a leaf an if so process it and pop stack
			 * otherwise move down
			 */
			if (prev == null || prev.getLeft() == current || prev.getRight() == current) {
				if (current.getLeft() != null)
					S.push(current.getLeft());
				else if (current.getRight() != null)
					S.push(current.getRight());
				else {
					S.pop();
					System.out.print(current.getvalue() + " ");
				}

				/*
				 * go up the tree from left node, if the child is right push it onto stack
				 * otherwise process parent and pop stack
				 */
			} else if (current.getLeft() == prev) {
				if (current.getRight() != null)
					S.push(current.getRight());
				else {
					S.pop();
					System.out.print(current.getvalue() + " ");
				}

				/*
				 * go up the tree from right node and after coming back from right node process
				 * parent and pop stack
				 */
			} else if (current.getRight() == prev) {
				S.pop();
				System.out.print(current.getvalue() + " ");
			}

			prev = current;
		}
	}

	public static void postorderIter(Node root) {
		if (root == null)
			return;

		Stack<Node> s = new Stack();
		Node current = root;

		while (true) {

			if (current != null) {
				if (current.getRight() != null)
					s.push(current.getRight());
				s.push(current);
				current = current.getLeft();
				continue;
			}

			if (s.isEmpty())
				return;
			current = s.pop();

			if (current.getRight() != null && !s.isEmpty() && current.getRight() == s.peek()) {
				s.pop();
				s.push(current);
				current = current.getRight();
			} else {
				System.out.print(current.getvalue() + " ");
				current = null;
			}
		}
	}

public static void postOrderTraversal(Node root) {
	Stack<Node> st = new Stack<Node>();
	st.push(root);
	Node temp = root;
	while (!st.empty()) {
		Node peekNode = st.peek();
		
		if (peekNode.getLeft() != null && ((peekNode.getRight() != temp) && (peekNode.getLeft() != temp))) {
			st.push(peekNode.getLeft());
		} else {
			if (peekNode.getRight() != null && peekNode.getRight() != temp) {
				st.push(peekNode.getRight());
			} else {
				temp = st.pop();
				System.out.print(temp.getvalue() + " ");
			}
		}
	}
	System.out.print(" ");
}

public static void postOrderTraversal2(Node root) {
	
	Stack<Node> s1 = new Stack<>();
	s1.push(root);
	
	Node tmp = root;
	Node peekNode = null;
	while(tmp.getLeft() != null && tmp.getRight() != null ) {
		peekNode = s1.peek();
		tmp = peekNode;
		
		if(peekNode.getRight() != null) {
			s1.push(peekNode.getRight());
		}
		
		if(peekNode.getLeft() != null) {
			s1.push(peekNode.getLeft());
		}
	}
	
	while(s1.size() > 0) {
		Node n = s1.pop();
		if( (n.getLeft() != null && n.getRight() != null) && ((n.getLeft() != tmp && n.getRight() != tmp) && n.getParent() != null) ) {
				s1.push(n);
				s1.push(n.getRight());
				s1.push(n.getLeft());
		} else {
			tmp = n;
			System.out.print(n.getvalue()+" ");
		}
	}
}
}
