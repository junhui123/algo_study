package Tree;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

public class commonAncestor {
	public static void main(String[] args) {
		Node root = new Node(20);
		Node n1 = root.addNewNode("L", 8);
		Node n2 = root.addNewNode("R", 22);
		Node n3 = n1.addNewNode("L", 4);
		Node n4 = n1.addNewNode("R", 12);
		Node n5 = n4.addNewNode("L", 10);
		Node n6 = n4.addNewNode("R", 14);

		long l1 = System.nanoTime();
		Node Ancestor = getCommonAncestor(root, 4, 14);
		System.out.println(Ancestor.getvalue());
		System.out.println(System.nanoTime()-l1);
		
		l1 = System.nanoTime();
		System.out.println(getCommonAncestor(root, n3, n6).getvalue());
		System.out.println(System.nanoTime()-l1);
		
		l1 = System.nanoTime();
		System.out.println(getCommonAncestor2(root, 4, 14).getvalue());
		System.out.println(System.nanoTime()-l1);
		
		l1 = System.nanoTime();
		System.out.println(getCommonAncestor3(root, n3, n6).getvalue());
		System.out.println(System.nanoTime()-l1);
		
	}
	
	public static List<Node> preOrderWithOutRecursion(Node root, int num1, int num2) {
		List<Node> rangeNode = new ArrayList<Node>();

		Stack<Node> stack = new Stack<Node>();
		stack.push(root);

		while (stack.size() > 0) {
			Node curr = stack.pop();
			if (curr.getvalue() == num1 || curr.getvalue() == num2) {
				rangeNode.add(curr);
			}

			Node n = curr.getRight();
			if (n != null)
				stack.push(n);

			n = curr.getLeft();
			if (n != null)
				stack.push(n);
		}

		return rangeNode;
	}

	//특정 범위만 주어진 경우 num1, num2에 해당하는 노드를 찾는 과정 부터 시작
	public static Node getCommonAncestor(Node root, int num1, int num2) {
		List<Node> rangeNode = preOrderWithOutRecursion(root, num1, num2);

		List<List> tmpList = new ArrayList<>();
		for (Node n : rangeNode) {
			List<Node> tmpNodeList = new ArrayList<>();
			Node tmp = n;
			while (tmp.getParent() != null && (num1 < tmp.getParent().getvalue() && num2 > tmp.getParent().getvalue())) {
				tmpNodeList.add(tmp.getParent());
				tmp = tmp.getParent();
			}
			tmpList.add(tmpNodeList);
		}

		List compareList = new ArrayList();
		Iterator<List> itor = tmpList.iterator();
		while (itor.hasNext()) {
			List tmp = itor.next();
			if (compareList.isEmpty())
				compareList.addAll(tmp);
			else
				compareList.retainAll(tmp);
		}
		return (Node) compareList.get(0);
	}
	
	//특정 2개의 노드가 주어진 경우 
	public static Node getCommonAncestor(Node root, Node n1, Node n2) {
		int num1 = n1.getvalue();
		int num2 = n2.getvalue();
		List<List> tmpList = new ArrayList<>();
		
		List<Node> tmpNodeList = new ArrayList<>();
		while (n1.getParent() != null && (num1 < n1.getParent().getvalue() && num2 > n1.getParent().getvalue())) {
			tmpNodeList.add(n1.getParent());
			n1 = n1.getParent();
		}
		tmpList.add(tmpNodeList);

		tmpNodeList = new ArrayList<>();
		while (n2.getParent() != null && (num1 < n2.getParent().getvalue() && num2 > n2.getParent().getvalue())) {
			tmpNodeList.add(n2.getParent());
			n2 = n2.getParent();
		}
		tmpList.add(tmpNodeList);

		List compareList = new ArrayList();
		Iterator<List> itor = tmpList.iterator();
		while (itor.hasNext()) {
			List tmp = itor.next();
			if (compareList.isEmpty())
				compareList.addAll(tmp);
			else
				compareList.retainAll(tmp);
		}

		return (Node) compareList.get(0);
	}

	//num1, num2 노드의 모든 공통 조상은 num1, num2 보다 크거나 작음.
	//사이에 해당하는 값은 가장 가까운 공통 조상
	public static Node getCommonAncestor2(Node root, int num1, int num2) {
		Node curr = root;
		while(curr != null) {
			int currValue = curr.getvalue();
			
			if(num1 < currValue && num2 < currValue) {
				curr = curr.getLeft();
			}
			
			else if(num1 > currValue && num2 > currValue) {
				curr = curr.getRight();
			}
			else {
				return curr;
			}
		}
		return null;
	}
	
	//노드가 부모를 알고 있는 경우만 사용 가능
	private static Node getCommonAncestor3(Node root, Node n1, Node n2) {
		if(n1 == null || n2 == null) 
			return null;

		Node prevN1 = null;
		Node currN1 = null;
		Node currN2 = null;
		if(n1.getParent() == n2.getParent()) {
			return n1.getParent();
		} else {
			if(prevN1 == null && currN1 == null) {
				prevN1 = n1.getParent();
				currN1 = n1.getParent();
			}
			
			if(currN2 == null) {
				currN2 = n2.getParent();
			}
			
			while(prevN1 != currN2) {
				prevN1 = currN1;
				currN1 = currN1.getParent();
				currN2 = currN2.getParent();
				
				if(prevN1 == currN2) {
					return currN2;
				}
			}
		}
		
		return currN2;
	}
}
