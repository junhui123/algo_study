package Tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Stack;

public class unSortTreeToHeap {
	public static void main(String[] args) {
		Node root = new Node(1);
		Node n1 = root.addNewNode("L", 2);
		Node n2 = root.addNewNode("R", 10);
		Node n3 = n1.addNewNode("L", 90);
		Node n4 = n1.addNewNode("R", 95);
		Node n5 = n4.addNewNode("L", 51);
		Node n6 = n4.addNewNode("R", 5);
		Node n7 = n5.addNewNode("L", 21);
		Node n8 = n5.addNewNode("R", 94);
		Node n9 = n6.addNewNode("L", 68);

		int mb = 1024 * 1024;
		Runtime runtime = Runtime.getRuntime();
		
		//속도는 빠르지만...코드가 어려움
		long l1 = System.nanoTime();
		int[] arrs = treeToArray(root);
		Node[] nArrs = createNode(arrs);
		Node newRoot = createMinHeap(nArrs);
		System.out.println(System.nanoTime()-l1);
		System.out.println(runtime.totalMemory() - runtime.freeMemory()); //2684392
		
		//속도는 느리지만 코드는 간담함..
		l1= System.nanoTime();
		Node newRoot2 = heapifyBinaryTree(root);
		System.out.println(System.nanoTime()-l1);
		System.out.println(runtime.totalMemory() - runtime.freeMemory()); //2684392
	}

	public static int[] treeToArray(Node root) {

		int size = 0;
		Stack<Node> stack = new Stack();
		Node n = root;
		while (n != null) {
			stack.push(n);
			n = n.getLeft();
		}

		while (stack.size() > 0) {
			n = stack.pop();
			size++;

			if (n.getRight() != null) {
				n = n.getRight();

				while (n != null) {
					stack.push(n);
					n = n.getLeft();
				}
			}
		}

		int[] arr = new int[size];
		int arrIndex = 0;

		stack = new Stack();
		n = root;
		while (n != null) {
			stack.push(n);
			n = n.getLeft();
		}

		while (stack.size() > 0) {
			n = stack.pop();
			arr[arrIndex++] = n.getvalue();

			if (n.getRight() != null) {
				n = n.getRight();

				while (n != null) {
					stack.push(n);
					n = n.getLeft();
				}
			}
		}

		arr = sortArray(arr);

		return arr;
	}

	public static int[] sortArray(int[] arrs) {
		Arrays.sort(arrs);
		return arrs;
	}

	public static Node[] createNode(int[] arr) {
		Node[] nArr = new Node[arr.length];
		int index = 0;
		for (int i : arr) {
			nArr[index++] = new Node(i);
		}
		return nArr;
	}

	// 최소힙 - 부모의 값이 양 방향의 자식의 값 보다 작거나 같음 (부모.val <= 왼쪽 자식.val, 오른쪽 자식.val)
	public static Node createMinHeap(Node[] arrs) {
		for (int i = 0; i < arrs.length; i++) {
			int leftInd = (i * 2) + 1;
			int rightInd = leftInd + 1;
			if (leftInd > arrs.length && rightInd > arrs.length) 
				break;
			
			try {
				if (leftInd < arrs.length) {
					arrs[i].setLeft(arrs[leftInd]);
				}

				if (rightInd < arrs.length) {
					arrs[i].setRight(arrs[rightInd]);
				}

				if (i == 0)
					arrs[i].setParent(null);
				else
					arrs[i].setParent(arrs[rightInd / 2]);
				
			} catch (ArrayIndexOutOfBoundsException e) {
				e.printStackTrace();
			}
		}

		return arrs[0];
	}

	private static int traverse(Node node, int count, Node[] arr) {
		if (node == null) {
			return count;
		}

		if (arr != null) {
			arr[count] = node;
		}

		count++;
		count = traverse(node.getLeft(), count, arr);
		count = traverse(node.getRight(), count, arr);
		return count;
	}

	public static Node heapifyBinaryTree(Node root) {
		int size = traverse(root, 0, null);
		Node[] nodeArray = new Node[size];
		traverse(root, 0, nodeArray);

		Arrays.sort(nodeArray, new Comparator<Node>() {
			@Override
			public int compare(Node o1, Node o2) {
				int mv = o1.getvalue();
				int nv = o2.getvalue();
				int result = mv < nv ? -1 : (mv == nv ? 0 : 1);
				System.out.println("mv = " + mv+" , nv = " + nv + " , result = " + result);
				return result;
			}
		});

		for (int i = 0; i < size; i++) {
			int left = 2 * i + 1;
			int right = left + 1;

			nodeArray[i].setLeft(left >= size ? null : nodeArray[left]);
			nodeArray[i].setRight(right >= size ? null : nodeArray[right]);
		}

		return nodeArray[0];
	}

}
